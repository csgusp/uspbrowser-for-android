1. 概要
 滋賀県立大学のポータルサイトを便利に見ることができるブラウザーです。
 情報を端末に保存しておくことができるため、オフラインでも情報を表示することが可能です。

2. 注意
 このアプリは滋賀県立大学の公式アプリではありません。
 コンピュータ研究会により独自に開発されたものであり、大学の公認を得ているわけではありません。
 このアプリの利用により生じうるいかなる事象に対しても、コンピュータ研究会は責任を持ちません。
 また、重要な情報は通常のブラウザーでご確認ください。

3. 機能
 ポータルサイトにアクセスし、スケジュール・お知らせ・休講情報・教室変更の一覧を表示できます。
 スケジュールやお知らせの詳細を取得しオフラインでも表示できます。
 一度取得しておけば、お知らせが削除された後でも表示することができます。
 添付ファイルを見たり、ダウンロードしたりすることができます。
 情報はタイトルや教科名で検索を行うことができます。

4. 既知の問題点
 Android の地域設定 (GMT ~ GMT-12) により、お知らせの掲載日やスケジュールの時刻表記がずれることがあります。