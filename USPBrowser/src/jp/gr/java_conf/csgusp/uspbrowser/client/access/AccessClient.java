/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.access;

import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.ccil.cowan.tagsoup.Parser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * AccessClient クラスは、電車やバスの時刻表の取得といった、
 * 各種のアクセスを容易に行う機能を提供するクラスです。
 *
 * @author leak4mk0
 * @version 27
 * @since 27
 */
public class AccessClient {
    private static final String TAG = "AccessClient";
    private static final boolean DEBUG = false;

    public static final int SUCCESS = 1;
    public static final int FAILURE_UNKNOWN = 0;
    public static final int FAILURE_NETWORK = -1;
    public static final int FAILURE_CONTENT = -2;

    private static final String API_URL = "http://apis.csgusp.net/uspbrowser/v1/access/";
    private static final String API_TRAIN_URI = API_URL + "trains.php";
    private static final String API_BUS_URI = API_URL + "buses.php";

    private static final String API_DIRECTION_KEY = "direction";
    private static final String API_DIRECTION_INBOUND_VALUE = "inbound";
    private static final String API_DIRECTION_OUTBOUND_VALUE = "outbound";
    private static final String API_WEEKDAY_KEY = "weekday";
    private static final String API_WEEKDAY_WORKDAY_VALUE = "workday";
    private static final String API_WEEKDAY_HOLIDAY_VALUE = "holiday";
    private static final String API_TIME_KEY = "time";

    private ThreadSafeClientConnManager mClientConnManager;
    private HttpClient mHttpClient;
    private HttpRequestBase mHttpRequest;
    private AccessContentHandler mAccessContentHandler;

    public AccessClient() {
        HttpParams params;
        SchemeRegistry registry;

        params = new BasicHttpParams();
        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(params, "Shift-JIS");
        registry = new SchemeRegistry();
        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        registry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        mClientConnManager = new ThreadSafeClientConnManager(params, registry);
        mHttpClient = new DefaultHttpClient(mClientConnManager, params);
        mAccessContentHandler = null;
    }

    public void setAccessContentHandler(AccessContentHandler handler) {
        mAccessContentHandler = handler;
    }

    public void abort() {
        if (mHttpRequest != null) {
            mHttpRequest.abort();
        }
    }

    public void dispose() {
        if (mClientConnManager != null) {
            mClientConnManager.shutdown();
        }
    }

    public int fetchTrainTimetable() {
        HttpResponse response;
        HttpEntity entity;
        String json;
        JSONArray array;
        List<TrainContent> contents;
        int length;

        if (DEBUG) Log.v(TAG, "fetchTrainTimetable(): Beginning");

        mHttpRequest = new HttpGet(URI.create(API_TRAIN_URI));
        try {
            response = mHttpClient.execute(mHttpRequest);
        } catch (ClientProtocolException e) {
            return FAILURE_NETWORK;
        } catch (IOException e) {
            return FAILURE_NETWORK;
        } finally {
            mHttpRequest = null;
        }

        entity = response.getEntity();
        try {
            json = EntityUtils.toString(entity, "UTF-8");
            array = new JSONArray(json);
        } catch (IOException e) {
            return FAILURE_NETWORK;
        } catch (JSONException e) {
            return FAILURE_CONTENT;
        }

        contents = new LinkedList<TrainContent>();
        length = array.length();
        for (int i = 0; i < length; i++) {
            JSONObject object;
            String direction;
            String weekday;
            int time;
            TrainContent content;

            try {
                object = array.getJSONObject(i);
                direction = object.getString(API_DIRECTION_KEY);
                weekday = object.getString(API_WEEKDAY_KEY);
                time = object.getInt(API_TIME_KEY);
            } catch (JSONException e) {
                direction = null;
                weekday = null;
                time = -1;
            }

            content = new TrainContent();
            if (API_DIRECTION_INBOUND_VALUE.equals(direction)) {
                content.setDirection(BusContent.DIRECTION_INBOUND);
            } else if (API_DIRECTION_OUTBOUND_VALUE.equals(direction)) {
                content.setDirection(BusContent.DIRECTION_OUTBOUND);
            } else {
                content.setDirection(BusContent.DIRECTION_UNKNOWN);
            }
            if (API_WEEKDAY_WORKDAY_VALUE.equals(weekday)) {
                content.setWeekday(BusContent.WEEKDAY_WORKDAY);
            } else if (API_WEEKDAY_HOLIDAY_VALUE.equals(weekday)) {
                content.setWeekday(BusContent.WEEKDAY_HOLIDAY);
            } else {
                content.setWeekday(BusContent.WEEKDAY_UNKNOWN);
            }
            content.setTime(time);

            if (content.isSet()) {
                contents.add(content);
            }
        }
        if (contents.size() <= 0) {
            return FAILURE_CONTENT;
        }
        if (mAccessContentHandler != null) {
            mAccessContentHandler.onTrainContents(contents);
        }

        if (DEBUG) Log.v(TAG, "fetchTrainTimetable(): Ending");

        return SUCCESS;
    }

    public int fetchBusTimetable() {
        HttpResponse response;
        HttpEntity entity;
        String json;
        JSONArray array;
        List<BusContent> contents;
        int length;

        if (DEBUG) Log.v(TAG, "fetchBusTimetable(): Beginning");

        mHttpRequest = new HttpGet(URI.create(API_BUS_URI));
        try {
            response = mHttpClient.execute(mHttpRequest);
        } catch (ClientProtocolException e) {
            return FAILURE_NETWORK;
        } catch (IOException e) {
            return FAILURE_NETWORK;
        } finally {
            mHttpRequest = null;
        }

        entity = response.getEntity();
        try {
            json = EntityUtils.toString(entity, "UTF-8");
            array = new JSONArray(json);
        } catch (IOException e) {
            return FAILURE_NETWORK;
        } catch (JSONException e) {
            return FAILURE_CONTENT;
        }

        contents = new LinkedList<BusContent>();
        length = array.length();
        for (int i = 0; i < length; i++) {
            JSONObject object;
            String direction;
            String weekday;
            int time;
            BusContent content;

            try {
                object = array.getJSONObject(i);
                direction = object.getString(API_DIRECTION_KEY);
                weekday = object.getString(API_WEEKDAY_KEY);
                time = object.getInt(API_TIME_KEY);
            } catch (JSONException e) {
                direction = null;
                weekday = null;
                time = -1;
            }

            content = new BusContent();
            if (API_DIRECTION_INBOUND_VALUE.equals(direction)) {
                content.setDirection(BusContent.DIRECTION_INBOUND);
            } else if (API_DIRECTION_OUTBOUND_VALUE.equals(direction)) {
                content.setDirection(BusContent.DIRECTION_OUTBOUND);
            } else {
                content.setDirection(BusContent.DIRECTION_UNKNOWN);
            }
            if (API_WEEKDAY_WORKDAY_VALUE.equals(weekday)) {
                content.setWeekday(BusContent.WEEKDAY_WORKDAY);
            } else if (API_WEEKDAY_HOLIDAY_VALUE.equals(weekday)) {
                content.setWeekday(BusContent.WEEKDAY_HOLIDAY);
            } else {
                content.setWeekday(BusContent.WEEKDAY_UNKNOWN);
            }
            content.setTime(time);

            if (content.isSet()) {
                contents.add(content);
            }
        }
        if (contents.size() <= 0) {
            return FAILURE_CONTENT;
        }
        if (mAccessContentHandler != null) {
            mAccessContentHandler.onBusContents(contents);
        }

        if (DEBUG) Log.v(TAG, "fetchBusTimetable(): Ending");

        return SUCCESS;
    }
}
