/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.access;

/**
 * TrainContent クラスは電車の時刻表を格納するクラスです。
 *
 * @author leak4mk0
 * @version 27
 * @since 27
 */
public class TrainContent {
    /**
     * 路線が不明であることを示す方向の値です。
     */
    public static int DIRECTION_UNKNOWN = 0;
    /**
     * 路線が上りであることを示す方向の値です。
     */
    public static int DIRECTION_INBOUND = 1;
    /**
     * 路線が下りであることを示す方向の値です。
     */
    public static int DIRECTION_OUTBOUND = 2;

    /**
     * 曜日が不明であることを示す曜日の値です。
     */
    public static int WEEKDAY_UNKNOWN = 0;
    /**
     * 曜日が平日であることを示す曜日の値です。
     */
    public static int WEEKDAY_WORKDAY = 1;
    /**
     * 曜日が祝日であることを示す曜日の値です。
     */
    public static int WEEKDAY_HOLIDAY = 2;

    private int mDirection;
    private int mWeekday;
    private int mTime;

    public int getDirection() {
        return mDirection;
    }

    public void setDirection(int direction) {
        mDirection = direction;
    }

    public int getWeekday() {
        return mWeekday;
    }

    public void setWeekday(int weekday) {
        mWeekday = weekday;
    }

    public int getTime() {
        return mTime;
    }

    public void setTime(int time) {
        mTime = time;
    }

    public boolean isSet() {
        if (mDirection != DIRECTION_INBOUND && mDirection != DIRECTION_OUTBOUND) {
            return false;
        }
        if (mWeekday != WEEKDAY_WORKDAY && mWeekday != WEEKDAY_HOLIDAY) {
            return false;
        }
        if (mTime < 0) {
            return false;
        }

        return true;
    }
}
