/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

/**
 * CancelledLectureContent クラスは休講情報を格納するクラスです。
 *
 * @author leak4mk0
 * @version 35
 * @since 1
 */
public class CancelledLectureContent {
    /**
     * 講義がないことを示す日付または時限の値です。休講または補講となる講義がないことを意味します。
     */
    public static final int LECTURE_NOT_EXISTING = -1;

    /**
     * 時間が指定されていないことを示す日付の値です。無期限であることを意味します。
     */
    public static final int PERIOD_NOT_SPECIFIED = 0;

    private static final int LECTURE_ID_PRIME = 31;
    private String mSubject;
    private String mProfessor;
    private long mCancelledLectureDate;
    private int mCancelledLecturePeriod;
    private long mMakeupLectureDate;
    private int mMakeupLecturePeriod;
    private String mMakeupLectureClassroom;
    private String mNote1;
    private String mNote2;
    private int mTaken;

    public CancelledLectureContent() {
        clear();
    }

    public void clear() {
        mSubject = null;
        mProfessor = null;
        mCancelledLectureDate = -1;
        mCancelledLecturePeriod = -1;
        mMakeupLectureDate = -1;
        mMakeupLecturePeriod = -1;
        mMakeupLectureClassroom = null;
        mNote1 = null;
        mNote2 = null;
        mTaken = -1;
    }

    public boolean isSet() {
        if (mSubject == null || mProfessor == null || mTaken < 0 ||
                ((mCancelledLectureDate < 0 || mCancelledLecturePeriod < 0)
                        && (mMakeupLectureDate < 0 || mMakeupLecturePeriod < 0))) {
            return false;
        }

        return true;
    }

    public int getCancelledLectureId() {
        int id;

        if (mCancelledLectureDate < 0 || mCancelledLecturePeriod < 0
                || mSubject == null || mProfessor == null) {
            return -1;
        }
        id = 1;
        id = LECTURE_ID_PRIME * id + (int) (mCancelledLectureDate ^ (mCancelledLectureDate >>> 32));
        id = LECTURE_ID_PRIME * id + mCancelledLecturePeriod;
        id = LECTURE_ID_PRIME * id + mSubject.hashCode();
        id = LECTURE_ID_PRIME * id + mProfessor.hashCode();
        id &= Integer.MAX_VALUE;

        return id;
    }

    public int getMakeupLectureId() {
        int id;

        if (mMakeupLectureDate < 0 || mMakeupLecturePeriod < 0
                || mSubject == null || mProfessor == null) {
            return -1;
        }
        id = 1;
        id = LECTURE_ID_PRIME * id + (int) (mMakeupLectureDate ^ (mMakeupLectureDate >>> 32));
        id = LECTURE_ID_PRIME * id + mMakeupLecturePeriod;
        id = LECTURE_ID_PRIME * id + mSubject.hashCode();
        id = LECTURE_ID_PRIME * id + mProfessor.hashCode();
        id &= Integer.MAX_VALUE;

        return id;
    }

    public String getSubject() {
        return mSubject;
    }

    public void setSubject(String subject) {
        mSubject = subject;
    }

    public String getProfessor() {
        return mProfessor;
    }

    public void setProfessor(String professor) {
        mProfessor = professor;
    }

    public long getCancelledLectureDate() {
        return mCancelledLectureDate;
    }

    public void setCancelledLectureDate(long cancelledLectureDate) {
        mCancelledLectureDate = cancelledLectureDate;
    }

    public void setCancelledLectureDate(int year, int month, int day) {
        if (month <= 2) {
            year--;
            month += 12;
        }
        mCancelledLectureDate = (365 * year + year / 4 - year / 100 + year / 400
                + 306 * (month + 1) / 10 - 428 + day - 719163) * 86400000L;
    }

    public int getCancelledLecturePeriod() {
        return mCancelledLecturePeriod;
    }

    public void setCancelledLecturePeriod(int cancelledLecturePeriod) {
        mCancelledLecturePeriod = cancelledLecturePeriod;
    }

    public long getMakeupLectureDate() {
        return mMakeupLectureDate;
    }

    public void setMakeupLectureDate(long makeupLectureDate) {
        mMakeupLectureDate = makeupLectureDate;
    }

    public void setMakeupLectureDate(int year, int month, int day) {
        if (month <= 2) {
            year--;
            month += 12;
        }
        mMakeupLectureDate = (365 * year + year / 4 - year / 100 + year / 400
                + 306 * (month + 1) / 10 - 428 + day - 719163) * 86400000L;
    }

    public int getMakeupLecturePeriod() {
        return mMakeupLecturePeriod;
    }

    public void setMakeupLecturePeriod(int makeupLecturePeriod) {
        mMakeupLecturePeriod = makeupLecturePeriod;
    }

    public String getMakeupLectureClassroom() {
        return mMakeupLectureClassroom;
    }

    public void setMakeupLectureClassroom(String classroom) {
        mMakeupLectureClassroom = classroom;
    }

    public String getNote1() {
        return mNote1;
    }

    public void setNote1(String note1) {
        mNote1 = note1;
    }

    public String getNote2() {
        return mNote2;
    }

    public void setNote2(String note2) {
        mNote2 = note2;
    }

    public boolean isTaken() {
        return mTaken > 0;
    }

    public void setTaken(boolean taken) {
        mTaken = taken ? 1 : 0;
    }
}
