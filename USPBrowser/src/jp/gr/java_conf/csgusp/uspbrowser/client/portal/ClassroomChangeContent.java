/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

/**
 * ClassroomChangeContent クラスは教室変更を格納するクラスです。
 *
 * @author leak4mk0
 * @version 35
 * @since 1
 */
public class ClassroomChangeContent {
    /**
     * 時間が指定されていないことを示す日付の値です。無期限であることを意味します。
     */
    public static final int PERIOD_NOT_SPECIFIED = 0;

    private static final int LECTURE_ID_PRIME = 31;
    private String mSubject;
    private String mProfessor;
    private long mDate;
    private int mPeriod;
    private String mOldClassroom;
    private String mNewClassroom;
    private String mNote1;
    private String mNote2;
    private int mTaken;

    public ClassroomChangeContent() {
        clear();
    }

    public void clear() {
        mSubject = null;
        mProfessor = null;
        mDate = -1;
        mPeriod = -1;
        mOldClassroom = null;
        mNewClassroom = null;
        mNote1 = null;
        mNote2 = null;
        mTaken = -1;
    }

    public boolean isSet() {
        if (mSubject == null || mProfessor == null || mDate < 0 || mPeriod < 0
                || mOldClassroom == null || mNewClassroom == null || mTaken < 0) {
            return false;
        }

        return true;
    }

    public int getLectureId() {
        int code;

        code = 1;
        code = LECTURE_ID_PRIME * code + (int) (mDate ^ (mDate >>> 32));
        code = LECTURE_ID_PRIME * code + mPeriod;
        code = LECTURE_ID_PRIME * code + ((mSubject == null) ? 0 : mSubject.hashCode());
        code = LECTURE_ID_PRIME * code + ((mProfessor == null) ? 0 : mProfessor.hashCode());
        code &= Integer.MAX_VALUE;

        return code;
    }

    public String getSubject() {
        return mSubject;
    }

    public void setSubject(String subject) {
        mSubject = subject;
    }

    public String getProfessor() {
        return mProfessor;
    }

    public void setProfessor(String professor) {
        mProfessor = professor;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        mDate = date;
    }

    public void setDate(int year, int month, int day) {
        if (month <= 2) {
            year--;
            month += 12;
        }
        mDate = (365 * year + year / 4 - year / 100 + year / 400
                + 306 * (month + 1) / 10 - 428 + day - 719163) * 86400000L;
    }

    public int getPeriod() {
        return mPeriod;
    }

    public void setPeriod(int period) {
        mPeriod = period;
    }

    public String getOldClassroom() {
        return mOldClassroom;
    }

    public void setOldClassroom(String oldClassroom) {
        mOldClassroom = oldClassroom;
    }

    public String getNewClassroom() {
        return mNewClassroom;
    }

    public void setNewClassroom(String newClassroom) {
        mNewClassroom = newClassroom;
    }

    public String getNote1() {
        return mNote1;
    }

    public void setNote1(String note1) {
        mNote1 = note1;
    }

    public String getNote2() {
        return mNote2;
    }

    public void setNote2(String note2) {
        mNote2 = note2;
    }

    public boolean isTaken() {
        return mTaken > 0;
    }

    public void setTaken(boolean taken) {
        mTaken = taken ? 1 : 0;
    }
}
