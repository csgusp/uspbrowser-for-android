/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

import jp.gr.java_conf.csgusp.uspbrowser.util.HtmlAttribute;
import jp.gr.java_conf.csgusp.uspbrowser.util.HtmlElement;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * FetchInformationHandler クラスは PortalClient クラスでお知らせの詳細を取得する際に、
 * HTML を解析して、一覧を読み出すクラスです。
 *
 * @author leak4mk0
 * @version 18
 * @since 1
 */
public class FetchInformationHandler extends DefaultHandler {
    private static final int SPAN_UNKNOWN = 0;
    private static final int SPAN_TITLE = 1;
    private static final int TABLE_UNKNOWN = 0;
    private static final int TABLE_INFORMATION = 1;
    private static final int TABLE_ATTACHMENT = 2;
    private static final int ROW_UNKNOWN = 0;
    private static final int ROW_PUBLIC_DATE = 1;
    private static final int ROW_PUBLIC_PERSON = 2;
    private static final int ROW_CONTENT = 3;
    private static final int COLUMN_UNKNOWN = 0;
    private static final int COLUMN_NAME = 1;
    private static final int COLUMN_VALUE = 2;
    private static final int COLUMN_FILENAME = 1;
    private static final int COLUMN_DESCRIPTION = 2;
    private Stack<StringBuilder> mBuilderStack;
    private Pattern mPublicDatePattern;
    private int mCurrentSpan;
    private int mCurrentTable;
    private int mCurrentRow;
    private int mCurrentColumn;
    private InformationContent mInformationContent;
    private InformationAttachment mInformationAttachment;
    private PortalContentHandler mPortalContentHandler;
    private URI mURI;
    private boolean mSuccess;

    public FetchInformationHandler() {
        mBuilderStack = new Stack<StringBuilder>();
        mPublicDatePattern = Pattern.compile("(\\d+)/(\\d+)/(\\d+)(?:[^\\d]+(\\d+)/(\\d+)/(\\d+))?");
        mPortalContentHandler = null;
    }

    public void startDocument() {
        mCurrentTable = TABLE_UNKNOWN;
        mInformationContent = new InformationContent();
        mInformationAttachment = new InformationAttachment();
        mSuccess = false;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        int localCode;

        localCode = localName.hashCode();
        mBuilderStack.push(new StringBuilder());

        if (HtmlElement.SPAN_CODE == localCode && HtmlElement.SPAN_NAME.equals(localName)) {
            String clazz;

            // DIV タグを検出した場合、タグの種類を保存する。
            clazz = attributes.getValue(HtmlAttribute.CLASS_NAME);
            if ("diary".equals(clazz)) {
                mCurrentSpan = SPAN_TITLE;
            } else {
                mCurrentSpan = SPAN_UNKNOWN;
            }
        } else if (HtmlElement.TABLE_CODE == localCode && HtmlElement.TABLE_NAME.equals(localName)) {
            // TABLE タグを検出した場合、テーブルの種類を保存する。
            mCurrentTable++;
        } else if (HtmlElement.TR_CODE == localCode && HtmlElement.TR_NAME.equals(localName)) {
            // TR タグを検出した場合、行の解析の用意をする。
            mCurrentRow = ROW_UNKNOWN;
            mCurrentColumn = COLUMN_UNKNOWN;
        } else if (HtmlElement.TD_CODE == localCode && HtmlElement.TD_NAME.equals(localName)) {
            // TD タグを検出した場合、列の種類を保存する。
            mCurrentColumn++;

            // HTML の構文エラーに対処する。BR タグは 5 行の位置に記述してはいけない。
            // TagSoup は TABLE タグを閉じてソースを整形するため、テーブルの解析に影響を及ぼす。
            //
            // 0001: <table width='100%' class='table'>
            // 0001: <tr><td class='th' align='center' width='90'>掲載期間</td>
            // 0002: <td class='td'>…</td></tr>
            // 0003: <tr><td class='th' align='center'>掲載者</td>
            // 0004: <td class='td'>…</td></tr>
            // 0005: <br>
            // 0006: <tr><td class='th' align='center'>本文</td>
            // 0007: <td class='td'>…</td></tr>
            // 0008: </table>
            if (TABLE_ATTACHMENT == mCurrentTable) {
                String clazz;

                clazz = attributes.getValue(HtmlAttribute.CLASS_NAME);
                if ("th".equals(clazz)) {
                    mCurrentTable = TABLE_INFORMATION;
                }
            }
        } else if (HtmlElement.A_CODE == localCode && HtmlElement.A_NAME.equals(localName)) {
            String href;

            // A タグを検出した場合、添付ファイルのリンク先を格納する。
            href = attributes.getValue(HtmlAttribute.HREF_NAME);
            if (href == null) {
                return;
            }

            switch (mCurrentTable) {
                case TABLE_ATTACHMENT:
                    // 添付ファイルを取り扱う。
                    switch (mCurrentColumn) {
                        case COLUMN_FILENAME:
                            // リンク先を絶対パスにして格納する。
                            try {
                                URL attachmentURL;
                                URI attachmentURI;

                                attachmentURL = new URL(mURI.toURL(), href);
                                attachmentURI = new URI(attachmentURL.getProtocol(),
                                        attachmentURL.getUserInfo(),
                                        attachmentURL.getHost(),
                                        attachmentURL.getPort(),
                                        attachmentURL.getPath(),
                                        attachmentURL.getQuery(),
                                        attachmentURL.getRef());
                                mInformationAttachment.setUrl(attachmentURI.toString());
                            } catch (MalformedURLException e) {
                                e.printStackTrace();
                                break;
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                                break;
                            }

                            break;
                    }

                    break;
            }
        } else if (HtmlElement.INPUT_CODE == localCode && HtmlElement.INPUT_NAME.equals(localName)) {
            String name;
            String value;

            // INPUT タグを検出した場合、ID を格納する。
            name = attributes.getValue(HtmlAttribute.NAME_NAME);
            value = attributes.getValue(HtmlAttribute.VALUE_NAME);
            if ("inpCDOYA".equals(name) && value != null) {
                mInformationContent.setInformationId(Integer.parseInt(value));
            }
        }
    }

    public void characters(char[] ch, int start, int length) {
        mBuilderStack.peek().append(ch, start, length);
    }

    public void endElement(String uri, String localName, String qName) {
        int localCode;
        String text;

        localCode = localName.hashCode();
        text = mBuilderStack.pop().toString().replace('\u00A0', '\u0020');

        if (HtmlElement.TITLE_CODE == localCode && HtmlElement.TITLE_NAME.equals(localName)) {
            // TITLE タグを検出した場合、解析が成功したことにする。
            mSuccess = true;
        } else if (HtmlElement.SPAN_CODE == localCode && HtmlElement.SPAN_NAME.equals(localName)) {
            // SPAN タグを検出した場合、タグの位置によって適切な位置にデータを格納する。
            switch (mCurrentSpan) {
                case SPAN_TITLE:
                    mInformationContent.setTitle(text);

                    break;
            }
        } else if (HtmlElement.TR_CODE == localCode && HtmlElement.TR_NAME.equals(localName)) {
            // TABLE タグを検出した場合、添付ファイルのテーブルなら添付ファイルを登録する。
            switch (mCurrentTable) {
                case TABLE_ATTACHMENT:
                    // データが正常かチェックする。
                    if (!mInformationAttachment.isSet()) {
                        mInformationAttachment.clear();

                        break;
                    }
                    mInformationContent.addInformationAttachment(mInformationAttachment);
                    mInformationAttachment = new InformationAttachment();

                    break;
            }
        } else if (HtmlElement.TD_CODE == localCode && HtmlElement.TD_NAME.equals(localName)) {
            // TD タグを検出した場合、タグの位置によって適切な位置にデータを格納する。
            switch (mCurrentTable) {
                case TABLE_INFORMATION:
                    // お知らせを取り扱う。
                    switch (mCurrentColumn) {
                        case COLUMN_NAME:
                            // 文字列より行の種類を判断して次の列に備える。
                            if (text.contains("掲載期間")) {
                                mCurrentRow = ROW_PUBLIC_DATE;
                            } else if (text.contains("掲載者")) {
                                mCurrentRow = ROW_PUBLIC_PERSON;
                            } else if (text.contains("本文")) {
                                mCurrentRow = ROW_CONTENT;
                            } else {
                                mCurrentRow = ROW_UNKNOWN;
                            }

                            break;

                        case COLUMN_VALUE:
                            switch (mCurrentRow) {
                                case ROW_PUBLIC_DATE:
                                    if (!text.contains("無期限")) {
                                        Matcher matcher;

                                        matcher = mPublicDatePattern.matcher(text);
                                        if (matcher.find()) {
                                            int year;
                                            int month;
                                            int day;

                                            year = Integer.parseInt(matcher.group(1));
                                            month = Integer.parseInt(matcher.group(2));
                                            day = Integer.parseInt(matcher.group(3));
                                            mInformationContent.setPublicStartDate(year, month, day);
                                            if (matcher.group(6) != null) {
                                                year = Integer.parseInt(matcher.group(4));
                                                month = Integer.parseInt(matcher.group(5));
                                                day = Integer.parseInt(matcher.group(6));
                                            }
                                            mInformationContent.setPublicEndDate(year, month, day);
                                        }
                                    } else {
                                        mInformationContent.setPublicStartDate(
                                                InformationContent.DATE_NOT_SPECIFIED);
                                        mInformationContent.setPublicEndDate(
                                                InformationContent.DATE_NOT_SPECIFIED);
                                    }

                                    break;

                                case ROW_PUBLIC_PERSON:
                                    // 掲載者を格納する。
                                    mInformationContent.setPublicPerson(text);

                                    break;

                                case ROW_CONTENT:
                                    // 内容を格納する。
                                    while (text.startsWith("\n")) {
                                        text = text.substring("\n".length());
                                    }
                                    while (text.endsWith("\n")) {
                                        text = text.substring(0, text.length() - "\n".length());
                                    }
                                    mInformationContent.setContent(text);

                                    break;
                            }

                            break;
                    }

                    break;

                case TABLE_ATTACHMENT:
                    // 添付ファイルを取り扱う。
                    switch (mCurrentColumn) {
                        case COLUMN_DESCRIPTION:
                            // 説明を格納する。
                            mInformationAttachment.setDescription(text.trim());

                            break;
                    }
                    break;
            }
        } else if (HtmlElement.A_CODE == localCode && HtmlElement.A_NAME.equals(localName)) {
            // A タグを検出した場合、ファイル名を格納する。
            switch (mCurrentTable) {
                case TABLE_ATTACHMENT:
                    // 添付ファイルを取り扱う。
                    switch (mCurrentColumn) {
                        case COLUMN_FILENAME:
                            // ファイル名を格納する。
                            mInformationAttachment.setFilename(text.trim());

                            break;
                    }
            }
        }
    }

    public void endDocument() {
        if (!mInformationContent.isSet(true)) {
            mInformationContent = null;

            return;
        }
        mSuccess = true;
        if (mPortalContentHandler != null) {
            mPortalContentHandler.onInformation(mInformationContent, true);
        }
        mInformationContent = null;
    }

    public void setPortalContentHandler(PortalContentHandler handler) {
        mPortalContentHandler = handler;
    }

    public void setURI(URI uri) {
        mURI = uri;
    }

    public boolean isSuccess() {
        return mSuccess;
    }
}
