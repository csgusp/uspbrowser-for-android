/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

import jp.gr.java_conf.csgusp.uspbrowser.util.HtmlAttribute;
import jp.gr.java_conf.csgusp.uspbrowser.util.HtmlElement;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * FetchScheduleHandler クラスは PortalClient クラスでスケジュールの詳細を取得する際に、
 * HTML を解析して、一覧を読み出すクラスです。
 *
 * @author leak4mk0
 * @version 30
 * @since 1
 */
public class FetchScheduleHandler extends DefaultHandler {
    private static final int DIV_UNKNOWN = 0;
    private static final int DIV_AREA = 1;
    private static final int DIV_LEFT = 2;
    private static final int DIV_RIGHT = 3;
    private static final int SPAN_UNKNOWN = 0;
    private static final int SPAN_DIARY = 1;
    private static final int SPAN_DIARY2 = 2;
    private Stack<StringBuilder> mBuilderStack;
    private Pattern mPlacePattern;
    private Pattern mPriorityPattern;
    private Pattern mDatePattern;
    private Pattern mTimePattern;
    private int mCurrentDiv;
    private int mParentDiv;
    private int mCurrentSpan;
    private StringBuilder mContentBuilder;
    private ScheduleContent mScheduleContent;
    private PortalContentHandler mPortalContentHandler;
    private boolean mSuccess;

    public FetchScheduleHandler() {
        mBuilderStack = new Stack<StringBuilder>();
        mPlacePattern = Pattern.compile("^場所：(.*)$");
        mPriorityPattern = Pattern.compile("^重要度：(.*)$");
        mDatePattern = Pattern.compile("(\\d+)年(\\d+)月(\\d+)日");
        mTimePattern = Pattern.compile("(\\d+):(\\d+)～(\\d+):(\\d+)");
        mContentBuilder = new StringBuilder();
        mPortalContentHandler = null;
    }

    public void startDocument() {
        mCurrentDiv = DIV_UNKNOWN;
        mParentDiv = DIV_UNKNOWN;
        mCurrentSpan = SPAN_UNKNOWN;
        mScheduleContent = new ScheduleContent();
        mSuccess = false;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        int localCode;

        localCode = localName.hashCode();
        mBuilderStack.push(new StringBuilder());

        if (HtmlElement.DIV_CODE == localCode && HtmlElement.DIV_NAME.equals(localName)) {
            String clazz;

            // DIV タグを検出した場合、タグの種類を保存する。
            mParentDiv = mCurrentDiv;

            clazz = attributes.getValue(HtmlAttribute.CLASS_NAME);
            if ("area".equals(clazz)) {
                mCurrentDiv = DIV_AREA;
            } else if ("left".equals(clazz)) {
                mCurrentDiv = DIV_LEFT;
            } else if ("right".equals(clazz)) {
                mCurrentDiv = DIV_RIGHT;
            } else {
                mCurrentDiv = DIV_UNKNOWN;
            }
        } else if (HtmlElement.SPAN_CODE == localCode && HtmlElement.SPAN_NAME.equals(localName)) {
            String clazz;

            // SPAN タグを検出した場合、タグの種類を保存する。
            clazz = attributes.getValue(HtmlAttribute.CLASS_NAME);
            if ("diary".equals(clazz)) {
                mCurrentSpan = SPAN_DIARY;
            } else if ("diary2".equals(clazz)) {
                mCurrentSpan = SPAN_DIARY2;
            } else {
                mCurrentSpan = SPAN_UNKNOWN;
            }
        } else if (HtmlElement.INPUT_CODE == localCode && HtmlElement.INPUT_NAME.equals(localName)) {
            String name;
            String value;

            // INPUT タグを検出した場合、ID を格納する。
            name = attributes.getValue(HtmlAttribute.NAME_NAME);
            value = attributes.getValue(HtmlAttribute.VALUE_NAME);
            if ("inpCDJH".equals(name) && value != null) {
                int scheduleId;

                scheduleId = Integer.parseInt(value);
                mScheduleContent.setScheduleId(scheduleId);
            }
        }
    }

    public void characters(char[] ch, int start, int length) {
        mBuilderStack.peek().append(ch, start, length);
    }

    public void endElement(String uri, String localName, String qName) {
        int localCode;
        String text;

        localCode = localName.hashCode();
        text = mBuilderStack.pop().toString().replace('\u00A0', '\u0020');

        if (HtmlElement.DIV_CODE == localCode && HtmlElement.DIV_NAME.equals(localName)) {
            // DIV タグを検出した場合、保存されたテキストを格納する。
            switch (mCurrentDiv) {
                case DIV_AREA:
                    // 文末の無駄な改行を除去する。
                    while (mContentBuilder.length() > 0
                            && '\n' == mContentBuilder.charAt(mContentBuilder.length() - 1)) {
                        mContentBuilder.setLength(mContentBuilder.length() - 1);
                    }
                    mScheduleContent.setContent(mContentBuilder.toString());
                    mContentBuilder.setLength(0);

                    break;
            }
            mCurrentDiv = mParentDiv;
            mParentDiv = DIV_UNKNOWN;
        } else if (HtmlElement.SPAN_CODE == localCode && HtmlElement.SPAN_NAME.equals(localName)) {
            // SPAN タグを検出した場合、タグの位置によって適切な位置にデータを格納する。
            switch (mCurrentSpan) {
                case SPAN_DIARY:
                    // タイトルを格納する。
                    mScheduleContent.setTitle(text.trim());
                    break;

                case SPAN_DIARY2:
                    switch (mCurrentDiv) {
                        case DIV_LEFT: {
                            Matcher matcher;

                            // 場所または重要度を格納する。
                            matcher = mPlacePattern.matcher(text);
                            if (matcher.find()) {
                                mScheduleContent.setPlace(matcher.group(1));
                            } else {
                                matcher = mPriorityPattern.matcher(text);
                                if (matcher.find()) {
                                    mScheduleContent.setPriority(detectPriority(matcher.group(1)));
                                }
                            }

                            break;
                        }

                        case DIV_RIGHT: {
                            Matcher matcher;

                            // 日付または時間を格納する。
                            matcher = mDatePattern.matcher(text);
                            if (matcher.find()) {
                                int year;
                                int month;
                                int day;

                                year = Integer.parseInt(matcher.group(1));
                                month = Integer.parseInt(matcher.group(2));
                                day = Integer.parseInt(matcher.group(3));
                                mScheduleContent.setStartDate(year, month, day);
                                mScheduleContent.setEndDate(year, month, day);
                                mScheduleContent.setStartTime(ScheduleContent.TIME_NOT_SPECIFIED);
                                mScheduleContent.setEndTime(ScheduleContent.TIME_NOT_SPECIFIED);
                            } else {
                                matcher = mTimePattern.matcher(text);
                                if (matcher.find()) {
                                    int startHour;
                                    int startMinute;
                                    int endHour;
                                    int endMinute;

                                    startHour = Integer.parseInt(matcher.group(1));
                                    startMinute = Integer.parseInt(matcher.group(2));
                                    endHour = Integer.parseInt(matcher.group(3));
                                    endMinute = Integer.parseInt(matcher.group(4));
                                    mScheduleContent.setStartTime(startHour, startMinute, 0);
                                    mScheduleContent.setEndTime(endHour, endMinute, 0);
                                }
                            }

                            break;
                        }
                    }

                    break;
            }
        } else if (HtmlElement.P_CODE == localCode && HtmlElement.P_NAME.equals(localName)) {
            // P タグを検出した場合、タグ内部のテキストを保存する。
            switch (mCurrentDiv) {
                case DIV_AREA: {
                    // 文中の無駄な空白を除去する。
                    if (" ".equals(text)) {
                        text = "";
                    }
                    mContentBuilder.append(text);
                    mContentBuilder.append("\n");

                    break;
                }
            }
        }
    }

    public void endDocument() {
        if (!mScheduleContent.isSet(true)) {
            mScheduleContent = null;

            return;
        }
        mSuccess = true;
        if (mPortalContentHandler != null) {
            mPortalContentHandler.onSchedule(mScheduleContent, true);
        }
        mScheduleContent = null;
    }

    public void setPortalContentHandler(PortalContentHandler handler) {
        mPortalContentHandler = handler;
    }

    public boolean isSuccess() {
        return mSuccess;
    }

    private int detectPriority(String text) {
        int priority;

        if (text.contains("最重要")) {
            priority = ScheduleContent.PRIORITY_MOST_IMPORTANT;
        } else if (text.contains("重要")) {
            priority = ScheduleContent.PRIORITY_IMPORTANT;
        } else if (text.contains("普通")) {
            priority = ScheduleContent.PRIORITY_NORMAL;
        } else if (text.contains("緊急")) {
            priority = ScheduleContent.PRIORITY_URGENT;
        } else {
            priority = ScheduleContent.PRIORITY_UNKNOWN;
        }

        return priority;
    }
}
