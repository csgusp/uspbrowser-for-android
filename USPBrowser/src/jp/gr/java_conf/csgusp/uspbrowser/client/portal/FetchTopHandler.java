/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

import jp.gr.java_conf.csgusp.uspbrowser.util.HtmlAttribute;
import jp.gr.java_conf.csgusp.uspbrowser.util.HtmlElement;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * FetchTopHandler クラスは PortalClient クラスでお知らせ等の一覧を取得する際に、
 * HTML を解析して、一覧を読み出すクラスです。
 *
 * @author leak4mk0
 * @version 35
 * @since 1
 */
public class FetchTopHandler extends DefaultHandler {
    private static final int DOCUMENT_UNKNOWN = 0;
    private static final int DOCUMENT_TAKEN = 1;
    private static final int DOCUMENT_UNTAKEN = 2;
    private static final int TABLE_UNKNOWN = 0;
    private static final int TABLE_SCHEDULES = 1;
    private static final int TABLE_INFORMATION = 2;
    private static final int TABLE_CANCELLED_LECTURES = 3;
    private static final int TABLE_CLASSROOM_CHANGES = 4;
    private static final int ROW_UNKNOWN = 0;
    private static final int ROW_CANCELLED_LECTURE_SUBJECT = 1;
    private static final int ROW_CANCELLED_LECTURE_CANCELLED_LECTURE = 2;
    private static final int ROW_CANCELLED_LECTURE_MAKEUP_LECTURE = 3;
    private static final int ROW_CANCELLED_LECTURE_MAKEUP_LECTURE_CLASSROOM = 4;
    private static final int ROW_CANCELLED_LECTURE_NOTE1 = 5;
    private static final int ROW_CANCELLED_LECTURE_NOTE2 = 6;
    private static final int ROW_CLASSROOM_CHANGE_SUBJECT = 1;
    private static final int ROW_CLASSROOM_CHANGE_DATE = 2;
    private static final int ROW_CLASSROOM_CHANGE_CLASSROOM = 3;
    private static final int ROW_CLASSROOM_CHANGE_NOTE1 = 4;
    private static final int ROW_CLASSROOM_CHANGE_NOTE2 = 5;
    private static final int COLUMN_UNKNOWN = 0;
    private static final int COLUMN_SCHEDULE_DATE = 1;
    private static final int COLUMN_SCHEDULE_TITLE = 2;
    private static final int COLUMN_INFORMATION_IS_READ = 1;
    private static final int COLUMN_INFORMATION_IS_ATTACHED = 2;
    private static final int COLUMN_INFORMATION_PUBLIC_DATE = 3;
    private static final int COLUMN_INFORMATION_PUBLIC_PERSON = 4;
    private static final int COLUMN_INFORMATION_PRIORITY = 5;
    private static final int COLUMN_INFORMATION_CATEGORY = 6;
    private static final int COLUMN_INFORMATION_TITLE = 7;
    private static final int COLUMN_NAME = 1;
    private static final int COLUMN_VALUE = 2;
    private Stack<StringBuilder> mBuilderStack;
    private Pattern mScheduleDatePattern;
    private Pattern mScheduleTitlePattern;
    private Pattern mInformationDatePattern;
    private Pattern mInformationTitlePattern;
    private Pattern mCancelledLectureSubjectPattern;
    private Pattern mCancelledLectureDatePattern;
    private Pattern mClassroomChangeSubjectPattern;
    private Pattern mClassroomChangeDatePattern;
    private Pattern mClassroomChangeClassroomPattern;
    private List<Integer> mCancelledLectureTakenCancelledLectureIds;
    private List<Integer> mCancelledLectureTakenMakeupLectureIds;
    private List<Integer> mClassroomChangeTakenLectureIds;
    private int mCurrentDocument;
    private int mCurrentTable;
    private int mCurrentRow;
    private int mCurrentColumn;
    private int mThisYear;
    private int mThisMonth;
    private ScheduleContent mScheduleContent;
    private InformationContent mInformationContent;
    private CancelledLectureContent mCancelledLectureContent;
    private ClassroomChangeContent mClassroomChangeContent;
    private PortalContentHandler mPortalContentHandler;
    private boolean mSuccess;

    public FetchTopHandler() {
        Calendar calendar;

        mBuilderStack = new Stack<StringBuilder>();
        mScheduleDatePattern = Pattern.compile("(\\d+)/(\\d+)/(\\d+)");
        mScheduleTitlePattern = Pattern.compile("cdjh=(\\d+).+cdren=(\\d+)");
        mInformationDatePattern = Pattern.compile("(\\d+)/(\\d+)/(\\d+)");
        mInformationTitlePattern = Pattern.compile("cdmsg=(\\d+)");
        mCancelledLectureSubjectPattern = Pattern.compile("([^\\s].*)【担当：(.+)】");
        mCancelledLectureDatePattern = Pattern.compile("(\\d+)/(\\d+)[^\\d]+(\\d+)?");
        mClassroomChangeSubjectPattern = Pattern.compile("([^\\s].*)【担当：(.+)】");
        mClassroomChangeDatePattern = Pattern.compile("(\\d+)/(\\d+)[^\\d]+(\\d+)?");
        mClassroomChangeClassroomPattern = Pattern.compile("([^\\s]+)\\s*→\\s*([^\\s]+)");
        mCancelledLectureTakenCancelledLectureIds = new ArrayList<Integer>();
        mCancelledLectureTakenMakeupLectureIds = new ArrayList<Integer>();
        mClassroomChangeTakenLectureIds = new ArrayList<Integer>();
        mCurrentDocument = DOCUMENT_UNKNOWN;
        calendar = Calendar.getInstance();
        mThisYear = calendar.get(Calendar.YEAR);
        mThisMonth = calendar.get(Calendar.MONTH) + 1;
        mScheduleContent = new ScheduleContent();
        mInformationContent = new InformationContent();
        mCancelledLectureContent = new CancelledLectureContent();
        mClassroomChangeContent = new ClassroomChangeContent();
        mPortalContentHandler = null;
        mSuccess = false;
    }

    public void startDocument() {
        mCurrentDocument++;
        mSuccess = false;
    }

    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        int localCode;

        localCode = localName.hashCode();
        mBuilderStack.push(new StringBuilder());

        if (HtmlElement.TR_CODE == localCode && HtmlElement.TR_NAME.equals(localName)) {
            mCurrentColumn = COLUMN_UNKNOWN;
            mCurrentRow = ROW_UNKNOWN;
        } else if (HtmlElement.TD_CODE == localCode && HtmlElement.TD_NAME.equals(localName)) {
            mCurrentColumn++;
        } else if (HtmlElement.A_CODE == localCode && HtmlElement.A_NAME.equals(localName)) {
            String href;
            Matcher matcher;

            href = attributes.getValue(HtmlAttribute.HREF_NAME);
            switch (mCurrentTable) {
                case TABLE_SCHEDULES:
                    if (mCurrentColumn == COLUMN_SCHEDULE_TITLE) {
                        matcher = mScheduleTitlePattern.matcher(href);
                        if (matcher.find()) {
                            mScheduleContent.setScheduleId(Integer.parseInt(matcher.group(1)));
                            mScheduleContent.setStudentId(Integer.parseInt(matcher.group(2)));
                        }
                    }

                    break;

                case TABLE_INFORMATION:
                    if (mCurrentColumn == COLUMN_INFORMATION_TITLE) {
                        matcher = mInformationTitlePattern.matcher(href);
                        if (matcher.find()) {
                            mInformationContent.setInformationId(Integer.parseInt(matcher.group(1)));
                        }
                    }

                    break;
            }
        } else if ((HtmlElement.IMG_CODE == localCode && HtmlElement.IMG_NAME.equals(localName))
                || (HtmlElement.IMAGE_CODE == localCode && HtmlElement.IMAGE_NAME.equals(localName))) {
            String src;

            src = attributes.getValue(HtmlAttribute.SRC_NAME);
            if (src == null) {
                return;
            }
            switch (mCurrentTable) {
                case TABLE_INFORMATION:
                    switch (mCurrentColumn) {
                        case COLUMN_INFORMATION_IS_READ:
                            mInformationContent.setRead(src.contains("email_open.gif"));

                            break;

                        case COLUMN_INFORMATION_IS_ATTACHED:
                            mInformationContent.setAttached(src.contains("attach.gif"));

                            break;
                    }

                    break;
            }
        }
    }

    public void characters(char[] ch, int start, int length) {
        mBuilderStack.peek().append(ch, start, length);
    }

    public void endElement(String uri, String localName, String qName) {
        int localCode;
        String text;

        localCode = localName.hashCode();
        text = mBuilderStack.pop().toString();

        if (HtmlElement.TITLE_CODE == localCode && HtmlElement.TITLE_NAME.equals(localName)) {
            // TITLE タグを検出した場合、解析が成功したことにする。
            mSuccess = true;
        } else if ((HtmlElement.H1_CODE == localCode && HtmlElement.H1_NAME.equals(localName))
                || (HtmlElement.H2_CODE == localCode && HtmlElement.H2_NAME.equals(localName))
                || (HtmlElement.H3_CODE == localCode && HtmlElement.H3_NAME.equals(localName))
                || (HtmlElement.H4_CODE == localCode && HtmlElement.H4_NAME.equals(localName))
                || (HtmlElement.H5_CODE == localCode && HtmlElement.H5_NAME.equals(localName))
                || (HtmlElement.H6_CODE == localCode && HtmlElement.H6_NAME.equals(localName))) {
            // H タグを検出した場合、次のテーブルの中身を予想する。
            mCurrentTable = detectTable(text, mCurrentDocument == DOCUMENT_TAKEN);
        } else if (HtmlElement.TABLE_CODE == localCode && HtmlElement.TABLE_NAME.equals(localName)) {
            // TABLE タグを検出した場合、休講情報または教室変更のテーブルならデータを送信する。
            switch (mCurrentTable) {
                case TABLE_CANCELLED_LECTURES: {
                    int cancelledLectureId;
                    int makeupLectureId;

                    // 履修している講義かチェックする。
                    cancelledLectureId = mCancelledLectureContent.getCancelledLectureId();
                    makeupLectureId = mCancelledLectureContent.getMakeupLectureId();
                    switch (mCurrentDocument) {
                        case DOCUMENT_TAKEN:
                            if (cancelledLectureId >= 0) {
                                mCancelledLectureTakenCancelledLectureIds.add(cancelledLectureId);
                            }
                            if (makeupLectureId >= 0) {
                                mCancelledLectureTakenMakeupLectureIds.add(makeupLectureId);
                            }
                            mCancelledLectureContent.setTaken(true);

                            break;

                        case DOCUMENT_UNTAKEN:
                            if (cancelledLectureId >= 0
                                    && mCancelledLectureTakenCancelledLectureIds
                                    .contains(cancelledLectureId)) {
                                break;
                            }
                            if (makeupLectureId >= 0
                                    && mCancelledLectureTakenMakeupLectureIds
                                    .contains(makeupLectureId)) {
                                break;
                            }
                            mCancelledLectureContent.setTaken(false);

                            break;
                    }
                    // データが正常かチェックする。
                    if (!mCancelledLectureContent.isSet()) {
                        mCancelledLectureContent.clear();

                        break;
                    }
                    // ハンドラーが設定されていれば、データを送信する。
                    if (mPortalContentHandler != null) {
                        mPortalContentHandler.onCancelledLecture(mCancelledLectureContent);
                    }
                    mCancelledLectureContent = new CancelledLectureContent();

                    break;
                }

                case TABLE_CLASSROOM_CHANGES: {
                    int lectureId;

                    // 履修している講義かチェックする。
                    lectureId = mClassroomChangeContent.getLectureId();
                    switch (mCurrentDocument) {
                        case DOCUMENT_TAKEN:
                            if (lectureId >= 0) {
                                mClassroomChangeTakenLectureIds.add(lectureId);
                            }
                            mClassroomChangeContent.setTaken(true);

                            break;

                        case DOCUMENT_UNTAKEN:
                            if (lectureId >= 0
                                    && mClassroomChangeTakenLectureIds.contains(lectureId)) {
                                break;
                            }
                            mClassroomChangeContent.setTaken(false);

                            break;
                    }
                    // データが正常かチェックする。
                    if (!mClassroomChangeContent.isSet()) {
                        mClassroomChangeContent.clear();

                        break;
                    }
                    // ハンドラーが設定されていれば、データを送信する。
                    if (mPortalContentHandler != null) {
                        mPortalContentHandler.onClassroomChange(mClassroomChangeContent);
                    }
                    mClassroomChangeContent = new ClassroomChangeContent();

                    break;
                }
            }
        } else if (HtmlElement.TR_CODE == localCode && HtmlElement.TR_NAME.equals(localName)) {
            // TR タグを検出した場合、スケジュールまたはお知らせのテーブルならデータを送信する。
            switch (mCurrentTable) {
                case TABLE_SCHEDULES:
                    // データが正常かチェックする。
                    if (!mScheduleContent.isSet(false)) {
                        mScheduleContent.clear();

                        break;
                    }
                    // ハンドラーが設定されていれば、データを送信する。
                    if (mPortalContentHandler != null) {
                        mPortalContentHandler.onSchedule(mScheduleContent, false);
                    }
                    mScheduleContent = new ScheduleContent();

                    break;

                case TABLE_INFORMATION:
                    // データが正常かチェックする。
                    if (!mInformationContent.isSet(false)) {
                        mInformationContent.clear();

                        break;
                    }
                    // ハンドラーが設定されていれば、データを送信する。
                    if (mPortalContentHandler != null) {
                        mPortalContentHandler.onInformation(mInformationContent, false);
                    }
                    mInformationContent = new InformationContent();

                    break;
            }
        } else if (HtmlElement.TD_CODE == localCode && HtmlElement.TD_NAME.equals(localName)) {
            // TD タグを検出した場合、タグの位置によって適切な位置にデータを格納する。
            switch (mCurrentTable) {
                case TABLE_SCHEDULES:
                    // スケジュールを取り扱う。
                    switch (mCurrentColumn) {
                        case COLUMN_SCHEDULE_DATE: {
                            Matcher matcher;

                            // 日付を解析して格納する。
                            matcher = mScheduleDatePattern.matcher(text);
                            if (matcher.find()) {
                                int year;
                                int month;
                                int day;

                                year = Integer.parseInt(matcher.group(1)) + 2000;
                                month = Integer.parseInt(matcher.group(2));
                                day = Integer.parseInt(matcher.group(3));
                                mScheduleContent.setStartDate(year, month, day);
                            }

                            break;
                        }
                    }

                case TABLE_INFORMATION:
                    // お知らせを取り扱う。
                    switch (mCurrentColumn) {
                        case COLUMN_INFORMATION_PUBLIC_DATE: {
                            Matcher matcher;

                            // 公開日を解析して格納する。
                            matcher = mInformationDatePattern.matcher(text);
                            if (matcher.find()) {
                                int year;
                                int month;
                                int day;

                                year = Integer.parseInt(matcher.group(1)) + 2000;
                                month = Integer.parseInt(matcher.group(2));
                                day = Integer.parseInt(matcher.group(3));
                                mInformationContent.setPublicStartDate(year, month, day);
                            }

                            break;
                        }

                        case COLUMN_INFORMATION_PUBLIC_PERSON:
                            // 掲載者を格納する。
                            mInformationContent.setPublicPerson(text);

                            break;

                        case COLUMN_INFORMATION_PRIORITY: {
                            // 文字列より重要度を判定して格納する。
                            mInformationContent.setPriority(detectInformationPriority(text));

                            break;
                        }

                        case COLUMN_INFORMATION_CATEGORY: {
                            // 文字列よりジャンルを判定して格納する。
                            mInformationContent.setCategory(detectInformationCategory(text));

                            break;
                        }
                    }

                    break;

                case TABLE_CANCELLED_LECTURES:
                    // 休講情報を取り扱う。
                    switch (mCurrentColumn) {
                        case COLUMN_NAME:
                            // 文字列より行の種類を判断して次の列に備える。
                            mCurrentRow = detectCancelledLecturesRow(text);

                            break;

                        case COLUMN_VALUE:
                            switch (mCurrentRow) {
                                case ROW_CANCELLED_LECTURE_SUBJECT: {
                                    Matcher matcher;

                                    // 講義名を解析して格納する。
                                    matcher = mCancelledLectureSubjectPattern.matcher(text);
                                    if (matcher.find()) {
                                        mCancelledLectureContent.setSubject(matcher.group(1));
                                        mCancelledLectureContent.setProfessor(matcher.group(2));
                                    }

                                    break;
                                }

                                case ROW_CANCELLED_LECTURE_CANCELLED_LECTURE: {
                                    Matcher matcher;

                                    // 休講日を解析して格納する。
                                    matcher = mCancelledLectureDatePattern.matcher(text);
                                    if (matcher.find()) {
                                        int year;
                                        int month;
                                        int day;
                                        int period;

                                        year = mThisYear;
                                        month = Integer.parseInt(matcher.group(1));
                                        day = Integer.parseInt(matcher.group(2));
                                        if (matcher.group(3) != null) {
                                            period = Integer.parseInt(matcher.group(3));
                                        } else {
                                            period = CancelledLectureContent.PERIOD_NOT_SPECIFIED;
                                        }
                                        if (mThisMonth >= 4 && month < 4) {
                                            year++;
                                        } else if (mThisMonth < 4 && month >= 4) {
                                            year--;
                                        }
                                        mCancelledLectureContent.setCancelledLectureDate(year, month, day);
                                        mCancelledLectureContent.setCancelledLecturePeriod(period);
                                    }

                                    break;
                                }

                                case ROW_CANCELLED_LECTURE_MAKEUP_LECTURE: {
                                    Matcher matcher;

                                    // 補講日を解析して格納する。
                                    matcher = mCancelledLectureDatePattern.matcher(text);
                                    if (matcher.find()) {
                                        int year;
                                        int month;
                                        int day;
                                        int period;

                                        year = mThisYear;
                                        month = Integer.parseInt(matcher.group(1));
                                        day = Integer.parseInt(matcher.group(2));
                                        if (matcher.group(3) != null) {
                                            period = Integer.parseInt(matcher.group(3));
                                        } else {
                                            period = CancelledLectureContent.PERIOD_NOT_SPECIFIED;
                                        }
                                        if (mThisMonth >= 4 && month < 4) {
                                            year++;
                                        } else if (mThisMonth < 4 && month >= 4) {
                                            year--;
                                        }
                                        mCancelledLectureContent.setMakeupLectureDate(year, month, day);
                                        mCancelledLectureContent.setMakeupLecturePeriod(period);
                                    }

                                    break;
                                }

                                case ROW_CANCELLED_LECTURE_MAKEUP_LECTURE_CLASSROOM:
                                    // 補講教室を格納する。
                                    mCancelledLectureContent.setMakeupLectureClassroom(text.trim());

                                    break;

                                case ROW_CANCELLED_LECTURE_NOTE1:
                                    // 備考 1 を格納する。
                                    mCancelledLectureContent.setNote1(text.trim());

                                    break;

                                case ROW_CANCELLED_LECTURE_NOTE2:
                                    // 備考 2 を格納する。
                                    mCancelledLectureContent.setNote2(text.trim());

                                    break;
                            }

                            break;
                    }

                    break;

                case TABLE_CLASSROOM_CHANGES:
                    // 教室変更を取り扱う。
                    switch (mCurrentColumn) {
                        case COLUMN_NAME:
                            // 文字列より行の種類を判断して次の列に備える。
                            mCurrentRow = detectClassroomChangesRow(text);

                            break;

                        case COLUMN_VALUE:
                            switch (mCurrentRow) {
                                case ROW_CLASSROOM_CHANGE_SUBJECT: {
                                    Matcher matcher;

                                    // 講義名を格納して代入する。
                                    matcher = mClassroomChangeSubjectPattern.matcher(text);
                                    if (matcher.find()) {
                                        mClassroomChangeContent.setSubject(matcher.group(1));
                                        mClassroomChangeContent.setProfessor(matcher.group(2));
                                    }

                                    break;
                                }

                                case ROW_CLASSROOM_CHANGE_DATE: {
                                    Matcher matcher;

                                    // 日時を解析して格納する。
                                    matcher = mClassroomChangeDatePattern.matcher(text);
                                    if (matcher.find()) {
                                        int year;
                                        int month;
                                        int day;
                                        int period;

                                        year = mThisYear;
                                        month = Integer.parseInt(matcher.group(1));
                                        day = Integer.parseInt(matcher.group(2));
                                        if (matcher.group(3) != null) {
                                            period = Integer.parseInt(matcher.group(3));
                                        } else {
                                            period = ClassroomChangeContent.PERIOD_NOT_SPECIFIED;
                                        }
                                        if (mThisMonth >= 4 && month < 4) {
                                            year++;
                                        } else if (mThisMonth < 4 && month >= 4) {
                                            year--;
                                        }
                                        mClassroomChangeContent.setDate(year, month, day);
                                        mClassroomChangeContent.setPeriod(period);
                                    }

                                    break;
                                }

                                case ROW_CLASSROOM_CHANGE_CLASSROOM: {
                                    Matcher matcher;

                                    // 教室を解析して格納する。
                                    matcher = mClassroomChangeClassroomPattern.matcher(text);
                                    if (matcher.find()) {
                                        mClassroomChangeContent.setOldClassroom(matcher.group(1));
                                        mClassroomChangeContent.setNewClassroom(matcher.group(2));
                                    }

                                    break;
                                }

                                case ROW_CLASSROOM_CHANGE_NOTE1:
                                    // 備考 1 を格納する。
                                    mClassroomChangeContent.setNote1(text.trim());

                                    break;

                                case ROW_CLASSROOM_CHANGE_NOTE2:
                                    // 備考 2 を格納する。
                                    mClassroomChangeContent.setNote2(text.trim());

                                    break;
                            }

                            break;
                    }

                    break;
            }
        } else if (HtmlElement.A_CODE == localCode && HtmlElement.A_NAME.equals(localName)) {
            // A タグを検出した場合、タグの位置によって適切な位置にデータを格納する。
            switch (mCurrentTable) {
                case TABLE_SCHEDULES:
                    // スケジュールを取り扱う。
                    switch (mCurrentColumn) {
                        case COLUMN_SCHEDULE_TITLE:
                            // タイトルを格納する。
                            mScheduleContent.setTitle(text.trim());

                            break;
                    }

                    break;

                case TABLE_INFORMATION:
                    // お知らせを取り扱う。
                    switch (mCurrentColumn) {
                        case COLUMN_INFORMATION_TITLE:
                            // タイトルを格納する。
                            mInformationContent.setTitle(text.trim());

                            break;
                    }

                    break;
            }
        } else if (HtmlElement.FONT_CODE == localCode && HtmlElement.FONT_NAME.equals(localName)) {
            // FONT タグを検出した場合、休講情報の日時を適切に設定する。
            switch (mCurrentTable) {
                case TABLE_CANCELLED_LECTURES:
                    // 休講情報を取り扱う。
                    switch (mCurrentColumn) {
                        case COLUMN_VALUE:
                            switch (mCurrentRow) {
                                case ROW_CANCELLED_LECTURE_CANCELLED_LECTURE:
                                    // 補講の文字列を検索し、休講の日時か補講の日時か判断する。
                                    if (text.contains("補講")) {
                                        mCurrentRow = ROW_CANCELLED_LECTURE_MAKEUP_LECTURE;
                                    }

                                    break;
                            }

                            break;
                    }

                    break;
            }
        }
    }

    public void endDocument() {
        mScheduleContent.clear();
        mInformationContent.clear();
        mCancelledLectureContent.clear();
        mClassroomChangeContent.clear();
    }

    public void setPortalContentHandler(PortalContentHandler handler) {
        mPortalContentHandler = handler;
    }

    public boolean isSuccess() {
        return mSuccess;
    }

    private int detectTable(String text, boolean taken) {
        int table;

        if (taken) {
            if (mCurrentDocument == DOCUMENT_TAKEN && text.contains("スケジュール")) {
                table = TABLE_SCHEDULES;
            } else if (mCurrentDocument == DOCUMENT_TAKEN && text.contains("お知らせ")) {
                table = TABLE_INFORMATION;
            } else if (text.contains("休講情報")) {
                table = TABLE_CANCELLED_LECTURES;
            } else if (text.contains("教室変更")) {
                table = TABLE_CLASSROOM_CHANGES;
            } else {
                table = TABLE_UNKNOWN;
            }
        } else {
            if (text.contains("休講情報")) {
                table = TABLE_CANCELLED_LECTURES;
            } else if (text.contains("教室変更")) {
                table = TABLE_CLASSROOM_CHANGES;
            } else {
                table = TABLE_UNKNOWN;
            }
        }

        return table;
    }

    private int detectInformationPriority(String text) {
        int priority;

        if (text.contains("最重要")) {
            priority = InformationContent.PRIORITY_MOST_IMPORTANT;
        } else if (text.contains("重要")) {
            priority = InformationContent.PRIORITY_IMPORTANT;
        } else if (text.contains("普通")) {
            priority = InformationContent.PRIORITY_NORMAL;
        } else if (text.contains("緊急")) {
            priority = InformationContent.PRIORITY_UNKNOWN;
        } else {
            priority = InformationContent.PRIORITY_NORMAL;
        }

        return priority;
    }

    private int detectInformationCategory(String text) {
        int category;

        if (text.contains("お知らせ")) {
            category = InformationContent.CATEGORY_INFORMATION;
        } else if (text.contains("掲示")) {
            category = InformationContent.CATEGORY_NOTICE;
        } else if (text.contains("呼び出し")) {
            category = InformationContent.CATEGORY_CALL;
        } else if (text.contains("宿題提示")) {
            category = InformationContent.CATEGORY_HOMEWORK;
        } else if (text.contains("申込受付")) {
            category = InformationContent.CATEGORY_APPLICATION;
        } else {
            category = InformationContent.CATEGORY_UNKNOWN;
        }

        return category;
    }

    private int detectCancelledLecturesRow(String text) {
        int row;

        if (text.contains("授業科目")) {
            row = ROW_CANCELLED_LECTURE_SUBJECT;
        } else if (text.contains("月日・時限")) {
            row = ROW_CANCELLED_LECTURE_CANCELLED_LECTURE;
        } else if (text.contains("(補講)")) {
            row = ROW_CANCELLED_LECTURE_MAKEUP_LECTURE;
        } else if (text.contains("(補講教室)")) {
            row = ROW_CANCELLED_LECTURE_MAKEUP_LECTURE_CLASSROOM;
        } else if (text.contains("備考１")) {
            row = ROW_CANCELLED_LECTURE_NOTE1;
        } else if (text.contains("備考２")) {
            row = ROW_CANCELLED_LECTURE_NOTE2;
        } else {
            row = ROW_UNKNOWN;
        }

        return row;
    }

    private int detectClassroomChangesRow(String text) {
        int row;

        if (text.contains("授業科目")) {
            row = ROW_CLASSROOM_CHANGE_SUBJECT;
        } else if (text.contains("月日・時限")) {
            row = ROW_CLASSROOM_CHANGE_DATE;
        } else if (text.contains("教室")) {
            row = ROW_CLASSROOM_CHANGE_CLASSROOM;
        } else if (text.contains("備考１")) {
            row = ROW_CLASSROOM_CHANGE_NOTE1;
        } else if (text.contains("備考２")) {
            row = ROW_CLASSROOM_CHANGE_NOTE2;
        } else {
            row = ROW_UNKNOWN;
        }

        return row;
    }
}
