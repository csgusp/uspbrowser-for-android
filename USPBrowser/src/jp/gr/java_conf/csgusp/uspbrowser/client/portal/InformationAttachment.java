/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

/**
 * InformationAttachment クラスはお知らせの添付ファイルを格納するクラスです。
 *
 * @author leak4mk0
 * @version 16
 * @since 1
 */
public class InformationAttachment {
	private String mUrl;
	private String mFilename;
	private String mDescription;

	public InformationAttachment() {
		clear();
	}

	public void clear() {
		mUrl = null;
		mFilename = null;
		mDescription = null;
	}

	public boolean isSet() {
		if (mUrl == null || mFilename == null) {
			return false;
		}

		return true;
	}

	public String getUrl() {
		return mUrl;
	}

	public void setUrl(String url) {
		mUrl = url;
	}

	public String getFilename() {
		return mFilename;
	}

	public void setFilename(String filename) {
		mFilename = filename;
	}

	public String getDescription() {
		return mDescription;
	}

	public void setDescription(String description) {
		mDescription = description;
	}
}
