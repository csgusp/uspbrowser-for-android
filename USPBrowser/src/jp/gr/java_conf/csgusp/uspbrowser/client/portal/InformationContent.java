/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

import java.util.ArrayList;
import java.util.List;

/**
 * InformationContent クラスはお知らせを格納するクラスです。
 *
 * @author leak4mk0
 * @version 16
 * @since 1
 */
public class InformationContent {
    /**
     * 日付が指定されていないことを示す日付の値です。無期限であることを意味します。
     */
    public static final int DATE_NOT_SPECIFIED = 1;
    /**
     * 重要度が不明であることを示す値です。
     */
    public static final int PRIORITY_UNKNOWN = 0;
    /**
     * 重要度が最重要であることを示す値です。
     */
    public static final int PRIORITY_MOST_IMPORTANT = 1;
    /**
     * 重要度が重要であることを示す値です。
     */
    public static final int PRIORITY_IMPORTANT = 2;
    /**
     * 重要度が普通であることを示す値です。
     */
    public static final int PRIORITY_NORMAL = 3;
    /**
     * 重要度が緊急であることを示す値です。
     */
    public static final int PRIORITY_URGENT = 9;
    /**
     * ジャンルが不明であることを示す値です。
     */
    public static final int CATEGORY_UNKNOWN = 0;
    /**
     * ジャンルがお知らせであることを示す値です。
     */
    public static final int CATEGORY_INFORMATION = 1;
    /**
     * ジャンルが掲示であることを示す値です。
     */
    public static final int CATEGORY_NOTICE = 2;
    /**
     * ジャンルが呼び出しであることを示す値です。
     */
    public static final int CATEGORY_CALL = 3;
    /**
     * ジャンルが宿題提示であることを示す値です。
     */
    public static final int CATEGORY_HOMEWORK = 8;
    /**
     * ジャンルが申込受付であることを示す値です。
     */
    public static final int CATEGORY_APPLICATION = 9;
    private int mInformationId;
    private String mTitle;
    private long mPublicStartDate;
    private long mPublicEndDate;
    private String mPublicPerson;
    private int mPriority;
    private int mCategory;
    private String mContent;
    private int mRead;
    private int mAttached;
    private List<InformationAttachment> mInformationAttachments;

    public InformationContent() {
        clear();
    }

    /**
     * 格納されている値を破棄し、データを初期化します。
     */
    public void clear() {
        mInformationId = -1;
        mTitle = null;
        mPriority = -1;
        mCategory = -1;
        mContent = null;
        mPublicPerson = null;
        mPublicStartDate = -1;
        mPublicEndDate = -1;
        mInformationAttachments = null;
        mRead = -1;
        mAttached = -1;
    }

    /**
     * 格納されている値を検査し、データをチェックします。
     *
     * @param detail 詳細な値を検査するかどうかを指定します。
     * @return データが正常かどうかを返します。
     */
    public boolean isSet(boolean detail) {
        if (mInformationId < 0 || mTitle == null || mPublicStartDate < 0 || mPublicPerson == null) {
            return false;
        }
        if (detail) {
            if (mPublicEndDate < 0 || mContent == null) {
                return false;
            }
        } else {
            if (mPriority < 0 || mCategory < 0 || mRead < 0 || mAttached < 0) {
                return false;
            }
        }

        return true;
    }

    public int getInformationId() {
        return mInformationId;
    }

    public void setInformationId(int informationId) {
        mInformationId = informationId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getPublicPerson() {
        return mPublicPerson;
    }

    public void setPublicPerson(String publicPerson) {
        mPublicPerson = publicPerson;
    }

    public long getPublicStartDate() {
        return mPublicStartDate;
    }

    public void setPublicStartDate(long publicStartDate) {
        mPublicStartDate = publicStartDate;
    }

    public void setPublicStartDate(int year, int month, int day) {
        if (month <= 2) {
            year--;
            month += 12;
        }
        mPublicStartDate = (365 * year + year / 4 - year / 100 + year / 400
                + 306 * (month + 1) / 10 - 428 + day - 719163) * 86400000L;
    }

    public long getPublicEndDate() {
        return mPublicEndDate;
    }

    public void setPublicEndDate(long publicEndDate) {
        mPublicEndDate = publicEndDate;
    }

    public void setPublicEndDate(int year, int month, int day) {
        if (month <= 2) {
            year--;
            month += 12;
        }
        mPublicEndDate = (365 * year + year / 4 - year / 100 + year / 400
                + 306 * (month + 1) / 10 - 428 + day - 719163) * 86400000L;
    }

    public int getPriority() {
        return mPriority;
    }

    public void setPriority(int priority) {
        mPriority = priority;
    }

    public int getCategory() {
        return mCategory;
    }

    public void setCategory(int category) {
        mCategory = category;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public boolean isRead() {
        return mRead > 0;
    }

    public void setRead(boolean read) {
        mRead = read ? 1 : 0;
    }

    public boolean isAttached() {
        return mAttached > 0;
    }

    public void setAttached(boolean attached) {
        mAttached = attached ? 1 : 0;
    }

    public List<InformationAttachment> getInformationAttachments() {
        return mInformationAttachments;
    }

    public void setInformationAttachments(List<InformationAttachment> informationAttachments) {
        mInformationAttachments = informationAttachments;
    }

    public void addInformationAttachment(InformationAttachment informationAttachment) {
        if (mInformationAttachments == null) {
            mInformationAttachments = new ArrayList<InformationAttachment>();
        }
        mInformationAttachments.add(informationAttachment);
    }
}
