/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

import jp.gr.java_conf.csgusp.uspbrowser.util.HtmlElement;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Stack;

/**
 * LogoutHandler クラスは PortalClient クラスでポータルサイトからログアウトする際に、
 * HTML を解析して、ログアウトの成否を判定するクラスです。
 *
 * @author leak4mk0
 * @version 13
 * @since 1
 */
public class LogoutHandler extends DefaultHandler {
	private Stack<StringBuilder> mBuilderStack;
	private PortalContentHandler mPortalContentHandler;
	private boolean mSuccess;

	public LogoutHandler() {
        mBuilderStack = new Stack<StringBuilder>();
        mPortalContentHandler = null;
        mSuccess = false;
    }

    public void startDocument() {
        mBuilderStack.clear();
        mSuccess = false;
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		mBuilderStack.push(new StringBuilder());
	}

	public void characters(char[] ch, int start, int length) {
		mBuilderStack.peek().append(ch, start, length);
	}

    public void endElement(String uri, String localName, String qName) {
        int localCode;
        String text;

        localCode = localName.hashCode();
        text = mBuilderStack.pop().toString();

        if (HtmlElement.TITLE_CODE == localCode && HtmlElement.TITLE_NAME.equals(localName)) {
            mSuccess = true;
            if (mPortalContentHandler == null) {
                return;
            }
            mPortalContentHandler.onLogon(text.contains("滋賀県立大学ポータルサイト") && !text.contains("エラー"));
        }
    }

	public void setPortalContentHandler(PortalContentHandler handler) {
		mPortalContentHandler = handler;
	}

	public boolean isSuccess() {
		return mSuccess;
	}
}
