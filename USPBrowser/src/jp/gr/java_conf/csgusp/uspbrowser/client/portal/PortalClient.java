/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

import jp.gr.java_conf.csgusp.uspbrowser.util.ModernSSLSocketFactory;

import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * PortalClient クラスは、ポータルサイトへのログオンや内容の取得といった、
 * 各種のアクセスを容易に行う機能を提供するクラスです。
 *
 * @author leak4mk0
 * @version 36
 * @since 1
 */
public class PortalClient {
	private static final String TAG = "PortalClient";
	private static final boolean DEBUG = false;

	public static final int SUCCESS = 1;
	public static final int FAILURE_UNKNOWN = 0;
	public static final int FAILURE_NETWORK = -1;
	public static final int FAILURE_CONTENT = -2;

	private static final String PORTAL_URI = "https://sgkwe.office.usp.ac.jp/SGKWeb/";
	private static final String LOGON_URI = PORTAL_URI + "Check.asp";
	private static final String LOGOUT_URI = PORTAL_URI + "LogOut.asp";
	private static final String TOP_URI = PORTAL_URI + "TopFrame_R.asp";
	private static final String SCHEDULE_URI = PORTAL_URI + "/evtdsp.asp?kbtb=1&cdjh=%d&cdren=%d";
	private static final String INFORMATION_URI = PORTAL_URI + "Naiyo.asp?cdmsg=%d";

	private ThreadSafeClientConnManager mClientConnManager;
	private HttpClient mHttpClient;
	private HttpRequestBase mHttpRequest;
	private PortalContentHandler mPortalContentHandler;

	public PortalClient() {
		HttpParams params;
		SchemeRegistry registry;

		params = new BasicHttpParams();
		HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
		HttpProtocolParams.setContentCharset(params, "Shift-JIS");
		registry = new SchemeRegistry();
		registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
		registry.register(new Scheme("https", ModernSSLSocketFactory.getSocketFactory(), 443));
		mClientConnManager = new ThreadSafeClientConnManager(params, registry);
		mHttpClient = new DefaultHttpClient(mClientConnManager, params);
		mPortalContentHandler = null;
	}

	public void setPortalContentHandler(PortalContentHandler handler) {
		mPortalContentHandler = handler;
	}

	public void abort() {
		if (mHttpRequest != null) {
			mHttpRequest.abort();
		}
	}

	public void dispose() {
		if (mClientConnManager != null) {
			mClientConnManager.shutdown();
		}
	}

	public int logon(String userId, String password) {
		HttpPost post;
		URI uri;
		List<NameValuePair> values;
		HttpResponse response;
		HttpEntity entity;
		Parser parser;
		LogonHandler handler;
		Header header;
		String charset;

		if (DEBUG) Log.v(TAG, "logon(): Beginning");
		post = new HttpPost();
		uri = URI.create(LOGON_URI);
		post.setURI(uri);
		values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("inpJAVA", "on"));
		values.add(new BasicNameValuePair("inpCDID", userId));
		values.add(new BasicNameValuePair("inpPASS", password));
		try {
			post.setEntity(new UrlEncodedFormEntity(values));
		} catch (UnsupportedEncodingException e) {
			return FAILURE_UNKNOWN;
		}
		try {
			mHttpRequest = post;
			response = mHttpClient.execute(post);
		} catch (IOException e) {
			return FAILURE_NETWORK;
		} finally {
			mHttpRequest = null;
		}
		entity = response.getEntity();
		parser = new Parser();
		handler = new LogonHandler();
		handler.setPortalContentHandler(mPortalContentHandler);
		parser.setContentHandler(handler);
		header = entity.getContentEncoding();
		charset = null;
		if (header != null) {
			charset = header.getValue();
		}
		if (charset == null) {
			charset = EntityUtils.getContentCharSet(entity);
		}
		if (charset == null) {
			charset = "Shift-JIS";
		}
		try {
			parser.parse(new InputSource(new InputStreamReader(entity.getContent(), charset)));
		} catch (IOException e) {
			return FAILURE_NETWORK;
		} catch (SAXException e) {
			return FAILURE_CONTENT;
		}
		if (!handler.isSuccess()) {
			return FAILURE_CONTENT;
		}
		if (DEBUG) Log.v(TAG, "logon(): Ending");

		return SUCCESS;
	}

	public int logout() {
		HttpGet get;
		URI uri;
		HttpResponse response;
		HttpEntity entity;
		Parser parser;
		LogoutHandler handler;
		Header header;
		String charset;

		if (DEBUG) Log.v(TAG, "logout(): Beginning");
		get = new HttpGet();
		uri = URI.create(LOGOUT_URI);
		get.setURI(uri);
		try {
			mHttpRequest = get;
			response = mHttpClient.execute(get);
		} catch (ClientProtocolException e) {
			return FAILURE_NETWORK;
		} catch (IOException e) {
			return FAILURE_NETWORK;
		} finally {
			mHttpRequest = null;
		}
		entity = response.getEntity();
		parser = new Parser();
		handler = new LogoutHandler();
		handler.setPortalContentHandler(mPortalContentHandler);
		parser.setContentHandler(handler);
		header = entity.getContentEncoding();
		charset = null;
		if (header != null) {
			charset = header.getValue();
		}
		if (charset == null) {
			charset = EntityUtils.getContentCharSet(entity);
		}
		if (charset == null) {
			charset = "Shift-JIS";
		}
		if (mPortalContentHandler == null) {
			mPortalContentHandler = new PortalDefaultHandler();
		}
		try {
			parser.parse(new InputSource(new InputStreamReader(entity.getContent(), charset)));
		} catch (IOException e) {
			return FAILURE_NETWORK;
		} catch (SAXException e) {
			return FAILURE_CONTENT;
		}
		if (!handler.isSuccess()) {
			return FAILURE_CONTENT;
		}
		if (DEBUG) Log.v(TAG, "logout(): Ending");

		return SUCCESS;
	}

	public int fetchTop() {
		HttpPost post;
		URI uri;
		List<NameValuePair> values;
		HttpResponse response;
		HttpEntity entity;
        ExecutorService executor;
        CallableHttpClient client;
        Future<HttpResponse> future;
		Parser parser;
		FetchTopHandler handler;
		Header header;
		String charset;

		if (DEBUG) Log.v(TAG, "fetchTop(): Beginning");

		post = new HttpPost();
		uri = URI.create(TOP_URI);
		post.setURI(uri);
		values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("inpKKRELOADFLG", "2"));
		values.add(new BasicNameValuePair("inpKHRELOADFLG", "2"));
		try {
			entity = new UrlEncodedFormEntity(values);
		} catch (UnsupportedEncodingException e) {
			return FAILURE_UNKNOWN;
		}
		post.setEntity(entity);
        mHttpRequest = post;
        client = new CallableHttpClient(mHttpClient, mHttpRequest);
        executor = Executors.newSingleThreadExecutor();
        future = executor.submit(client);

        post = new HttpPost();
        post.setURI(uri);
		values = new ArrayList<NameValuePair>();
		values.add(new BasicNameValuePair("inpKKRELOADFLG", "1"));
		values.add(new BasicNameValuePair("inpKHRELOADFLG", "1"));
		try {
			entity = new UrlEncodedFormEntity(values);
		} catch (UnsupportedEncodingException e) {
			return FAILURE_UNKNOWN;
		}
		post.setEntity(entity);
		try {
			mHttpRequest = post;
            if (DEBUG) Log.v(TAG, "PortalClient.mHttpClient.execute(): Beginning");
			response = mHttpClient.execute(post);
            if (DEBUG) Log.v(TAG, "PortalClient.mHttpClient.execute(): Ending");
        } catch (IOException e) {
			return FAILURE_NETWORK;
		} finally {
			mHttpRequest = null;
		}
        parser = new Parser();
        handler = new FetchTopHandler();
        handler.setPortalContentHandler(mPortalContentHandler);
        parser.setContentHandler(handler);
		entity = response.getEntity();
		header = entity.getContentEncoding();
		charset = null;
		if (header != null) {
			charset = header.getValue();
		}
		if (charset == null) {
			charset = EntityUtils.getContentCharSet(entity);
		}
		if (charset == null) {
			charset = "Shift-JIS";
		}
        try {
            parser.parse(new InputSource(new InputStreamReader(entity.getContent(), charset)));
        } catch (IOException e) {
            return FAILURE_NETWORK;
        } catch (SAXException e) {
            return FAILURE_CONTENT;
        }

        try {
            entity = future.get().getEntity();
        } catch (InterruptedException e) {
            return FAILURE_NETWORK;
        } catch (ExecutionException e) {
            return FAILURE_NETWORK;
        }
        try {
            parser.parse(new InputSource(new InputStreamReader(entity.getContent(), charset)));
        } catch (IOException e) {
            return FAILURE_NETWORK;
        } catch (SAXException e) {
            return FAILURE_CONTENT;
        }

		if (!handler.isSuccess()) {
			return FAILURE_CONTENT;
		}

		if (DEBUG) Log.v(TAG, "fetchTop(): Ending");

		return SUCCESS;
	}

	public int fetchSchedule(int scheduleId, int studentId) {
		HttpGet get;
		URI uri;
		HttpResponse response;
		HttpEntity entity;
		Parser parser;
		FetchScheduleHandler handler;
		Header header;
		String charset;

		if (DEBUG) Log.v(TAG, "fetchSchedule(): Begining");
		get = new HttpGet();
		uri = URI.create(String.format(Locale.ENGLISH, SCHEDULE_URI, scheduleId, studentId));
		get.setURI(uri);
		try {
			mHttpRequest = get;
			response = mHttpClient.execute(get);
		} catch (ClientProtocolException e) {
			return FAILURE_NETWORK;
		} catch (IOException e) {
			return FAILURE_NETWORK;
		} finally {
			mHttpRequest = null;
		}
		entity = response.getEntity();
		parser = new Parser();
		handler = new FetchScheduleHandler();
		handler.setPortalContentHandler(mPortalContentHandler);
		parser.setContentHandler(handler);
		header = entity.getContentEncoding();
		charset = null;
		if (header != null) {
			charset = header.getValue();
		}
		if (charset == null) {
			charset = EntityUtils.getContentCharSet(entity);
		}
		if (charset == null) {
			charset = "Shift-JIS";
		}
		if (mPortalContentHandler == null) {
			mPortalContentHandler = new PortalDefaultHandler();
		}
		try {
			parser.parse(new InputSource(new InputStreamReader(entity.getContent(), charset)));
		} catch (IOException e) {
			return FAILURE_NETWORK;
		} catch (SAXException e) {
			return FAILURE_CONTENT;
		}
		if (!handler.isSuccess()) {
			return FAILURE_CONTENT;
		}
		if (DEBUG) Log.v(TAG, "fetchSchedule(): Ending");

		return SUCCESS;
	}

	public int fetchInformation(int id) {
		HttpGet get;
		URI uri;
		HttpResponse response;
		HttpEntity entity;
		Parser parser;
		FetchInformationHandler handler;
		Header header;
		String charset;

		if (DEBUG) Log.v(TAG, "fetchInformation(): Begining");
		get = new HttpGet();
		uri = URI.create(String.format(Locale.ENGLISH, INFORMATION_URI, id));
		get.setURI(uri);
		try {
			mHttpRequest = get;
			response = mHttpClient.execute(get);
		} catch (ClientProtocolException e) {
			return FAILURE_NETWORK;
		} catch (IOException e) {
			return FAILURE_NETWORK;
		} finally {
			mHttpRequest = null;
		}
		entity = response.getEntity();
		parser = new Parser();
		handler = new FetchInformationHandler();
		handler.setPortalContentHandler(mPortalContentHandler);
		handler.setURI(uri);
		parser.setContentHandler(handler);
		header = entity.getContentEncoding();
		charset = null;
		if (header != null) {
			charset = header.getValue();
		}
		if (charset == null) {
			charset = EntityUtils.getContentCharSet(entity);
		}
		if (charset == null) {
			charset = "Shift-JIS";
		}
		if (mPortalContentHandler == null) {
			mPortalContentHandler = new PortalDefaultHandler();
		}
		try {
			parser.parse(new InputSource(new InputStreamReader(entity.getContent(), charset)));
		} catch (IOException e) {
			return FAILURE_NETWORK;
		} catch (SAXException e) {
			return FAILURE_CONTENT;
		}
		if (!handler.isSuccess()) {
			return FAILURE_CONTENT;
		}
		if (DEBUG) Log.v(TAG, "fetchInformation(): Ending");

		return SUCCESS;
	}

    private static class CallableHttpClient implements Callable<HttpResponse> {
        private HttpClient mHttpClient;
        private HttpRequestBase mHttpRequest;

        public CallableHttpClient(HttpClient httpClient, HttpRequestBase httpRequest) {
            mHttpClient = httpClient;
            mHttpRequest = httpRequest;
        }

        @Override
        public HttpResponse call() throws IOException {
            HttpResponse response;

            if (DEBUG) Log.v(TAG, "CallableHttpClient.mHttpClient.execute(): Beginning");
            response = mHttpClient.execute(mHttpRequest);
            if (DEBUG) Log.v(TAG, "CallableHttpClient.mHttpClient.execute(): Ending");

            return response;
        }

        public void abort() {
            mHttpRequest.abort();
            mHttpRequest = null;
        }
    }
}
