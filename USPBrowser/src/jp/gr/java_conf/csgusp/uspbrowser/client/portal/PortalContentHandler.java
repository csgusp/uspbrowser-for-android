/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

/**
 * PortalContentHandler インターフェースは PortalClient のコールバックを定義したクラスです。
 *
 * @author leak4mk0
 * @version 1
 * @since 1
 */
public interface PortalContentHandler {
	public void onLogon(boolean authorized);

	public void onLogout();

	public void onSchedule(ScheduleContent content, boolean detail);

	public void onInformation(InformationContent content, boolean detail);

	public void onCancelledLecture(CancelledLectureContent content);

	public void onClassroomChange(ClassroomChangeContent content);
}
