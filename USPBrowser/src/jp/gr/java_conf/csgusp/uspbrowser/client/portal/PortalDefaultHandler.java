/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

/**
 * PortalDefaultHandler クラスは PortalContentHandler の実装をしたクラスです。
 *
 * @author leak4mk0
 * @version 1
 * @since 1
 */
public class PortalDefaultHandler implements PortalContentHandler {
	@Override
	public void onLogon(boolean authorized) {
	}

	@Override
	public void onLogout() {
	}

	@Override
	public void onSchedule(ScheduleContent content, boolean detail) {
	}

	@Override
	public void onInformation(InformationContent content, boolean detail) {
	}

	@Override
	public void onCancelledLecture(CancelledLectureContent content) {
	}

	@Override
	public void onClassroomChange(ClassroomChangeContent content) {
	}
}
