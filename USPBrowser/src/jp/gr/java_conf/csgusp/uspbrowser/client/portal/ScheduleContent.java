/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.client.portal;

/**
 * Schedule クラスはお知らせのスケジュールを格納するクラスです。
 *
 * @author leak4mk0
 * @version 16
 * @since 1
 */
public class ScheduleContent {
    /**
     * 時間が指定されていないことを示す時間の値です。終日であることを意味します。
     */
    public static final int TIME_NOT_SPECIFIED = 24 * 60 * 60 * 1000 + 1;
    /**
     * 重要度が不明であることを示す値です。
     */
    public static final int PRIORITY_UNKNOWN = 0;
    /**
     * 重要度が最重要であることを示す値です。
     */
    public static final int PRIORITY_MOST_IMPORTANT = 1;
    /**
     * 重要度が重要であることを示す値です。
     */
    public static final int PRIORITY_IMPORTANT = 2;
    /**
     * 重要度が普通であることを示す値です。
     */
    public static final int PRIORITY_NORMAL = 3;
    /**
     * 重要度が緊急であることを示す値です。
     */
    public static final int PRIORITY_URGENT = 9;
    private int mStudentId;
    private int mScheduleId;
    private String mTitle;
    private long mStartDate;
    private long mEndDate;
    private long mStartTime;
    private long mEndTime;
    private String mPlace;
    private int mPriority;
    private String mContent;

    public ScheduleContent() {
        clear();
    }

    /**
     * 格納されている値を破棄し、データを初期化します。
     */
    public void clear() {
        mStudentId = -1;
        mScheduleId = -1;
        mTitle = null;
        mStartDate = -1;
        mEndDate = -1;
        mStartTime = -1;
        mEndTime = -1;
        mPriority = PRIORITY_UNKNOWN;
        mPlace = null;
        mContent = null;
    }

    /**
     * 格納されている値を検査し、データをチェックします。
     *
     * @param detail 詳細な値を検査するかどうかを指定します。
     * @return データが正常かどうかを返します。
     */
    public boolean isSet(boolean detail) {
        if (mScheduleId < 0 || mTitle == null || mStartDate < 0) {
            return false;
        }
        if (detail) {
            if (mEndDate < 0 || mStartTime < 0 || mEndTime < 0 || mContent == null) {
                return false;
            }
        } else {
            if (mStudentId < 0) {
                return false;
            }
        }

        return true;
    }

    public int getStudentId() {
        return mStudentId;
    }

    public void setStudentId(int studentId) {
        mStudentId = studentId;
    }

    public int getScheduleId() {
        return mScheduleId;
    }

    public void setScheduleId(int scheduleId) {
        mScheduleId = scheduleId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public long getStartDate() {
        return mStartDate;
    }

    public void setStartDate(long startDate) {
        mStartDate = startDate;
    }

    public void setStartDate(int year, int month, int day) {
        if (month <= 2) {
            year--;
            month += 12;
        }
        mStartDate = (365 * year + year / 4 - year / 100 + year / 400
                + 306 * (month + 1) / 10 - 428 + day - 719163) * 86400000L;
    }

    public long getEndDate() {
        return mEndDate;
    }

    public void setEndDate(long endDate) {
        mEndDate = endDate;
    }

    public void setEndDate(int year, int month, int day) {
        if (month <= 2) {
            year--;
            month += 12;
        }
        mEndDate = (365 * year + year / 4 - year / 100 + year / 400
                + 306 * (month + 1) / 10 - 428 + day - 719163) * 86400000L;
    }

    public long getStartTime() {
        return mStartTime;
    }

    public void setStartTime(long startTime) {
        mStartTime = startTime;
    }

    public void setStartTime(int hour, int minute, int second) {
        mStartTime = hour * 3600000L + minute * 60000L + second * 1000L;
    }

    public long getEndTime() {
        return mEndTime;
    }

    public void setEndTime(long endTime) {
        mEndTime = endTime;
    }

    public void setEndTime(int hour, int minute, int second) {
        mEndTime = hour * 3600000L + minute * 60000L + second * 1000L;
    }

    public String getPlace() {
        return mPlace;
    }

    public void setPlace(String place) {
        mPlace = place;
    }

    public int getPriority() {
        return mPriority;
    }

    public void setPriority(int priority) {
        mPriority = priority;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }
}
