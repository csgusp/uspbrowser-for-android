/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * ブラウザの設定にアクセスするクラス。
 *
 * @author leak4mk0
 * @version 34
 * @see android.content.SharedPreferences
 * @since 1
 */
public class BrowserPreferences implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String KEY_BROWSER_VERSION = "browser_version";
    public static final String KEY_BROWSER_SETUP = "browser_setup";
    public static final String KEY_BROWSER_LICENSE_VERSION = "browser_license_version";
    public static final String KEY_BROWSER_ACCEPT_LICENSE = "browser_accept_license";
    public static final String KEY_BROWSER_ACCEPT_GOOGLE_TERMS = "browser_accept_google_terms";
    public static final String KEY_BROWSER_HOME_PAGE = "browser_home_page";
    public static final String KEY_BROWSER_LAST_PAGE = "browser_last_page";
    public static final String KEY_BROWSER_SHOW_TIPS = "browser_show_tips";

    public static final String KEY_SYSTEM_USER_ID = "system_user_id";
    public static final String KEY_SYSTEM_PASSWORD = "system_password";
    public static final String KEY_PORTAL_FETCH_AT_LAUNCH = "portal_fetch_at_launch";
    public static final String KEY_PORTAL_FETCH_CONTENTS_AUTOMATICALLY = "portal_fetch_contents_automatically";
    public static final String KEY_PORTAL_SHOW_DELETED_CONTENTS = "portal_show_deleted_contents";
    public static final String KEY_PORTAL_SEARCH_FILTER = "portal_search_filter";
    public static final String KEY_PORTAL_READ_TIME = "portal_read_time";
    public static final String KEY_WIFI_ENABLED = "wifi_enabled";
    public static final String KEY_WIFI_SSID = "wifi_ssid";
    public static final String KEY_WIFI_KEY = "wifi_key";
    public static final String KEY_WIFI_AUTHENTICATION_URL = "wifi_authentication_url";
    public static final String KEY_WIFI_AUTHENTICATION_BODY = "wifi_authentication_body";

    public static final String VALUE_BROWSER_HOME_PAGE_LAST_PAGE = "last_page";
    public static final String VALUE_BROWSER_HOME_PAGE_PORTAL_SCHEDULES = "portal_schedules";
    public static final String VALUE_BROWSER_HOME_PAGE_PORTAL_INFORMATION = "portal_information";
    public static final String VALUE_BROWSER_HOME_PAGE_PORTAL_CANCELLED_LECTURES = "portal_cancelled_lectures";
    public static final String VALUE_BROWSER_HOME_PAGE_PORTAL_CLASSROOM_CHANGES = "portal_classroom_changes";
    public static final String VALUE_BROWSER_HOME_PAGE_ACCESS_TRAINS_INBOUND = "access_trains_inbound";
    public static final String VALUE_BROWSER_HOME_PAGE_ACCESS_TRAINS_OUTBOUND = "access_trains_outbound";
    public static final String VALUE_BROWSER_HOME_PAGE_ACCESS_BUSES_INBOUND = "access_buses_inbound";
    public static final String VALUE_BROWSER_HOME_PAGE_ACCESS_BUSES_OUTBOUND = "access_buses_outbound";
    public static final String VALUE_BROWSER_HOME_PAGE_LINKS_UNIVERSITY = "links_university";
    public static final String VALUE_BROWSER_HOME_PAGE_LINKS_WEATHER = "links_weather";

    public static final int PAGE_LAST_PAGE = 1;
    public static final int PAGE_PORTAL_SCHEDULES = 2;
    public static final int PAGE_PORTAL_INFORMATION = 3;
    public static final int PAGE_PORTAL_CANCELLED_LECTURES = 4;
    public static final int PAGE_PORTAL_CLASSROOM_CHANGES = 5;
    public static final int PAGE_ACCESS_TRAINS_INBOUND = 7;
    public static final int PAGE_ACCESS_TRAINS_OUTBOUND = 8;
    public static final int PAGE_ACCESS_BUSES_INBOUND = 9;
    public static final int PAGE_ACCESS_BUSES_OUTBOUND = 10;
    public static final int PAGE_LINKS_UNIVERSITY = 11;
    public static final int PAGE_LINKS_WEATHER = 12;
    public static final int PAGE_WIFI_CONNECTION = 6;

    public interface OnBrowserPreferenceChangeListener {
        void onBrowserPreferenceChanged(BrowserPreferences browserPreferences, String key);
    }

    private static final String CIPHER_ALGORITHM = "AES";
    private static final String CIPHER_TRANSFORMATION = "AES/CBC/PKCS5Padding";
    private static final byte[] CIPHER_KEY = {
            -0x73, -0x75, -0x10, -0x29, 0x42, 0x51, -0x56, -0x3d,
            -0x74, 0x36, 0x4d, 0x73, 0x06, 0x6f, -0x24, 0x48 };
    private static final byte[] CIPHER_IV = {
            0x44, 0x29, -0x38, 0x0d, 0x30, -0x46, 0x4b, 0x1b,
            0x7a, -0x24, 0x2c, 0x07, 0x7f, -0x48, -0x71, 0x09 };

    private static final int BROWSER_VERSION_DEFAULT = 0;
    private static final boolean BROWSER_SETUP_DEFAULT = false;
    private static final int BROWSER_LICENSE_VERSION_DEFAULT = 0;
    private static final boolean BROWSER_ACCEPT_LICENSE_DEFAULT = false;
    private static final boolean BROWSER_ACCEPT_GOOLE_TERMS_DEFAULT = false;
    private static final int BROWSER_HOME_PAGE_DEFAULT = PAGE_LAST_PAGE;
    private static final int BROWSER_LAST_PAGE_DEFAULT = PAGE_PORTAL_INFORMATION;
    private static final boolean BROWSER_SHOW_TIPS_DEFAULT = true;
    private static final String SYSTEM_USER_ID_DEFAULT = null;
    private static final String SYSTEM_PASSWORD_DEFAULT = null;
    private static final boolean PORTAL_FETCH_AT_LAUNCH_DEFAULT = false;
    private static final boolean PORTAL_FETCH_CONTENTS_AUTOMATICALLY_DEFAULT = false;
    private static final boolean PORTAL_SHOW_DELETED_CONTENTS_DEFAULT = false;
    private static final String PORTAL_SEARCH_FILTER_DEFAULT = null;
    private static final int PORTAL_READ_TIME_DEFAULT = 3000;
    private static final boolean WIFI_ENABLE_DEFAULT = false;
    private static final String WIFI_SSID_DEFAULT = "010e314d56b226d642afe4bda576d301";
    private static final String WIFI_KEY_DEFAULT = "";
    private static final String WIFI_AUTHENTICATION_URL_DEFAULT =
            "5da0ed6e032a1553e4acbe7c8c39ed1cb83c83a2e532ee03f3abdd837c487b4e";
    private static final String WIFI_AUTHENTICATION_BODY_DEFAULT =
            "0ff076ba1bc3d2637eef7e05756f571cf9991e01a6250eb4141a3af1074bbf77" +
                    "cff47b090804eefe7ef55e65e647c83a8a36955387866fd95b687c3da1f3ca2d" +
                    "f3128f5e9632ed2177c2206a04f2ae96108166b7a0df81a0c9f2984397d5d43a" +
                    "ffd2930c105c21f08217bf24948c2ea9196d63912a37203173df13d12ec05d89" +
                    "8b01dabc2ba07ef292469f248c7120c7";

    private static BrowserPreferences mBrowserPreferences;

    private SharedPreferences mSharedPreferences;
    private List<OnBrowserPreferenceChangeListener> mBrowserPreferenceChangeListeners;

    private BrowserPreferences() {
    }

    private BrowserPreferences(Context context) {
        int version;

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);
        mBrowserPreferenceChangeListeners = new ArrayList<BrowserPreferences.OnBrowserPreferenceChangeListener>();

        version = getBrowserVersion();
        if (version >= 22 && version <= 31) {
            mSharedPreferences.edit()
                    .remove("wifi_ssid")
                    .remove("wifi_key")
                    .remove("wifi_proxy")
                    .remove("wifi_authentication_url")
                    .commit();
        }

        setBrowserVersion(getBrowserVersion());
        setBrowserSetup(getBrowserSetup());
        setBrowserLicenseVersion(getBrowserLicenseVersion());
        setBrowserAcceptLicense(getBrowserAcceptLicense());
        setBrowserAcceptGoogleTerms(getBrowserAcceptGoogleTerms());
        setBrowserHomePage(getBrowserHomePage());
        setBrowserLastPage(getBrowserLastPage());
        setBrowserShowTips(getBrowserShowTips());

        setSystemUserId(getSystemUserId());
        setSystemPassword(getSystemPassword());

        setPortalFetchAtLaunch(getPortalFetchAtLaunch());
        setPortalFetchContentsAutomatically(getPortalFetchContentsAutomatically());
        setPortalShowDeletedContents(getPortalShowDeletedContents());
        setPortalSearchFilter(getPortalSearchFilter());
        setPortalReadTime(getPortalReadTime());

        setWifiEnabled(getWifiEnabled());
        setWifiSsid(getWifiSsid());
        setWifiKey(getWifiKey());
        setWifiAuthenticationUrl(getWifiAuthenticationUrl());
        setWifiAuthenticationBody(getWifiAuthenticationBody());
    }

    public static BrowserPreferences getInstance(Context context) {
        if (mBrowserPreferences == null) {
            mBrowserPreferences = new BrowserPreferences(context);
        }

        return mBrowserPreferences;
    }

    public static String decrypt(String ciphertext) {
        byte[] cipherbytes;
        Cipher cipher;
        SecretKeySpec key;
        IvParameterSpec iv;
        byte[] textbytes;
        String text;

        if (ciphertext == null) {
            return "";
        }
        cipherbytes = new byte[(ciphertext.length() + 1) / 2];
        for (int i = 0; i < cipherbytes.length; i++) {
            try {
                cipherbytes[i] = Integer.valueOf(ciphertext.substring(i * 2, i * 2 + 2), 16).byteValue();
            } catch (NumberFormatException e) {
                return "";
            }
        }
        try {
            cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
        } catch (NoSuchAlgorithmException e) {
            return "";
        } catch (NoSuchPaddingException e) {
            return "";
        }
        key = new SecretKeySpec(CIPHER_KEY, CIPHER_ALGORITHM);
        iv = new IvParameterSpec(CIPHER_IV);
        try {
            cipher.init(Cipher.DECRYPT_MODE, key, iv);
        } catch (InvalidKeyException e) {
            return "";
        } catch (InvalidAlgorithmParameterException e) {
            return "";
        }
        try {
            textbytes = cipher.doFinal(cipherbytes);
        } catch (IllegalBlockSizeException e) {
            return "";
        } catch (BadPaddingException e) {
            return "";
        }
        if (textbytes == null) {
            return "";
        }
        text = new String(textbytes);

        return text;
    }

    public static String encrypt(String text) {
        Cipher cipher;
        SecretKeySpec key;
        IvParameterSpec iv;
        byte[] cipherbytes;
        StringBuilder builder;
        String ciphertext;

        if (text == null) {
            return "";
        }
        try {
            cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
        } catch (NoSuchAlgorithmException e) {
            return "";
        } catch (NoSuchPaddingException e) {
            return "";
        }
        key = new SecretKeySpec(CIPHER_KEY, CIPHER_ALGORITHM);
        iv = new IvParameterSpec(CIPHER_IV);
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);
        } catch (InvalidKeyException e) {
            return "";
        } catch (InvalidAlgorithmParameterException e) {
            return "";
        }
        try {
            cipherbytes = cipher.doFinal(text.getBytes());
        } catch (IllegalBlockSizeException e) {
            return "";
        } catch (BadPaddingException e) {
            return "";
        }
        builder = new StringBuilder();
        for (int i = 0; i < cipherbytes.length; i++) {
            int cipherint;

            cipherint = cipherbytes[i] & 0xff;
            if (cipherint < 0x10) {
                builder.append('0');
            }
            builder.append(Integer.toHexString(cipherint));
        }
        ciphertext = builder.toString();

        return ciphertext;
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        for (OnBrowserPreferenceChangeListener listener : mBrowserPreferenceChangeListeners) {
            listener.onBrowserPreferenceChanged(this, key);
        }
    }

    public void registerOnBrowserPreferenceChangeListener(OnBrowserPreferenceChangeListener listener) {
        mBrowserPreferenceChangeListeners.add(listener);
    }

    public void unregisterOnBrowserPreferenceChangeListener(OnBrowserPreferenceChangeListener listener) {
        mBrowserPreferenceChangeListeners.remove(listener);
    }

    public int getBrowserVersion() {
        return mSharedPreferences.getInt(KEY_BROWSER_VERSION, BROWSER_VERSION_DEFAULT);
    }

    public void setBrowserVersion(int version) {
        mSharedPreferences.edit().putInt(KEY_BROWSER_VERSION, version).commit();
    }

    public boolean getBrowserSetup() {
        return mSharedPreferences.getBoolean(KEY_BROWSER_SETUP, BROWSER_SETUP_DEFAULT);
    }

    public void setBrowserSetup(boolean setup) {
        mSharedPreferences.edit().putBoolean(KEY_BROWSER_SETUP, setup).commit();
    }

    public int getBrowserLicenseVersion() {
        return mSharedPreferences.getInt(KEY_BROWSER_LICENSE_VERSION, BROWSER_LICENSE_VERSION_DEFAULT);
    }

    public void setBrowserLicenseVersion(int version) {
        mSharedPreferences.edit().putInt(KEY_BROWSER_LICENSE_VERSION, version).commit();
    }

    public boolean getBrowserAcceptLicense() {
        return mSharedPreferences.getBoolean(KEY_BROWSER_ACCEPT_LICENSE, BROWSER_ACCEPT_LICENSE_DEFAULT);
    }

    public void setBrowserAcceptLicense(boolean accept) {
        mSharedPreferences.edit().putBoolean(KEY_BROWSER_ACCEPT_LICENSE, accept).commit();
    }

    public boolean getBrowserAcceptGoogleTerms() {
        return mSharedPreferences.getBoolean(KEY_BROWSER_ACCEPT_GOOGLE_TERMS, BROWSER_ACCEPT_GOOLE_TERMS_DEFAULT);
    }

    public void setBrowserAcceptGoogleTerms(boolean accept) {
        mSharedPreferences.edit().putBoolean(KEY_BROWSER_ACCEPT_GOOGLE_TERMS, accept).commit();
    }

    public int getBrowserHomePage() {
        String homePageString;
        int homePage;

        homePageString = mSharedPreferences.getString(KEY_BROWSER_HOME_PAGE, null);
        if (homePageString == null) {
            homePage = BROWSER_HOME_PAGE_DEFAULT;
        } else if (homePageString.equals(VALUE_BROWSER_HOME_PAGE_LAST_PAGE)) {
            homePage = PAGE_LAST_PAGE;
        } else if (homePageString.equals(VALUE_BROWSER_HOME_PAGE_PORTAL_SCHEDULES)) {
            homePage = PAGE_PORTAL_SCHEDULES;
        } else if (homePageString.equals(VALUE_BROWSER_HOME_PAGE_PORTAL_INFORMATION)) {
            homePage = PAGE_PORTAL_SCHEDULES;
        } else if (homePageString.equals(VALUE_BROWSER_HOME_PAGE_PORTAL_CANCELLED_LECTURES)) {
            homePage = PAGE_PORTAL_SCHEDULES;
        } else if (homePageString.equals(VALUE_BROWSER_HOME_PAGE_PORTAL_CLASSROOM_CHANGES)) {
            homePage = PAGE_PORTAL_SCHEDULES;
        } else if (homePageString.equals(VALUE_BROWSER_HOME_PAGE_ACCESS_TRAINS_INBOUND)) {
            homePage = PAGE_ACCESS_TRAINS_INBOUND;
        } else if (homePageString.equals(VALUE_BROWSER_HOME_PAGE_ACCESS_TRAINS_OUTBOUND)) {
            homePage = PAGE_ACCESS_TRAINS_OUTBOUND;
        } else if (homePageString.equals(VALUE_BROWSER_HOME_PAGE_ACCESS_BUSES_INBOUND)) {
            homePage = PAGE_ACCESS_BUSES_INBOUND;
        } else if (homePageString.equals(VALUE_BROWSER_HOME_PAGE_ACCESS_BUSES_OUTBOUND)) {
            homePage = PAGE_ACCESS_BUSES_OUTBOUND;
        } else if (homePageString.equals(VALUE_BROWSER_HOME_PAGE_LINKS_UNIVERSITY)) {
            homePage = PAGE_LINKS_UNIVERSITY;
        } else if (homePageString.equals(VALUE_BROWSER_HOME_PAGE_LINKS_WEATHER)) {
            homePage = PAGE_LINKS_WEATHER;
        } else {
            homePage = BROWSER_HOME_PAGE_DEFAULT;
        }

        return homePage;
    }

    public void setBrowserHomePage(int homePage) {
        String homePageString;

        switch (homePage) {
            case PAGE_LAST_PAGE:
                homePageString = VALUE_BROWSER_HOME_PAGE_LAST_PAGE;

                break;

            case PAGE_PORTAL_SCHEDULES:
                homePageString = VALUE_BROWSER_HOME_PAGE_PORTAL_SCHEDULES;

                break;

            case PAGE_PORTAL_INFORMATION:
                homePageString = VALUE_BROWSER_HOME_PAGE_PORTAL_INFORMATION;

                break;

            case PAGE_PORTAL_CANCELLED_LECTURES:
                homePageString = VALUE_BROWSER_HOME_PAGE_PORTAL_CANCELLED_LECTURES;

                break;

            case PAGE_PORTAL_CLASSROOM_CHANGES:
                homePageString = VALUE_BROWSER_HOME_PAGE_PORTAL_CLASSROOM_CHANGES;

                break;

            case PAGE_ACCESS_TRAINS_INBOUND:
                homePageString = VALUE_BROWSER_HOME_PAGE_ACCESS_TRAINS_INBOUND;

                break;

            case PAGE_ACCESS_TRAINS_OUTBOUND:
                homePageString = VALUE_BROWSER_HOME_PAGE_ACCESS_TRAINS_OUTBOUND;

                break;

            case PAGE_ACCESS_BUSES_INBOUND:
                homePageString = VALUE_BROWSER_HOME_PAGE_ACCESS_BUSES_INBOUND;

                break;

            case PAGE_ACCESS_BUSES_OUTBOUND:
                homePageString = VALUE_BROWSER_HOME_PAGE_ACCESS_BUSES_OUTBOUND;

                break;

            case PAGE_LINKS_UNIVERSITY:
                homePageString = VALUE_BROWSER_HOME_PAGE_LINKS_UNIVERSITY;

                break;

            case PAGE_LINKS_WEATHER:
                homePageString = VALUE_BROWSER_HOME_PAGE_LINKS_WEATHER;

                break;

            default:
                homePageString = null;

                break;
        }

        mSharedPreferences.edit().putString(KEY_BROWSER_HOME_PAGE, homePageString).commit();
    }

    public int getBrowserLastPage() {
        return mSharedPreferences.getInt(KEY_BROWSER_LAST_PAGE, BROWSER_LAST_PAGE_DEFAULT);
    }

    public void setBrowserLastPage(int lastPage) {
        mSharedPreferences.edit().putInt(KEY_BROWSER_LAST_PAGE, lastPage).commit();
    }

    public boolean getBrowserShowTips() {
        return mSharedPreferences.getBoolean(KEY_BROWSER_SHOW_TIPS, BROWSER_SHOW_TIPS_DEFAULT);
    }

    public void setBrowserShowTips(boolean show) {
        mSharedPreferences.edit().putBoolean(KEY_BROWSER_SHOW_TIPS, show).commit();
    }

    public String getSystemUserId() {
        return decrypt(mSharedPreferences.getString(KEY_SYSTEM_USER_ID, SYSTEM_USER_ID_DEFAULT));
    }

    public void setSystemUserId(String userId) {
        mSharedPreferences.edit().putString(KEY_SYSTEM_USER_ID, encrypt(userId)).commit();
    }

    public String getSystemPassword() {
        return decrypt(mSharedPreferences.getString(KEY_SYSTEM_PASSWORD, SYSTEM_PASSWORD_DEFAULT));
    }

    public void setSystemPassword(String password) {
        mSharedPreferences.edit().putString(KEY_SYSTEM_PASSWORD, encrypt(password)).commit();
    }

    public boolean getPortalFetchAtLaunch() {
        return mSharedPreferences.getBoolean(KEY_PORTAL_FETCH_AT_LAUNCH, PORTAL_FETCH_AT_LAUNCH_DEFAULT);
    }

    public void setPortalFetchAtLaunch(boolean fetch) {
        mSharedPreferences.edit().putBoolean(KEY_PORTAL_FETCH_AT_LAUNCH, fetch).commit();
    }

    public boolean getPortalFetchContentsAutomatically() {
        return mSharedPreferences.getBoolean(KEY_PORTAL_FETCH_CONTENTS_AUTOMATICALLY, PORTAL_FETCH_CONTENTS_AUTOMATICALLY_DEFAULT);
    }

    public void setPortalFetchContentsAutomatically(boolean fetch) {
        mSharedPreferences.edit().putBoolean(KEY_PORTAL_FETCH_CONTENTS_AUTOMATICALLY, fetch).commit();
    }

    public boolean getPortalShowDeletedContents() {
        return mSharedPreferences.getBoolean(KEY_PORTAL_SHOW_DELETED_CONTENTS, PORTAL_SHOW_DELETED_CONTENTS_DEFAULT);
    }

    public void setPortalShowDeletedContents(boolean show) {
        mSharedPreferences.edit().putBoolean(KEY_PORTAL_SHOW_DELETED_CONTENTS, show).commit();
    }

    public String getPortalSearchFilter() {
        return mSharedPreferences.getString(KEY_PORTAL_SEARCH_FILTER, PORTAL_SEARCH_FILTER_DEFAULT);
    }

    public void setPortalSearchFilter(String filter) {
        mSharedPreferences.edit().putString(KEY_PORTAL_SEARCH_FILTER, filter).commit();
    }

    public int getPortalReadTime() {
        return Integer.parseInt(mSharedPreferences.getString(KEY_PORTAL_READ_TIME, String.valueOf(PORTAL_READ_TIME_DEFAULT)));
    }

    public void setPortalReadTime(int time) {
        mSharedPreferences.edit().putString(KEY_PORTAL_READ_TIME, String.valueOf(time)).commit();
    }

    public boolean getWifiEnabled() {
        return mSharedPreferences.getBoolean(KEY_WIFI_ENABLED, WIFI_ENABLE_DEFAULT);
    }

    public void setWifiEnabled(boolean enabled) {
        mSharedPreferences.edit().putBoolean(KEY_WIFI_ENABLED, enabled).commit();
    }

    public String getWifiSsid() {
        return decrypt(mSharedPreferences.getString(KEY_WIFI_SSID, WIFI_SSID_DEFAULT));
    }

    public void setWifiSsid(String ssid) {
        mSharedPreferences.edit().putString(KEY_WIFI_SSID, encrypt(ssid)).commit();
    }

    public String getWifiKey() {
        return decrypt(mSharedPreferences.getString(KEY_WIFI_KEY, WIFI_KEY_DEFAULT));
    }

    public void setWifiKey(String key) {
        mSharedPreferences.edit().putString(KEY_WIFI_KEY, encrypt(key)).commit();
    }

    public String getWifiAuthenticationUrl() {
        return decrypt(mSharedPreferences.getString(KEY_WIFI_AUTHENTICATION_URL, WIFI_AUTHENTICATION_URL_DEFAULT));
    }

    public void setWifiAuthenticationUrl(String url) {
        mSharedPreferences.edit().putString(KEY_WIFI_AUTHENTICATION_URL, encrypt(url)).commit();
    }

    public String getWifiAuthenticationBody() {
        return decrypt(mSharedPreferences.getString(KEY_WIFI_AUTHENTICATION_BODY, WIFI_AUTHENTICATION_BODY_DEFAULT));
    }

    public void setWifiAuthenticationBody(String body) {
        mSharedPreferences.edit().putString(KEY_WIFI_AUTHENTICATION_BODY, encrypt(body)).commit();
    }

    public void resetWifi() {
        mSharedPreferences.edit()
                .putString(KEY_WIFI_SSID, WIFI_SSID_DEFAULT)
                .putString(KEY_WIFI_KEY, WIFI_KEY_DEFAULT)
                .putString(KEY_WIFI_AUTHENTICATION_URL, WIFI_AUTHENTICATION_URL_DEFAULT)
                .putString(KEY_WIFI_AUTHENTICATION_BODY, WIFI_AUTHENTICATION_BODY_DEFAULT)
                .commit();
    }
}
