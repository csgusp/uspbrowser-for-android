/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.provider.access;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * 時刻表のデータベースのための各種定義をするクラス。
 *
 * @author leak4mk0
 * @version 27
 * @since 27
 */
public class AccessContract {
    public static final String CONTENT_AUTHORITY = "jp.gr.java_conf.csgusp.uspbrowser.provider.access";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    private static final String PATH_TRAINS = "trains";
    private static final String PATH_BUSES = "buses";

    private interface TimeColumns {
        public static final String DIRECTION = "direction";
        public static final String WEEKDAY = "weekday";
        public static final String TIME = "time";

        public static final int DIRECTION_INBOUND = 1;
        public static final int DIRECTION_OUTBOUND = 2;

        public static final int WEEKDAY_WORKDAY = 1;
        public static final int WEEKDAY_HOLIDAY = 2;
    }

    public static class Trains implements TimeColumns, BaseColumns {
        public static final String TABLE = "trains";
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_TRAINS).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.uspbrowser.access.trains";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.uspbrowser.access.trains";
        public static final String DEFAULT_SORT = TIME + " ASC";
    }

    public static class Buses implements TimeColumns, BaseColumns {
        public static final String TABLE = "buses";
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_BUSES).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.uspbrowser.access.buses";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.uspbrowser.access.buses";
        public static final String DEFAULT_SORT = TIME + " ASC";
    }
}
