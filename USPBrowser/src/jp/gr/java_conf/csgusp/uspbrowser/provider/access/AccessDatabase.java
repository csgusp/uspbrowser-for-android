/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.provider.access;

import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 時刻表のデータベースにアクセスするクラス。
 *
 * @author leak4mk0
 * @version 29
 * @since 27
 */
public class AccessDatabase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "access.db";
    private static final int DATABASE_VERSION = 1;

    /**
     * ポータルのデータベースにアクセスするクラスを生成します。
     *
     * @param context データベースを開くのに用いる
     */
    public AccessDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + AccessContract.Trains.TABLE + " ("
                + AccessContract.Trains._ID + " INTEGER PRIMARY KEY, "
                + AccessContract.Trains.DIRECTION + " INTEGER, "
                + AccessContract.Trains.WEEKDAY + " TEXT, "
                + AccessContract.Trains.TIME + " INTEGER)");

        db.execSQL("CREATE TABLE " + AccessContract.Buses.TABLE + " ("
                + AccessContract.Buses._ID + " INTEGER PRIMARY KEY, "
                + AccessContract.Buses.DIRECTION + " INTEGER, "
                + AccessContract.Trains.WEEKDAY + " TEXT, "
                + AccessContract.Trains.TIME + " INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        int version;

        version = oldVersion;
        if (version != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + AccessContract.Trains.TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + AccessContract.Buses.TABLE);

            onCreate(db);
        }
    }
}
