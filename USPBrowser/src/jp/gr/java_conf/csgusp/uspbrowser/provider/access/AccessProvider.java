/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.provider.access;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 時刻表のコンテンツプロバイダーを実装したクラスです。
 *
 * @author leak4mk0
 * @version 27
 * @since 27
 */
public class AccessProvider extends ContentProvider {
    private static final int TRAINS = 100;
    private static final int TRAIN_ID = 101;
    private static final int BUSES = 200;
    private static final int BUS_ID = 201;
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private static final HashMap<String, String> sTrainsProjectionMap = buildTrainsProjectionMap();
    private static final HashMap<String, String> sBusesProjectionMap = buildBusesProjectionMap();
    private AccessDatabase mAccessDatabase;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(AccessContract.CONTENT_AUTHORITY, AccessContract.Trains.TABLE, TRAINS);
        matcher.addURI(AccessContract.CONTENT_AUTHORITY, AccessContract.Trains.TABLE + "/#", TRAIN_ID);
        matcher.addURI(AccessContract.CONTENT_AUTHORITY, AccessContract.Buses.TABLE, BUSES);
        matcher.addURI(AccessContract.CONTENT_AUTHORITY, AccessContract.Buses.TABLE + "/#", BUS_ID);

        return matcher;
    }

    private static HashMap<String, String> buildTrainsProjectionMap() {
        final HashMap<String, String> map = new HashMap<String, String>();

        map.put(AccessContract.Trains._ID, AccessContract.Trains._ID);
        map.put(AccessContract.Trains.DIRECTION, AccessContract.Trains.DIRECTION);
        map.put(AccessContract.Trains.WEEKDAY, AccessContract.Trains.WEEKDAY);
        map.put(AccessContract.Trains.TIME, AccessContract.Trains.TIME);

        return map;
    }

    private static HashMap<String, String> buildBusesProjectionMap() {
        final HashMap<String, String> map = new HashMap<String, String>();

        map.put(AccessContract.Buses._ID, AccessContract.Buses._ID);
        map.put(AccessContract.Buses.DIRECTION, AccessContract.Buses.DIRECTION);
        map.put(AccessContract.Trains.WEEKDAY, AccessContract.Trains.WEEKDAY);
        map.put(AccessContract.Trains.TIME, AccessContract.Trains.TIME);

        return map;
    }
    @Override
    public boolean onCreate() {
        mAccessDatabase = new AccessDatabase(getContext());

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder builder;
        String order;
        SQLiteDatabase db;
        Cursor cursor;
        Context context;

        builder = new SQLiteQueryBuilder();

        switch (sUriMatcher.match(uri)) {
            case TRAINS:
                builder.setTables(AccessContract.Trains.TABLE);
                builder.setProjectionMap(sTrainsProjectionMap);
                order = AccessContract.Trains.DEFAULT_SORT;

                break;

            case TRAIN_ID:
                builder.setTables(AccessContract.Trains.TABLE);
                builder.setProjectionMap(sTrainsProjectionMap);
                builder.appendWhere(AccessContract.Trains._ID + " = " + uri.getLastPathSegment());
                order = AccessContract.Trains.DEFAULT_SORT;

                break;

            case BUSES:
                builder.setTables(AccessContract.Buses.TABLE);
                builder.setProjectionMap(sBusesProjectionMap);
                order = AccessContract.Buses.DEFAULT_SORT;

                break;

            case BUS_ID:
                builder.setTables(AccessContract.Buses.TABLE);
                builder.setProjectionMap(sBusesProjectionMap);
                builder.appendWhere(AccessContract.Buses._ID + " = " + uri.getLastPathSegment());
                order = AccessContract.Buses.DEFAULT_SORT;

                break;

            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        if (!TextUtils.isEmpty(sortOrder)) {
            order = sortOrder;
        }

        db = mAccessDatabase.getReadableDatabase();
        assert db != null;
        cursor = builder.query(db, projection, selection, selectionArgs, null, null, order);
        context = getContext();
        assert cursor != null;
        assert context != null;
        cursor.setNotificationUri(context.getContentResolver(), uri);

        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String table;
        SQLiteDatabase db;
        long rowId;
        Uri rowUri;
        Context context;

        switch (sUriMatcher.match(uri)) {
            case TRAINS:
                table = AccessContract.Trains.TABLE;

                break;

            case BUSES:
                table = AccessContract.Buses.TABLE;

                break;

            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        db = mAccessDatabase.getWritableDatabase();
        assert db != null;
        rowId = db.insertOrThrow(table, null, values);
        rowUri = ContentUris.withAppendedId(uri, rowId);
        context = getContext();
        assert context != null;
        context.getContentResolver().notifyChange(rowUri, null);

        return rowUri;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String table;
        String where;
        SQLiteDatabase db;
        int count;
        Context context;

        switch (sUriMatcher.match(uri)) {
            case TRAINS:
                table = AccessContract.Trains.TABLE;
                where = null;

                break;

            case TRAIN_ID:
                table = AccessContract.Trains.TABLE;
                where = AccessContract.Trains._ID + " = " + uri.getLastPathSegment();

                break;

            case BUSES:
                table = AccessContract.Buses.TABLE;
                where = null;

                break;

            case BUS_ID:
                table = AccessContract.Buses.TABLE;
                where = AccessContract.Buses._ID + " = " + uri.getLastPathSegment();

                break;

            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        if (!TextUtils.isEmpty(selection)) {
            if (!TextUtils.isEmpty(where)) {
                where += " AND ";
            } else {
                where = "";
            }
            where += selection;
        }

        db = mAccessDatabase.getWritableDatabase();
        assert db != null;
        count = db.update(table, values, where, selectionArgs);
        context = getContext();
        assert context != null;
        context.getContentResolver().notifyChange(uri, null);

        return count;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String table;
        String where;
        SQLiteDatabase db;
        int count;
        Context context;

        switch (sUriMatcher.match(uri)) {
            case TRAINS:
                table = AccessContract.Trains.TABLE;
                where = null;

                break;

            case TRAIN_ID:
                table = AccessContract.Trains.TABLE;
                where = AccessContract.Trains._ID + " = " + uri.getLastPathSegment();

                break;

            case BUSES:
                table = AccessContract.Buses.TABLE;
                where = null;

                break;

            case BUS_ID:
                table = AccessContract.Buses.TABLE;
                where = AccessContract.Buses._ID + " = " + uri.getLastPathSegment();

                break;

            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        if (!TextUtils.isEmpty(selection)) {
            if (!TextUtils.isEmpty(where)) {
                where += " AND ";
            } else {
                where = "";
            }
            where += selection;
        }

        db = mAccessDatabase.getWritableDatabase();
        assert db != null;
        count = db.delete(table, where, selectionArgs);
        context = getContext();
        assert context != null;
        context.getContentResolver().notifyChange(uri, null);

        return count;
    }

    @Override
    public String getType(Uri uri) {
        String type;

        switch (sUriMatcher.match(uri)) {
            case TRAINS:
                type = AccessContract.Trains.CONTENT_TYPE;

                break;

            case TRAIN_ID:
                type = AccessContract.Trains.CONTENT_ITEM_TYPE;

                break;

            case BUSES:
                type = AccessContract.Buses.CONTENT_TYPE;

                break;

            case BUS_ID:
                type = AccessContract.Buses.CONTENT_ITEM_TYPE;

                break;

            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        return type;
    }

    @Override
    public ContentProviderResult[] applyBatch (ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        SQLiteDatabase db;
        ContentProviderResult[] results;

        db = mAccessDatabase.getWritableDatabase();
        assert db != null;
        db.beginTransaction();
        try {
            results = super.applyBatch(operations);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        return results;
    }
}
