/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.provider.portal;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * ポータルのデータベースのための各種定義をするクラス。
 *
 * @author leak4mk0
 * @version 23
 * @since 1
 */
public class PortalContract {

    public static final String CONTENT_AUTHORITY = "jp.gr.java_conf.csgusp.uspbrowser.provider.portal";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    private static final String PATH_SCHEDULES = "schedules";
    private static final String PATH_INFORMATION = "information";
    private static final String PATH_INFORMATION_ATTACHMENTS = "information_attachments";
    private static final String PATH_CANCELLED_LECTURES = "cancelled_lectures";
    private static final String PATH_CLASSROOM_CHANGES = "classroom_changes";

    private interface SchedulesColumns {
        public static final String STUDENT_ID = "student_id";
        public static final String SCHEDULE_ID = "schedule_id";
        public static final String TITLE = "title";
        public static final String START_DATE = "start_date";
        public static final String END_DATE = "end_date";
        public static final String START_TIME = "start_time";
        public static final String END_TIME = "end_time";
        public static final String PLACE = "place";
        public static final String PRIORITY = "priority";
        public static final String CONTENT = "content";
        public static final String IS_DELETED = "is_deleted";
        public static final String IS_FETCHED = "is_fetched";
    }

    private interface InformationColumns {
        public static final String INFORMATION_ID = "information_id";
        public static final String TITLE = "title";
        public static final String PUBLIC_START_DATE = "public_start_date";
        public static final String PUBLIC_END_DATE = "public_end_date";
        public static final String PUBLIC_PERSON = "public_person";
        public static final String PRIORITY = "priority";
        public static final String CATEGORY = "category";
        public static final String IS_READ = "is_read";
        public static final String IS_ATTACHED = "is_attached";
        public static final String CONTENT = "content";
        public static final String IS_DELETED = "is_deleted";
        public static final String IS_FETCHED = "is_fetched";
    }

    private interface InformationAttachmentsColumns {
        public static final String INFORMATION_ID = "information_id";
        public static final String ATTACHMENT_ID = "attachment_id";
        public static final String URL = "url";
        public static final String FILENAME = "filename";
        public static final String DESCRIPTION = "description";
    }

    private interface CancelledLecturesColumns {
        public static final String CANCELLED_LECTURE_ID = "cancelled_lecture_id";
        public static final String MAKEUP_LECTURE_ID = "makeup_lecture_id";
        public static final String SUBJECT = "subject";
        public static final String PROFESSOR = "professor";
        public static final String CANCELLED_LECTURE_DATE = "cancelled_lecture_date";
        public static final String CANCELLED_LECTURE_PERIOD = "cancelled_lecture_period";
        public static final String MAKEUP_LECTURE_DATE = "makeup_lecture_date";
        public static final String MAKEUP_LECTURE_PERIOD = "makeup_lecture_period";
        public static final String MAKEUP_LECTURE_CLASSROOM = "makeup_lecture_classroom";
        public static final String NOTE_1 = "note_1";
        public static final String NOTE_2 = "note_2";
        public static final String IS_TAKEN = "is_taken";
        public static final String IS_DELETED = "is_deleted";
    }

    protected interface ClassroomChangesColumns {
        public static final String LECTURE_ID = "lecture_id";
        public static final String SUBJECT = "subject";
        public static final String PROFESSOR = "professor";
        public static final String LECTURE_DATE = "lecture_date";
        public static final String LECTURE_PERIOD = "lecture_period";
        public static final String OLD_CLASSROOM = "old_classroom";
        public static final String NEW_CLASSROOM = "new_classroom";
        public static final String NOTE_1 = "note_1";
        public static final String NOTE_2 = "note_2";
        public static final String IS_TAKEN = "is_taken";
        public static final String IS_DELETED = "is_deleted";
    }

    public static class Schedules implements SchedulesColumns, BaseColumns {
        public static final String TABLE = "schedules";
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_SCHEDULES).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.uspbrowser.portal.schedule";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.uspbrowser.portal.schedule";
        public static final String DEFAULT_SORT = START_DATE + " ASC, " + END_DATE + " ASC";
        public static final long TIME_NOT_SPECIFIED = 24 * 60 * 60 * 1000 + 1;

        public static String getId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    public static class Information implements InformationColumns, BaseColumns {
        public static final String TABLE = "information";
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_INFORMATION).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.uspbrowser.portal.information";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.uspbrowser.portal.information";
        public static final String DEFAULT_SORT = INFORMATION_ID + " DESC";
        public static final long DATE_NOT_SPECIFIED = 1;

        public static Uri buildUri(String id) {
            return CONTENT_URI.buildUpon().appendPath(id).build();
        }

        public static String getId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    public static class InformationAttachments implements InformationAttachmentsColumns, BaseColumns {
        public static final String TABLE = "information_attachments";
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_INFORMATION_ATTACHMENTS).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.uspbrowser.portal.information";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.uspbrowser.portal.information";
        public static final String DEFAULT_SORT = _ID + " ASC";

        public static Uri buildUri(String id) {
            return CONTENT_URI.buildUpon().appendPath(id).build();
        }

        public static String getId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    public static class CancelledLectures implements CancelledLecturesColumns, BaseColumns {
        public static final String TABLE = "cancelled_lectures";
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CANCELLED_LECTURES).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.uspbrowser.portal.cancelled_lecture";
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/vnd.uspbrowser.portal.cancelled_lecture";
        public static final String DEFAULT_SORT = IS_TAKEN + " DESC, "
                + CANCELLED_LECTURE_DATE + " DESC, " + CANCELLED_LECTURE_PERIOD + " DESC";

        public static Uri buildUri(String id) {
            return CONTENT_URI.buildUpon().appendPath(id).build();
        }

        public static String getId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }

    public static class ClassroomChanges implements ClassroomChangesColumns, BaseColumns {
        public static final String TABLE = "classroom_changes";
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_CLASSROOM_CHANGES).build();
        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.uspbrowser.portal.classroom_changes";
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/vnd.uspbrowser.portal.classroom_changes";
        public static final String DEFAULT_SORT = IS_TAKEN + " DESC, "
                + LECTURE_DATE + " DESC, " + LECTURE_PERIOD + " DESC";

        public static Uri buildUri(String id) {
            return CONTENT_URI.buildUpon().appendPath(id).build();
        }

        public static String getId(Uri uri) {
            return uri.getPathSegments().get(1);
        }
    }
}
