/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.provider.portal;

import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.CancelledLectures;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.ClassroomChanges;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.Information;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.InformationAttachments;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.Schedules;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * ポータルのデータベースにアクセスするクラス。
 *
 * @author leak4mk0
 * @version 16
 * @since 13
 */
public class PortalDatabase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "portal.db";
    private static final int DATABASE_VERSION = 20;

    /**
     * ポータルのデータベースにアクセスするクラスを生成します。
     *
     * @param context データベースを開くのに用いる
     */
    public PortalDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Schedules.TABLE + " ("
                + Schedules._ID + " INTEGER PRIMARY KEY, "
                + Schedules.STUDENT_ID + " INTEGER, "
                + Schedules.SCHEDULE_ID + " INTEGER, "
                + Schedules.TITLE + " TEXT, "
                + Schedules.START_DATE + " INTEGER, "
                + Schedules.END_DATE + " INTEGER, "
                + Schedules.START_TIME + " INTEGER, "
                + Schedules.END_TIME + " INTEGER, "
                + Schedules.PLACE + " TEXT, "
                + Schedules.PRIORITY + " INTEGER, "
                + Schedules.CONTENT + " TEXT, "
                + Schedules.IS_DELETED + " INTEGER, "
                + Schedules.IS_FETCHED + " INTEGER, "
                + "UNIQUE (" + Schedules.STUDENT_ID + ", " + Schedules.SCHEDULE_ID + ") ON CONFLICT IGNORE)");

        db.execSQL("CREATE TABLE " + Information.TABLE + " ("
                + Information._ID + " INTEGER PRIMARY KEY, "
                + Information.INFORMATION_ID + " INTEGER, "
                + Information.TITLE + " TEXT, "
                + Information.PUBLIC_START_DATE + " INTEGER, "
                + Information.PUBLIC_END_DATE + " INTEGER, "
                + Information.PUBLIC_PERSON + " TEXT, "
                + Information.PRIORITY + " INTEGER, "
                + Information.CATEGORY + " INTEGER, "
                + Information.IS_READ + " INTEGER, "
                + Information.IS_ATTACHED + " INTEGER, "
                + Information.CONTENT + " TEXT, "
                + Information.IS_DELETED + " INTEGER, "
                + Information.IS_FETCHED + " INTEGER, "
                + "UNIQUE (" + Information.INFORMATION_ID + ") ON CONFLICT IGNORE)");

        db.execSQL("CREATE TABLE " + InformationAttachments.TABLE + " ("
                + InformationAttachments._ID + " INTEGER PRIMARY KEY, "
                + InformationAttachments.INFORMATION_ID + " INTEGER, "
                + InformationAttachments.ATTACHMENT_ID + " INTEGER, "
                + InformationAttachments.URL + " TEXT, "
                + InformationAttachments.FILENAME + " TEXT, "
                + InformationAttachments.DESCRIPTION + " TEXT, "
                + "UNIQUE (" + InformationAttachments.INFORMATION_ID + ", "
                + InformationAttachments.ATTACHMENT_ID + ") ON CONFLICT IGNORE)");

        db.execSQL("CREATE TABLE " + CancelledLectures.TABLE + " ("
                + CancelledLectures._ID + " INTEGER PRIMARY KEY, "
                + CancelledLectures.CANCELLED_LECTURE_ID + " INTEGER, "
                + CancelledLectures.MAKEUP_LECTURE_ID + " INTEGER, "
                + CancelledLectures.SUBJECT + " TEXT, "
                + CancelledLectures.PROFESSOR + " TEXT, "
                + CancelledLectures.CANCELLED_LECTURE_DATE + " INTEGER, "
                + CancelledLectures.CANCELLED_LECTURE_PERIOD + " INTEGER, "
                + CancelledLectures.MAKEUP_LECTURE_DATE + " INTEGER, "
                + CancelledLectures.MAKEUP_LECTURE_PERIOD + " INTEGER, "
                + CancelledLectures.MAKEUP_LECTURE_CLASSROOM + " TEXT, "
                + CancelledLectures.NOTE_1 + " TEXT, "
                + CancelledLectures.NOTE_2 + " TEXT, "
                + CancelledLectures.IS_TAKEN + " INTEGER, "
                + CancelledLectures.IS_DELETED + " INTEGER, "
                + "UNIQUE (" + CancelledLectures.CANCELLED_LECTURE_ID + ", "
                + CancelledLectures.MAKEUP_LECTURE_ID + ") ON CONFLICT IGNORE)");

        db.execSQL("CREATE TABLE " + ClassroomChanges.TABLE + " ("
                + ClassroomChanges._ID + " INTEGER PRIMARY KEY, "
                + ClassroomChanges.LECTURE_ID + " INTEGER, "
                + ClassroomChanges.SUBJECT + " TEXT, "
                + ClassroomChanges.PROFESSOR + " TEXT, "
                + ClassroomChanges.LECTURE_DATE + " INTEGER, "
                + ClassroomChanges.LECTURE_PERIOD + " INTEGER, "
                + ClassroomChanges.OLD_CLASSROOM + " TEXT, "
                + ClassroomChanges.NEW_CLASSROOM + " TEXT, "
                + ClassroomChanges.NOTE_1 + " TEXT, "
                + ClassroomChanges.NOTE_2 + " TEXT, "
                + ClassroomChanges.IS_TAKEN + " INTEGER, "
                + ClassroomChanges.IS_DELETED + " INTEGER, "
                + "UNIQUE (" + ClassroomChanges.LECTURE_ID + ") ON CONFLICT IGNORE)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        int version;

        version = oldVersion;
        if (version == 18) {
            db.execSQL("ALTER TABLE schedules RENAME TO schedules_old");
            db.execSQL("ALTER TABLE information RENAME TO information_old");
            db.execSQL("ALTER TABLE information_attachments RENAME TO information_attachments_old");
            db.execSQL("ALTER TABLE cancelled_lectures RENAME TO cancelled_lectures_old");
            db.execSQL("ALTER TABLE classroom_changes RENAME TO classroom_changes_old");

            onCreate(db);

            db.execSQL("UPDATE schedules_old SET begin_time = 86400001 WHERE is_all_day > 0");
            db.execSQL("UPDATE schedules_old SET end_time = 86400001 WHERE is_all_day > 0");
            db.execSQL("UPDATE information_old SET public_end_date = 1 WHERE is_public_date_infinite > 0");

            db.execSQL("INSERT INTO " + Schedules.TABLE + " SELECT "
                    + "_id, student_id, _id, title, begin_date, end_date, begin_time, end_time, "
                    + "place, priority, content, is_deleted, is_fetched "
                    + "FROM schedules_old");
            db.execSQL("INSERT INTO " + Information.TABLE + " SELECT "
                    + "_id, _id, title, public_begin_date, public_end_date, public_person, "
                    + "priority, category, is_read_local, is_attachment, content, is_deleted, is_fetched "
                    + "FROM information_old");
            db.execSQL("INSERT INTO " + InformationAttachments.TABLE + " SELECT "
                    + "_id, information_id, _id, url, filename, description "
                    + "FROM information_attachments_old");
            db.execSQL("INSERT INTO " + CancelledLectures.TABLE + " SELECT "
                    + "_id, cancelled_lecture_code, makeup_lecture_code, subject, professor, "
                    + "cancelled_lecture_date, cancelled_lecture_period, "
                    + "makeup_lecture_date, makeup_lecture_period, makeup_lecture_classroom, "
                    + "note_1, note_2, is_taken, is_deleted "
                    + "FROM cancelled_lectures_old "
                    + "WHERE cancelled_lecture_date < 1388502000000 "
                    + "AND makeup_lecture_date < 1388502000000");
            db.execSQL("INSERT INTO " + ClassroomChanges.TABLE + " SELECT "
                    + "_id, lecture_code, subject, professor, date, period, "
                    + "old_classroom, new_classroom, note_1, note_2, is_taken, is_deleted "
                    + "FROM classroom_changes_old WHERE date < 1388502000000");

            db.execSQL("DROP TABLE schedules_old");
            db.execSQL("DROP TABLE information_old");
            db.execSQL("DROP TABLE information_attachments_old");
            db.execSQL("DROP TABLE cancelled_lectures_old");
            db.execSQL("DROP TABLE classroom_changes_old");

            version = 20;
        }
        if (version == 19) {
            db.execSQL("ALTER TABLE schedules RENAME TO schedules_old");
            db.execSQL("ALTER TABLE information RENAME TO information_old");
            db.execSQL("ALTER TABLE information_attachments RENAME TO information_attachments_old");
            db.execSQL("DROP TABLE cancelled_lectures");
            db.execSQL("DROP TABLE classroom_changes");

            onCreate(db);

            db.execSQL("DROP TABLE schedules");
            db.execSQL("DROP TABLE information");
            db.execSQL("DROP TABLE information_attachments");
            db.execSQL("ALTER TABLE schedules_old RENAME TO schedules");
            db.execSQL("ALTER TABLE information_old RENAME TO information");
            db.execSQL("ALTER TABLE information_attachments_old RENAME TO information_attachments");

            version = 20;
        }
        if (version != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + Schedules.TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + Information.TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + InformationAttachments.TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + CancelledLectures.TABLE);
            db.execSQL("DROP TABLE IF EXISTS " + ClassroomChanges.TABLE);

            onCreate(db);
        }
    }
}
