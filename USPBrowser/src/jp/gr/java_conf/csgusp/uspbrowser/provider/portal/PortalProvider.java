/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.provider.portal;

import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.CancelledLectures;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.ClassroomChanges;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.Information;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.InformationAttachments;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.Schedules;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * ポータルのコンテンツプロバイダーを実装したクラスです。
 *
 * @author leak4mk0
 * @version 13
 * @since 1
 */
public class PortalProvider extends ContentProvider {
    private static final int SCHEDULES = 100;
    private static final int SCHEDULE_ID = 101;
    private static final int INFORMATION = 200;
    private static final int INFORMATION_ID = 201;
    private static final int INFORMATION_ATTACHMENTS = 300;
    private static final int INFORMATION_ATTACHMENT_ID = 301;
    private static final int CANCELLED_LECTURES = 400;
    private static final int CANCELLED_LECTURE_ID = 401;
    private static final int CLASSROOM_CHANGES = 500;
    private static final int CLASSROOM_CHANGE_ID = 501;
    private static final UriMatcher sUriMatcher = buildUriMatcher();
    private static final HashMap<String, String>
            sSchedulesProjectionMap = buildSchedulesProjectionMap();
    private static final HashMap<String, String>
            sInformationProjectionMap = buildInformationProjectionMap();
    private static final HashMap<String, String>
            sInformationAttachmentsProjectionMap = buildInformationAttachmentsProjectionMap();
    private static final HashMap<String, String>
            sCancelledLecturesProjectionMap = buildCancelledLecturesProjectionMap();
    private static final HashMap<String, String>
            sClassroomChangesProjectionMap = buildClassroomChangesProjectionMap();
    private PortalDatabase mPortalDatabase;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);

        matcher.addURI(PortalContract.CONTENT_AUTHORITY, "schedules", SCHEDULES);
        matcher.addURI(PortalContract.CONTENT_AUTHORITY, "schedules/#", SCHEDULE_ID);
        matcher.addURI(PortalContract.CONTENT_AUTHORITY, "information", INFORMATION);
        matcher.addURI(PortalContract.CONTENT_AUTHORITY, "information/#", INFORMATION_ID);
        matcher.addURI(PortalContract.CONTENT_AUTHORITY, "information_attachments", INFORMATION_ATTACHMENTS);
        matcher.addURI(PortalContract.CONTENT_AUTHORITY, "information_attachments/#", INFORMATION_ATTACHMENT_ID);
        matcher.addURI(PortalContract.CONTENT_AUTHORITY, "cancelled_lectures", CANCELLED_LECTURES);
        matcher.addURI(PortalContract.CONTENT_AUTHORITY, "cancelled_lectures/#", CANCELLED_LECTURE_ID);
        matcher.addURI(PortalContract.CONTENT_AUTHORITY, "classroom_changes", CLASSROOM_CHANGES);
        matcher.addURI(PortalContract.CONTENT_AUTHORITY, "classroom_changes/#", CLASSROOM_CHANGE_ID);

        return matcher;
    }

    private static HashMap<String, String> buildSchedulesProjectionMap() {
        final HashMap<String, String> map = new HashMap<String, String>();

        map.put(Schedules._ID, Schedules._ID);
        map.put(Schedules.STUDENT_ID, Schedules.STUDENT_ID);
        map.put(Schedules.SCHEDULE_ID, Schedules.SCHEDULE_ID);
        map.put(Schedules.TITLE, Schedules.TITLE);
        map.put(Schedules.START_DATE, Schedules.START_DATE);
        map.put(Schedules.END_DATE, Schedules.END_DATE);
        map.put(Schedules.START_TIME, Schedules.START_TIME);
        map.put(Schedules.END_TIME, Schedules.END_TIME);
        map.put(Schedules.PLACE, Schedules.PLACE);
        map.put(Schedules.PRIORITY, Schedules.PRIORITY);
        map.put(Schedules.CONTENT, Schedules.CONTENT);
        map.put(Schedules.IS_DELETED, Schedules.IS_DELETED);
        map.put(Schedules.IS_FETCHED, Schedules.IS_FETCHED);

        return map;
    }

    private static HashMap<String, String> buildInformationProjectionMap() {
        final HashMap<String, String> map = new HashMap<String, String>();

        map.put(Information._ID, Information._ID);
        map.put(Information.INFORMATION_ID, Information.INFORMATION_ID);
        map.put(Information.TITLE, Information.TITLE);
        map.put(Information.PUBLIC_START_DATE, Information.PUBLIC_START_DATE);
        map.put(Information.PUBLIC_END_DATE, Information.PUBLIC_END_DATE);
        map.put(Information.PUBLIC_PERSON, Information.PUBLIC_PERSON);
        map.put(Information.PRIORITY, Information.PRIORITY);
        map.put(Information.CATEGORY, Information.CATEGORY);
        map.put(Information.IS_READ, Information.IS_READ);
        map.put(Information.IS_ATTACHED, Information.IS_ATTACHED);
        map.put(Information.CONTENT, Information.CONTENT);
        map.put(Information.IS_DELETED, Information.IS_DELETED);
        map.put(Information.IS_FETCHED, Information.IS_FETCHED);

        return map;
    }

    private static HashMap<String, String> buildInformationAttachmentsProjectionMap() {
        final HashMap<String, String> map = new HashMap<String, String>();

        map.put(InformationAttachments._ID, InformationAttachments._ID);
        map.put(InformationAttachments.INFORMATION_ID, InformationAttachments.INFORMATION_ID);
        map.put(InformationAttachments.ATTACHMENT_ID, InformationAttachments.ATTACHMENT_ID);
        map.put(InformationAttachments.URL, InformationAttachments.URL);
        map.put(InformationAttachments.FILENAME, InformationAttachments.FILENAME);
        map.put(InformationAttachments.DESCRIPTION, InformationAttachments.DESCRIPTION);

        return map;
    }

    private static HashMap<String, String> buildCancelledLecturesProjectionMap() {
        final HashMap<String, String> map = new HashMap<String, String>();

        map.put(CancelledLectures._ID, CancelledLectures._ID);
        map.put(CancelledLectures.CANCELLED_LECTURE_ID, CancelledLectures.CANCELLED_LECTURE_ID);
        map.put(CancelledLectures.MAKEUP_LECTURE_ID, CancelledLectures.MAKEUP_LECTURE_ID);
        map.put(CancelledLectures.SUBJECT, CancelledLectures.SUBJECT);
        map.put(CancelledLectures.PROFESSOR, CancelledLectures.PROFESSOR);
        map.put(CancelledLectures.IS_TAKEN, CancelledLectures.IS_TAKEN);
        map.put(CancelledLectures.CANCELLED_LECTURE_DATE, CancelledLectures.CANCELLED_LECTURE_DATE);
        map.put(CancelledLectures.CANCELLED_LECTURE_PERIOD, CancelledLectures.CANCELLED_LECTURE_PERIOD);
        map.put(CancelledLectures.MAKEUP_LECTURE_DATE, CancelledLectures.MAKEUP_LECTURE_DATE);
        map.put(CancelledLectures.MAKEUP_LECTURE_PERIOD, CancelledLectures.MAKEUP_LECTURE_PERIOD);
        map.put(CancelledLectures.MAKEUP_LECTURE_CLASSROOM, CancelledLectures.MAKEUP_LECTURE_CLASSROOM);
        map.put(CancelledLectures.NOTE_1, CancelledLectures.NOTE_1);
        map.put(CancelledLectures.NOTE_2, CancelledLectures.NOTE_2);
        map.put(CancelledLectures.IS_DELETED, CancelledLectures.IS_DELETED);

        return map;
    }

    private static HashMap<String, String> buildClassroomChangesProjectionMap() {
        final HashMap<String, String> map = new HashMap<String, String>();

        map.put(ClassroomChanges._ID, ClassroomChanges._ID);
        map.put(ClassroomChanges.LECTURE_ID, ClassroomChanges.LECTURE_ID);
        map.put(ClassroomChanges.SUBJECT, ClassroomChanges.SUBJECT);
        map.put(ClassroomChanges.PROFESSOR, ClassroomChanges.PROFESSOR);
        map.put(ClassroomChanges.IS_TAKEN, ClassroomChanges.IS_TAKEN);
        map.put(ClassroomChanges.LECTURE_DATE, ClassroomChanges.LECTURE_DATE);
        map.put(ClassroomChanges.LECTURE_PERIOD, ClassroomChanges.LECTURE_PERIOD);
        map.put(ClassroomChanges.OLD_CLASSROOM, ClassroomChanges.OLD_CLASSROOM);
        map.put(ClassroomChanges.NEW_CLASSROOM, ClassroomChanges.NEW_CLASSROOM);
        map.put(ClassroomChanges.NOTE_1, ClassroomChanges.NOTE_1);
        map.put(ClassroomChanges.NOTE_2, ClassroomChanges.NOTE_2);
        map.put(ClassroomChanges.IS_DELETED, ClassroomChanges.IS_DELETED);

        return map;
    }

    @Override
    public boolean onCreate() {
        mPortalDatabase = new PortalDatabase(getContext());

        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder builder;
        String order;
        SQLiteDatabase db;
        Cursor cursor;

        builder = new SQLiteQueryBuilder();

        switch (sUriMatcher.match(uri)) {
            case SCHEDULES:
                builder.setTables(Schedules.TABLE);
                builder.setProjectionMap(sSchedulesProjectionMap);
                order = Schedules.DEFAULT_SORT;

                break;

            case SCHEDULE_ID:
                builder.setTables(Schedules.TABLE);
                builder.setProjectionMap(sSchedulesProjectionMap);
                builder.appendWhere(Schedules._ID + " = " + Schedules.getId(uri));
                order = Schedules.DEFAULT_SORT;

                break;

            case INFORMATION:
                builder.setTables(Information.TABLE);
                builder.setProjectionMap(sInformationProjectionMap);
                order = Information.DEFAULT_SORT;

                break;

            case INFORMATION_ID:
                builder.setTables(Information.TABLE);
                builder.setProjectionMap(sInformationProjectionMap);
                builder.appendWhere(Information._ID + " = " + Information.getId(uri));
                order = Information.DEFAULT_SORT;

                break;

            case INFORMATION_ATTACHMENTS:
                builder.setTables(InformationAttachments.TABLE);
                builder.setProjectionMap(sInformationAttachmentsProjectionMap);
                order = InformationAttachments.DEFAULT_SORT;

                break;

            case INFORMATION_ATTACHMENT_ID:
                builder.setTables(InformationAttachments.TABLE);
                builder.setProjectionMap(sInformationAttachmentsProjectionMap);
                builder.appendWhere(InformationAttachments._ID + " = " + InformationAttachments.getId(uri));
                order = InformationAttachments.DEFAULT_SORT;

                break;

            case CANCELLED_LECTURES:
                builder.setTables(CancelledLectures.TABLE);
                builder.setProjectionMap(sCancelledLecturesProjectionMap);
                order = CancelledLectures.DEFAULT_SORT;

                break;

            case CANCELLED_LECTURE_ID:
                builder.setTables(CancelledLectures.TABLE);
                builder.setProjectionMap(sCancelledLecturesProjectionMap);
                builder.appendWhere(CancelledLectures._ID + " = " + CancelledLectures.getId(uri));
                order = CancelledLectures.DEFAULT_SORT;

                break;

            case CLASSROOM_CHANGES:
                builder.setTables(ClassroomChanges.TABLE);
                builder.setProjectionMap(sClassroomChangesProjectionMap);
                order = ClassroomChanges.DEFAULT_SORT;

                break;

            case CLASSROOM_CHANGE_ID:
                builder.setTables(ClassroomChanges.TABLE);
                builder.setProjectionMap(sClassroomChangesProjectionMap);
                builder.appendWhere(ClassroomChanges._ID + " = " + ClassroomChanges.getId(uri));
                order = ClassroomChanges.DEFAULT_SORT;

                break;

            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        if (!TextUtils.isEmpty(sortOrder)) {
            order = sortOrder;
        }

        db = mPortalDatabase.getReadableDatabase();
        cursor = builder.query(db, projection, selection, selectionArgs, null, null, order);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        String table;
        SQLiteDatabase db;
        long rowId;
        Uri rowUri;

        switch (sUriMatcher.match(uri)) {
            case SCHEDULES:
                table = Schedules.TABLE;

                break;

            case INFORMATION:
                table = Information.TABLE;

                break;

            case INFORMATION_ATTACHMENTS:
                table = InformationAttachments.TABLE;

                break;

            case CANCELLED_LECTURES:
                table = CancelledLectures.TABLE;

                break;

            case CLASSROOM_CHANGES:
                table = ClassroomChanges.TABLE;

                break;

            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        db = mPortalDatabase.getWritableDatabase();
        rowId = db.insertOrThrow(table, null, values);
        rowUri = ContentUris.withAppendedId(uri, rowId);
        getContext().getContentResolver().notifyChange(rowUri, null);

        return rowUri;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        String table;
        String where;
        SQLiteDatabase db;
        int count;

        switch (sUriMatcher.match(uri)) {
            case SCHEDULES:
                table = Schedules.TABLE;
                where = null;

                break;

            case SCHEDULE_ID:
                table = Schedules.TABLE;
                where = Schedules._ID + " = " + Schedules.getId(uri);

                break;

            case INFORMATION:
                table = Information.TABLE;
                where = null;

                break;

            case INFORMATION_ID:
                table = Information.TABLE;
                where = Information._ID + " = " + Information.getId(uri);

                break;

            case INFORMATION_ATTACHMENTS:
                table = InformationAttachments.TABLE;
                where = null;

                break;

            case INFORMATION_ATTACHMENT_ID:
                table = InformationAttachments.TABLE;
                where = InformationAttachments._ID + " = " + InformationAttachments.getId(uri);

                break;

            case CANCELLED_LECTURES:
                table = CancelledLectures.TABLE;
                where = null;

                break;

            case CANCELLED_LECTURE_ID:
                table = CancelledLectures.TABLE;
                where = CancelledLectures._ID + " = " + CancelledLectures.getId(uri);

                break;

            case CLASSROOM_CHANGES:
                table = ClassroomChanges.TABLE;
                where = null;

                break;

            case CLASSROOM_CHANGE_ID:
                table = ClassroomChanges.TABLE;
                where = ClassroomChanges._ID + " = " + ClassroomChanges.getId(uri);

                break;

            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        if (!TextUtils.isEmpty(selection)) {
            if (!TextUtils.isEmpty(where)) {
                where += " AND ";
            } else {
                where = "";
            }
            where += selection;
        }

        db = mPortalDatabase.getWritableDatabase();
        count = db.update(table, values, where, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);

        return count;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        String table;
        String where;
        SQLiteDatabase db;
        int count;

        switch (sUriMatcher.match(uri)) {
            case SCHEDULES:
                table = Schedules.TABLE;
                where = null;

                break;

            case SCHEDULE_ID:
                table = Schedules.TABLE;
                where = Schedules._ID + " = " + Schedules.getId(uri);

                break;

            case INFORMATION:
                table = Information.TABLE;
                where = null;

                break;

            case INFORMATION_ID:
                table = Information.TABLE;
                where = Information._ID + " = " + Information.getId(uri);

                break;

            case INFORMATION_ATTACHMENTS:
                table = InformationAttachments.TABLE;
                where = null;

                break;

            case INFORMATION_ATTACHMENT_ID:
                table = InformationAttachments.TABLE;
                where = InformationAttachments._ID + " = " + InformationAttachments.getId(uri);

                break;

            case CANCELLED_LECTURES:
                table = CancelledLectures.TABLE;
                where = null;

                break;

            case CANCELLED_LECTURE_ID:
                table = CancelledLectures.TABLE;
                where = CancelledLectures._ID + " = " + CancelledLectures.getId(uri);

                break;

            case CLASSROOM_CHANGES:
                table = ClassroomChanges.TABLE;
                where = null;

                break;

            case CLASSROOM_CHANGE_ID:
                table = ClassroomChanges.TABLE;
                where = ClassroomChanges._ID + " = " + ClassroomChanges.getId(uri);

                break;

            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        if (!TextUtils.isEmpty(selection)) {
            if (!TextUtils.isEmpty(where)) {
                where += " AND ";
            } else {
                where = "";
            }
            where += selection;
        }

        db = mPortalDatabase.getWritableDatabase();
        count = db.delete(table, where, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);

        return count;
    }

    @Override
    public String getType(Uri uri) {
        String type;

        switch (sUriMatcher.match(uri)) {
            case SCHEDULES:
                type = Schedules.CONTENT_TYPE;

                break;

            case SCHEDULE_ID:
                type = Schedules.CONTENT_ITEM_TYPE;

                break;

            case INFORMATION:
                type = Information.CONTENT_TYPE;

                break;

            case INFORMATION_ID:
                type = Information.CONTENT_ITEM_TYPE;

                break;

            case INFORMATION_ATTACHMENTS:
                type = InformationAttachments.CONTENT_TYPE;

                break;

            case INFORMATION_ATTACHMENT_ID:
                type = InformationAttachments.CONTENT_ITEM_TYPE;

                break;

            case CANCELLED_LECTURES:
                type = CancelledLectures.CONTENT_TYPE;

                break;

            case CANCELLED_LECTURE_ID:
                type = CancelledLectures.CONTENT_ITEM_TYPE;

                break;

            case CLASSROOM_CHANGES:
                type = ClassroomChanges.CONTENT_TYPE;

                break;

            case CLASSROOM_CHANGE_ID:
                type = ClassroomChanges.CONTENT_ITEM_TYPE;

                break;

            default:
                throw new IllegalArgumentException("Unknown uri: " + uri);
        }

        return type;
    }

    @Override
    public ContentProviderResult[] applyBatch (ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        SQLiteDatabase db;
        ContentProviderResult[] results;

        db = mPortalDatabase.getWritableDatabase();
        db.beginTransaction();
        try {
            results = super.applyBatch(operations);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        return results;
    }
}
