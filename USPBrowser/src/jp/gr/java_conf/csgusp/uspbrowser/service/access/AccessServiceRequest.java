/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.service.access;

/**
 * AccessServiceRequest クラスはアクセスサービスへのリクエスト機能を実装したクラスです。
 *
 * @author leak4mk0
 * @version 27
 * @since 27
 */
public class AccessServiceRequest {
	private int mId;
	private int mAction;
	private boolean mCancelled;

	public AccessServiceRequest() {
		mId = -1;
		mAction = AccessService.ACTION_UNKNOWN;
	}

    public static AccessServiceRequest newFetchTrainTimetableRequest(int id) {
        AccessServiceRequest request;

        request = new AccessServiceRequest();
        request.setId(id);
        request.setAction(AccessService.ACTION_FETCH_TRAIN_TIMETABLE);

        return request;
    }

    public static AccessServiceRequest newFetchBusTimetableRequest(int id) {
        AccessServiceRequest request;

        request = new AccessServiceRequest();
        request.setId(id);
        request.setAction(AccessService.ACTION_FETCH_BUS_TIMETABLE);

        return request;
    }

	/**
	 * mIdを取得します。
	 * @return mId
	 */
	public int getId() {
		return mId;
	}

	/**
	 * idを設定します。
	 * @param id id
	 */
	public void setId(int id) {
		mId = id;
	}

	/**
	 * mActionを取得します。
	 * @return mAction
	 */
	public int getAction() {
		return mAction;
	}

	/**
	 * actionを設定します。
	 * @param action action
	 */
	public void setAction(int action) {
		mAction = action;
	}

	public boolean isCancelled() {
		return mCancelled;
	}

	public void setCancelled(boolean cancelled) {
		mCancelled = cancelled;
	}
}
