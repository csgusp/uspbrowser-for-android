/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.service.access;

import jp.gr.java_conf.csgusp.uspbrowser.service.access.IAccessService.Stub;

import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;

import java.lang.Thread.State;
import java.util.LinkedList;

/**
 * アクセスサービスの機能の定義を行っているクラスです。
 *
 * @author leak4mk0
 * @version 27
 * @since 27
 */
public class AccessServiceStub extends Stub {
    private static final String TAG = "AccessServiceStub";
    private static final boolean DEBUG = false;

    private AccessService mAccessService;
    private RemoteCallbackList<IAccessServiceCallback> mRemoteCallbackList;
    private LinkedList<AccessServiceRequest> mAccessServiceRequests;
    private static AccessServiceThread mAccessServiceThread = null;
    private int mLastId;

    public AccessServiceStub(AccessService accessService) {
        if (DEBUG) Log.v(TAG, "AccessServiceStub()");
        mAccessService = accessService;
        mRemoteCallbackList = new RemoteCallbackList<IAccessServiceCallback>();
        mAccessServiceRequests = new LinkedList<AccessServiceRequest>();
        mLastId = 0;
    }

    @Override
    public void registerCallback(IAccessServiceCallback callback) {
        if (callback == null) {
            return;
        }

        mRemoteCallbackList.register(callback);
    }

    @Override
    public void unregisterCallback(IAccessServiceCallback callback) {
        if (callback == null) {
            return;
        }

        mRemoteCallbackList.unregister(callback);
    }

    @Override
    public int requestFetchTrainTimetable() throws RemoteException {
        AccessServiceRequest request;

        if (DEBUG) Log.v(TAG, "requestFetchTrainTimetable()");
        for (AccessServiceRequest r : mAccessServiceRequests) {
            if (r.getAction() == AccessService.ACTION_FETCH_TRAIN_TIMETABLE) {
                if (DEBUG)
                    Log.v(TAG, "requestFetchTrainTimetable() : Deflect request because requested.");

                return AccessService.ID_REQUESTED;
            }
        }
        mLastId++;
        request = AccessServiceRequest.newFetchTrainTimetableRequest(mLastId);
        mAccessServiceRequests.offer(request);
        runRequest();

        return mLastId;
    }

    @Override
    public int requestFetchBusTimetable() {
        AccessServiceRequest request;

        if (DEBUG) Log.v(TAG, "requestFetchBusTimetable()");
        for (AccessServiceRequest r : mAccessServiceRequests) {
            if (r.getAction() == AccessService.ACTION_FETCH_BUS_TIMETABLE) {
                if (DEBUG)
                    Log.v(TAG, "requestFetchBusTimetable() : Deflect request because requested.");

                return AccessService.ID_REQUESTED;
            }
        }
        mLastId++;
        request = AccessServiceRequest.newFetchBusTimetableRequest(mLastId);
        mAccessServiceRequests.offer(request);
        runRequest();

        return mLastId;
    }

    @Override
    public void cancelRequest(int id) {
        if (DEBUG) Log.v(TAG, "cancelRequest(" + String.valueOf(id) + ")");
        for (AccessServiceRequest request : mAccessServiceRequests) {
            if (id == AccessService.ID_ALL || request.getId() == id) {
                request.setCancelled(true);
            }
        }
    }

    @Override
    public boolean isRunning() {
        return mAccessServiceThread != null && mAccessServiceThread.isFetching();
    }

    private void runRequest() {
        if (DEBUG) Log.v(TAG, "runRequest()");
        if (mAccessServiceThread == null || mAccessServiceThread.getState() == State.TERMINATED) {
            if (DEBUG) Log.v(TAG, "runRequest() : Create an instance of AccessServiceThread.");
            mAccessServiceThread = new AccessServiceThread();
        }
        if (mAccessServiceThread.getState() == State.NEW) {
            if (DEBUG) Log.v(TAG, "runRequest() : Set an instance of AccessServiceThread.");
            mAccessServiceThread.setContentResolver(mAccessService.getContentResolver());
            mAccessServiceThread.setRemoteCallbackList(mRemoteCallbackList);
            mAccessServiceThread.setAccessServiceRequests(mAccessServiceRequests);
            mAccessServiceThread.start();
        }
    }
}
