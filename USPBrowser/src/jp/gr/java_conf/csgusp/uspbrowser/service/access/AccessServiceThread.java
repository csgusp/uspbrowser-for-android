/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.service.access;

import jp.gr.java_conf.csgusp.uspbrowser.client.access.AccessClient;
import jp.gr.java_conf.csgusp.uspbrowser.client.access.AccessContentHandler;
import jp.gr.java_conf.csgusp.uspbrowser.client.access.BusContent;
import jp.gr.java_conf.csgusp.uspbrowser.client.access.TrainContent;
import jp.gr.java_conf.csgusp.uspbrowser.provider.access.AccessContract;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.OperationApplicationException;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * アクセスサービスの処理スレッドを実装したクラスです。
 *
 * @author leak4mk0
 * @version 27
 * @since 27
 */
public class AccessServiceThread extends Thread implements AccessContentHandler {
    private static final String TAG = "AccessServiceThread";
    private static final boolean DEBUG = false;

    private boolean mFetching;
    private RemoteCallbackList<IAccessServiceCallback> mRemoteCallbackList;
    private LinkedList<AccessServiceRequest> mAccessServiceRequests;
    private ContentResolver mContentResolver;
    private AccessClient mAccessClient;
    private int mCurrentRequestId;

    private ArrayList<ContentProviderOperation> mContentProviderOperations;

    public AccessServiceThread() {
        mFetching = false;
        mRemoteCallbackList = null;
        mAccessServiceRequests = null;
        mContentProviderOperations = new ArrayList<ContentProviderOperation>();
        mAccessClient = null;
    }

    public void run() {
        int size;
        AccessServiceRequest request;
        boolean cancelled;

        if (DEBUG) Log.d(TAG, "run()");
        mAccessClient = new AccessClient();
        mAccessClient.setAccessContentHandler(this);

        mFetching = true;
        size = mRemoteCallbackList.beginBroadcast();
        for (int j = 0; j < size; j++) {
            try {
                mRemoteCallbackList.getBroadcastItem(j).onResult(AccessService.ACTION_BEGIN, AccessService.STATUS_RUNNING);
            } catch (RemoteException ignored) {
            }
        }
        mRemoteCallbackList.finishBroadcast();

        cancelled = false;
        while ((request = mAccessServiceRequests.poll()) != null) {
            int ret;
            int status;

            if (DEBUG)
                Log.d(TAG, "run() : Run request. It's id is " + String.valueOf(request.getId()) + " and action is " + String.valueOf(request.getAction()));
            if (!cancelled && !request.isCancelled()) {
                mCurrentRequestId = request.getId();

                switch (request.getAction()) {
                    case AccessService.ACTION_FETCH_TRAIN_TIMETABLE:
                        ret = mAccessClient.fetchTrainTimetable();

                        break;

                    case AccessService.ACTION_FETCH_BUS_TIMETABLE:
                        ret = mAccessClient.fetchBusTimetable();

                        break;

                    default:
                        ret = AccessClient.FAILURE_UNKNOWN;

                        break;
                }

                switch (ret) {
                    case AccessClient.SUCCESS:
                        switch (request.getAction()) {
                            case AccessService.ACTION_FETCH_TRAIN_TIMETABLE:
                            case AccessService.ACTION_FETCH_BUS_TIMETABLE:
                                try {
                                    mContentResolver.applyBatch(AccessContract.CONTENT_AUTHORITY, mContentProviderOperations);
                                } catch (RemoteException e) {
                                    status = AccessService.STATUS_FAILURE_IN_DATABASE;

                                    break;
                                } catch (OperationApplicationException e) {
                                    status = AccessService.STATUS_FAILURE_IN_DATABASE;

                                    break;
                                }
                                mContentProviderOperations.clear();

                                status = AccessService.STATUS_SUCCESS;

                                break;

                            default:
                                status = AccessService.STATUS_SUCCESS;

                                break;
                        }

                        break;

                    case AccessClient.FAILURE_UNKNOWN:
                        status = AccessService.STATUS_FAILURE_IN_UNKNOWN;

                        break;

                    case AccessClient.FAILURE_NETWORK:
                        status = AccessService.STATUS_FAILURE_IN_NETWORK;

                        break;

                    case AccessClient.FAILURE_CONTENT:
                        status = AccessService.STATUS_FAILURE_IN_CONTENT;

                        break;

                    default:
                        status = AccessService.STATUS_FAILURE_IN_UNKNOWN;

                        break;
                }

                mCurrentRequestId = -1;

                if (status != AccessService.STATUS_SUCCESS) {
                    if (DEBUG)
                        Log.d(TAG, "run() : Failed request. It's status is " + String.valueOf(status));
                    cancelled = true;
                }
            } else {
                if (DEBUG) Log.d(TAG, "run() : Cancelled request.");
                status = AccessService.STATUS_FAILURE_IN_CANCEL;
            }

            if (request.getId() != AccessService.ID_INTERNAL) {
                size = mRemoteCallbackList.beginBroadcast();
                for (int j = 0; j < size; j++) {
                    try {
                        mRemoteCallbackList.getBroadcastItem(j).onResult(request.getAction(), status);
                    } catch (RemoteException ignored) {
                    }
                }
                mRemoteCallbackList.finishBroadcast();
            }
        }

        mFetching = false;
        size = mRemoteCallbackList.beginBroadcast();
        for (int j = 0; j < size; j++) {
            try {
                mRemoteCallbackList.getBroadcastItem(j).onResult(AccessService.ACTION_END,
                        cancelled ? AccessService.STATUS_FAILURE : AccessService.STATUS_SUCCESS);
            } catch (RemoteException ignored) {
            }
        }
        mRemoteCallbackList.finishBroadcast();

        mAccessClient.dispose();
    }

    @Override
    public void onTrainContents(List<TrainContent> contents) {
        mContentProviderOperations.add(
                ContentProviderOperation.newDelete(AccessContract.Trains.CONTENT_URI)
                        .build()
        );
        for (TrainContent content : contents) {
            mContentProviderOperations.add(
                    ContentProviderOperation.newInsert(AccessContract.Trains.CONTENT_URI)
                            .withValue(AccessContract.Trains.DIRECTION, content.getDirection())
                            .withValue(AccessContract.Trains.WEEKDAY, content.getWeekday())
                            .withValue(AccessContract.Trains.TIME, content.getTime())
                            .build()
            );
        }
    }

    @Override
    public void onBusContents(List<BusContent> contents) {
        mContentProviderOperations.add(
                ContentProviderOperation.newDelete(AccessContract.Buses.CONTENT_URI)
                        .build()
        );
        for (BusContent content : contents) {
            mContentProviderOperations.add(
                    ContentProviderOperation.newInsert(AccessContract.Buses.CONTENT_URI)
                            .withValue(AccessContract.Buses.DIRECTION, content.getDirection())
                            .withValue(AccessContract.Buses.WEEKDAY, content.getWeekday())
                            .withValue(AccessContract.Buses.TIME, content.getTime())
                            .build()
            );
        }
    }

    public boolean isFetching() {
        return mFetching;
    }

    public RemoteCallbackList<IAccessServiceCallback> getRemoteCallbackList() {
        return mRemoteCallbackList;
    }

    public void setRemoteCallbackList(RemoteCallbackList<IAccessServiceCallback> remoteCallbackList) {
        mRemoteCallbackList = remoteCallbackList;
    }

    public List<AccessServiceRequest> getAccessServiceRequests() {
        return mAccessServiceRequests;
    }

    public void setAccessServiceRequests(LinkedList<AccessServiceRequest> accessServiceRequests) {
        mAccessServiceRequests = accessServiceRequests;
    }

    public ContentResolver getContentResolver() {
        return mContentResolver;
    }

    public void setContentResolver(ContentResolver contentResolver) {
        mContentResolver = contentResolver;
    }
}
