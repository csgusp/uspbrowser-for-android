/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.service.portal;

import jp.gr.java_conf.csgusp.uspbrowser.util.ModernSSLSocketFactory;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * ポータルサービスの実装を行っているクラスです。
 *
 * @author leak4mk0
 * @version 34
 * @since 1
 */
public class PortalService extends Service {
    private static final String TAG = "PortalService";
    private static final boolean DEBUG = false;

	public static final int ACTION_UNKNOWN = 0;
	public static final int ACTION_BEGIN = 1;
	public static final int ACTION_END = 2;
	public static final int ACTION_LOGON = 3;
	public static final int ACTION_LOGOUT = 4;
	public static final int ACTION_FETCH_TOP = 5;
	public static final int ACTION_FETCH_CONTENTS = 6;
	public static final int ACTION_FETCH_SCHEDULE = 7;
	public static final int ACTION_FETCH_INFORMATION = 8;

	public static final int STATUS_UNKNOWN = 0x00000000;
	public static final int STATUS_WAITING = 0x00010000;
	public static final int STATUS_RUNNING = 0x00020000;
	public static final int STATUS_SUCCESS = 0x00040000;
	public static final int STATUS_FAILURE = 0x00080000;
	public static final int STATUS_FAILURE_IN_UNKNOWN = 0x00080000;
	public static final int STATUS_FAILURE_IN_CANCEL = 0x00080001;
	public static final int STATUS_FAILURE_IN_NETWORK = 0x00080002;
	public static final int STATUS_FAILURE_IN_CONTENT = 0x00080004;
	public static final int STATUS_FAILURE_IN_DATABASE = 0x00080008;
	public static final int STATUS_FAILURE_IN_AUTHORIZATION = 0x00080101;

	public static final int ID_ALL = -1;
	public static final int ID_REQUESTED = -2;
	public static final int ID_INTERNAL = -3;

	private IPortalService.Stub mStub;

	public PortalService() {
        if (DEBUG) Log.v(TAG, "PortalService() : Create an instance of PortalServiceStub.");
        ModernSSLSocketFactory.initialize(this);
        mStub = new PortalServiceStub(this);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return mStub;
	}
}
