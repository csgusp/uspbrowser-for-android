/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.service.portal;

/**
 * PortalServiceRequest クラスはポータルサービスへのリクエスト機能を実装したクラスです。
 *
 * @author leak4mk0
 * @version 12
 * @since 1
 */
public class PortalServiceRequest {
	private int mId;
	private int mAction;
	private boolean mCancelled;
	private String mUserId;
	private String mPassword;
	private int mScheduleId;
	private int mInformationId;

	public PortalServiceRequest() {
		mId = -1;
		mAction = PortalService.ACTION_UNKNOWN;
		mUserId = null;
		mPassword = null;
		mScheduleId = -1;
		mInformationId = -1;
	}

	public static PortalServiceRequest newLogonRequest(int id, String userId, String password) {
		PortalServiceRequest request;

		request = new PortalServiceRequest();
		request.setId(id);
		request.setAction(PortalService.ACTION_LOGON);
		request.setUserId(userId);
		request.setPassword(password);

		return request;
	}

	public static PortalServiceRequest newLogoutRequest(int id) {
		PortalServiceRequest request;

		request = new PortalServiceRequest();
		request.setId(id);
		request.setAction(PortalService.ACTION_LOGOUT);

		return request;
	}

	public static PortalServiceRequest newFetchTopRequest(int id) {
		PortalServiceRequest request;

		request = new PortalServiceRequest();
		request.setId(id);
		request.setAction(PortalService.ACTION_FETCH_TOP);

		return request;
	}

	public static PortalServiceRequest newFetchContentsRequest(int id) {
		PortalServiceRequest request;

		request = new PortalServiceRequest();
		request.setId(id);
		request.setAction(PortalService.ACTION_FETCH_CONTENTS);

		return request;
	}

	public static PortalServiceRequest newFetchScheduleRequest(int id, int scheduleId) {
		PortalServiceRequest request;

		request = new PortalServiceRequest();
		request.setId(id);
		request.setAction(PortalService.ACTION_FETCH_SCHEDULE);
		request.setScheduleId(scheduleId);

		return request;
	}

	public static PortalServiceRequest newFetchInformationRequest(int id, int informationId) {
		PortalServiceRequest request;

		request = new PortalServiceRequest();
		request.setId(id);
		request.setAction(PortalService.ACTION_FETCH_INFORMATION);
		request.setInformationId(informationId);

		return request;
	}

	/**
	 * mIdを取得します。
	 * @return mId
	 */
	public int getId() {
		return mId;
	}

	/**
	 * idを設定します。
	 * @param id id
	 */
	public void setId(int id) {
		mId = id;
	}

	/**
	 * mActionを取得します。
	 * @return mAction
	 */
	public int getAction() {
		return mAction;
	}

	/**
	 * actionを設定します。
	 * @param action action
	 */
	public void setAction(int action) {
		mAction = action;
	}

	public boolean isCancelled() {
		return mCancelled;
	}

	public void setCancelled(boolean cancelled) {
		mCancelled = cancelled;
	}

	/**
	 * mUserIdを取得します。
	 * @return mUserId
	 */
	public String getUserId() {
	    return mUserId;
	}

	/**
	 * mUserIdを設定します。
	 * @param userId mUserId
	 */
	public void setUserId(String userId) {
	    mUserId = userId;
	}

	/**
	 * mPasswordを取得します。
	 * @return password
	 */
	public String getPassword() {
	    return mPassword;
	}

	/**
	 * mPasswordを設定します。
	 * @param password mPassword
	 */
	public void setPassword(String password) {
	    mPassword = password;
	}

	/**
	 * @return scheduleId
	 */
	public int getScheduleId() {
		return mScheduleId;
	}

	/**
	 * @param scheduleId セットする scheduleId
	 */
	public void setScheduleId(int scheduleId) {
		mScheduleId = scheduleId;
	}

	/**
	 * @return informationId
	 */
	public int getInformationId() {
		return mInformationId;
	}

	/**
	 * @param informationId セットする informationId
	 */
	public void setInformationId(int informationId) {
		mInformationId = informationId;
	}
}
