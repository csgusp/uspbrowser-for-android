/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.service.portal;

import java.lang.Thread.State;
import java.util.LinkedList;

import jp.gr.java_conf.csgusp.uspbrowser.service.portal.IPortalService.Stub;
import android.os.RemoteCallbackList;
import android.util.Log;

/**
 * ポータルサービスの機能の定義を行っているクラスです。
 *
 * @author leak4mk0
 * @version 16
 * @since 1
 */
public class PortalServiceStub extends Stub {
    private static final String TAG = "PortalServiceStub";
    private static final boolean DEBUG = false;

    private PortalService mPortalService;
	private RemoteCallbackList<IPortalServiceCallback> mRemoteCallbackList;
	private LinkedList<PortalServiceRequest> mPortalServiceRequests;
	private static PortalServiceThread mPortalServiceThread = null;
	private boolean mLogonRequested;
	private boolean mLogoutRequested;
	private int mLastId;
	private String mLastUserId;
	private String mLastPassword;

	public PortalServiceStub(PortalService portalService) {
        if (DEBUG) Log.v(TAG, "PortalServiceStub()");
        mPortalService = portalService;
		mRemoteCallbackList = new RemoteCallbackList<IPortalServiceCallback>();
		mPortalServiceRequests = new LinkedList<PortalServiceRequest>();
		mLogonRequested = false;
		mLogoutRequested = false;
		mLastId = 0;
		mLastUserId = null;
		mLastPassword = null;
	}

	@Override
	public void registerCallback(IPortalServiceCallback callback) {
		if (callback == null) {
			return;
		}

		mRemoteCallbackList.register(callback);
	}

	@Override
	public void unregisterCallback(IPortalServiceCallback callback) {
		if (callback == null) {
			return;
		}

		mRemoteCallbackList.unregister(callback);
	}

	@Override
	public int requestLogon(String userId, String password) {
		PortalServiceRequest request;

		if (DEBUG) Log.v(TAG, "requestLogon(" + userId + ", " + password + ")");
        if (mPortalServiceRequests.size() > 0 && mLogonRequested
				&& userId.equals(mLastUserId) && password.equals(mLastPassword)) {
			if (DEBUG) {
				Log.d(TAG, "requestLogon() : Deflect request because requested.");
			}

			return PortalService.ID_REQUESTED;
		}
		mLogonRequested = true;
		mLogoutRequested = false;
		mLastId++;
		mLastUserId = userId;
		mLastPassword = password;
		request = PortalServiceRequest.newLogonRequest(mLastId, userId, password);
		mPortalServiceRequests.offer(request);
		runRequest();

		return mLastId;
	}

	@Override
	public int requestLogout() {
		PortalServiceRequest request;

		if (DEBUG) Log.v(TAG, "requestLogout()");
		if (mPortalServiceRequests.size() > 0 && mLogoutRequested) {
			if (DEBUG) {
				Log.v(TAG, "requestLogout() : Deflect request because requested.");
			}

			return PortalService.ID_REQUESTED;
		}
		mLogoutRequested = true;
		mLastId++;
		request = PortalServiceRequest.newLogoutRequest(mLastId);
		mPortalServiceRequests.offer(request);
		runRequest();

		return mLastId;
	}

	@Override
	public int requestFetchTop() {
		PortalServiceRequest request;

		if (DEBUG) Log.v(TAG, "requestFetchTop()");
		mLastId++;
		request = PortalServiceRequest.newFetchTopRequest(mLastId);
		mPortalServiceRequests.offer(request);
		runRequest();

		return mLastId;
	}

	@Override
	public int requestFetchContents() {
		PortalServiceRequest request;

		if (DEBUG) Log.v(TAG, "requestFetchContents()");
		mLastId++;
		request = PortalServiceRequest.newFetchContentsRequest(mLastId);
		mPortalServiceRequests.offer(request);
		runRequest();

		return mLastId;
	}

	@Override
	public int requestFetchSchedule(int scheduleId) {
		PortalServiceRequest request;

		if (DEBUG) Log.v(TAG, "requestFetchSchedule(" + String.valueOf(scheduleId) + ")");
        for (PortalServiceRequest r : mPortalServiceRequests) {
            if (r.getAction() == PortalService.ACTION_FETCH_SCHEDULE
                    && r.getScheduleId() == scheduleId) {
                if (DEBUG) Log.v(TAG, "requestFetchSchedule() : Deflect request because requested.");

                return PortalService.ID_REQUESTED;
            }
        }
		mLastId++;
		request = PortalServiceRequest.newFetchScheduleRequest(mLastId, scheduleId);
		mPortalServiceRequests.offer(request);
		runRequest();

		return mLastId;
	}

	@Override
	public int requestFetchInformation(int informationId) {
		PortalServiceRequest request;

		if (DEBUG) Log.v(TAG, "requestFetchInformation(" + String.valueOf(informationId) + ")");
        for (PortalServiceRequest r : mPortalServiceRequests) {
            if (r.getAction() == PortalService.ACTION_FETCH_INFORMATION
                    && r.getInformationId() == informationId) {
                if (DEBUG) Log.v(TAG, "requestFetchInformation() : Deflect request because requested.");

                return PortalService.ID_REQUESTED;
            }
        }
		mLastId++;
		request = PortalServiceRequest.newFetchInformationRequest(mLastId, informationId);
		mPortalServiceRequests.offer(request);
		runRequest();

		return mLastId;
	}

	@Override
	public void cancelRequest(int id) {
		if (DEBUG) Log.v(TAG, "cancelRequest(" + String.valueOf(id) + ")");
		for (PortalServiceRequest request : mPortalServiceRequests) {
			if (id == PortalService.ID_ALL || request.getId() == id) {
				request.setCancelled(true);
			}
		}
	}

	@Override
	public boolean isRunning() {
		return mPortalServiceThread != null && mPortalServiceThread.isFetching();
	}

	private void runRequest() {
		if (DEBUG) Log.v(TAG, "runRequest()");
		if (mPortalServiceRequests.size() < 2 && mLogonRequested && mLogoutRequested) {
			if (DEBUG) Log.v(TAG, "runRequest() : Reset requested flags because they are old.");
			mLogonRequested = false;
			mLogoutRequested = false;
		}
		if (!mLogonRequested) {
			PortalServiceRequest request;

			if (DEBUG) Log.v(TAG, "runRequest() : Insert logon request because of flag.");
            mLogonRequested = true;
			mLastId++;
			request = PortalServiceRequest.newLogonRequest(mLastId, mLastUserId, mLastPassword);
			mPortalServiceRequests.addFirst(request);
		}
		if (!mLogoutRequested) {
			PortalServiceRequest request;

			if (DEBUG)Log.v(TAG, "runRequest() : Insert logout request because of flag.");
			mLogoutRequested = true;
			mLastId++;
			request = PortalServiceRequest.newLogoutRequest(mLastId);
			mPortalServiceRequests.addLast(request);
		} else {
			PortalServiceRequest logoutRequest;
			PortalServiceRequest otherRequest;

			if (DEBUG) Log.v(TAG, "runRequest() : Swap requests because logout request should be last.");
			otherRequest = mPortalServiceRequests.removeLast();
			logoutRequest = mPortalServiceRequests.removeLast();
			mPortalServiceRequests.addLast(otherRequest);
			if (otherRequest.getAction() != PortalService.ACTION_LOGOUT) {
				mPortalServiceRequests.addLast(logoutRequest);
			}
		}
		if (mPortalServiceThread == null || mPortalServiceThread.getState() == State.TERMINATED) {
            if (DEBUG) Log.v(TAG, "runRequest() : Create an instance of PortalServiceThread.");
			mPortalServiceThread = new PortalServiceThread();
		}
		if (mPortalServiceThread.getState() == State.NEW) {
            if (DEBUG) Log.v(TAG, "runRequest() : Set an instance of PortalServiceThread.");
			mPortalServiceThread.setContentResolver(mPortalService.getContentResolver());
			mPortalServiceThread.setRemoteCallbackList(mRemoteCallbackList);
			mPortalServiceThread.setPortalServiceRequests(mPortalServiceRequests);
			mPortalServiceThread.start();
		}
	}
}
