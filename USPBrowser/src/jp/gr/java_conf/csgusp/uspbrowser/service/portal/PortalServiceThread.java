/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.service.portal;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import jp.gr.java_conf.csgusp.uspbrowser.client.portal.CancelledLectureContent;
import jp.gr.java_conf.csgusp.uspbrowser.client.portal.ClassroomChangeContent;
import jp.gr.java_conf.csgusp.uspbrowser.client.portal.InformationAttachment;
import jp.gr.java_conf.csgusp.uspbrowser.client.portal.InformationContent;
import jp.gr.java_conf.csgusp.uspbrowser.client.portal.PortalClient;
import jp.gr.java_conf.csgusp.uspbrowser.client.portal.PortalContentHandler;
import jp.gr.java_conf.csgusp.uspbrowser.client.portal.ScheduleContent;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.Schedules;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.Information;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.InformationAttachments;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.CancelledLectures;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract.ClassroomChanges;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;

/**
 * ポータルサービスの処理スレッドを実装したクラスです。
 *
 * @author leak4mk0
 * @version 16
 * @since 1
 */
public class PortalServiceThread extends Thread implements PortalContentHandler {
    private static final String TAG = "PortalServiceThread";
    private static final boolean DEBUG = false;

    private boolean mFetching;
    private RemoteCallbackList<IPortalServiceCallback> mRemoteCallbackList;
    private LinkedList<PortalServiceRequest> mPortalServiceRequests;
    private ContentResolver mContentResolver;
    private PortalClient mPortalClient;

    private boolean mAuthorized;
    private List<Integer> mDatabaseScheduleIds;
    private List<Integer> mDatabaseScheduleDeletedIds;
    private List<Integer> mDatabaseInformationIds;
    private List<Integer> mDatabaseInformationDeletedIds;
    private List<Integer> mDatabaseCancelledLectureIds;
    private List<Integer> mDatabaseCancelledLectureDeletedIds;
    private List<Integer> mDatabaseCancelledLectureCancelledLectureCodes;
    private List<Integer> mDatabaseCancelledLectureMakeupLectureCodes;
    private List<Integer> mDatabaseClassroomChangeIds;
    private List<Integer> mDatabaseClassroomChangeDeletedIds;
    private List<Integer> mDatabaseClassroomChangeLectureIds;
    private ArrayList<ContentProviderOperation> mContentProviderOperations;

    public PortalServiceThread() {
        mFetching = false;
        mRemoteCallbackList = null;
        mPortalServiceRequests = null;
        mDatabaseScheduleIds = new ArrayList<Integer>();
        mDatabaseScheduleDeletedIds = new ArrayList<Integer>();
        mDatabaseInformationIds = new ArrayList<Integer>();
        mDatabaseInformationDeletedIds = new ArrayList<Integer>();
        mDatabaseCancelledLectureIds = new ArrayList<Integer>();
        mDatabaseCancelledLectureDeletedIds = new ArrayList<Integer>();
        mDatabaseCancelledLectureCancelledLectureCodes = new ArrayList<Integer>();
        mDatabaseCancelledLectureMakeupLectureCodes = new ArrayList<Integer>();
        mDatabaseClassroomChangeIds = new ArrayList<Integer>();
        mDatabaseClassroomChangeDeletedIds = new ArrayList<Integer>();
        mDatabaseClassroomChangeLectureIds = new ArrayList<Integer>();
        mContentProviderOperations = new ArrayList<ContentProviderOperation>();
        mPortalClient = null;
    }

    public void run() {
        int size;
        PortalServiceRequest request;
        boolean cancelled;

        if (DEBUG) Log.d(TAG, "run()");
        mPortalClient = new PortalClient();
        mPortalClient.setPortalContentHandler(this);

        mFetching = true;
        size = mRemoteCallbackList.beginBroadcast();
        for (int j = 0; j < size; j++) {
            try {
                mRemoteCallbackList.getBroadcastItem(j).onResult(PortalService.ACTION_BEGIN, PortalService.STATUS_RUNNING);
            } catch (RemoteException e) {
                continue;
            }
        }
        mRemoteCallbackList.finishBroadcast();

        cancelled = false;
        while ((request = mPortalServiceRequests.poll()) != null) {
            int ret;
            int status;

            if (DEBUG) Log.d(TAG, "run() : Run request. It's id is " + String.valueOf(request.getId()) + " and action is " + String.valueOf(request.getAction()));
            if (!cancelled && !request.isCancelled()) {
                Cursor cursor;

                switch (request.getAction()) {
                    case PortalService.ACTION_LOGON:
                        ret = mPortalClient.logon(request.getUserId(), request.getPassword());

                        break;

                    case PortalService.ACTION_LOGOUT:
                        ret = mPortalClient.logout();

                        break;

                    case PortalService.ACTION_FETCH_TOP: {
                        int idColumnIndex;
                        int cancelledLectureIdColumnIndex;
                        int makeupLectureIdColumnIndex;
                        int lectureId;

                        mDatabaseScheduleIds.clear();
                        cursor = mContentResolver.query(Schedules.CONTENT_URI,
                                new String[] { Schedules.SCHEDULE_ID }, null, null, null);
                        if (cursor.moveToFirst()) {
                            idColumnIndex = cursor.getColumnIndex(Schedules.SCHEDULE_ID);
                            do {
                                mDatabaseScheduleIds.add(cursor.getInt(idColumnIndex));
                            } while (cursor.moveToNext());
                        }
                        cursor.close();
                        mDatabaseScheduleDeletedIds.clear();
                        mDatabaseScheduleDeletedIds.addAll(mDatabaseScheduleIds);

                        mDatabaseInformationIds.clear();
                        cursor = mContentResolver.query(Information.CONTENT_URI,
                                new String[] { Information.INFORMATION_ID }, null, null, null);
                        if (cursor.moveToFirst()) {
                            idColumnIndex = cursor.getColumnIndex(Information.INFORMATION_ID);
                            do {
                                mDatabaseInformationIds.add(cursor.getInt(idColumnIndex));
                            } while (cursor.moveToNext());
                        }
                        cursor.close();
                        mDatabaseInformationDeletedIds.clear();
                        mDatabaseInformationDeletedIds.addAll(mDatabaseInformationIds);

                        mDatabaseCancelledLectureCancelledLectureCodes.clear();
                        mDatabaseCancelledLectureMakeupLectureCodes.clear();
                        cursor = mContentResolver.query(CancelledLectures.CONTENT_URI,
                                new String[] { CancelledLectures._ID,
                                        CancelledLectures.CANCELLED_LECTURE_ID,
                                        CancelledLectures.MAKEUP_LECTURE_ID }, null, null, null);
                        if (cursor.moveToFirst()) {
                            idColumnIndex = cursor.getColumnIndex(CancelledLectures._ID);
                            cancelledLectureIdColumnIndex = cursor.getColumnIndex(CancelledLectures.CANCELLED_LECTURE_ID);
                            makeupLectureIdColumnIndex = cursor.getColumnIndex(CancelledLectures.MAKEUP_LECTURE_ID);
                            do {
                                mDatabaseCancelledLectureIds.add(cursor.getInt(idColumnIndex));
                                mDatabaseCancelledLectureCancelledLectureCodes.add(cursor.getInt(cancelledLectureIdColumnIndex));
                                mDatabaseCancelledLectureMakeupLectureCodes.add(cursor.getInt(makeupLectureIdColumnIndex));
                            } while (cursor.moveToNext());
                        }
                        cursor.close();
                        mDatabaseCancelledLectureDeletedIds.clear();
                        mDatabaseCancelledLectureDeletedIds.addAll(mDatabaseCancelledLectureIds);

                        mDatabaseClassroomChangeLectureIds.clear();
                        cursor = mContentResolver.query(ClassroomChanges.CONTENT_URI,
                                new String[] { ClassroomChanges._ID,
                                        ClassroomChanges.LECTURE_ID }, null, null, null);
                        if (cursor.moveToFirst()) {
                            idColumnIndex = cursor.getColumnIndex(ClassroomChanges._ID);
                            lectureId = cursor.getColumnIndex(ClassroomChanges.LECTURE_ID);
                            do {
                                mDatabaseClassroomChangeIds.add(cursor.getInt(idColumnIndex));
                                mDatabaseClassroomChangeLectureIds.add(cursor.getInt(lectureId));
                            } while (cursor.moveToNext());
                            cursor.close();
                        }
                        cursor.close();
                        mDatabaseClassroomChangeDeletedIds.clear();
                        mDatabaseClassroomChangeDeletedIds.addAll(mDatabaseClassroomChangeIds);

                        ret = mPortalClient.fetchTop();

                        break;
                    }

                    case PortalService.ACTION_FETCH_CONTENTS: {
                        int scheduleIdColumnIndex;
                        int informationIdColumnIndex;
                        int addLocation;

                        addLocation = 0;
                        cursor = mContentResolver.query(Schedules.CONTENT_URI,
                                new String[] { Schedules.SCHEDULE_ID,
                                        Schedules.IS_DELETED,
                                        Schedules.IS_FETCHED },
                                Schedules.IS_DELETED + " = 0 AND "
                                        + Schedules.IS_FETCHED + " = 0", null, null);
                        if (cursor.moveToFirst()) {
                            if (DEBUG) Log.d(TAG, "run() : Insert fetch schedule requests. The number of requests is " + String.valueOf(cursor.getCount()));
                            scheduleIdColumnIndex = cursor.getColumnIndex(Schedules.SCHEDULE_ID);
                            do {
                                mPortalServiceRequests.add(addLocation, PortalServiceRequest.newFetchScheduleRequest(
                                        PortalService.ID_INTERNAL, cursor.getInt(scheduleIdColumnIndex)));
                                addLocation++;
                            } while (cursor.moveToNext());
                        }
                        cursor.close();
                        cursor = mContentResolver.query(Information.CONTENT_URI,
                                new String[] { Information.INFORMATION_ID,
                                        Information.IS_DELETED,
                                        Information.IS_FETCHED },
                                Information.IS_DELETED + " = 0 AND "
                                        + Information.IS_FETCHED + " = 0", null, null);
                        if (cursor.moveToFirst()) {
                            if (DEBUG) Log.d(TAG, "run() : Insert fetch information requests. The number of requests is " + String.valueOf(cursor.getCount()));
                            informationIdColumnIndex = cursor.getColumnIndex(Information.INFORMATION_ID);
                            do {
                                mPortalServiceRequests.add(addLocation, PortalServiceRequest.newFetchInformationRequest(
                                        PortalService.ID_INTERNAL, cursor.getInt(informationIdColumnIndex)));
                                addLocation++;
                            } while (cursor.moveToNext());
                        }
                        cursor.close();

                        ret = PortalClient.SUCCESS;

                        break;
                    }

                    case PortalService.ACTION_FETCH_SCHEDULE: {
                        int studentId;

                        mDatabaseScheduleIds.clear();
                        mDatabaseScheduleIds.add(request.getScheduleId());
                        mDatabaseScheduleDeletedIds.clear();
                        cursor = mContentResolver.query(Schedules.CONTENT_URI,
                                new String[] { Schedules.SCHEDULE_ID,
                                        Schedules.STUDENT_ID },
                                Schedules.SCHEDULE_ID + " = ?",
                                new String[] { String.valueOf(request.getScheduleId()) }, null);
                        if (cursor.moveToFirst()) {
                            studentId = cursor.getInt(cursor.getColumnIndex(Schedules.STUDENT_ID));
                        } else {
                            studentId = 0;
                        }
                        cursor.close();

                        ret = mPortalClient.fetchSchedule(request.getScheduleId(), studentId);

                        break;
                    }

                    case PortalService.ACTION_FETCH_INFORMATION:
                        mDatabaseInformationIds.clear();
                        mDatabaseInformationIds.add(request.getInformationId());
                        mDatabaseInformationDeletedIds.clear();

                        ret = mPortalClient.fetchInformation(request.getInformationId());

                        break;

                    default:
                        ret = PortalClient.FAILURE_UNKNOWN;

                        break;
                }

                switch (ret) {
                    case PortalClient.SUCCESS:
                        switch (request.getAction()) {
                            case PortalService.ACTION_LOGON:
                                status = mAuthorized ?
                                        PortalService.STATUS_SUCCESS : PortalService.STATUS_FAILURE_IN_AUTHORIZATION;

                                break;

                            case PortalService.ACTION_FETCH_TOP:
                                for (int id : mDatabaseScheduleDeletedIds) {
                                    ContentProviderOperation.Builder builder;

                                    builder = ContentProviderOperation.newUpdate(Schedules.CONTENT_URI)
                                            .withSelection(Schedules.SCHEDULE_ID + " = ?",
                                                    new String[] {String.valueOf(id)})
                                            .withValue(Schedules.IS_DELETED, 1);
                                    mContentProviderOperations.add(builder.build());
                                }

                                for (int id : mDatabaseInformationDeletedIds) {
                                    ContentProviderOperation.Builder builder;

                                    builder = ContentProviderOperation.newUpdate(Information.CONTENT_URI)
                                            .withSelection(Information.INFORMATION_ID + " = ?",
                                                    new String[] {String.valueOf(id)})
                                            .withValue(Information.IS_DELETED, 1);
                                    mContentProviderOperations.add(builder.build());
                                }

                                for (int id : mDatabaseCancelledLectureDeletedIds) {
                                    ContentProviderOperation.Builder builder;

                                    builder = ContentProviderOperation.newUpdate(
                                            ContentUris.withAppendedId(CancelledLectures.CONTENT_URI, id))
                                            .withValue(CancelledLectures.IS_DELETED, 1);
                                    mContentProviderOperations.add(builder.build());
                                }

                                for (int id : mDatabaseClassroomChangeDeletedIds) {
                                    ContentProviderOperation.Builder builder;

                                    builder = ContentProviderOperation.newUpdate(
                                            ContentUris.withAppendedId(ClassroomChanges.CONTENT_URI, id))
                                            .withValue(ClassroomChanges.IS_DELETED, 1);
                                    mContentProviderOperations.add(builder.build());
                                }

                            case PortalService.ACTION_FETCH_SCHEDULE:
                            case PortalService.ACTION_FETCH_INFORMATION:
                                try {
                                    mContentResolver.applyBatch(PortalContract.CONTENT_AUTHORITY, mContentProviderOperations);
                                } catch (RemoteException e) {
                                    status = PortalService.STATUS_FAILURE_IN_DATABASE;

                                    break;
                                } catch (OperationApplicationException e) {
                                    status = PortalService.STATUS_FAILURE_IN_DATABASE;

                                    break;
                                }
                                mContentProviderOperations.clear();

                                status = PortalService.STATUS_SUCCESS;

                                break;

                            default:
                                status = PortalService.STATUS_SUCCESS;

                                break;
                        }

                        break;

                    case PortalClient.FAILURE_UNKNOWN:
                        status = PortalService.STATUS_FAILURE_IN_UNKNOWN;

                        break;

                    case PortalClient.FAILURE_NETWORK:
                        status = PortalService.STATUS_FAILURE_IN_NETWORK;

                        break;

                    case PortalClient.FAILURE_CONTENT:
                        status = PortalService.STATUS_FAILURE_IN_CONTENT;

                        break;

                    default:
                        status = PortalService.STATUS_FAILURE_IN_UNKNOWN;

                        break;
                }

                if (status != PortalService.STATUS_SUCCESS) {
                    if (DEBUG) Log.d(TAG, "run() : Failed request. It's status is " + String.valueOf(status));
                    cancelled = true;
                }
            } else {
                if (DEBUG) Log.d(TAG, "run() : Cancelled request.");
                status = PortalService.STATUS_FAILURE_IN_CANCEL;
            }

            if (request.getId() != PortalService.ID_INTERNAL) {
                size = mRemoteCallbackList.beginBroadcast();
                for (int j = 0; j < size; j++) {
                    try {
                        mRemoteCallbackList.getBroadcastItem(j).onResult(request.getAction(), status);
                    } catch (RemoteException e) {
                        continue;
                    }
                }
                mRemoteCallbackList.finishBroadcast();
            }
        }

        mFetching = false;
        size = mRemoteCallbackList.beginBroadcast();
        for (int j = 0; j < size; j++) {
            try {
                mRemoteCallbackList.getBroadcastItem(j).onResult(PortalService.ACTION_END,
                        cancelled ? PortalService.STATUS_FAILURE : PortalService.STATUS_SUCCESS);
            } catch (RemoteException e) {
                continue;
            }
        }
        mRemoteCallbackList.finishBroadcast();

        mPortalClient.dispose();
    }


    @Override
    public void onLogon(boolean authorized) {
        mAuthorized = authorized;
    }

    @Override
    public void onLogout() {
    }

    @Override
    public void onSchedule(ScheduleContent content, boolean detail) {
        ContentProviderOperation.Builder builder;

        if (mDatabaseScheduleIds.contains(content.getScheduleId())) {
            int index;

            index = mDatabaseScheduleDeletedIds.indexOf(content.getScheduleId());
            if (index >= 0) {
                mDatabaseScheduleDeletedIds.remove(index);
            }
            builder = ContentProviderOperation.newUpdate(Schedules.CONTENT_URI)
                    .withSelection(Schedules.SCHEDULE_ID + " = ?",
                            new String[] { String.valueOf(content.getScheduleId()) });
        } else {
            builder = ContentProviderOperation.newInsert(Schedules.CONTENT_URI)
                    .withValue(Schedules.STUDENT_ID, content.getStudentId())
                    .withValue(Schedules.SCHEDULE_ID, content.getScheduleId())
                    .withValue(Schedules.IS_FETCHED, 0);
        }
        if (!detail) {
            builder = builder.withValue(Schedules.TITLE, content.getTitle())
                    .withValue(Schedules.START_DATE, content.getStartDate())
                    .withValue(Schedules.IS_DELETED, 0);
        } else {
            builder = builder.withValue(Schedules.END_DATE, content.getEndDate())
                    .withValue(Schedules.START_TIME, content.getStartTime())
                    .withValue(Schedules.END_TIME, content.getEndTime())
                    .withValue(Schedules.PRIORITY, content.getPriority())
                    .withValue(Schedules.PLACE, content.getPlace())
                    .withValue(Schedules.CONTENT, content.getContent())
                    .withValue(Schedules.IS_FETCHED, 1);
        }
        mContentProviderOperations.add(builder.build());
    }

    @Override
    public void onInformation(InformationContent content, boolean detail) {
        ContentProviderOperation.Builder builder;

        if (mDatabaseInformationIds.contains(content.getInformationId())) {
            int index;

            index = mDatabaseInformationDeletedIds.indexOf(content.getInformationId());
            if (index >= 0) {
                mDatabaseInformationDeletedIds.remove(index);
            }
            builder = ContentProviderOperation.newUpdate(Information.CONTENT_URI)
                    .withSelection(Information.INFORMATION_ID + " = ?",
                            new String[] { String.valueOf(content.getInformationId()) });
        } else {
            builder = ContentProviderOperation.newInsert(Information.CONTENT_URI)
                    .withValue(Information.INFORMATION_ID, content.getInformationId())
                    .withValue(Information.IS_READ, content.isRead() ? 1 : 0)
                    .withValue(Information.IS_FETCHED, 0);
        }
        if (!detail) {
            builder = builder.withValue(Information.TITLE, content.getTitle())
                    .withValue(Information.PUBLIC_START_DATE, content.getPublicStartDate())
                    .withValue(Information.PUBLIC_PERSON, content.getPublicPerson())
                    .withValue(Information.PRIORITY, content.getPriority())
                    .withValue(Information.CATEGORY, content.getCategory())
                    .withValue(Information.IS_ATTACHED, content.isAttached() ? 1 : 0)
                    .withValue(Information.IS_DELETED, 0);
        } else {
            List<InformationAttachment> attachments;

            builder = builder.withValue(Information.PUBLIC_END_DATE, content.getPublicEndDate())
                    .withValue(Information.CONTENT, content.getContent())
                    .withValue(Information.IS_FETCHED, 1);
            mContentProviderOperations.add(
                    ContentProviderOperation.newDelete(InformationAttachments.CONTENT_URI)
                            .withSelection(InformationAttachments.INFORMATION_ID + " = ?",
                                    new String[] { String.valueOf(content.getInformationId()) }).build());
            attachments = content.getInformationAttachments();
            if (attachments != null) {
                int size;

                size = attachments.size();
                for (int i = 0; i < size; i++) {
                    InformationAttachment attachment;

                    attachment = attachments.get(i);
                    mContentProviderOperations.add(
                            ContentProviderOperation.newInsert(InformationAttachments.CONTENT_URI)
                                    .withValue(InformationAttachments.INFORMATION_ID, content.getInformationId())
                                    .withValue(InformationAttachments.ATTACHMENT_ID, i)
                                    .withValue(InformationAttachments.URL, attachment.getUrl())
                                    .withValue(InformationAttachments.FILENAME, attachment.getFilename())
                                    .withValue(InformationAttachments.DESCRIPTION, attachment.getDescription())
                                    .build());
                }
            }
        }
        mContentProviderOperations.add(builder.build());
    }

    @Override
    public void onCancelledLecture(CancelledLectureContent content) {
        int cancelledLectureId;
        int makeupLectureId;
        ContentProviderOperation.Builder builder;
        int index;

        cancelledLectureId = content.getCancelledLectureId();
        makeupLectureId = content.getMakeupLectureId();
        index = -1;
        if (cancelledLectureId >= 0) {
            index = mDatabaseCancelledLectureCancelledLectureCodes.indexOf(cancelledLectureId);
        }
        if (makeupLectureId >= 0 && index < 0) {
            index = mDatabaseCancelledLectureMakeupLectureCodes.indexOf(makeupLectureId);
        }
        if (index >= 0) {
            int id;

            id = mDatabaseCancelledLectureIds.get(index);
            index = mDatabaseCancelledLectureDeletedIds.indexOf(id);
            if (index >= 0) {
                mDatabaseCancelledLectureDeletedIds.remove(index);
            }

            builder = ContentProviderOperation.newUpdate(
                    ContentUris.withAppendedId(CancelledLectures.CONTENT_URI, id));
        } else {
            builder = ContentProviderOperation.newInsert(CancelledLectures.CONTENT_URI);
        }
        builder = builder.withValue(CancelledLectures.SUBJECT, content.getSubject())
                .withValue(CancelledLectures.PROFESSOR, content.getProfessor())
                .withValue(CancelledLectures.NOTE_1, content.getNote1())
                .withValue(CancelledLectures.NOTE_2, content.getNote2())
                .withValue(CancelledLectures.IS_TAKEN, content.isTaken() ? 1 : 0)
                .withValue(CancelledLectures.IS_DELETED, 0)
                .withValue(CancelledLectures.CANCELLED_LECTURE_ID, cancelledLectureId)
                .withValue(CancelledLectures.MAKEUP_LECTURE_ID, makeupLectureId)
                .withValue(CancelledLectures.CANCELLED_LECTURE_DATE, content.getCancelledLectureDate())
                .withValue(CancelledLectures.CANCELLED_LECTURE_PERIOD, content.getCancelledLecturePeriod())
                .withValue(CancelledLectures.MAKEUP_LECTURE_DATE, content.getMakeupLectureDate())
                .withValue(CancelledLectures.MAKEUP_LECTURE_PERIOD, content.getMakeupLecturePeriod())
                .withValue(CancelledLectures.MAKEUP_LECTURE_CLASSROOM, content.getMakeupLectureClassroom());
        mContentProviderOperations.add(builder.build());
    }

    @Override
    public void onClassroomChange(ClassroomChangeContent content) {
        int lectureId;
        ContentProviderOperation.Builder builder;
        int index;

        lectureId = content.getLectureId();
        index = -1;
        if (lectureId >= 0) {
            index = mDatabaseClassroomChangeLectureIds.indexOf(content.getLectureId());
        }
        if (index >= 0) {
            int id;

            id = mDatabaseClassroomChangeIds.get(index);
            index = mDatabaseClassroomChangeDeletedIds.indexOf(id);
            if (index >= 0) {
                mDatabaseClassroomChangeDeletedIds.remove(index);
            }

            builder = ContentProviderOperation.newUpdate(
                    ContentUris.withAppendedId(ClassroomChanges.CONTENT_URI, id));
        } else {
            builder = ContentProviderOperation.newInsert(ClassroomChanges.CONTENT_URI);
        }
        builder = builder.withValue(ClassroomChanges.SUBJECT, content.getSubject())
                .withValue(ClassroomChanges.PROFESSOR, content.getProfessor())
                .withValue(ClassroomChanges.LECTURE_DATE, content.getDate())
                .withValue(ClassroomChanges.LECTURE_PERIOD, content.getPeriod())
                .withValue(ClassroomChanges.OLD_CLASSROOM, content.getOldClassroom())
                .withValue(ClassroomChanges.NEW_CLASSROOM, content.getNewClassroom())
                .withValue(ClassroomChanges.NOTE_1, content.getNote1())
                .withValue(ClassroomChanges.NOTE_2, content.getNote2())
                .withValue(ClassroomChanges.IS_TAKEN, content.isTaken() ? 1 : 0)
                .withValue(ClassroomChanges.IS_DELETED, 0)
                .withValue(ClassroomChanges.LECTURE_ID, lectureId);
        mContentProviderOperations.add(builder.build());
    }

    public boolean isFetching() {
        return mFetching;
    }

    /**
     * @return remoteCallbackList
     */
    public RemoteCallbackList<IPortalServiceCallback> getRemoteCallbackList() {
        return mRemoteCallbackList;
    }

    /**
     * @param remoteCallbackList セットする remoteCallbackList
     */
    public void setRemoteCallbackList(RemoteCallbackList<IPortalServiceCallback> remoteCallbackList) {
        mRemoteCallbackList = remoteCallbackList;
    }

    /**
     * @return portalServiceRequests
     */
    public List<PortalServiceRequest> getPortalServiceRequests() {
        return mPortalServiceRequests;
    }

    /**
     * @param portalServiceRequests セットする portalServiceRequests
     */
    public void setPortalServiceRequests(LinkedList<PortalServiceRequest> portalServiceRequests) {
        mPortalServiceRequests = portalServiceRequests;
    }

    /**
     * @return contentResolver
     */
    public ContentResolver getContentResolver() {
        return mContentResolver;
    }

    /**
     * @param contentResolver セットする contentResolver
     */
    public void setContentResolver(ContentResolver contentResolver) {
        mContentResolver = contentResolver;
    }
}
