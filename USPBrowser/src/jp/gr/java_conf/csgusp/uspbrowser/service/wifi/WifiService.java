/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.service.wifi;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Wi-Fi サービスの実装を行っているクラスです。
 *
 * @author leak4mk0
 * @version 21
 * @since 19
 */
public class WifiService extends Service {
    private static final String TAG = "WifiService";
    private static final boolean DEBUG = false;

    public static final int ACTION_UNKNOWN = 0;
    public static final int ACTION_CONNECT = 1;
    public static final int ACTION_DISCONNECT = 2;

    public static final int STATUS_UNKNOWN = 0x00000000;
    public static final int STATUS_RUNNING = 0x00010000;
    public static final int STATUS_SUCCESS = 0x00020000;
    public static final int STATUS_SUCCESS_NO_SERVICE = 0x00020001;
    public static final int STATUS_SUCCESS_OTHER_SERVICE = 0x00020002;
    public static final int STATUS_SUCCESS_NO_AUTHENTICATION = 0x00020004;
    public static final int STATUS_FAILURE = 0x00040000;
    public static final int STATUS_FAILURE_IN_UNKNOWN = 0x00040000;
    public static final int STATUS_FAILURE_IN_RUNNING = 0x00040001;
    public static final int STATUS_FAILURE_IN_NETWORK = 0x00040002;
    public static final int STATUS_FAILURE_IN_AUTHORIZATION = 0x00040004;

    private IWifiService.Stub mStub;

    public WifiService() {
        if (DEBUG) Log.v(TAG, "WifiService() : Create an instance of WifiServiceStub.");
        mStub = new WifiServiceStub(this);
    }

    public IBinder onBind(Intent intent) {
        return mStub;
    }
}
