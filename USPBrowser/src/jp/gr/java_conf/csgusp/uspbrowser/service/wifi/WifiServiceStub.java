/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.service.wifi;

import jp.gr.java_conf.csgusp.uspbrowser.service.wifi.IWifiService.Stub;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;

import org.apache.http.conn.ssl.SSLSocketFactory;

import java.io.IOException;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Wi-Fi サービスの機能の定義を行っているクラスです。
 *
 * @author leak4mk0
 * @version 31
 * @since 19
 */
public class WifiServiceStub extends Stub {
    private static final String TAG = "WifiServiceStub";
    private static final boolean DEBUG = false;

    protected WifiService mWifiService;
    protected RemoteCallbackList<IWifiServiceCallback> mRemoteCallbackList;
    protected boolean mConnecting;
    protected boolean mConnected;
    protected boolean mNoService;
    protected boolean mOtherService;
    protected boolean mNoAuthentication;
    protected String[] mSsids;
    protected String[] mKeys;
    protected String mAuthenticationUrl;
    protected String mAuthenticationBody;
    protected String mUserId;
    protected String mPassword;
    protected boolean mForced;

    private static WifiServiceThread sWifiServiceThread = null;

    public WifiServiceStub(WifiService service) {
        mWifiService = service;
        mRemoteCallbackList = new RemoteCallbackList<IWifiServiceCallback>();
    }

    @Override
    public void registerCallback(IWifiServiceCallback callback) {
        if (callback == null) {
            return;
        }

        mRemoteCallbackList.register(callback);
    }

    @Override
    public void unregisterCallback(IWifiServiceCallback callback) {
        if (callback == null) {
            return;
        }

        mRemoteCallbackList.unregister(callback);
    }

    @Override
    public void connect(String ssid, String key,
                        String authenticationUrl, String authenticationBody,
                        String userId, String password, boolean forced) {
        if (DEBUG) Log.d(TAG, "connect()");

        if (mConnecting) {
            return;
        }
        if (sWifiServiceThread == null || sWifiServiceThread.getState() == Thread.State.TERMINATED) {
            sWifiServiceThread = new WifiServiceThread(this);
        }
        if (sWifiServiceThread.getState() == Thread.State.NEW) {
            String[] ssids;
            String[] keys;

            mConnecting = true;
            mConnected = false;

            ssids = ssid.split(",");
            mSsids = new String[ssids.length];
            for (int i = 0; i < ssids.length; i++) {
                mSsids[i] = "\"" + ssids[i] + "\"";
            }
            keys = key.split(",");
            mKeys = new String[ssids.length];
            for (int i = 0; i < ssids.length; i++) {
                mKeys[i] = "\"" + keys[i < keys.length ? i : 0] + "\"";
            }
            mAuthenticationUrl = authenticationUrl;
            mAuthenticationBody = authenticationBody;
            mUserId = userId;
            mPassword = password;
            mForced = forced;
            sWifiServiceThread.start();
        } else {
            int size;

            size = mRemoteCallbackList.beginBroadcast();
            for (int j = 0; j < size; j++) {
                try {
                    mRemoteCallbackList.getBroadcastItem(j).onResult(
                            WifiService.ACTION_CONNECT, WifiService.STATUS_FAILURE_IN_RUNNING);
                } catch (RemoteException ignored) {
                }
            }
            mRemoteCallbackList.finishBroadcast();
        }
    }

    @Override
    public void disconnect(String ssid) {
        if (DEBUG) Log.d(TAG, "disconnect()");

        if (mConnecting) {
            return;
        }
        if (sWifiServiceThread == null || sWifiServiceThread.getState() == Thread.State.TERMINATED) {
            sWifiServiceThread = new WifiServiceThread(this);
        }
        if (sWifiServiceThread.getState() == Thread.State.NEW) {
            String[] ssids;

            mConnecting = true;
            mConnected = true;

            ssids = ssid.split(",");
            mSsids = new String[ssids.length];
            for (int i = 0; i < ssids.length; i++) {
                mSsids[i] = "\"" + ssids[i] + "\"";
            }
            sWifiServiceThread.start();
        } else {
            int size;

            size = mRemoteCallbackList.beginBroadcast();
            for (int j = 0; j < size; j++) {
                try {
                    mRemoteCallbackList.getBroadcastItem(j).onResult(
                            WifiService.ACTION_DISCONNECT, WifiService.STATUS_FAILURE_IN_RUNNING);
                } catch (RemoteException ignored) {
                }
            }
            mRemoteCallbackList.finishBroadcast();
        }
    }

    @Override
    public boolean isConnecting() {
        return mConnecting;
    }

    @Override
    public boolean isConnected() {
        return mConnected;
    }

    @Override
    public boolean isNoService() {
        return mNoService;
    }

    @Override
    public boolean isOtherService() {
        return mOtherService;
    }

    @Override
    public boolean isNoAuthentication() {
        return mNoAuthentication;
    }
}
