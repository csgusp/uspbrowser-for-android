/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.service.wifi;

import jp.gr.java_conf.csgusp.uspbrowser.util.AllSSLSocketFactory;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.RemoteException;
import android.text.format.DateFormat;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.List;

/**
 * Wi-Fi サービスの処理スレッドを実装したクラスです。
 *
 * @author leak4mk0
 * @version 33
 * @since 19
 */
class WifiServiceThread extends Thread {
    private static final String TAG = "WifiServiceThread";
    private static final boolean DEBUG = false;

    public static final int CONNECTION_TIMEOUT = 30000;
    public static final int CONNECTION_SLEEP = 100;

    public static final int AUTHENTICATION_TIMEOUT = 10000;

    public static final String AUTHENTICATION_BODY_USER_ID_TEXT = "%USER_ID%";
    public static final String AUTHENTICATION_BODY_PASSWORD_TEXT = "%PASSWORD%";

    private WifiServiceStub mWifiServiceStub;

    public WifiServiceThread(WifiServiceStub stub) {
        mWifiServiceStub = stub;
    }

    public void run() {
        if (!mWifiServiceStub.mConnected) {
            int size;
            int status;

            // Connecting
            if (DEBUG) Log.d(TAG, "run() : Begin connecting.");

            size = mWifiServiceStub.mRemoteCallbackList.beginBroadcast();
            for (int j = 0; j < size; j++) {
                try {
                    mWifiServiceStub.mRemoteCallbackList.getBroadcastItem(j).onResult(
                            WifiService.ACTION_CONNECT, WifiService.STATUS_RUNNING);
                } catch (RemoteException ignored) {
                }
            }
            mWifiServiceStub.mRemoteCallbackList.finishBroadcast();

            if (!createNetwork()) {
                status = WifiService.STATUS_FAILURE_IN_NETWORK;
            } else if (!connectToNetwork()) {
                status = WifiService.STATUS_FAILURE_IN_NETWORK;
            } else if (!loginToNetwork()) {
                status = WifiService.STATUS_FAILURE_IN_AUTHORIZATION;
            } else {
                if (mWifiServiceStub.mNoService) {
                    status = WifiService.STATUS_SUCCESS_NO_SERVICE;
                } else if (mWifiServiceStub.mOtherService) {
                    status = WifiService.STATUS_SUCCESS_OTHER_SERVICE;
                } else if (mWifiServiceStub.mNoAuthentication) {
                    status = WifiService.STATUS_SUCCESS_NO_AUTHENTICATION;
                } else {
                    status = WifiService.STATUS_SUCCESS;
                }
                mWifiServiceStub.mConnected = true;
            }

            mWifiServiceStub.mConnecting = false;

            size = mWifiServiceStub.mRemoteCallbackList.beginBroadcast();
            for (int j = 0; j < size; j++) {
                try {
                    mWifiServiceStub.mRemoteCallbackList.getBroadcastItem(j).onResult(
                            WifiService.ACTION_CONNECT, status);
                } catch (RemoteException ignored) {
                }
            }
            mWifiServiceStub.mRemoteCallbackList.finishBroadcast();

            if (DEBUG) Log.d(TAG, "run() : Finish connecting.");
        } else {
            int size;
            int status;

            // Disconnecting
            if (DEBUG) Log.d(TAG, "run() : Start disconnecting.");

            size = mWifiServiceStub.mRemoteCallbackList.beginBroadcast();
            for (int j = 0; j < size; j++) {
                try {
                    mWifiServiceStub.mRemoteCallbackList.getBroadcastItem(j).onResult(
                            WifiService.ACTION_DISCONNECT, WifiService.STATUS_RUNNING);
                } catch (RemoteException ignored) {
                }
            }
            mWifiServiceStub.mRemoteCallbackList.finishBroadcast();

            if (!disconnectToNetwork()) {
                status = WifiService.STATUS_FAILURE_IN_NETWORK;
            } else if (!destroyNetwork()) {
                status = WifiService.STATUS_FAILURE_IN_NETWORK;
            } else {
                status = WifiService.STATUS_SUCCESS;
                mWifiServiceStub.mConnected = false;
            }

            mWifiServiceStub.mConnecting = false;

            size = mWifiServiceStub.mRemoteCallbackList.beginBroadcast();
            for (int j = 0; j < size; j++) {
                try {
                    mWifiServiceStub.mRemoteCallbackList.getBroadcastItem(j).onResult(
                            WifiService.ACTION_DISCONNECT, status);
                } catch (RemoteException ignored) {
                }
            }
            mWifiServiceStub.mRemoteCallbackList.finishBroadcast();

            if (DEBUG) Log.d(TAG, "run() : Finish disconnecting.");
        }
    }

    private boolean createNetwork() {
        WifiManager wifiManager;
        List<WifiConfiguration> configurations;

        if (DEBUG) Log.d(TAG, "createNetwork()");
        wifiManager = (WifiManager) mWifiServiceStub.mWifiService.getSystemService(Context.WIFI_SERVICE);

        if (mWifiServiceStub.mForced) {
            long timeout;

            if (!wifiManager.isWifiEnabled()) {
                wifiManager.setWifiEnabled(true);
            }
            timeout = System.currentTimeMillis() + CONNECTION_TIMEOUT;
            while (timeout > System.currentTimeMillis()) {
                if (wifiManager.isWifiEnabled()) {
                    break;
                }
                try {
                    Thread.sleep(CONNECTION_SLEEP);
                } catch (InterruptedException ignored) {
                }
            }
            if (timeout <= System.currentTimeMillis()) {
                return false;
            }
            if (!wifiManager.isWifiEnabled()) {
                return false;
            }
        } else {
            if (!wifiManager.isWifiEnabled()) {
                return true;
            }
        }

        configurations = wifiManager.getConfiguredNetworks();
        for (int i = 0; i < mWifiServiceStub.mSsids.length; i++) {
            boolean existed;

            existed = false;
            for (WifiConfiguration configuration : configurations) {
                if (configuration.SSID.equals(mWifiServiceStub.mSsids[i])) {
                    existed = true;

                    break;
                }
            }
            if (!existed) {
                WifiConfiguration configuration;

                configuration = new WifiConfiguration();
                configuration.SSID = mWifiServiceStub.mSsids[i];
                if (mWifiServiceStub.mKeys[i].equals("\"\"")) {
                    configuration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                } else {
                    configuration.preSharedKey = mWifiServiceStub.mKeys[i];
                    configuration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                    configuration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                    configuration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                    configuration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                    configuration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                }
                wifiManager.addNetwork(configuration);
                wifiManager.enableNetwork(configuration.networkId, false);
                wifiManager.saveConfiguration();
            }
        }

        return true;
    }

    private boolean connectToNetwork() {
        WifiManager wifiManager;
        String currentSsid;
        String targetSsid;
        List<ScanResult> results;
        boolean connected;
        int maxLevel;
        long timeout;

        if (DEBUG) Log.d(TAG, "connectToNetwork()");
        wifiManager = (WifiManager) mWifiServiceStub.mWifiService.getSystemService(Context.WIFI_SERVICE);

        if (!wifiManager.isWifiEnabled()) {
            mWifiServiceStub.mNoService = true;
            mWifiServiceStub.mOtherService = false;

            return true;
        }

        currentSsid = wifiManager.getConnectionInfo().getSSID();
        currentSsid = currentSsid != null ? currentSsid : "";
        currentSsid = currentSsid.startsWith("\"") && currentSsid.endsWith("\"") ?
                currentSsid : "\"" + currentSsid + "\"";
        connected = false;
        for (String ssid : mWifiServiceStub.mSsids) {
            if (ssid.equals(currentSsid)) {
                connected = true;

                break;
            }
        }
        if (connected) {
            mWifiServiceStub.mNoService = false;
            mWifiServiceStub.mOtherService = false;

            return true;
        }

        // Scanning
        if (mWifiServiceStub.mForced) {
            WifiReceiver receiver;

            receiver = new WifiReceiver();
            mWifiServiceStub.mWifiService.registerReceiver(
                    receiver,
                    new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
            wifiManager.startScan();
            timeout = System.currentTimeMillis() + CONNECTION_TIMEOUT;
            while (timeout > System.currentTimeMillis()) {
                if (receiver.isScanned()) {
                    break;
                }
                try {
                    Thread.sleep(CONNECTION_SLEEP);
                } catch (InterruptedException ignored) {
                }
            }
            mWifiServiceStub.mWifiService.unregisterReceiver(receiver);
            if (timeout <= System.currentTimeMillis()) {
                return false;
            }
        }

        // Selecting
        results = wifiManager.getScanResults();
        targetSsid = null;
        maxLevel = Integer.MIN_VALUE;
        for (ScanResult result : results) {
            for (String ssid : mWifiServiceStub.mSsids) {
                String foundSsid;

                foundSsid = result.SSID.startsWith("\"") && result.SSID.endsWith("\"") ?
                        result.SSID : "\"" + result.SSID + "\"";
                if (ssid.equals(foundSsid)) {
                    if (maxLevel < result.level) {
                        targetSsid = foundSsid;
                        maxLevel = result.level;
                    }

                    break;
                }
            }
        }
        if (targetSsid == null) {
            mWifiServiceStub.mNoService = true;
            mWifiServiceStub.mOtherService = false;

            return true;
        }

        // Switching
        mWifiServiceStub.mNoService = false;
        if (mWifiServiceStub.mForced) {
            List<WifiConfiguration> configurations;
            WifiConfiguration targetConfiguration;
            int maxPriority;

            mWifiServiceStub.mOtherService = false;
            configurations = wifiManager.getConfiguredNetworks();
            targetConfiguration = null;
            maxPriority = -1;
            for (WifiConfiguration configuration : configurations) {
                if (configuration.SSID.equals(targetSsid)) {
                    targetConfiguration = configuration;
                }
                if (maxPriority < configuration.priority) {
                    maxPriority = configuration.priority;
                }
            }
            if (targetConfiguration != null) {
                if (maxPriority >= 0) {
                    targetConfiguration.priority = maxPriority + 1;
                    wifiManager.updateNetwork(targetConfiguration);
                }
                wifiManager.enableNetwork(targetConfiguration.networkId, true);
            }

            timeout = System.currentTimeMillis() + CONNECTION_TIMEOUT;
            while (timeout > System.currentTimeMillis()) {
                currentSsid = wifiManager.getConnectionInfo().getSSID();
                currentSsid = currentSsid != null ? currentSsid : "";
                currentSsid = currentSsid.startsWith("\"") && currentSsid.endsWith("\"") ?
                        currentSsid : "\"" + currentSsid + "\"";
                if (targetSsid.equals(currentSsid)) {
                    break;
                }
                try {
                    Thread.sleep(CONNECTION_SLEEP);
                } catch (InterruptedException ignored) {
                }
            }

            for (WifiConfiguration configuration : configurations) {
                wifiManager.enableNetwork(configuration.networkId, false);
            }
            wifiManager.saveConfiguration();

            if (timeout <= System.currentTimeMillis()) {
                return false;
            }
        } else {
            mWifiServiceStub.mOtherService = !currentSsid.equals("\"\"");
        }

        return true;
    }

    private boolean loginToNetwork() {
        WifiManager wifiManager;
        ConnectivityManager connectivityManager;
        String currentSsid;
        boolean connected;
        HttpParams params;
        SchemeRegistry registry;
        ThreadSafeClientConnManager connManager;
        HttpClient client;
        HttpPost post;
        String body;
        HttpResponse response;
        long timeout;

        if (DEBUG) Log.d(TAG, "loginToNetwork()");
        wifiManager = (WifiManager) mWifiServiceStub.mWifiService.getSystemService(Context.WIFI_SERVICE);
        connectivityManager = (ConnectivityManager) mWifiServiceStub.mWifiService.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (!wifiManager.isWifiEnabled()) {
            return true;
        }
        currentSsid = wifiManager.getConnectionInfo().getSSID();
        currentSsid = currentSsid != null ? currentSsid : "";
        currentSsid = currentSsid.startsWith("\"") && currentSsid.endsWith("\"") ?
                currentSsid : "\"" + currentSsid + "\"";
        connected = false;
        for (String ssid : mWifiServiceStub.mSsids) {
            if (ssid.equals(currentSsid)) {
                connected = true;

                break;
            }
        }
        if (!connected) {
            return true;
        }

        timeout = System.currentTimeMillis() + CONNECTION_TIMEOUT;
        while (timeout > System.currentTimeMillis()) {
            NetworkInfo networkInfo;

            networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (networkInfo != null && networkInfo.isConnected()) {
                break;
            }
            try {
                Thread.sleep(CONNECTION_SLEEP);
            } catch (InterruptedException ignored) {
            }
        }
        if (timeout <= System.currentTimeMillis()) {
            return false;
        }

        params = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(params, AUTHENTICATION_TIMEOUT);
        HttpConnectionParams.setSoTimeout(params, AUTHENTICATION_TIMEOUT);
        registry = new SchemeRegistry();
        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        registry.register(new Scheme("https", AllSSLSocketFactory.getSocketFactory(), 443));
        connManager = new ThreadSafeClientConnManager(params, registry);
        client = new DefaultHttpClient(connManager, params);

        post = new HttpPost();
        post.setURI(URI.create(mWifiServiceStub.mAuthenticationUrl));
        post.setHeader("Content-Type", "application/x-www-form-urlencoded");
        body = mWifiServiceStub.mAuthenticationBody;
        body = body.replace(AUTHENTICATION_BODY_USER_ID_TEXT, mWifiServiceStub.mUserId);
        body = body.replace(AUTHENTICATION_BODY_PASSWORD_TEXT, mWifiServiceStub.mPassword);
        try {
            post.setEntity(new StringEntity(body));
        } catch (UnsupportedEncodingException e) {
            return false;
        }
        response = null;
        timeout = System.currentTimeMillis() + CONNECTION_TIMEOUT;
        while (timeout > System.currentTimeMillis()) {
            try {
                response = client.execute(post);
            } catch (IOException ignored) {
            } catch (IllegalStateException e) {
                break;
            }
            if (response != null) {
                break;
            }
            try {
                Thread.sleep(CONNECTION_SLEEP);
            } catch (InterruptedException ignored) {
            }
        }
        mWifiServiceStub.mNoAuthentication = response == null;
        if (DEBUG) {
            String path;
            path = Environment.getExternalStorageDirectory().getPath() + "/login-" +
                    DateFormat.format("yyMMddkkmmss", System.currentTimeMillis()) + ".html";

            if (response != null) {
                HttpEntity entity;
                InputStream input;
                OutputStream output;
                byte[] buffer = new byte[256];
                int size;

                entity = response.getEntity();
                try {
                    input = entity.getContent();
                    output = new FileOutputStream(path);
                    while (-1 != (size = input.read(buffer))) {
                        output.write(buffer, 0, size);
                    }
                    input.close();
                    output.close();
                } catch (IOException ignored) {
                }
            } else {
                OutputStream output;
                try {
                    output = new FileOutputStream(path.replace(".html", ".txt"));
                    output.write("Failed".getBytes());
                    output.close();
                } catch (IOException ignored) {
                }
            }
        }

        return true;
    }

    private boolean disconnectToNetwork() {
        WifiManager wifiManager;
        List<WifiConfiguration> configurations;
        long timeout;

        if (DEBUG) Log.d(TAG, "disconnectToNetwork()");
        wifiManager = (WifiManager) mWifiServiceStub.mWifiService.getSystemService(Context.WIFI_SERVICE);

        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
        timeout = System.currentTimeMillis() + CONNECTION_TIMEOUT;
        while (timeout > System.currentTimeMillis()) {
            if (wifiManager.isWifiEnabled()) {
                break;
            }
            try {
                Thread.sleep(CONNECTION_SLEEP);
            } catch (InterruptedException ignored) {
            }
        }
        if (timeout <= System.currentTimeMillis()) {
            return false;
        }

        configurations = wifiManager.getConfiguredNetworks();
        for (WifiConfiguration configuration : configurations) {
            for (String ssid : mWifiServiceStub.mSsids) {
                if (ssid.equals(configuration.SSID)) {
                    wifiManager.disableNetwork(configuration.networkId);

                    break;
                }
            }
        }
        wifiManager.saveConfiguration();

        timeout = System.currentTimeMillis() + CONNECTION_TIMEOUT;
        while (timeout > System.currentTimeMillis()) {
            String currentSsid;
            boolean connected;

            currentSsid = wifiManager.getConnectionInfo().getSSID();
            currentSsid = currentSsid != null ? currentSsid : "";
            currentSsid = currentSsid.startsWith("\"") && currentSsid.endsWith("\"") ?
                    currentSsid : "\"" + currentSsid + "\"";
            connected = false;
            for (String ssid : mWifiServiceStub.mSsids) {
                if (ssid.equals(currentSsid)) {
                    connected = true;

                    break;
                }
            }
            if (!connected) {
                break;
            }
            try {
                Thread.sleep(CONNECTION_SLEEP);
            } catch (InterruptedException ignored) {
            }
        }
        if (timeout <= System.currentTimeMillis()) {
            return false;
        }

        return true;
    }

    private boolean destroyNetwork() {
        WifiManager wifiManager;

        if (DEBUG) Log.d(TAG, "destroyNetwork()");
        wifiManager = (WifiManager) mWifiServiceStub.mWifiService.getSystemService(Context.WIFI_SERVICE);

        if (wifiManager.isWifiEnabled()) {
            List<WifiConfiguration> configurations;

            configurations = wifiManager.getConfiguredNetworks();
            for (WifiConfiguration configuration : configurations) {
                for (String ssid : mWifiServiceStub.mSsids) {
                    if (ssid.equals(configuration.SSID)) {
                        wifiManager.removeNetwork(configuration.networkId);

                        break;
                    }
                }
                wifiManager.saveConfiguration();
            }
        }

        return true;
    }

    private static class WifiReceiver extends BroadcastReceiver {
        private boolean mScanned;

        @Override
        public void onReceive(Context context, Intent intent) {
            String action;

            action = intent.getAction();
            if (action != null && action.equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                mScanned = true;
            }
        }

        public boolean isScanned() {
            return mScanned;
        }
    }
}
