/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.preference.BrowserPreferences;
import jp.gr.java_conf.csgusp.uspbrowser.service.access.AccessService;
import jp.gr.java_conf.csgusp.uspbrowser.service.access.IAccessService;
import jp.gr.java_conf.csgusp.uspbrowser.service.access.IAccessServiceCallback;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.IPortalService;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.IPortalServiceCallback;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.PortalService;
import jp.gr.java_conf.csgusp.uspbrowser.service.wifi.IWifiService;
import jp.gr.java_conf.csgusp.uspbrowser.service.wifi.IWifiServiceCallback;
import jp.gr.java_conf.csgusp.uspbrowser.ui.MenuListFragment.MenuListAdapter;
import jp.gr.java_conf.csgusp.uspbrowser.ui.MenuListFragment.MenuListItem;
import jp.gr.java_conf.csgusp.uspbrowser.ui.access.BusListFragment;
import jp.gr.java_conf.csgusp.uspbrowser.ui.access.TrainListFragment;
import jp.gr.java_conf.csgusp.uspbrowser.ui.link.BrowserFragment;
import jp.gr.java_conf.csgusp.uspbrowser.ui.portal.CancelledLectureListFragment;
import jp.gr.java_conf.csgusp.uspbrowser.ui.portal.ClassroomChangeListFragment;
import jp.gr.java_conf.csgusp.uspbrowser.ui.portal.InformationDetailsActivity;
import jp.gr.java_conf.csgusp.uspbrowser.ui.portal.InformationListFragment;
import jp.gr.java_conf.csgusp.uspbrowser.ui.portal.ScheduleDetailsActivity;
import jp.gr.java_conf.csgusp.uspbrowser.ui.portal.ScheduleListFragment;
import jp.gr.java_conf.csgusp.uspbrowser.ui.setup.SetupActivity;
import jp.gr.java_conf.csgusp.uspbrowser.ui.wifi.ConnectionFragment;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * ブラウザーのアクティビティを実装したクラス。
 *
 * @author leak4mk0
 * @version 35
 * @see android.app.Activity
 * @since 1
 */
public class BrowserActivity extends SherlockFragmentActivity
        implements MenuListFragment.OnCreateMenuListListener, MenuListFragment.OnMenuListItemSelectedListener,
        DocumentFragment.OnLicenseResultListener, DocumentFragment.OnDocumentResultListener,
        BrowserPreferences.OnBrowserPreferenceChangeListener, ScheduleListFragment.OnScheduleListItemClickListener,
        InformationListFragment.OnInformationListItemClickListener {
    private static final String TAG_DOCUMENT_FRAGMENT = "document_fragment";

    private static final int WHAT_BROWSER_HANDLER_PORTAL_SERVICE = 1;
    private static final int WHAT_BROWSER_HANDLER_ACCESS_SERVICE = 2;
    private static final int WHAT_BROWSER_HANDLER_WIFI_SERVICE = 3;
    private static final int WHAT_BROWSER_HANDLER_TIPS = 4;

    private SlidingMenu mSlidingMenu;
    private int mCurrentCheckedId;
    private BrowserPreferences mBrowserPreferences;
    private IPortalService mPortalService;
    private PortalServiceConnection mPortalServiceConnection;
    private PortalServiceCallback mPortalServiceCallback;
    private IAccessService mAccessService;
    private AccessServiceConnection mAccessServiceConnection;
    private AccessServiceCallback mAccessServiceCallback;
    private IWifiService mWifiService;
    private WifiServiceConnection mWifiServiceConnection;
    private WifiServiceCallback mWifiServiceCallback;
    private BrowserHandler mBrowserHandler;
    private Timer mTipsTimer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar;
        Fragment fragment;
        FragmentTransaction transaction;
        int page;

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.browser_activity);
        setSupportProgressBarIndeterminateVisibility(false);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mSlidingMenu = new SlidingMenu(this);
        mSlidingMenu.setBehindWidthRes(R.dimen.sliding_menu_behind_width);
        mSlidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        mSlidingMenu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
        mSlidingMenu.setMenu(R.layout.browser_menu_fragment);

        transaction = getSupportFragmentManager().beginTransaction();
        fragment = new MenuListFragment();
        transaction.replace(R.id.details_frame_layout, fragment);
        transaction.commit();

        mBrowserPreferences = BrowserPreferences.getInstance(this);

        mPortalServiceConnection = new PortalServiceConnection();
        mPortalServiceCallback = new PortalServiceCallback();
        mAccessServiceConnection = new AccessServiceConnection();
        mAccessServiceCallback = new AccessServiceCallback();
        mWifiServiceConnection = new WifiServiceConnection();
        mWifiServiceCallback = new WifiServiceCallback();
        mBrowserHandler = new BrowserHandler(this);
        bindService(new Intent(IPortalService.class.getName()), mPortalServiceConnection, BIND_AUTO_CREATE);
        bindService(new Intent(IAccessService.class.getName()), mAccessServiceConnection, BIND_AUTO_CREATE);
        bindService(new Intent(IWifiService.class.getName()), mWifiServiceConnection, BIND_AUTO_CREATE);

        if (!mBrowserPreferences.getBrowserSetup()) {
            startActivity(new Intent(this, SetupActivity.class));
            finish();
            return;
        }

        page = mBrowserPreferences.getBrowserHomePage();
        if (page == BrowserPreferences.PAGE_LAST_PAGE) {
            page = mBrowserPreferences.getBrowserLastPage();
        }
        switch (page) {
            case BrowserPreferences.PAGE_PORTAL_SCHEDULES:
                mCurrentCheckedId = R.id.portal_schedules_item;

                break;

            case BrowserPreferences.PAGE_PORTAL_INFORMATION:
                mCurrentCheckedId = R.id.portal_information_item;

                break;

            case BrowserPreferences.PAGE_PORTAL_CANCELLED_LECTURES:
                mCurrentCheckedId = R.id.portal_cancelled_lectures_item;

                break;

            case BrowserPreferences.PAGE_PORTAL_CLASSROOM_CHANGES:
                mCurrentCheckedId = R.id.portal_classroom_changes_item;

                break;

            case BrowserPreferences.PAGE_ACCESS_TRAINS_INBOUND:
                mCurrentCheckedId = R.id.access_trains_inbound_item;

                break;

            case BrowserPreferences.PAGE_ACCESS_TRAINS_OUTBOUND:
                mCurrentCheckedId = R.id.access_trains_outbound_item;

                break;

            case BrowserPreferences.PAGE_ACCESS_BUSES_INBOUND:
                mCurrentCheckedId = R.id.access_buses_inbound_item;

                break;

            case BrowserPreferences.PAGE_ACCESS_BUSES_OUTBOUND:
                mCurrentCheckedId = R.id.access_buses_outbound_item;

                break;

            case BrowserPreferences.PAGE_WIFI_CONNECTION:
                mCurrentCheckedId = R.id.wifi_connection_item;

                break;

            case BrowserPreferences.PAGE_LINKS_UNIVERSITY:
                mCurrentCheckedId = R.id.links_university_item;

                break;

            case BrowserPreferences.PAGE_LINKS_WEATHER:
                mCurrentCheckedId = R.id.links_weather_item;

                break;
        }
        loadBrowser();
    }

    @Override
    public void onResume() {
        super.onResume();

        mBrowserPreferences.registerOnBrowserPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        int page;

        super.onPause();

        mBrowserPreferences.registerOnBrowserPreferenceChangeListener(this);

        switch (mCurrentCheckedId) {
            case R.id.portal_schedules_item:
                page = BrowserPreferences.PAGE_PORTAL_SCHEDULES;

                break;

            case R.id.portal_information_item:
                page = BrowserPreferences.PAGE_PORTAL_INFORMATION;

                break;

            case R.id.portal_cancelled_lectures_item:
                page = BrowserPreferences.PAGE_PORTAL_CANCELLED_LECTURES;

                break;

            case R.id.portal_classroom_changes_item:
                page = BrowserPreferences.PAGE_PORTAL_CLASSROOM_CHANGES;

                break;

            case R.id.access_trains_inbound_item:
                page = BrowserPreferences.PAGE_ACCESS_TRAINS_INBOUND;

                break;

            case R.id.access_trains_outbound_item:
                page = BrowserPreferences.PAGE_ACCESS_TRAINS_OUTBOUND;

                break;

            case R.id.access_buses_inbound_item:
                page = BrowserPreferences.PAGE_ACCESS_BUSES_INBOUND;

                break;

            case R.id.access_buses_outbound_item:
                page = BrowserPreferences.PAGE_ACCESS_BUSES_OUTBOUND;

                break;

            case R.id.wifi_connection_item:
                page = BrowserPreferences.PAGE_WIFI_CONNECTION;

                break;

            case R.id.links_university_item:
                page = BrowserPreferences.PAGE_LINKS_UNIVERSITY;

                break;

            case R.id.links_weather_item:
                page = BrowserPreferences.PAGE_LINKS_WEATHER;

                break;

            default:
                page = 0;
        }

        if (page > 0) {
            mBrowserPreferences.setBrowserLastPage(page);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mTipsTimer != null) {
            mTipsTimer.cancel();
            mTipsTimer = null;
        }
        if (mPortalService != null) {
            try {
                mPortalService.cancelRequest(PortalService.ID_ALL);
                mPortalService.unregisterCallback(mPortalServiceCallback);
            } catch (RemoteException ignored) {
            }
        }
        unbindService(mPortalServiceConnection);
        if (mAccessService != null) {
            try {
                mAccessService.cancelRequest(AccessService.ID_ALL);
                mAccessService.unregisterCallback(mAccessServiceCallback);
            } catch (RemoteException ignored) {
            }
        }
        unbindService(mAccessServiceConnection);
        if (mWifiService != null) {
            try {
                mWifiService.unregisterCallback(mWifiServiceCallback);
            } catch (RemoteException ignored) {
            }
        }
        unbindService(mWifiServiceConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.browser, menu);

        return true;
    }

    @Override
    public void onCreateMenuList(MenuListAdapter list) {
        list.addCategory(R.string.title_portal, 0);
        list.addItem(R.string.title_portal_schedules, R.drawable.ic_menu_schedules, R.id.portal_schedules_item, true);
        list.addItem(R.string.title_portal_information, R.drawable.ic_menu_information,
                R.id.portal_information_item, true);
        list.addItem(R.string.title_portal_cancelled_lectures, R.drawable.ic_menu_cancelled_lectures,
                R.id.portal_cancelled_lectures_item, true);
        list.addItem(R.string.title_portal_classroom_changes, R.drawable.ic_menu_classroom_changes,
                R.id.portal_classroom_changes_item, true);
        list.addItem(R.string.title_open_in_browser, R.drawable.ic_menu_open_in_broswer,
                R.id.portal_open_in_browser_item, false);
        list.addCategory(R.string.title_access, 0);
        list.addItem(R.string.title_access_trains_inbound, R.drawable.ic_menu_train,
                R.id.access_trains_inbound_item, true);
        list.addItem(R.string.title_access_trains_outbound, R.drawable.ic_menu_train,
                R.id.access_trains_outbound_item, true);
        list.addItem(R.string.title_access_buses_inbound, R.drawable.ic_menu_bus,
                R.id.access_buses_inbound_item, true);
        list.addItem(R.string.title_access_buses_outbound, R.drawable.ic_menu_bus,
                R.id.access_buses_outbound_item, true);
        list.addCategory(R.string.title_wifi, 0);
        list.addItem(R.string.title_wifi_connection, R.drawable.ic_menu_wifi_connection,
                R.id.wifi_connection_item, true);
        list.addCategory(R.string.title_links, 0);
        list.addItem(R.string.title_links_university, R.drawable.ic_menu_links,
                R.id.links_university_item, true);
        list.addItem(R.string.title_links_weather, R.drawable.ic_menu_links,
                R.id.links_weather_item, true);
        list.checkById(mCurrentCheckedId, true);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem refreshItem;
        MenuItem cancelItem;
        boolean running;

        super.onPrepareOptionsMenu(menu);

        refreshItem = menu.findItem(R.id.menu_refresh);
        cancelItem = menu.findItem(R.id.menu_cancel);

        switch (mCurrentCheckedId) {
            case R.id.portal_schedules_item:
            case R.id.portal_information_item:
            case R.id.portal_cancelled_lectures_item:
            case R.id.portal_classroom_changes_item:
            case R.id.access_trains_inbound_item:
            case R.id.access_trains_outbound_item:
            case R.id.access_buses_inbound_item:
            case R.id.access_buses_outbound_item:
                running = false;
                if (mPortalService != null) {
                    try {
                        running = mPortalService.isRunning();
                    } catch (RemoteException ignored) {
                    }
                }
                if (mAccessService != null && !running) {
                    try {
                        running = mAccessService.isRunning();
                    } catch (RemoteException ignored) {
                    }
                }
                refreshItem.setVisible(!running);
                cancelItem.setVisible(running);

                break;

            case R.id.wifi_connection_item:
            case R.id.links_university_item:
            case R.id.links_weather_item:
                refreshItem.setVisible(false);
                cancelItem.setVisible(false);

                break;
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        PackageInfo packageInfo;
        boolean ret;

        switch (item.getItemId()) {
            case android.R.id.home:
                mSlidingMenu.toggle();
                ret = true;

                break;

            case R.id.menu_refresh:
                try {
                    String userId;
                    String password;

                    userId = mBrowserPreferences.getSystemUserId();
                    password = mBrowserPreferences.getSystemPassword();
                    mPortalService.requestLogon(userId, password);
                    mPortalService.requestFetchTop();
                    if (mBrowserPreferences.getPortalFetchContentsAutomatically()) {
                        mPortalService.requestFetchContents();
                    }
                    mPortalService.requestLogout();

                    mAccessService.requestFetchTrainTimetable();
                    mAccessService.requestFetchBusTimetable();
                } catch (RemoteException ignored) {
                }
                ret = true;

                break;

            case R.id.menu_cancel:
                try {
                    mPortalService.cancelRequest(PortalService.ID_ALL);
                    mAccessService.cancelRequest(AccessService.ID_ALL);
                } catch (RemoteException ignored) {
                }
                ret = true;

                break;

            case R.id.menu_settings:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                ret = true;

                break;

            case R.id.menu_feedback:
                intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(getString(R.string.url_email)));
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback_subject));
                try {
                    packageInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
                } catch (NameNotFoundException e) {
                    packageInfo = null;
                }
                if (packageInfo != null) {
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.feedback_text,
                            Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE, packageInfo.versionName, packageInfo.versionCode));
                } else {
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.feedback_text,
                            Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE, R.string.unknown, -1));
                }
                if (getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
                ret = true;

                break;

            case R.id.menu_help:
                startActivity(new Intent(getApplicationContext(), HelpActivity.class));
                ret = true;

                break;

            default:
                ret = super.onOptionsItemSelected(item);
        }

        return ret;
    }

    @Override
    public void onMenuListItemSelected(MenuListItem item) {
        Fragment fragment;
        Intent intent;

        if (item.isChecked()) {
            mCurrentCheckedId = item.getItemId();
        }
        fragment = null;
        intent = null;
        switch (item.getItemId()) {
            case R.id.portal_schedules_item:
                fragment = ScheduleListFragment.newInstance();

                break;

            case R.id.portal_information_item:
                fragment = InformationListFragment.newInstance();

                break;
            case R.id.portal_cancelled_lectures_item:
                fragment = CancelledLectureListFragment.newInstance();

                break;

            case R.id.portal_classroom_changes_item:
                fragment = ClassroomChangeListFragment.newInstance();

                break;

            case R.id.portal_open_in_browser_item:
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.url_portal)));

                break;

            case R.id.access_trains_inbound_item:
                fragment = TrainListFragment.newInstance(true);

                break;

            case R.id.access_trains_outbound_item:
                fragment = TrainListFragment.newInstance(false);

                break;

            case R.id.access_buses_inbound_item:
                fragment = BusListFragment.newInstance(true);

                break;

            case R.id.access_buses_outbound_item:
                fragment = BusListFragment.newInstance(false);

                break;

            case R.id.wifi_connection_item:
                fragment = ConnectionFragment.newInstance();

                break;

            case R.id.links_university_item:
                fragment = BrowserFragment.newInstance(getString(R.string.url_university));

                break;

            case R.id.links_weather_item:
                fragment = BrowserFragment.newInstance(getString(R.string.url_weather));

                break;
        }

        if (fragment != null) {
            FragmentTransaction transaction;

            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.list_frame_layout, fragment);
            transaction.commit();
        }
        if (intent != null) {
            PackageManager manager;
            ComponentName name;

            manager = getPackageManager();
            name = new ComponentName(this, BrowserActivity.class);
            manager.setComponentEnabledSetting(name,
                    PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            startActivity(intent);
            manager.setComponentEnabledSetting(name,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
        }

        mSlidingMenu.showContent();
    }

    @Override
    public void onLicenseResult(boolean accept) {
        if (accept) {
            int licenseVersion;

            mBrowserPreferences.setBrowserAcceptLicense(true);
            licenseVersion = getResources().getInteger(R.integer.license_version);
            mBrowserPreferences.setBrowserLicenseVersion(licenseVersion);
            loadBrowser();
        } else {
            mBrowserPreferences.setBrowserAcceptLicense(false);
            finish();
        }
    }

    @Override
    public void onDocumentResult(int type) {
        switch (type) {
            case DocumentFragment.DOCUMENT_NEWS:
                loadBrowser();

                break;
        }
    }

    @Override
    public void onBrowserPreferenceChanged(BrowserPreferences browserPreferences, String key) {
    }

    @Override
    public void OnInformationListItemClick(int[] ids, int index) {
        Intent intent;

        intent = new Intent(BrowserActivity.this, InformationDetailsActivity.class);
        intent.putExtra(InformationDetailsActivity.KEY_IDS, ids);
        intent.putExtra(InformationDetailsActivity.KEY_INDEX, index);
        startActivity(intent);
    }

    @Override
    public void OnScheduleListItemClick(int[] ids, int index) {
        Intent intent;

        intent = new Intent(BrowserActivity.this, ScheduleDetailsActivity.class);
        intent.putExtra(ScheduleDetailsActivity.KEY_IDS, ids);
        intent.putExtra(ScheduleDetailsActivity.KEY_INDEX, index);
        startActivity(intent);
    }

    private boolean loadBrowser() {
        int version;
        int licenseVersion;

        try {
            version = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA).versionCode;
        } catch (NameNotFoundException ignored) {
            version = 0;
        }
        licenseVersion = getResources().getInteger(R.integer.license_version);
        if (!mBrowserPreferences.getBrowserAcceptLicense()
                || mBrowserPreferences.getBrowserLicenseVersion() != licenseVersion) {
            DocumentFragment.newInstance(DocumentFragment.DOCUMENT_LICENSE).show(getSupportFragmentManager(), TAG_DOCUMENT_FRAGMENT);

            return false;
        } else if (mBrowserPreferences.getBrowserVersion() < version) {
            mBrowserPreferences.setBrowserVersion(version);
            DocumentFragment.newInstance(DocumentFragment.DOCUMENT_NEWS).show(getSupportFragmentManager(), TAG_DOCUMENT_FRAGMENT);

            return false;
        }
        if (mBrowserPreferences.getBrowserShowTips() && mTipsTimer == null) {
            mTipsTimer = new Timer();
            mTipsTimer.schedule(new TipsTimerTask(), 1000);
        }

        return true;
    }

    private class PortalServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mPortalService = IPortalService.Stub.asInterface(service);
            try {
                mPortalService.registerCallback(mPortalServiceCallback);
                if (mBrowserPreferences.getPortalFetchAtLaunch()) {
                    String userId;
                    String password;

                    userId = mBrowserPreferences.getSystemUserId();
                    password = mBrowserPreferences.getSystemPassword();
                    mPortalService.requestLogon(userId, password);
                    mPortalService.requestFetchTop();
                    if (mBrowserPreferences.getPortalFetchContentsAutomatically()) {
                        mPortalService.requestFetchContents();
                    }
                    mPortalService.requestLogout();
                }
            } catch (RemoteException ignored) {
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mPortalService = null;
        }
    }

    private class PortalServiceCallback extends IPortalServiceCallback.Stub {
        @Override
        public void onResult(int action, int status) {
            Message message;

            message = new Message();
            message.what = WHAT_BROWSER_HANDLER_PORTAL_SERVICE;
            message.arg1 = action;
            message.arg2 = status;
            mBrowserHandler.sendMessage(message);
        }
    }

    private class AccessServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mAccessService = IAccessService.Stub.asInterface(service);
            try {
                mAccessService.registerCallback(mAccessServiceCallback);
                mAccessService.requestFetchTrainTimetable();
                mAccessService.requestFetchBusTimetable();
            } catch (RemoteException ignored) {
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mAccessService = null;
        }
    }

    private class AccessServiceCallback extends IAccessServiceCallback.Stub {
        @Override
        public void onResult(int action, int status) {
            Message message;

            message = new Message();
            message.what = WHAT_BROWSER_HANDLER_ACCESS_SERVICE;
            message.arg1 = action;
            message.arg2 = status;
            mBrowserHandler.sendMessage(message);
        }
    }

    private class WifiServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mWifiService = IWifiService.Stub.asInterface(service);
            try {
                mWifiService.registerCallback(mWifiServiceCallback);
                if (mBrowserPreferences.getWifiEnabled()) {
                    String ssid;
                    String key;
                    String authenticationUrl;
                    String authenticationBody;
                    String userId;
                    String password;

                    mBrowserPreferences.setWifiEnabled(true);
                    ssid = mBrowserPreferences.getWifiSsid();
                    key = mBrowserPreferences.getWifiKey();
                    authenticationUrl = mBrowserPreferences.getWifiAuthenticationUrl();
                    authenticationBody = mBrowserPreferences.getWifiAuthenticationBody();
                    userId = mBrowserPreferences.getSystemUserId();
                    password = mBrowserPreferences.getSystemPassword();
                    try {
                        mWifiService.connect(ssid, key, authenticationUrl, authenticationBody, userId, password, false);
                    } catch (RemoteException ignored) {
                    }
                }
            } catch (RemoteException ignored) {
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mWifiService = null;
        }
    }

    private class WifiServiceCallback extends IWifiServiceCallback.Stub {
        @Override
        public void onResult(int action, int status) {
            Message message;

            message = new Message();
            message.what = WHAT_BROWSER_HANDLER_WIFI_SERVICE;
            message.arg1 = action;
            message.arg2 = status;
            mBrowserHandler.sendMessage(message);
        }
    }

    private static class BrowserHandler extends Handler {
        BrowserActivity mBrowserActivity;

        public BrowserHandler(BrowserActivity browserActivity) {
            mBrowserActivity = browserActivity;
        }

        @Override
        public void handleMessage(Message msg) {
            boolean running;

            switch (msg.what) {
                case WHAT_BROWSER_HANDLER_PORTAL_SERVICE:
                    switch (msg.arg1) {
                        case PortalService.ACTION_BEGIN:
                            mBrowserActivity.setSupportProgressBarIndeterminateVisibility(true);
                            mBrowserActivity.supportInvalidateOptionsMenu();

                            break;

                        case PortalService.ACTION_END:
                            if (mBrowserActivity.mAccessService != null) {
                                try {
                                    running = mBrowserActivity.mAccessService.isRunning();
                                } catch (RemoteException ex) {
                                    running = false;
                                }
                            } else {
                                running = false;
                            }
                            mBrowserActivity.setSupportProgressBarIndeterminateVisibility(running);
                            mBrowserActivity.supportInvalidateOptionsMenu();

                            break;

                        default:
                            switch (msg.arg2) {
                                case PortalService.STATUS_FAILURE_IN_NETWORK:
                                    Toast.makeText(mBrowserActivity.getApplicationContext(),
                                            R.string.massage_fetch_failure_network, Toast.LENGTH_LONG).show();

                                    break;

                                case PortalService.STATUS_FAILURE_IN_AUTHORIZATION:
                                    Toast.makeText(mBrowserActivity.getApplicationContext(),
                                            R.string.message_fetch_failure_authorization, Toast.LENGTH_LONG).show();

                                    break;
                            }

                            break;
                    }

                    break;

                case WHAT_BROWSER_HANDLER_ACCESS_SERVICE:
                    switch (msg.arg1) {
                        case AccessService.ACTION_BEGIN:
                            mBrowserActivity.setSupportProgressBarIndeterminateVisibility(true);
                            mBrowserActivity.supportInvalidateOptionsMenu();

                            break;

                        case AccessService.ACTION_END:
                            if (mBrowserActivity.mPortalService != null) {
                                try {
                                    running = mBrowserActivity.mPortalService.isRunning();
                                } catch (RemoteException ex) {
                                    running = false;
                                }
                            } else {
                                running = false;
                            }
                            mBrowserActivity.setSupportProgressBarIndeterminateVisibility(running);
                            mBrowserActivity.supportInvalidateOptionsMenu();

                            break;

                        default:
                            break;
                    }
                    break;

                case WHAT_BROWSER_HANDLER_TIPS:
                    String[] tips;

                    tips = mBrowserActivity.getResources().getStringArray(R.array.browser_tips_texts);
                    Toast.makeText(mBrowserActivity.getApplicationContext(),
                            tips[new Random().nextInt(tips.length)], Toast.LENGTH_LONG).show();

                    break;
            }
        }
    }

    private class TipsTimerTask extends TimerTask {
        @Override
        public void run() {
            Message message;

            message = new Message();
            message.what = WHAT_BROWSER_HANDLER_TIPS;
            mBrowserHandler.sendMessage(message);
        }
    }
}
