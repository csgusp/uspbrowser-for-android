/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebView.HitTestResult;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * DocumentFragment クラスは、利用規約やヘルプといった、
 * ドキュメントを表示するダイアログを実装したクラスです。
 *
 * @author leak4mk0
 * @version 12
 * @since 1
 */
public class DocumentFragment extends DialogFragment
        implements DialogInterface.OnCancelListener, DialogInterface.OnClickListener {
    public static final int DOCUMENT_UNKNOWN = 0;
    public static final int DOCUMENT_LICENSE = 1;
    public static final int DOCUMENT_NEWS = 2;
    public static final int DOCUMENT_HELP = 3;
    public static final int DOCUMENT_GOOGLE_TERMS = 4;

    private static final String KEY_DOCUMENT = "document";

    public interface OnLicenseResultListener {
        public void onLicenseResult(boolean accept);
    }

    public interface OnGoogleTermsResultListener {
        public void onGoogleTermsResult(boolean accept);
    }

    public interface OnDocumentResultListener {
        public void onDocumentResult(int type);
    }

    public static DocumentFragment newInstance(int type) {
        DocumentFragment fragment;
        Bundle args;

        fragment = new DocumentFragment();
        args = new Bundle();
        args.putInt(KEY_DOCUMENT, type);
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int document;
        AlertDialog.Builder builder;
        AlertDialog dialog;
        View view;
        WebView webView;
        String url;

        document = getArguments().getInt(KEY_DOCUMENT, DOCUMENT_UNKNOWN);
        builder = new AlertDialog.Builder(getActivity());
        view = getActivity().getLayoutInflater().inflate(R.layout.document_fragment, null, false);
        webView = (WebView) view.findViewById(R.id.web_view);

        switch (document) {
            case DOCUMENT_LICENSE:
                builder.setPositiveButton(R.string.accept, this);
                builder.setNegativeButton(R.string.decline, this);
                url = getString(R.string.url_terms);

                break;

            case DOCUMENT_NEWS:
                builder.setPositiveButton(R.string.ok, this);
                url = getString(R.string.url_news);

                break;

            case DOCUMENT_HELP:
                builder.setPositiveButton(R.string.ok, this);
                url = getString(R.string.url_help);

                break;

            case DOCUMENT_GOOGLE_TERMS:
                builder.setPositiveButton(R.string.accept, this);
                builder.setNegativeButton(R.string.decline, this);
                url = getString(R.string.url_google_terms);

                break;

            default:
                url = null;

                break;
        }
        builder.setView(view);
        dialog = builder.create();
        dialog.setTitle(R.string.title_document);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new DocumentWebChromeClient(dialog));
        webView.setWebViewClient(new DocumentWebViewClient(dialog));
        webView.loadUrl(url);

        return dialog;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        Activity activity;
        int document;

        activity = getActivity();
        document = getArguments().getInt(KEY_DOCUMENT, DOCUMENT_UNKNOWN);
        switch (document) {
            case DOCUMENT_LICENSE:
                if (activity instanceof OnLicenseResultListener) {
                    OnLicenseResultListener listener;

                    listener = (OnLicenseResultListener) activity;
                    listener.onLicenseResult(false);
                }

                break;

            case DOCUMENT_GOOGLE_TERMS:
                if (activity instanceof OnGoogleTermsResultListener) {
                    OnGoogleTermsResultListener listener;

                    listener = (OnGoogleTermsResultListener) activity;
                    listener.onGoogleTermsResult(false);
                }

                break;

            default:
                if (activity instanceof OnDocumentResultListener) {
                    OnDocumentResultListener listener;

                    listener = (OnDocumentResultListener) activity;
                    listener.onDocumentResult(document);
                }

                break;
        }
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        Activity activity;
        int document;

        activity = getActivity();
        document = getArguments().getInt(KEY_DOCUMENT, DOCUMENT_UNKNOWN);
        switch (document) {
            case DOCUMENT_LICENSE:
                if (activity instanceof OnLicenseResultListener) {
                    OnLicenseResultListener listener;

                    listener = (OnLicenseResultListener) activity;
                    listener.onLicenseResult(which == AlertDialog.BUTTON_POSITIVE);
                }

                break;

            case DOCUMENT_GOOGLE_TERMS:
                if (activity instanceof OnGoogleTermsResultListener) {
                    OnGoogleTermsResultListener listener;

                    listener = (OnGoogleTermsResultListener) activity;
                    listener.onGoogleTermsResult(which == AlertDialog.BUTTON_POSITIVE);
                }

                break;
        }
        dismiss();
    }

    private class DocumentWebViewClient extends WebViewClient {
        Dialog mDialog;

        public DocumentWebViewClient(Dialog dialog) {
            mDialog = dialog;
        }

        public void onPageFinished(WebView view, String url) {
            ProgressBar progressBar;

            progressBar = (ProgressBar) mDialog.findViewById(R.id.progress_bar);
            progressBar.setVisibility(View.GONE);
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            HitTestResult result;
            Intent intent;

            result = view.getHitTestResult();
            if (url == null || result == null || result.getType() == HitTestResult.UNKNOWN_TYPE) {
                return false;
            } else if (url.contains("android_asset")) {
                return false;
            } else if (url.startsWith("mailto:")) {
                intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
            } else {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            }
            startActivity(intent);

            return true;
        }
    }

    private class DocumentWebChromeClient extends WebChromeClient {
        Dialog mDialog;

        public DocumentWebChromeClient(Dialog dialog) {
            mDialog = dialog;
        }

        @Override
        public void onReceivedTitle(WebView view, String title) {
            mDialog.setTitle(title);
        }
    }
}
