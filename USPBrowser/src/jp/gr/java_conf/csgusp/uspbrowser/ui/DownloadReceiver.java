/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui;

import jp.gr.java_conf.csgusp.uspbrowser.R;

import android.annotation.TargetApi;
import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import java.io.File;
import java.net.URI;

/**
 * DownloadReceiver クラスは、アプリにより行われたダウンロード作業が、
 * 完了したときの動作を定義したクラスです。
 *
 * @author leak4mk0
 * @version 24
 * @since 1
 */
public class DownloadReceiver extends BroadcastReceiver {
    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    @Override
    public void onReceive(Context context, Intent intent) {
        String action;

        action = intent.getAction();
        if (action != null && action.equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {
            long id;

            id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
            if (id >= 0) {
                DownloadManager manager;
                Query query;
                Cursor cursor;

                manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                query = new Query();
                query.setFilterById(id);
                cursor = manager.query(query);
                if (cursor.moveToFirst()) {
                    int status;
                    int reason;
                    String localUri;
                    String mediaType;
                    String localFilename;
                    Intent fileIntent;

                    status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                    reason = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
                    localUri = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
                    mediaType = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE));

                    switch (status) {
                        case DownloadManager.STATUS_SUCCESSFUL:
                            localFilename = new File(URI.create(localUri)).getAbsolutePath();
                            fileIntent = new Intent(Intent.ACTION_VIEW);
                            fileIntent.setDataAndType(Uri.parse(localUri), mediaType);
                            Toast.makeText(context,
                                    context.getString(R.string.message_download_success, localFilename),
                                    Toast.LENGTH_LONG).show();
                            if (context.getPackageManager().queryIntentActivities(fileIntent, 0).size() > 0) {
                                fileIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(fileIntent);
                            } else {
                                Toast.makeText(context, R.string.message_download_failure_opening,
                                        Toast.LENGTH_LONG).show();
                            }

                            break;

                        case DownloadManager.STATUS_FAILED:
                            switch (reason) {
                                case DownloadManager.ERROR_FILE_ALREADY_EXISTS:
                                    localFilename = new File(URI.create(localUri)).getAbsolutePath();
                                    fileIntent = new Intent(Intent.ACTION_VIEW);
                                    fileIntent.setDataAndType(Uri.parse(localUri), mediaType);
                                    Toast.makeText(context,
                                            context.getString(R.string.message_download_failure_existed, localFilename),
                                            Toast.LENGTH_LONG).show();
                                    if (context.getPackageManager().queryIntentActivities(fileIntent, 0).size() > 0) {
                                        fileIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        context.startActivity(fileIntent);
                                    } else {
                                        Toast.makeText(context, R.string.message_download_failure_opening,
                                                Toast.LENGTH_LONG).show();
                                    }

                                    break;

                                default:
                                    Toast.makeText(context, R.string.message_download_failure,
                                            Toast.LENGTH_LONG).show();
                            }

                            break;
                    }
                }
            }
        }
    }
}
