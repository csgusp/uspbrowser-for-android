/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.PortalService;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

/**
 * HelpActivity クラスは、ヘルプ画面の Activity の実装です。
 *
 * @author leak4mk0
 * @version 13
 * @since 13
 */
public class HelpActivity extends SherlockActivity {
    public void onCreate(Bundle savedInstanceState) {
        WebView webView;
        String url;

        super.onCreate(savedInstanceState);

        url = getString(R.string.url_help);

        setContentView(R.layout.help_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        webView = (WebView) findViewById(R.id.web_view);
        webView.loadUrl(url);
        webView.setWebViewClient(new HelpWebViewClient(url));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        PackageInfo packageInfo;
        boolean ret;

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                ret = true;

                break;

            default:
                ret = super.onOptionsItemSelected(item);
        }

        return ret;
    }

    @Override
    public boolean onKeyDown (int keyCode, KeyEvent event) {
        WebView webView;

        webView = (WebView) findViewById(R.id.web_view);
        if (keyCode == KeyEvent.KEYCODE_BACK && webView.canGoBack()) {
            webView.goBack();

            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    private class HelpWebViewClient extends WebViewClient {
        private String mHost;

        public HelpWebViewClient(String url) {
            mHost = Uri.parse(url).getHost();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Uri uri;
            String host;

            uri = Uri.parse(url);
            host = uri.getHost();
            if (host != null && host.equals(mHost)) {
                return false;
            }

            startActivity(new Intent(Intent.ACTION_VIEW, uri));

            return true;
        }
    }
}