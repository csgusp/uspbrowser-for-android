/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui;

import jp.gr.java_conf.csgusp.uspbrowser.R;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * MenuListFragment クラスは、サイドメニューのフラグメントを実装したクラスです。
 *
 * @author leak4mk0
 * @version 12
 * @since 1
 */
public class MenuListFragment extends ListFragment {
    private Activity mActivity;
    private Context mContext;
    private MenuListAdapter mAdapter;

    public interface OnCreateMenuListListener {
        public void onCreateMenuList(MenuListAdapter list);
    }

    public interface OnMenuListItemSelectedListener {
        public void onMenuListItemSelected(MenuListItem item);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof OnCreateMenuListListener) || !(activity instanceof OnMenuListItemSelectedListener)) {
            throw new ClassCastException();
        }
        mActivity = activity;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ListView list;

        mContext = new ContextThemeWrapper(mActivity, R.style.SlidingMenuStyle);

        list = new ListView(mContext);
        list.setId(android.R.id.list);
        list.setDrawSelectorOnTop(false);
        list.setDividerHeight(0);
        list.setBackgroundResource(R.drawable.abs__ab_solid_dark_holo);

        return list;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAdapter = new MenuListAdapter(mContext);
        setListAdapter(mAdapter);

        ((OnCreateMenuListListener) mActivity).onCreateMenuList(mAdapter);
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        MenuListItem item;
        super.onListItemClick(l, v, position, id);

        item = mAdapter.getItem(position);
        if (item.isCheckable()) {
            mAdapter.check(position);
        }
        ((OnMenuListItemSelectedListener) mActivity).onMenuListItemSelected(item);
    }

    public class MenuListItem {
        private CharSequence mTitle;
        private Drawable mIcon;
        private int mItemId;
        private boolean mCategory;
        private boolean mCheckable;
        private boolean mChecked;

        public MenuListItem(CharSequence title, Drawable icon, int id, boolean category, boolean checkable) {
            mTitle = title;
            mIcon = icon;
            mItemId = id;
            mCategory = category;
            mCheckable = checkable;
            mChecked = false;
        }

        public CharSequence getTitle() {
            return mTitle;
        }

        public void setTitle(CharSequence title) {
            mTitle = title;
        }

        public Drawable getIcon() {
            return mIcon;
        }

        public void setIcon(Drawable icon) {
            mIcon = icon;
        }

        public int getItemId() {
            return mItemId;
        }

        public void setItemId(int itemId) {
            mItemId = itemId;
        }

        public boolean isCategory() {
            return mCategory;
        }

        public void setCategory(boolean category) {
            mCategory = category;
        }

        public boolean isCheckable() {
            return mCheckable;
        }

        public void setCheckable(boolean checkable) {
            mCheckable = checkable;
        }

        public boolean isChecked() {
            return mChecked;
        }

        public void setChecked(boolean checked) {
            mChecked = checked;
        }

    }

    public class MenuListAdapter extends ArrayAdapter<MenuListItem> {
        public MenuListAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            MenuListItem item;
            TextView text;
            View line;

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.menu_list_item, parent, false);
            }

            item = getItem(position);

            text = (TextView) convertView.findViewById(R.id.menu_text_view);
            line = convertView.findViewById(R.id.menu_line_view);

            convertView.setBackgroundColor(item.isChecked() ? Color.BLACK : Color.TRANSPARENT);
            text.setText(item.getTitle());
            text.setTextSize(item.isCategory() ? 14 : 22);
            text.setCompoundDrawablesWithIntrinsicBounds(item.getIcon(), null, null, null);
            line.setVisibility(item.isCategory() ? View.VISIBLE : View.GONE);

            return convertView;
        }

        public boolean isEnabled(int position) {
            return !getItem(position).isCategory();
        }

        public MenuListItem addCategory(int titleId, int iconId) {
            MenuListItem item;
            CharSequence title;
            Drawable icon;

            title = getResources().getText(titleId);
            icon = iconId > 0 ? getResources().getDrawable(iconId) : null;
            item = new MenuListItem(title, icon, 0, true, false);
            add(item);

            return item;
        }

        public MenuListItem addCategory(CharSequence title, Drawable icon) {
            MenuListItem item;

            item = new MenuListItem(title, icon, 0, true, false);
            add(item);

            return item;
        }

        public MenuListItem addItem(int titleId, int iconId, int id, boolean selectable) {
            MenuListItem item;
            CharSequence title;
            Drawable icon;

            title = getResources().getText(titleId);
            icon = iconId > 0 ? getResources().getDrawable(iconId) : null;
            item = new MenuListItem(title, icon, id, false, selectable);
            add(item);

            return item;
        }

        public MenuListItem addItem(CharSequence title, Drawable icon, int id, boolean selectable) {
            MenuListItem item;

            item = new MenuListItem(title, icon, id, false, selectable);
            add(item);

            return item;
        }

        public void check(int index) {
            int count;

            count = getCount();
            for (int i = 0; i < count; i++) {
                getItem(i).setChecked(i == index && getItem(i).isCheckable());
            }
            notifyDataSetChanged();
        }

        public void checkById(int id, boolean perform) {
            int count;
            int index;

            count = getCount();
            index = -1;
            for (int i = 0; i < count; i++) {
                boolean check;

                check = getItem(i).getItemId() == id;
                if (check) {
                    index = i;
                }
                getItem(i).setChecked(check && getItem(i).isCheckable());
            }
            notifyDataSetChanged();
            if (perform) {
                ((OnMenuListItemSelectedListener) mActivity).onMenuListItemSelected(getItem(index));
            }
        }
    }
}
