/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.preference.BrowserPreferences;

import android.annotation.TargetApi;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.MenuItem;

import java.util.Calendar;
import java.util.List;

/**
 * 設定アクティビティを実装したクラス。
 *
 * @author leak4mk0
 * @version 25
 * @see android.preference.PreferenceActivity
 * @since 1
 */
public class SettingsActivity extends SherlockPreferenceActivity implements Preference.OnPreferenceClickListener {
    private static final String ACTION_SET_GENERAL = "jp.gr.java_conf.csgusp.uspbrowser.SET_GENERAL";
    private static final String ACTION_SET_SYSTEM = "jp.gr.java_conf.csgusp.uspbrowser.SET_SYSTEM";
    private static final String ACTION_SET_PORTAL = "jp.gr.java_conf.csgusp.uspbrowser.SET_PORTAL";
    private static final String ACTION_SET_WIFI = "jp.gr.java_conf.csgusp.uspbrowser.SET_WIFI";
    private static final String ACTION_SET_ABOUT = "jp.gr.java_conf.csgusp.uspbrowser.SET_ABOUT";

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActionBar bar;
        String action;

        super.onCreate(savedInstanceState);

        bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);

        action = getIntent().getAction();
        if (ACTION_SET_GENERAL.equals(action)) {
            addPreferencesFromResource(R.xml.general_preferences);
        } else if (ACTION_SET_SYSTEM.equals(action)) {
            addPreferencesFromResource(R.xml.system_preferences);
        } else if (ACTION_SET_PORTAL.equals(action)) {
            addPreferencesFromResource(R.xml.portal_preferences);
        } else if (ACTION_SET_WIFI.equals(action)) {
            Preference resetSettingsPreference;

            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.wifi_preference);

            resetSettingsPreference = findPreference("wifi_reset_settings");
            if (resetSettingsPreference != null) {
                resetSettingsPreference.setOnPreferenceClickListener(this);
            }
        } else if (ACTION_SET_ABOUT.equals(action)) {
            addPreferencesFromResource(R.xml.about_preference);
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            addPreferencesFromResource(R.xml.preference_headers);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean ret;

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                ret = true;

                break;

            default:
                ret = super.onOptionsItemSelected(item);
        }

        return ret;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onBuildHeaders(List<Header> target) {
        loadHeadersFromResource(R.xml.preference_headers, target);
    }

    @Override
    protected boolean isValidFragment(String fragmentName) {
        boolean ret;

        ret = GeneralFragment.class.getName().equals(fragmentName) ||
                SystemFragment.class.getName().equals(fragmentName) ||
                PortalFragment.class.getName().equals(fragmentName) ||
                WifiFragment.class.getName().equals(fragmentName) ||
                AboutFragment.class.getName().equals(fragmentName);

        return ret;
    }

    @SuppressWarnings("deprecation")
    @Override
    public boolean onPreferenceClick(Preference preference) {
        BrowserPreferences browserPreferences;

        browserPreferences = BrowserPreferences.getInstance(preference.getContext());
        browserPreferences.resetWifi();
        setPreferenceScreen(null);
        addPreferencesFromResource(R.xml.wifi_preference);

        return true;
    }

    /**
     * 一般設定のフラグメントを実装したクラス。
     *
     * @author leak4mk0
     * @version 24
     * @see android.preference.PreferenceFragment
     * @since 13
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralFragment extends PreferenceFragment {
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.general_preferences);
        }
    }

    /**
     * 統合認証システム設定のフラグメントを実装したクラス。
     *
     * @author leak4mk0
     * @version 24
     * @see android.preference.PreferenceFragment
     * @since 13
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class SystemFragment extends PreferenceFragment {
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.system_preferences);
        }
    }

    /**
     * ポータル設定のフラグメントを実装したクラス。
     *
     * @author leak4mk0
     * @version 24
     * @see android.preference.PreferenceFragment
     * @since 13
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class PortalFragment extends PreferenceFragment {
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.portal_preferences);
        }
    }

    /**
     * Wi-Fi 設定のフラグメントを実装したクラス。
     *
     * @author leak4mk0
     * @version 32
     * @see android.preference.PreferenceFragment
     * @since 19
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class WifiFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener {
        public void onCreate(Bundle savedInstanceState) {
            Preference resetSettingsPreference;

            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.wifi_preference);

            resetSettingsPreference = findPreference("wifi_reset_settings");
            if (resetSettingsPreference != null) {
                resetSettingsPreference.setOnPreferenceClickListener(this);
            }
        }

        @Override
        public boolean onPreferenceClick(Preference preference) {
            BrowserPreferences browserPreferences;

            browserPreferences = BrowserPreferences.getInstance(preference.getContext());
            browserPreferences.resetWifi();
            setPreferenceScreen(null);
            addPreferencesFromResource(R.xml.wifi_preference);

            return true;
        }
    }

    /**
     * Wi-Fi 設定のフラグメントを実装したクラス。
     *
     * @author leak4mk0
     * @version 25
     * @see android.preference.PreferenceFragment
     * @since 25
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class AboutFragment extends PreferenceFragment {
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.about_preference);
        }
    }
}
