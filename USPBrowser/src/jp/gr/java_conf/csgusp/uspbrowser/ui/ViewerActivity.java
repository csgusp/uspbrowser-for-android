/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.preference.BrowserPreferences;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebView.HitTestResult;
import android.webkit.WebViewClient;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

/**
 * ドキュメントビューアーのアクティビティを実装したクラス。
 *
 * @author leak4mk0
 * @version 1
 * @see android.app.Activity
 * @since 1
 */
public class ViewerActivity extends SherlockFragmentActivity implements DocumentFragment.OnGoogleTermsResultListener {
    public static final String NAME_DOCUMENT_URL = "document_url";

    private static final String TAG_DOCUMENT_FRAGMENT = "document_fragment";

    BrowserPreferences mBrowserPreferences;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        ActionBar bar;
        WebView webView;

        super.onCreate(savedInstanceState);

        setContentView(R.layout.viewer_activity);

        bar = getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);

        webView = (WebView) findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new ViewerWebViewClient());

        mBrowserPreferences = BrowserPreferences.getInstance(this);
        if (!mBrowserPreferences.getBrowserAcceptGoogleTerms()) {
            DocumentFragment.newInstance(DocumentFragment.DOCUMENT_GOOGLE_TERMS)
                    .show(getSupportFragmentManager(), TAG_DOCUMENT_FRAGMENT);
        } else {
            webView.loadUrl(getString(R.string.url_google_document_viewer,
                    getIntent().getStringExtra(NAME_DOCUMENT_URL)));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.viewer, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        PackageInfo packageInfo;
        boolean ret;

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                ret = true;

                break;

            case R.id.menu_settings:
                intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                ret = true;

                break;

            case R.id.menu_feedback:
                intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(getString(R.string.url_email)));
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback_subject));
                try {
                    packageInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
                } catch (PackageManager.NameNotFoundException e) {
                    packageInfo = null;
                }
                if (packageInfo != null) {
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.feedback_text,
                            Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE, packageInfo.versionName, packageInfo.versionCode));
                } else {
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.feedback_text,
                            Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE, R.string.unknown, -1));
                }
                startActivity(intent);
                ret = true;

                break;

            case R.id.menu_help:
                startActivity(new Intent(getApplicationContext(), HelpActivity.class));
                ret = true;

                break;

            default:
                ret = super.onOptionsItemSelected(item);
        }

        return ret;
    }

    @Override
    public void onGoogleTermsResult(boolean accept) {
        if (accept) {
            WebView webView;

            mBrowserPreferences.setBrowserAcceptGoogleTerms(true);
            webView = (WebView) findViewById(R.id.web_view);
            webView.loadUrl(getString(R.string.url_google_document_viewer,
                    getIntent().getStringExtra(NAME_DOCUMENT_URL)));
        } else {
            mBrowserPreferences.setBrowserAcceptGoogleTerms(false);
            finish();
        }
    }

    private class ViewerWebViewClient extends WebViewClient {
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            HitTestResult result;
            Intent intent;

            result = view.getHitTestResult();
            if (url == null || result == null || result.getType() == HitTestResult.UNKNOWN_TYPE) {
                return false;
            }
            if (url.startsWith("mailto:")) {
                intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(url));
            } else {
                intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            }
            startActivity(intent);

            return true;
        }
    }
}
