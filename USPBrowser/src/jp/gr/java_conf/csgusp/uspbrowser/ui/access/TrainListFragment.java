/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.access;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.provider.access.AccessContract.Trains;
import jp.gr.java_conf.csgusp.uspbrowser.ui.widget.TimetableAdapter;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 電車時刻表のフラグメントを実装したクラス。
 *
 * @author leak4mk0
 * @version 34
 * @since 27
 */
public class TrainListFragment extends SherlockListFragment implements TimetableAdapter.TimetableCallbacks {
    private static final String KEY_DIRECTION_IS_INBOUND = "direction_is_inbound";

    private static int REFRESH_TIME = 10000;

    private TimetableAdapter mTimetableAdapter;
    private Timer mRefreshTimer;
    private RefreshHandler mRefreshHandler;

    public static TrainListFragment newInstance(boolean directionIsInbound) {
        TrainListFragment fragment;
        Bundle args;

        fragment = new TrainListFragment();
        args = new Bundle();
        args.putBoolean(KEY_DIRECTION_IS_INBOUND, directionIsInbound);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        int direction;
        int headerResourceId;

        super.onActivityCreated(savedInstanceState);

        if (getArguments().getBoolean(KEY_DIRECTION_IS_INBOUND)) {
            direction = Trains.DIRECTION_INBOUND;
            headerResourceId = R.layout.access_trains_inbound_list_header;
        } else {
            direction = Trains.DIRECTION_OUTBOUND;
            headerResourceId = R.layout.access_trains_outbound_list_header;
        }
        mTimetableAdapter = new TimetableAdapter(
                getActivity(), getLoaderManager(), Trains.CONTENT_URI,
                Trains.DIRECTION, direction,
                Trains.WEEKDAY, Trains.WEEKDAY_WORKDAY, Trains.WEEKDAY_HOLIDAY,
                Trains.TIME,
                headerResourceId, R.layout.access_list_item, R.color.next_color,
                R.id.hour_text_view, R.id.left_min_text_view, R.id.right_min_text_view);
        mTimetableAdapter.setTimetableLoaderCallbacks(this);

        mRefreshTimer = new Timer();
        mRefreshTimer.schedule(new RefreshTimerTask(), REFRESH_TIME);
        mRefreshHandler = new RefreshHandler();

        setEmptyText(getText(R.string.empty_access_trains));
        setListAdapter(mTimetableAdapter);
        setListShown(false);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mRefreshTimer == null) {
            new RefreshTimerTask().run();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mRefreshTimer != null) {
            mRefreshTimer.cancel();
            mRefreshTimer = null;
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem searchItem;
        MenuItem notTakenItem;
        MenuItem deletedItem;
        SearchView searchView;

        super.onPrepareOptionsMenu(menu);

        searchItem = menu.findItem(R.id.menu_search);
        notTakenItem = menu.findItem(R.id.menu_not_taken);
        deletedItem = menu.findItem(R.id.menu_deleted);
        searchView = (SearchView) searchItem.getActionView();

        searchItem.setVisible(false);
        notTakenItem.setVisible(false);
        deletedItem.setVisible(false);

        if (searchView != null) {
            searchView.setVisibility(View.GONE);
        } else {
            searchItem.setVisible(false);
        }
    }

    @Override
    public void onLoadFinished() {
        if (isResumed()) {
            setListShown(true);
        } else {
            setListShownNoAnimation(true);
        }
    }

    private class RefreshTimerTask extends TimerTask {
        @Override
        public void run() {
            if (mRefreshHandler != null) {
                mRefreshHandler.sendEmptyMessage(0);
            }
        }
    }

    private class RefreshHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if (mTimetableAdapter != null) {
                mTimetableAdapter.refresh();
            }

            mRefreshTimer = new Timer();
            mRefreshTimer.schedule(new RefreshTimerTask(), REFRESH_TIME);
        }
    }
}