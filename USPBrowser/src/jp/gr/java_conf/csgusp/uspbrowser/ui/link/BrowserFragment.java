/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.link;

import jp.gr.java_conf.csgusp.uspbrowser.R;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;

/**
 * ブラウザのフラグメントを実装したクラス。
 *
 * @author leak4mk0
 * @version 34
 * @since 34
 */
public class BrowserFragment extends SherlockFragment implements View.OnKeyListener {
    private static final String KEY_URL = "url";

    private WebView mWebView;

    public static BrowserFragment newInstance(String url) {
        BrowserFragment fragment;
        Bundle args;

        fragment = new BrowserFragment();
        args = new Bundle();
        args.putString(KEY_URL, url);
        fragment.setArguments(args);

        return fragment;
    }

    @SuppressLint("SetJavaScriptEnabled")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        String url;

        url = getArguments().getString(KEY_URL);

        mWebView = new WebView(getActivity());
        mWebView.setOnKeyListener(this);
        mWebView.setWebViewClient(new BrowserWebViewClient(url));
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl(url);

        return mWebView;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem searchItem;
        MenuItem notTakenItem;
        MenuItem deletedItem;
        SearchView searchView;

        super.onPrepareOptionsMenu(menu);

        searchItem = menu.findItem(R.id.menu_search);
        notTakenItem = menu.findItem(R.id.menu_not_taken);
        deletedItem = menu.findItem(R.id.menu_deleted);
        searchView = (SearchView) searchItem.getActionView();

        searchItem.setVisible(false);
        notTakenItem.setVisible(false);
        deletedItem.setVisible(false);

        if (searchView != null) {
            searchView.setVisibility(View.GONE);
        } else {
            searchItem.setVisible(false);
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            if (keyCode == KeyEvent.KEYCODE_BACK && mWebView.canGoBack()) {
                mWebView.goBack();

                return true;
            }
        }

        return false;
    }

    private class BrowserWebViewClient extends WebViewClient {
        private String mHost;

        public BrowserWebViewClient(String url) {
            mHost = Uri.parse(url).getHost();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Uri uri;
            String host;

            uri = Uri.parse(url);
            host = uri.getHost();
            if (host != null && host.equals(mHost)) {
                return false;
            }

            startActivity(new Intent(Intent.ACTION_VIEW, uri));

            return true;
        }
    }
}
