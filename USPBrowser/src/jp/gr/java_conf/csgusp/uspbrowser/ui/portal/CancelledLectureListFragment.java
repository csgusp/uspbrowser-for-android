/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.gr.java_conf.csgusp.uspbrowser.ui.portal;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;

/**
 * ポータルの休講リストのフラグメントを実装したクラス。
 *
 * @author leak4mk0
 * @version 35
 * @see android.app.ListFragment
 * @since 1
 */
public class CancelledLectureListFragment extends SherlockListFragment
		implements LoaderCallbacks<Cursor>, OnQueryTextListener {
	private static final String KEY_QUERY_TEXT = "query_text";
	private static final String KEY_SHOW_DELETED = "show_deleted";
	private static final String KEY_SHOW_NOT_TAKEN = "show_not_taken";

	private CancelledLectureCursorAdapter mCancelledLectureCursorAdapter;

	public static CancelledLectureListFragment newInstance() {
		CancelledLectureListFragment fragment;
		Bundle args;

		fragment = new CancelledLectureListFragment();
		args = new Bundle();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mCancelledLectureCursorAdapter = new CancelledLectureCursorAdapter(getActivity(), null, 0);

		setEmptyText(getText(R.string.empty_cancelled_lectures));
		setListAdapter(mCancelledLectureCursorAdapter);
		setListShown(false);
		setHasOptionsMenu(true);

		getLoaderManager().initLoader(0, getArguments(), this);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		MenuItem searchItem;
		MenuItem notTakenItem;
		MenuItem deletedItem;
		SearchView searchView;

		Bundle args;

		super.onPrepareOptionsMenu(menu);

		searchItem = menu.findItem(R.id.menu_search);
		notTakenItem = menu.findItem(R.id.menu_not_taken);
		deletedItem = menu.findItem(R.id.menu_deleted);
		searchView = (SearchView) searchItem.getActionView();

		args = getArguments();

		notTakenItem.setVisible(true);
		notTakenItem.setChecked(args.getBoolean(KEY_SHOW_NOT_TAKEN));
		deletedItem.setVisible(true);
		deletedItem.setChecked(args.getBoolean(KEY_SHOW_DELETED));

        if (searchView != null) {
            searchView.setOnQueryTextListener(this);
            searchView.setIconified(TextUtils.isEmpty(args.getString(KEY_QUERY_TEXT)));
        } else {
            searchItem.setVisible(false);
        }
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean ret;
		boolean checked;
		Bundle args;

		switch (item.getItemId()) {
		case R.id.menu_not_taken:
			args = getArguments();
			checked = !item.isChecked();
			item.setChecked(checked);
			args.putBoolean(KEY_SHOW_NOT_TAKEN, checked);
			getLoaderManager().restartLoader(0, args, this);

			ret = true;

			break;

		case R.id.menu_deleted:
			args = getArguments();
			checked = !item.isChecked();
			item.setChecked(checked);
			args.putBoolean(KEY_SHOW_DELETED, checked);
			getLoaderManager().restartLoader(0, args, this);

			ret = true;

			break;

		default:
			ret = super.onOptionsItemSelected(item);

			break;
		}

		return ret;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		CursorLoader loader;
		Uri uri;
		String[] projection;
		String queryText;
		StringBuilder selectionBuilder;
		String[] selectionArgs;

		uri = PortalContract.CancelledLectures.CONTENT_URI;
		projection = new String[] { PortalContract.CancelledLectures._ID,
				PortalContract.CancelledLectures.SUBJECT,
				PortalContract.CancelledLectures.PROFESSOR,
				PortalContract.CancelledLectures.CANCELLED_LECTURE_DATE,
				PortalContract.CancelledLectures.CANCELLED_LECTURE_PERIOD,
				PortalContract.CancelledLectures.MAKEUP_LECTURE_DATE,
				PortalContract.CancelledLectures.MAKEUP_LECTURE_PERIOD,
				PortalContract.CancelledLectures.MAKEUP_LECTURE_CLASSROOM,
				PortalContract.CancelledLectures.NOTE_1,
				PortalContract.CancelledLectures.NOTE_2,
				PortalContract.CancelledLectures.IS_TAKEN,
				PortalContract.CancelledLectures.IS_DELETED };
		queryText = args.getString(KEY_QUERY_TEXT);
		selectionBuilder = new StringBuilder();
		selectionArgs = null;
		if (!TextUtils.isEmpty(queryText)) {
			queryText = "%" + queryText + "%";
			selectionBuilder.append("(");
			selectionBuilder.append(PortalContract.CancelledLectures.SUBJECT);
			selectionBuilder.append(" LIKE ? OR ");
			selectionBuilder.append(PortalContract.CancelledLectures.PROFESSOR);
			selectionBuilder.append(" LIKE ? )");
			selectionArgs = new String[] { queryText, queryText };
		}
		if (!args.getBoolean(KEY_SHOW_NOT_TAKEN)) {
			if (selectionBuilder.length() > 0) {
				selectionBuilder.append(" AND ");
			}
			selectionBuilder.append(PortalContract.CancelledLectures.IS_TAKEN);
			selectionBuilder.append(" = 1");
		}
		if (!args.getBoolean(KEY_SHOW_DELETED)) {
			if (selectionBuilder.length() > 0) {
				selectionBuilder.append(" AND ");
			}
			selectionBuilder.append(PortalContract.CancelledLectures.IS_DELETED);
			selectionBuilder.append(" = 0");
		}
		loader = new CursorLoader(getActivity(), uri, projection, selectionBuilder.toString(), selectionArgs, null);

		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mCancelledLectureCursorAdapter.swapCursor(data);

		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mCancelledLectureCursorAdapter.swapCursor(null);
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		Bundle args;
		String oldText;

		args = getArguments();
		oldText = args.getString(KEY_QUERY_TEXT);
		newText = TextUtils.isEmpty(newText) ? null : newText;
		if ((oldText != null && !oldText.equals(newText)) || (newText != null && !newText.equals(oldText))) {
			args.putString(KEY_QUERY_TEXT, newText);
			getLoaderManager().restartLoader(0, args, this);
		}

		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

	private static class CancelledLectureCursorAdapter extends CursorAdapter {

		public CancelledLectureCursorAdapter(Context context, Cursor c, int flags) {
			super(context, c, flags);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			TextView subjectTextView;
			TextView professorTextView;
			ImageView deletedImageView;
			TextView cancelledLectureDateTextView;
			TextView cancelledLecturePeriodTextView;
			TextView rightArrowTextView;
			TextView makeupLectureDateTextView;
			TextView makeupLecturePeriodTextView;
			TextView makeupLectureClassroomTextView;
			TextView note1TextView;
			TextView note2TextView;

			String subject;
			String professor;
			boolean deleted;
			long cancelledLectureDate;
			int cancelledLecturePeriod;
			long makeupLectureDate;
			int makeupLecturePeriod;
			String makeupLectureClassroom;
			String note1;
			String note2;

			subjectTextView = (TextView) view.findViewById(R.id.subject_text_view);
			professorTextView = (TextView) view.findViewById(R.id.professor_text_view);
			deletedImageView = (ImageView) view.findViewById(R.id.deleted_image_view);
			cancelledLectureDateTextView = (TextView) view.findViewById(R.id.cancelled_lecture_date_text_view);
			cancelledLecturePeriodTextView = (TextView) view.findViewById(R.id.cancelled_lecture_period_text_view);
			rightArrowTextView = (TextView) view.findViewById(R.id.right_arrow_text_view);
			makeupLectureDateTextView = (TextView) view.findViewById(R.id.makeup_lecture_date_text_view);
			makeupLecturePeriodTextView = (TextView) view.findViewById(R.id.makeup_lecture_period_text_view);
			makeupLectureClassroomTextView = (TextView) view.findViewById(R.id.makeup_lecture_classroom_text_view);
			note1TextView = (TextView) view.findViewById(R.id.note_1_text_view);
			note2TextView = (TextView) view.findViewById(R.id.note_2_text_view);

			subject = cursor.getString(cursor.getColumnIndex(PortalContract.CancelledLectures.SUBJECT));
			professor = cursor.getString(cursor.getColumnIndex(PortalContract.CancelledLectures.PROFESSOR));
			deleted = cursor.getInt(cursor.getColumnIndex(PortalContract.CancelledLectures.IS_DELETED)) > 0;
			cancelledLectureDate = cursor.getLong(cursor.getColumnIndex(
					PortalContract.CancelledLectures.CANCELLED_LECTURE_DATE));
			cancelledLecturePeriod = cursor.getInt(cursor.getColumnIndex(
					PortalContract.CancelledLectures.CANCELLED_LECTURE_PERIOD));
			makeupLectureDate = cursor.getLong(cursor.getColumnIndex(
					PortalContract.CancelledLectures.MAKEUP_LECTURE_DATE));
			makeupLecturePeriod = cursor.getInt(cursor.getColumnIndex(
					PortalContract.CancelledLectures.MAKEUP_LECTURE_PERIOD));
			makeupLectureClassroom = cursor.getString(cursor.getColumnIndex(
					PortalContract.CancelledLectures.MAKEUP_LECTURE_CLASSROOM));
			note1 = cursor.getString(cursor.getColumnIndex(PortalContract.CancelledLectures.NOTE_1));
			note2 = cursor.getString(cursor.getColumnIndex(PortalContract.CancelledLectures.NOTE_2));

			view.setBackgroundResource(deleted ? R.drawable.bg_deleted : R.drawable.bg_read);

			subjectTextView.setText(subject);
			professorTextView.setText(professor);
			deletedImageView.setVisibility(deleted ? View.VISIBLE : View.INVISIBLE);

			if (cancelledLectureDate > 0) {
				cancelledLectureDateTextView.setText(DateUtils.formatDateTime(context, cancelledLectureDate,
						DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY
								| DateUtils.FORMAT_ABBREV_WEEKDAY | DateUtils.FORMAT_NUMERIC_DATE));
				cancelledLectureDateTextView.setVisibility(View.VISIBLE);
                if (cancelledLecturePeriod > 0) {
                    cancelledLecturePeriodTextView.setText(
                            context.getString(R.string.template_period, cancelledLecturePeriod));
                } else {
                    cancelledLectureDateTextView.setText("");
                }
				cancelledLecturePeriodTextView.setVisibility(View.VISIBLE);
			} else {
				cancelledLectureDateTextView.setVisibility(View.GONE);
				cancelledLecturePeriodTextView.setVisibility(View.GONE);
			}
			if (makeupLectureDate > 0) {
				rightArrowTextView.setVisibility(View.VISIBLE);
				makeupLectureDateTextView.setText(DateUtils.formatDateTime(context, makeupLectureDate,
						DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY
								| DateUtils.FORMAT_ABBREV_WEEKDAY | DateUtils.FORMAT_NUMERIC_DATE));
				makeupLectureDateTextView.setVisibility(View.VISIBLE);
                if (makeupLecturePeriod > 0) {
                    makeupLecturePeriodTextView.setText(
                            context.getString(R.string.template_period, makeupLecturePeriod));
                } else {
                    makeupLecturePeriodTextView.setText("");
                }
				makeupLecturePeriodTextView.setVisibility(View.VISIBLE);
			} else {
				rightArrowTextView.setVisibility(View.GONE);
				makeupLectureDateTextView.setVisibility(View.GONE);
				makeupLecturePeriodTextView.setVisibility(View.GONE);
			}

			if (!TextUtils.isEmpty(makeupLectureClassroom)) {
				makeupLectureClassroomTextView.setText(String.valueOf(makeupLectureClassroom));
			} else {
				makeupLectureClassroomTextView.setVisibility(View.VISIBLE);
				makeupLectureClassroomTextView.setVisibility(View.GONE);
			}

			if (!TextUtils.isEmpty(note1)) {
				note1TextView.setText(note1);
				note1TextView.setVisibility(View.VISIBLE);
			} else {
				note1TextView.setVisibility(View.GONE);
			}
			if (!TextUtils.isEmpty(note2)) {
				note2TextView.setText(note2);
				note2TextView.setVisibility(View.VISIBLE);
			} else {
				note2TextView.setVisibility(View.GONE);
			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			View view;

			view = LayoutInflater.from(context).inflate(R.layout.portal_cancelled_lecture_list_item, parent, false);

			return view;
		}
	}
}
