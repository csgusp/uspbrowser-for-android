/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.portal;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;

/**
 * ポータルの教室変更リストのフラグメントを実装したクラス。
 *
 * @author leak4mk0
 * @version 35
 * @see android.app.ListFragment
 * @since 1
 */
public class ClassroomChangeListFragment extends SherlockListFragment
		implements LoaderCallbacks<Cursor>, OnQueryTextListener {
	private static final String KEY_QUERY_TEXT = "query_text";
	private static final String KEY_SHOW_DELETED = "show_deleted";
	private static final String KEY_SHOW_NOT_TAKEN = "show_not_taken";

	private ClassroomChangeCursorAdapter mClassroomChangeCursorAdapter;

	public static ClassroomChangeListFragment newInstance() {
		ClassroomChangeListFragment fragment;
		Bundle args;

		fragment = new ClassroomChangeListFragment();
		args = new Bundle();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mClassroomChangeCursorAdapter = new ClassroomChangeCursorAdapter(getActivity(), null, 0);

		setEmptyText(getText(R.string.empty_classroom_changes));
		setListAdapter(mClassroomChangeCursorAdapter);
		setListShown(false);
		setHasOptionsMenu(true);

		getLoaderManager().initLoader(0, getArguments(), this);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		MenuItem searchItem;
		MenuItem notTakenItem;
		MenuItem deletedItem;
		SearchView searchView;

		Bundle args;

		super.onPrepareOptionsMenu(menu);

		searchItem = menu.findItem(R.id.menu_search);
		notTakenItem = menu.findItem(R.id.menu_not_taken);
		deletedItem = menu.findItem(R.id.menu_deleted);
		searchView = (SearchView) searchItem.getActionView();

		args = getArguments();

		notTakenItem.setVisible(true);
		notTakenItem.setChecked(args.getBoolean(KEY_SHOW_NOT_TAKEN));
		deletedItem.setVisible(true);
		deletedItem.setChecked(args.getBoolean(KEY_SHOW_DELETED));

        if (searchView != null) {
            searchView.setOnQueryTextListener(this);
            searchView.setIconified(TextUtils.isEmpty(args.getString(KEY_QUERY_TEXT)));
        } else {
            searchItem.setVisible(false);
        }
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean ret;
		boolean checked;
		Bundle args;

		switch (item.getItemId()) {
		case R.id.menu_not_taken:
			args = getArguments();
			checked = !item.isChecked();
			item.setChecked(checked);
			args.putBoolean(KEY_SHOW_NOT_TAKEN, checked);
			getLoaderManager().restartLoader(0, args, this);

			ret = true;

			break;

		case R.id.menu_deleted:
			args = getArguments();
			checked = !item.isChecked();
			item.setChecked(checked);
			args.putBoolean(KEY_SHOW_DELETED, checked);
			getLoaderManager().restartLoader(0, args, this);

			ret = true;

			break;

		default:
			ret = super.onOptionsItemSelected(item);

			break;
		}

		return ret;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		CursorLoader loader;
		Uri uri;
		String[] projection;
		String queryText;
		StringBuilder selectionBuilder;
		String[] selectionArgs;

		uri = PortalContract.ClassroomChanges.CONTENT_URI;
		projection = new String[] { PortalContract.ClassroomChanges._ID,
				PortalContract.ClassroomChanges.SUBJECT,
				PortalContract.ClassroomChanges.PROFESSOR,
				PortalContract.ClassroomChanges.LECTURE_DATE,
				PortalContract.ClassroomChanges.LECTURE_PERIOD,
				PortalContract.ClassroomChanges.OLD_CLASSROOM,
				PortalContract.ClassroomChanges.NEW_CLASSROOM,
				PortalContract.ClassroomChanges.NOTE_1,
				PortalContract.ClassroomChanges.NOTE_2,
				PortalContract.ClassroomChanges.IS_TAKEN,
				PortalContract.ClassroomChanges.IS_DELETED };
		queryText = args.getString(KEY_QUERY_TEXT);
		selectionBuilder = new StringBuilder();
		selectionArgs = null;
		if (!TextUtils.isEmpty(queryText)) {
			queryText = "%" + queryText + "%";
			selectionBuilder.append("(");
			selectionBuilder.append(PortalContract.ClassroomChanges.SUBJECT);
			selectionBuilder.append(" LIKE ? OR ");
			selectionBuilder.append(PortalContract.ClassroomChanges.PROFESSOR);
			selectionBuilder.append(" LIKE ? )");
			selectionArgs = new String[] { queryText, queryText };
		}
		if (!args.getBoolean(KEY_SHOW_NOT_TAKEN)) {
			if (selectionBuilder.length() > 0) {
				selectionBuilder.append(" AND ");
			}
			selectionBuilder.append(PortalContract.ClassroomChanges.IS_TAKEN);
			selectionBuilder.append(" = 1");
		}
		if (!args.getBoolean(KEY_SHOW_DELETED)) {
			if (selectionBuilder.length() > 0) {
				selectionBuilder.append(" AND ");
			}
			selectionBuilder.append(PortalContract.ClassroomChanges.IS_DELETED);
			selectionBuilder.append(" = 0");
		}
		loader = new CursorLoader(getActivity(), uri, projection, selectionBuilder.toString(), selectionArgs, null);

		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mClassroomChangeCursorAdapter.swapCursor(data);

		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mClassroomChangeCursorAdapter.swapCursor(null);
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		Bundle args;
		String oldText;

		args = getArguments();
		oldText = args.getString(KEY_QUERY_TEXT);
		newText = TextUtils.isEmpty(newText) ? null : newText;
		if ((oldText != null && !oldText.equals(newText)) || (newText != null && !newText.equals(oldText))) {
			args.putString(KEY_QUERY_TEXT, newText);
			getLoaderManager().restartLoader(0, args, this);
		}

		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

	private static class ClassroomChangeCursorAdapter extends CursorAdapter {

		public ClassroomChangeCursorAdapter(Context context, Cursor c, int flags) {
			super(context, c, flags);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			TextView subjectTextView;
			TextView professorTextView;
			ImageView deletedImageView;
			TextView dateTextView;
			TextView periodTextView;
			TextView oldClassroomTextView;
			TextView newClassroomTextView;
			TextView note1TextView;
			TextView note2TextView;

			String subject;
			String professor;
			boolean deleted;
			long date;
			int period;
			String oldClassroom;
			String newClassroom;
			String note1;
			String note2;

			subjectTextView = (TextView) view.findViewById(R.id.subject_text_view);
			deletedImageView = (ImageView) view.findViewById(R.id.deleted_image_view);
			professorTextView = (TextView) view.findViewById(R.id.professor_text_view);
			dateTextView = (TextView) view.findViewById(R.id.date_text_view);
			periodTextView = (TextView) view.findViewById(R.id.period_text_view);
			oldClassroomTextView = (TextView) view.findViewById(R.id.old_classroom_text_view);
			newClassroomTextView = (TextView) view.findViewById(R.id.new_classroom_text_view);
			note1TextView = (TextView) view.findViewById(R.id.note_1_text_view);
			note2TextView = (TextView) view.findViewById(R.id.note_2_text_view);

			subject = cursor.getString(cursor.getColumnIndex(PortalContract.ClassroomChanges.SUBJECT));
			professor = cursor.getString(cursor.getColumnIndex(PortalContract.ClassroomChanges.PROFESSOR));
			deleted = cursor.getInt(cursor.getColumnIndex(PortalContract.ClassroomChanges.IS_DELETED)) > 0;
			date = cursor.getLong(cursor.getColumnIndex(PortalContract.ClassroomChanges.LECTURE_DATE));
			period = cursor.getInt(cursor.getColumnIndex(PortalContract.ClassroomChanges.LECTURE_PERIOD));
			oldClassroom = cursor.getString(cursor.getColumnIndex(PortalContract.ClassroomChanges.OLD_CLASSROOM));
			newClassroom = cursor.getString(cursor.getColumnIndex(PortalContract.ClassroomChanges.NEW_CLASSROOM));
			note1 = cursor.getString(cursor.getColumnIndex(PortalContract.ClassroomChanges.NOTE_1));
			note2 = cursor.getString(cursor.getColumnIndex(PortalContract.ClassroomChanges.NOTE_2));

			view.setBackgroundResource(deleted ? R.drawable.bg_deleted : R.drawable.bg_read);

			subjectTextView.setText(subject);
			professorTextView.setText(professor);
			deletedImageView.setVisibility(deleted ? View.VISIBLE : View.INVISIBLE);

			dateTextView.setText(DateUtils.formatDateTime(context, date,
					DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY
							| DateUtils.FORMAT_ABBREV_WEEKDAY | DateUtils.FORMAT_NUMERIC_DATE));
            if (period > 0) {
                periodTextView.setText(context.getString(R.string.template_period, period));
            } else {
                periodTextView.setText("");
            }

			oldClassroomTextView.setText(String.valueOf(oldClassroom));
			newClassroomTextView.setText(String.valueOf(newClassroom));

			if (!TextUtils.isEmpty(note1)) {
				note1TextView.setText(note1);
				note1TextView.setVisibility(View.VISIBLE);
			} else {
				note1TextView.setVisibility(View.GONE);
			}
			if (!TextUtils.isEmpty(note2)) {
				note2TextView.setText(note2);
				note2TextView.setVisibility(View.VISIBLE);
			} else {
				note2TextView.setVisibility(View.GONE);
			}
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			View view;

			view = LayoutInflater.from(context).inflate(R.layout.portal_classroom_change_list_item, parent, false);

			return view;
		}
	}
}
