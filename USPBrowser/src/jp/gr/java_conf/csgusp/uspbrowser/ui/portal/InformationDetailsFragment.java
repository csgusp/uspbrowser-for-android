/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.portal;

import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.client.portal.InformationContent;
import jp.gr.java_conf.csgusp.uspbrowser.preference.BrowserPreferences;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.IPortalService;
import jp.gr.java_conf.csgusp.uspbrowser.ui.ViewerActivity;

import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

/**
 * InformationDetailsFragment クラスは、お知らせの詳細を表示するフラグメントを実装したクラスです。
 *
 * @author leak4mk0
 * @version 23
 * @since 1
 */
public class InformationDetailsFragment extends SherlockFragment implements LoaderCallbacks<Cursor>, OnClickListener {
    private static final String KEY_DATABASE_ID = "id";
    private static final String KEY_INFORMATION_ID = "information_id";
    private static final int ID_INFORMATION_LOADER = 1;
    private static final int ID_ATTACHMENT_LOADER = 2;

    private BrowserPreferences mBrowserPreferences;
    private Cursor mInformationCursor;
    private Cursor mAttachmentCursor;
    private IPortalService mPortalService;
    private PortalServiceConnection mPortalServiceConnection;
    private Timer mReadTimer;
    private String mInformationText;
    private String mAttachmentText;

    public static InformationDetailsFragment newInstance(int id) {
        InformationDetailsFragment fragment;
        Bundle args;

        fragment = new InformationDetailsFragment();
        args = new Bundle();
        args.putInt(KEY_DATABASE_ID, id);
        args.putInt(KEY_INFORMATION_ID, 0);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBrowserPreferences = BrowserPreferences.getInstance(getActivity());

        setHasOptionsMenu(true);

        mPortalServiceConnection = new PortalServiceConnection();
        getActivity().bindService(new Intent(IPortalService.class.getName()),
                mPortalServiceConnection, SherlockFragmentActivity.BIND_AUTO_CREATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        TextView titleTextView;
        TextView publicDateTextView;
        TextView publicPersonTextView;
        ImageView deletedImageView;
        ImageView readImageView;
        ImageView attachedImageView;
        ImageView priorityImageView;
        TextView contentTextView;
        TextView attachmentHeaderTextView;
        View attachmentSeparatorView;
        LinearLayout attachmentLinearLayout;
        ProgressBar attachmentProgressBar;

        view = inflater.inflate(R.layout.portal_information_details_fragment, container, false);
        titleTextView = (TextView) view.findViewById(R.id.title_text_view);
        publicDateTextView = (TextView) view.findViewById(R.id.public_date_text_view);
        publicPersonTextView = (TextView) view.findViewById(R.id.public_person_text_view);
        deletedImageView = (ImageView) view.findViewById(R.id.deleted_image_view);
        readImageView = (ImageView) view.findViewById(R.id.read_image_view);
        attachedImageView = (ImageView) view.findViewById(R.id.attached_image_view);
        priorityImageView = (ImageView) view.findViewById(R.id.priority_image_view);
        contentTextView = (TextView) view.findViewById(R.id.content_text_view);
        attachmentHeaderTextView = (TextView) view.findViewById(R.id.attachments_header_text_view);
        attachmentSeparatorView = view.findViewById(R.id.attachments_separator_view);
        attachmentLinearLayout = (LinearLayout) view.findViewById(R.id.attachments_linear_layout);
        attachmentProgressBar = (ProgressBar) view.findViewById(R.id.attachments_progress_bar);

        titleTextView.setVisibility(View.INVISIBLE);
        publicDateTextView.setVisibility(View.INVISIBLE);
        publicPersonTextView.setVisibility(View.INVISIBLE);
        deletedImageView.setVisibility(View.INVISIBLE);
        readImageView.setVisibility(View.INVISIBLE);
        attachedImageView.setVisibility(View.INVISIBLE);
        priorityImageView.setVisibility(View.INVISIBLE);
        contentTextView.setVisibility(View.GONE);
        attachmentHeaderTextView.setVisibility(View.GONE);
        attachmentSeparatorView.setVisibility(View.GONE);
        attachmentLinearLayout.setVisibility(View.GONE);
        attachmentProgressBar.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(ID_INFORMATION_LOADER, getArguments(), this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        getActivity().unbindService(mPortalServiceConnection);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String shareText;
        ShareCompat.IntentBuilder intentBuilder;
        boolean ret;

        switch (item.getItemId()) {
            case R.id.menu_share:
                shareText = "";
                shareText += mInformationText != null ? mInformationText : "";
                shareText += mAttachmentText != null ? mAttachmentText : "";
                intentBuilder = ShareCompat.IntentBuilder.from(getActivity());
                intentBuilder.setText(shareText);
                intentBuilder.setType("text/plain");
                intentBuilder.startChooser();
                ret = true;

                break;

            case R.id.menu_refresh:
                try {
                    String userId;
                    String password;

                    userId = mBrowserPreferences.getSystemUserId();
                    password = mBrowserPreferences.getSystemPassword();
                    mPortalService.requestLogon(userId, password);
                    mPortalService.requestFetchInformation(getArguments().getInt(KEY_INFORMATION_ID));
                    mPortalService.requestLogout();
                } catch (RemoteException ignored) {
                }
                ret = false;

                break;

            default:
                ret = super.onOptionsItemSelected(item);
        }

        return ret;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            if (mReadTimer == null) {
                mReadTimer = new Timer();
                mReadTimer.schedule(new ReadTimerTask(), mBrowserPreferences.getPortalReadTime());
            }
        } else {
            if (mReadTimer != null) {
                mReadTimer.cancel();
                mReadTimer = null;
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri;
        String[] projection;
        String selection;
        String selectionArgs[];

        switch (id) {
            case ID_INFORMATION_LOADER:
                uri = ContentUris.withAppendedId(PortalContract.Information.CONTENT_URI, args.getInt(KEY_DATABASE_ID));
                projection = new String[]{PortalContract.Information._ID,
                        PortalContract.Information.INFORMATION_ID,
                        PortalContract.Information.TITLE,
                        PortalContract.Information.PUBLIC_START_DATE,
                        PortalContract.Information.PUBLIC_END_DATE,
                        PortalContract.Information.PUBLIC_PERSON,
                        PortalContract.Information.CONTENT,
                        PortalContract.Information.IS_ATTACHED,
                        PortalContract.Information.IS_FETCHED,
                        PortalContract.Information.IS_DELETED,
                        PortalContract.Information.IS_READ,
                        PortalContract.Information.IS_ATTACHED,
                        PortalContract.Information.PRIORITY};
                selection = null;
                selectionArgs = null;

                break;

            case ID_ATTACHMENT_LOADER:
                uri = PortalContract.InformationAttachments.CONTENT_URI;
                projection = new String[]{PortalContract.InformationAttachments._ID,
                        PortalContract.InformationAttachments.INFORMATION_ID,
                        PortalContract.InformationAttachments.FILENAME,
                        PortalContract.InformationAttachments.DESCRIPTION,
                        PortalContract.InformationAttachments.URL};
                selection = PortalContract.InformationAttachments.INFORMATION_ID + " = ?";
                selectionArgs = new String[]{String.valueOf(args.getInt(KEY_DATABASE_ID))};

                break;

            default:
                uri = null;
                projection = null;
                selection = null;
                selectionArgs = null;

                break;
        }

        return new CursorLoader(getActivity(), uri, projection, selection, selectionArgs, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        View view;
        TextView titleTextView;
        TextView publicDateTextView;
        TextView publicPersonTextView;
        ImageView deletedImageView;
        ImageView readImageView;
        ImageView attachedImageView;
        ImageView priorityImageView;
        TextView contentTextView;
        ProgressBar contentProgressBar;
        TextView attachmentHeaderTextView;
        View attachmentSeparatorView;
        LinearLayout attachmentLinearLayout;
        ProgressBar attachmentProgressBar;

        int informationId;
        String title;
        long publicBeginDate;
        long publicEndDate;
        String publicDateText;
        String publicPerson;
        String content;
        boolean deleted;
        boolean read;
        boolean attached;
        int priority;
        boolean fetched;

        StringBuilder textBuilder;

        switch (loader.getId()) {
            case ID_INFORMATION_LOADER:
                mInformationCursor = data;

                view = getView();
                titleTextView = (TextView) view.findViewById(R.id.title_text_view);
                publicDateTextView = (TextView) view.findViewById(R.id.public_date_text_view);
                publicPersonTextView = (TextView) view.findViewById(R.id.public_person_text_view);
                deletedImageView = (ImageView) view.findViewById(R.id.deleted_image_view);
                readImageView = (ImageView) view.findViewById(R.id.read_image_view);
                attachedImageView = (ImageView) view.findViewById(R.id.attached_image_view);
                priorityImageView = (ImageView) view.findViewById(R.id.priority_image_view);
                contentTextView = (TextView) view.findViewById(R.id.content_text_view);
                contentProgressBar = (ProgressBar) view.findViewById(R.id.content_progress_bar);
                attachmentHeaderTextView = (TextView) view.findViewById(R.id.attachments_header_text_view);
                attachmentSeparatorView = view.findViewById(R.id.attachments_separator_view);
                attachmentProgressBar = (ProgressBar) view.findViewById(R.id.attachments_progress_bar);

                if (!data.moveToFirst()) {
                    contentProgressBar.setVisibility(View.GONE);

                    break;
                }

                informationId = data.getInt(data.getColumnIndex(PortalContract.Information.INFORMATION_ID));
                title = data.getString(data.getColumnIndex(PortalContract.Information.TITLE));
                publicBeginDate = data.getLong(data.getColumnIndex(PortalContract.Information.PUBLIC_START_DATE));
                publicEndDate = data.getLong(data.getColumnIndex(PortalContract.Information.PUBLIC_END_DATE));
                publicPerson = data.getString(data.getColumnIndex(PortalContract.Information.PUBLIC_PERSON));
                content = data.getString(data.getColumnIndex(PortalContract.Information.CONTENT));
                deleted = data.getInt(data.getColumnIndex(PortalContract.Information.IS_DELETED)) > 0;
                read = data.getInt(data.getColumnIndex(PortalContract.Information.IS_READ)) > 0;
                attached = data.getInt(data.getColumnIndex(PortalContract.Information.IS_ATTACHED)) > 0;
                priority = data.getInt(data.getColumnIndex(PortalContract.Information.PRIORITY));
                fetched = data.getInt(data.getColumnIndex(PortalContract.Information.IS_FETCHED)) > 0;

                getArguments().putInt(KEY_INFORMATION_ID, informationId);
                titleTextView.setText(title);
                titleTextView.setVisibility(View.VISIBLE);
                publicPersonTextView.setText(publicPerson);
                publicPersonTextView.setVisibility(View.VISIBLE);
                deletedImageView.setVisibility(deleted ? View.VISIBLE : View.INVISIBLE);
                readImageView.setVisibility(read ? View.INVISIBLE : View.VISIBLE);
                attachedImageView.setVisibility(attached ? View.VISIBLE : View.INVISIBLE);

                switch (priority) {
                    case InformationContent.PRIORITY_MOST_IMPORTANT:
                        priorityImageView.setImageResource(R.drawable.ic_important);
                        priorityImageView.setVisibility(View.VISIBLE);

                        break;

                    case InformationContent.PRIORITY_IMPORTANT:
                        priorityImageView.setImageResource(R.drawable.ic_half_important);
                        priorityImageView.setVisibility(View.VISIBLE);

                        break;

                    case InformationContent.PRIORITY_NORMAL:
                        priorityImageView.setImageResource(R.drawable.ic_not_important);
                        priorityImageView.setVisibility(View.VISIBLE);

                        break;

                    case InformationContent.PRIORITY_URGENT:
                        priorityImageView.setImageResource(R.drawable.ic_warning);
                        priorityImageView.setVisibility(View.VISIBLE);

                        break;

                    default:
                        priorityImageView.setVisibility(View.INVISIBLE);

                        break;
                }

                if (!fetched) {
                    publicDateText = DateUtils.formatDateTime(getActivity(), publicBeginDate,
                            DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY
                                    | DateUtils.FORMAT_ABBREV_WEEKDAY | DateUtils.FORMAT_NUMERIC_DATE);
                    if (!deleted) {
                        try {
                            String userId;
                            String password;

                            userId = mBrowserPreferences.getSystemUserId();
                            password = mBrowserPreferences.getSystemPassword();
                            mPortalService.requestLogon(userId, password);
                            mPortalService.requestFetchInformation(informationId);
                            mPortalService.requestLogout();
                        } catch (RemoteException ignored) {
                        }
                    } else {
                        contentTextView.setText(R.string.deleted);
                        contentTextView.setVisibility(View.VISIBLE);
                        contentProgressBar.setVisibility(View.GONE);
                    }
                } else {
                    Pattern bracketPattern;
                    Matcher bracketMatcher;

                    if (publicEndDate != PortalContract.Information.DATE_NOT_SPECIFIED) {
                        publicDateText = DateUtils.formatDateRange(getActivity(), publicBeginDate, publicEndDate,
                                DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY
                                        | DateUtils.FORMAT_ABBREV_WEEKDAY | DateUtils.FORMAT_NUMERIC_DATE);
                    } else {
                        publicDateText = getString(R.string.infinite);
                    }
                    bracketPattern = Pattern.compile(" ?(?:\\(|（)((?:http://|https://|rtsp://).*?)(?:\\)|）) ?");
                    bracketMatcher = bracketPattern.matcher(content);
                    contentTextView.setText(bracketMatcher.replaceAll(" ($1) "));
                    contentTextView.setVisibility(View.VISIBLE);
                    contentProgressBar.setVisibility(View.GONE);
                    if (attached) {
                        Bundle args;

                        attachmentHeaderTextView.setVisibility(View.VISIBLE);
                        attachmentSeparatorView.setVisibility(View.VISIBLE);
                        attachmentProgressBar.setVisibility(View.VISIBLE);

                        args = new Bundle();
                        args.putInt(KEY_DATABASE_ID, informationId);
                        getLoaderManager().initLoader(ID_ATTACHMENT_LOADER, args, this);
                    }
                }
                publicDateTextView.setText(publicDateText);
                publicDateTextView.setVisibility(View.VISIBLE);

                textBuilder = new StringBuilder();
                textBuilder.append(getString(R.string.prefix_title));
                textBuilder.append(" ");
                textBuilder.append(title);
                textBuilder.append("\n");
                textBuilder.append(getString(R.string.prefix_public_date));
                textBuilder.append(" ");
                textBuilder.append(publicDateText);
                textBuilder.append("\n");
                textBuilder.append(getString(R.string.prefix_public_person));
                textBuilder.append(" ");
                textBuilder.append(publicPerson);
                textBuilder.append("\n");
                if (!TextUtils.isEmpty(content)) {
                    textBuilder.append(getString(R.string.prefix_content));
                    textBuilder.append("\n");
                    textBuilder.append(content);
                    textBuilder.append("\n");
                }
                mInformationText = textBuilder.toString();

                break;

            case ID_ATTACHMENT_LOADER:
                mAttachmentCursor = data;

                view = getView();
                attachmentLinearLayout = (LinearLayout) view.findViewById(R.id.attachments_linear_layout);
                attachmentProgressBar = (ProgressBar) view.findViewById(R.id.attachments_progress_bar);

                if (!data.moveToFirst()) {
                    attachmentProgressBar.setVisibility(View.GONE);

                    break;
                }

                textBuilder = new StringBuilder();
                textBuilder.append(getString(R.string.prefix_attachments));
                textBuilder.append("\n");

                attachmentLinearLayout.removeAllViews();
                do {
                    int attachmentId;
                    String filename;
                    String description;
                    String url;

                    View attachmentView;
                    TextView filenameTextView;
                    TextView descriptionTextView;
                    Button saveButton;
                    Button openButton;

                    attachmentId = data.getInt(data.getColumnIndex(PortalContract.InformationAttachments._ID));
                    filename = data.getString(data.getColumnIndex(PortalContract.InformationAttachments.FILENAME));
                    description = data.getString(data.getColumnIndex(PortalContract.InformationAttachments.DESCRIPTION));
                    url = data.getString(data.getColumnIndex(PortalContract.InformationAttachments.URL));

                    attachmentView = getLayoutInflater(getArguments()).inflate(R.layout.portal_information_attachment, null);

                    filenameTextView = (TextView) attachmentView.findViewById(R.id.filename_text_view);
                    descriptionTextView = (TextView) attachmentView.findViewById(R.id.description_text_view);
                    saveButton = (Button) attachmentView.findViewById(R.id.preview_button);
                    openButton = (Button) attachmentView.findViewById(R.id.download_button);

                    filenameTextView.setText(filename);
                    if (!TextUtils.isEmpty(description)) {
                        descriptionTextView.setText(description);
                        descriptionTextView.setVisibility(View.VISIBLE);
                    } else {
                        descriptionTextView.setVisibility(View.GONE);
                    }
                    saveButton.setOnClickListener(this);
                    saveButton.setTag(attachmentId);
                    openButton.setOnClickListener(this);
                    openButton.setTag(attachmentId);

                    attachmentLinearLayout.addView(attachmentView);

                    textBuilder.append(filename);
                    textBuilder.append("\n");
                    if (!TextUtils.isEmpty(description)) {
                        textBuilder.append(description);
                        textBuilder.append("\n");
                    }
                    textBuilder.append(url.replace("https://", "http://"));
                    textBuilder.append("\n");
                } while (data.moveToNext());
                attachmentLinearLayout.setVisibility(View.VISIBLE);
                attachmentProgressBar.setVisibility(View.GONE);

                mAttachmentText = textBuilder.toString();

                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case ID_INFORMATION_LOADER:
                mInformationCursor = null;

                break;

            case ID_ATTACHMENT_LOADER:
                mAttachmentCursor = null;

                break;
        }
    }

    @SuppressLint({"InlinedApi", "NewApi"})
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.preview_button:
                if (mAttachmentCursor.moveToFirst()) {
                    int id;
                    String url;

                    id = (Integer) v.getTag();
                    url = null;
                    do {
                        if (mAttachmentCursor.getInt(mAttachmentCursor
                                .getColumnIndex(PortalContract.InformationAttachments._ID)) == id) {
                            url = mAttachmentCursor.getString(mAttachmentCursor
                                    .getColumnIndex(PortalContract.InformationAttachments.URL));
                        }
                    } while (mAttachmentCursor.moveToNext());
                    if (url != null) {
                        Intent intent;

                        intent = new Intent(getActivity(), ViewerActivity.class);
                        intent.putExtra(ViewerActivity.NAME_DOCUMENT_URL, url);
                        startActivity(intent);
                    }
                }

                break;

            case R.id.download_button:
                if (mAttachmentCursor.moveToFirst()) {
                    int id;
                    String url;
                    String filename;

                    id = (Integer) v.getTag();
                    url = null;
                    filename = null;
                    do {
                        if (mAttachmentCursor.getInt(mAttachmentCursor
                                .getColumnIndex(PortalContract.InformationAttachments._ID)) == id) {
                            url = mAttachmentCursor.getString(mAttachmentCursor
                                    .getColumnIndex(PortalContract.InformationAttachments.URL));
                            filename = mAttachmentCursor.getString(mAttachmentCursor
                                    .getColumnIndex(PortalContract.InformationAttachments.FILENAME));
                        }
                    } while (mAttachmentCursor.moveToNext());
                    if (url != null) {
                        Uri uri;

                        uri = Uri.parse(Uri.encode(url.replace("https://", "http://"), ";/?:@&="));
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
                            Intent intent;

                            intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        } else if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                            DownloadManager manager;
                            DownloadManager.Request request;
                            File file;

                            manager = (DownloadManager) getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                            request = new DownloadManager.Request(uri);

                            file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                            file.mkdirs();
                            file = new File(file, filename);
                            request.setDestinationUri(Uri.fromFile(file));
                            request.setTitle(filename);
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
                                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                            }
                            manager.enqueue(request);
                        } else {
                            Toast.makeText(getActivity(), R.string.message_download_failure_storage, Toast.LENGTH_LONG).show();
                        }
                    }
                }

                break;
        }
    }

    public int getInformationId() {
        return getArguments().getInt(KEY_DATABASE_ID);
    }

    public void setInformationId(int informationId) {
        getArguments().putInt(KEY_DATABASE_ID, informationId);
    }

    private class ReadTimerTask extends TimerTask {
        @Override
        public void run() {
            int id;
            boolean read;

            if (mInformationCursor == null) {
                return;
            }
            id = mInformationCursor.getInt(mInformationCursor.getColumnIndex(PortalContract.Information._ID));
            read = mInformationCursor.getInt(mInformationCursor
                    .getColumnIndex(PortalContract.Information.IS_READ)) > 0;
            if (!read) {
                ContentValues values;

                values = new ContentValues();
                values.put(PortalContract.Information.IS_READ, 1);
                getActivity().getContentResolver().update(
                        ContentUris.withAppendedId(PortalContract.Information.CONTENT_URI, id), values, null, null);
            }
        }
    }

    private class PortalServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mPortalService = IPortalService.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mPortalService = null;
        }
    }
}
