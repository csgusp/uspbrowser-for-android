/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.portal;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.IPortalService;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.IPortalServiceCallback;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.PortalService;
import jp.gr.java_conf.csgusp.uspbrowser.ui.HelpActivity;
import jp.gr.java_conf.csgusp.uspbrowser.ui.SettingsActivity;
import jp.gr.java_conf.csgusp.uspbrowser.ui.widget.ArrayPagerAdapter;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;

/**
 * ScheduleDetailsActivity クラスは、スケジュールの詳細を表示するアクティビティを実装したクラスです。
 *
 * @author leak4mk0
 * @version 35
 * @since 1
 */
public class ScheduleDetailsActivity extends SherlockFragmentActivity {
    public static final String KEY_IDS = "ids";
    public static final String KEY_INDEX = "index";

    private IPortalService mPortalService;
    private PortalServiceConnection mPortalServiceConnection;
    private PortalServiceCallback mPortalServiceCallback;
    private ScheduleDetailsHandler mScheduleDetailsHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ActionBar actionBar;
        ViewPager viewPager;
        PagerTitleStrip pagerTitleStrip;
        ScheduleDetailsPagerAdapter pagerAdapter;
        int[] ids;
        int index;

        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.portal_schedule_details_activity);
        setSupportProgressBarIndeterminateVisibility(false);

        mPortalServiceConnection = new PortalServiceConnection();
        mPortalServiceCallback = new PortalServiceCallback();
        mScheduleDetailsHandler = new ScheduleDetailsHandler(this);
        bindService(new Intent(IPortalService.class.getName()), mPortalServiceConnection, BIND_AUTO_CREATE);

        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        ids = (int[]) getIntent().getExtras().get(KEY_IDS);
        index = getIntent().getExtras().getInt(KEY_INDEX);

        pagerAdapter = new ScheduleDetailsPagerAdapter(getSupportFragmentManager());
        pagerAdapter.swapArray(ids);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOnPageChangeListener(pagerAdapter);
        viewPager.setCurrentItem(index, false);

        pagerTitleStrip = (PagerTitleStrip) findViewById(R.id.pager_title_strip);
        pagerTitleStrip.setTextSpacing(pagerTitleStrip.getTextSpacing());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mPortalService != null) {
            try {
                mPortalService.unregisterCallback(mPortalServiceCallback);
            } catch (RemoteException ignored) {
            }
        }
        unbindService(mPortalServiceConnection);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.portal_schedule_details, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem refreshItem;
        MenuItem cancelItem;
        boolean running;

        super.onPrepareOptionsMenu(menu);

        refreshItem = menu.findItem(R.id.menu_refresh);
        cancelItem = menu.findItem(R.id.menu_cancel);

        running = false;
        if (mPortalService != null) {
            try {
                running = mPortalService.isRunning();
            } catch (RemoteException ignored) {
            }
        }
        refreshItem.setVisible(!running);
        cancelItem.setVisible(running);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        PackageInfo packageInfo;
        boolean ret;

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                ret = true;

                break;

            case R.id.menu_cancel:
                try {
                    mPortalService.cancelRequest(PortalService.ID_ALL);
                } catch (RemoteException ignored) {
                }
                ret = true;

                break;

            case R.id.menu_settings:
                intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
                ret = true;

                break;

            case R.id.menu_feedback:
                intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(getString(R.string.url_email)));
                intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.feedback_subject));
                try {
                    packageInfo = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_META_DATA);
                } catch (PackageManager.NameNotFoundException e) {
                    packageInfo = null;
                }
                if (packageInfo != null) {
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.feedback_text,
                            Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE, packageInfo.versionName, packageInfo.versionCode));
                } else {
                    intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.feedback_text,
                            Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE, R.string.unknown, -1));
                }
                startActivity(intent);
                ret = true;

                break;

            case R.id.menu_help:
                startActivity(new Intent(getApplicationContext(), HelpActivity.class));
                ret = true;

                break;

            default:
                ret = super.onOptionsItemSelected(item);
        }

        return ret;
    }

    private class ScheduleDetailsPagerAdapter
            extends ArrayPagerAdapter implements ViewPager.OnPageChangeListener {
        private int mCurrentPosition;

        public ScheduleDetailsPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public int getItemPosition(Object object) {
            int schedulePosition;
            int[] scheduleArray;

            schedulePosition = POSITION_NONE;
            scheduleArray = getArray();
            if (object != null && object instanceof ScheduleDetailsFragment && scheduleArray != null) {
                int scheduleId;

                scheduleId = ((ScheduleDetailsFragment) object).getScheduleId();
                for (int i = 0; i < scheduleArray.length; i++) {
                    if (scheduleArray[i] == scheduleId) {
                        schedulePosition = i;

                        break;
                    }
                }
            }

            return schedulePosition;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title;

            if (mCurrentPosition > position) {
                title = getString(R.string.newer);
            } else if (mCurrentPosition < position) {
                title = getString(R.string.older);
            } else {
                title = getString(R.string.template_page, position + 1, getCount());
            }

            return title;
        }

        @Override
        public Fragment newFragment(FragmentManager manager, int id) {
            return ScheduleDetailsFragment.newInstance(id);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            mCurrentPosition = position;
        }
    }

    private class PortalServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mPortalService = IPortalService.Stub.asInterface(service);
            try {
                mPortalService.registerCallback(mPortalServiceCallback);
                setSupportProgressBarIndeterminateVisibility(mPortalService.isRunning());
            } catch (RemoteException ignored) {
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mPortalService = null;
        }
    }

    private class PortalServiceCallback extends IPortalServiceCallback.Stub {
        @Override
        public void onResult(int action, int status) {
            Message message;

            message = new Message();
            message.arg1 = action;
            message.arg2 = status;
            mScheduleDetailsHandler.sendMessage(message);
        }
    }

    private static class ScheduleDetailsHandler extends Handler {
        ScheduleDetailsActivity mScheduleDetailsActivity;

        public ScheduleDetailsHandler(ScheduleDetailsActivity scheduleDetailsActivity) {
            mScheduleDetailsActivity = scheduleDetailsActivity;
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.arg1) {
                case PortalService.ACTION_BEGIN:
                    mScheduleDetailsActivity.setSupportProgressBarIndeterminateVisibility(true);
                    mScheduleDetailsActivity.supportInvalidateOptionsMenu();

                    break;

                case PortalService.ACTION_END:
                    mScheduleDetailsActivity.setSupportProgressBarIndeterminateVisibility(false);
                    mScheduleDetailsActivity.supportInvalidateOptionsMenu();

                    break;
            }
        }
    }
}
