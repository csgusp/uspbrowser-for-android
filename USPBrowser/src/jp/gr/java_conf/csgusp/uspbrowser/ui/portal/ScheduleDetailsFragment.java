/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.portal;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.client.portal.ScheduleContent;
import jp.gr.java_conf.csgusp.uspbrowser.preference.BrowserPreferences;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.IPortalService;

import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.util.Linkify;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ScheduleDetailsFragment クラスは、スケジュールの詳細を表示するフラグメントを実装したクラスです。
 *
 * @author leak4mk0
 * @version 23
 * @since 1
 */
public class ScheduleDetailsFragment extends SherlockFragment implements LoaderCallbacks<Cursor> {
    private static final String KEY_DATABASE_ID = "id";
    private static final String KEY_SCHEDULE_ID = "schedule_id";

    private BrowserPreferences mBrowserPreferences;
    private IPortalService mPortalService;
    private PortalServiceConnection mPortalServiceConnection;
    private String mScheduleText;

    public static ScheduleDetailsFragment newInstance(int id) {
        ScheduleDetailsFragment fragment;
        Bundle args;

        fragment = new ScheduleDetailsFragment();
        args = new Bundle();
        args.putInt(KEY_DATABASE_ID, id);
        args.putInt(KEY_SCHEDULE_ID, 0);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBrowserPreferences = BrowserPreferences.getInstance(getActivity());

        setHasOptionsMenu(true);

        mPortalServiceConnection = new PortalServiceConnection();
        getActivity().bindService(new Intent(IPortalService.class.getName()),
                mPortalServiceConnection, SherlockFragmentActivity.BIND_AUTO_CREATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        TextView titleTextView;
        TextView dateTextView;
        TextView timeHeaderTextView;
        TextView timeTextView;
        TextView placeHeaderTextView;
        TextView placeTextView;
        ImageView deletedImageView;
        ImageView priorityImageView;
        TextView contentTextView;

        view = inflater.inflate(R.layout.portal_schedule_details_fragment, container, false);
        titleTextView = (TextView) view.findViewById(R.id.title_text_view);
        dateTextView = (TextView) view.findViewById(R.id.date_text_view);
        timeHeaderTextView = (TextView) view.findViewById(R.id.time_header_text_view);
        timeTextView = (TextView) view.findViewById(R.id.time_text_view);
        placeHeaderTextView = (TextView) view.findViewById(R.id.place_header_text_view);
        placeTextView = (TextView) view.findViewById(R.id.place_text_view);
        deletedImageView = (ImageView) view.findViewById(R.id.deleted_image_view);
        priorityImageView = (ImageView) view.findViewById(R.id.priority_image_view);
        contentTextView = (TextView) view.findViewById(R.id.content_text_view);

        titleTextView.setVisibility(View.INVISIBLE);
        dateTextView.setVisibility(View.INVISIBLE);
        timeHeaderTextView.setVisibility(View.GONE);
        timeTextView.setVisibility(View.GONE);
        placeHeaderTextView.setVisibility(View.GONE);
        placeTextView.setVisibility(View.GONE);
        deletedImageView.setVisibility(View.INVISIBLE);
        priorityImageView.setVisibility(View.INVISIBLE);
        contentTextView.setVisibility(View.GONE);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(0, getArguments(), this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        getActivity().unbindService(mPortalServiceConnection);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String shareText;
        ShareCompat.IntentBuilder intentBuilder;
        boolean ret;

        switch (item.getItemId()) {
            case R.id.menu_share:
                shareText = mScheduleText != null ? mScheduleText : "";
                intentBuilder = ShareCompat.IntentBuilder.from(getActivity());
                intentBuilder.setText(shareText);
                intentBuilder.setType("text/plain");
                intentBuilder.startChooser();
                ret = true;

                break;

            case R.id.menu_refresh:
                try {
                    String userId;
                    String password;

                    userId = mBrowserPreferences.getSystemUserId();
                    password = mBrowserPreferences.getSystemPassword();
                    mPortalService.requestLogon(userId, password);
                    mPortalService.requestFetchSchedule(getArguments().getInt(KEY_SCHEDULE_ID));
                    mPortalService.requestLogout();
                } catch (RemoteException ignored) {
                }
                ret = false;

                break;

            default:
                ret = super.onOptionsItemSelected(item);
        }

        return ret;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getActivity(), ContentUris.withAppendedId(PortalContract.Schedules.CONTENT_URI,
                args.getInt(KEY_DATABASE_ID)),
                new String[]{PortalContract.Schedules._ID,
                        PortalContract.Schedules.SCHEDULE_ID,
                        PortalContract.Schedules.TITLE,
                        PortalContract.Schedules.START_DATE,
                        PortalContract.Schedules.END_DATE,
                        PortalContract.Schedules.START_TIME,
                        PortalContract.Schedules.END_TIME,
                        PortalContract.Schedules.PLACE,
                        PortalContract.Schedules.CONTENT,
                        PortalContract.Schedules.IS_DELETED,
                        PortalContract.Schedules.PRIORITY,
                        PortalContract.Schedules.IS_FETCHED}, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        View view;
        TextView titleTextView;
        TextView dateTextView;
        TextView timeHeaderTextView;
        TextView timeTextView;
        TextView placeHeaderTextView;
        TextView placeTextView;
        ImageView deletedImageView;
        ImageView priorityImageView;
        TextView contentTextView;
        ProgressBar contentProgressBar;

        int scheduleId;
        String title;
        long beginDate;
        long endDate;
        String dateText;
        long beginTime;
        long endTime;
        String timeText;
        String place;
        String content;
        boolean deleted;
        int priority;
        boolean fetched;

        StringBuilder textBuilder;

        view = getView();
        titleTextView = (TextView) view.findViewById(R.id.title_text_view);
        dateTextView = (TextView) view.findViewById(R.id.date_text_view);
        timeHeaderTextView = (TextView) view.findViewById(R.id.time_header_text_view);
        timeTextView = (TextView) view.findViewById(R.id.time_text_view);
        placeHeaderTextView = (TextView) view.findViewById(R.id.place_header_text_view);
        placeTextView = (TextView) view.findViewById(R.id.place_text_view);
        deletedImageView = (ImageView) view.findViewById(R.id.deleted_image_view);
        priorityImageView = (ImageView) view.findViewById(R.id.priority_image_view);
        contentTextView = (TextView) view.findViewById(R.id.content_text_view);
        contentProgressBar = (ProgressBar) view.findViewById(R.id.content_progress_bar);

        if (!data.moveToFirst()) {
            contentProgressBar.setVisibility(View.GONE);

            return;
        }

        scheduleId = data.getInt(data.getColumnIndex(PortalContract.Schedules.SCHEDULE_ID));
        title = data.getString(data.getColumnIndex(PortalContract.Schedules.TITLE));
        beginDate = data.getLong(data.getColumnIndex(PortalContract.Schedules.START_DATE));
        endDate = data.getLong(data.getColumnIndex(PortalContract.Schedules.END_DATE));
        beginTime = data.getInt(data.getColumnIndex(PortalContract.Schedules.START_TIME));
        endTime = data.getInt(data.getColumnIndex(PortalContract.Schedules.END_TIME));
        place = data.getString(data.getColumnIndex(PortalContract.Schedules.PLACE));
        content = data.getString(data.getColumnIndex(PortalContract.Schedules.CONTENT));
        deleted = data.getInt(data.getColumnIndex(PortalContract.Schedules.IS_DELETED)) > 0;
        priority = data.getInt(data.getColumnIndex(PortalContract.Schedules.PRIORITY));
        fetched = data.getInt(data.getColumnIndex(PortalContract.Schedules.IS_FETCHED)) > 0;

        getArguments().putInt(KEY_SCHEDULE_ID, scheduleId);
        titleTextView.setText(title);
        titleTextView.setVisibility(View.VISIBLE);
        deletedImageView.setVisibility(deleted ? View.VISIBLE : View.INVISIBLE);

        if (!fetched) {
            dateText = DateUtils.formatDateTime(getActivity(), beginDate,
                    DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY
                            | DateUtils.FORMAT_ABBREV_WEEKDAY | DateUtils.FORMAT_NUMERIC_DATE);
            timeText = null;
            if (!deleted) {
                try {
                    String userId;
                    String password;

                    userId = mBrowserPreferences.getSystemUserId();
                    password = mBrowserPreferences.getSystemPassword();
                    mPortalService.requestLogon(userId, password);
                    mPortalService.requestFetchSchedule(scheduleId);
                    mPortalService.requestLogout();
                } catch (RemoteException ignored) {
                }
            } else {
                contentTextView.setText(R.string.deleted);
                contentTextView.setVisibility(View.VISIBLE);
                contentProgressBar.setVisibility(View.GONE);
            }
        } else {
            Pattern bracketPattern;
            Matcher bracketMatcher;

            dateText = DateUtils.formatDateRange(getActivity(), beginDate, endDate,
                    DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY
                            | DateUtils.FORMAT_ABBREV_WEEKDAY | DateUtils.FORMAT_NUMERIC_DATE);
            timeHeaderTextView.setVisibility(View.VISIBLE);
            if (endTime != PortalContract.Schedules.TIME_NOT_SPECIFIED) {
                timeText = DateUtils.formatDateRange(getActivity(),
                        beginTime, endTime, DateUtils.FORMAT_SHOW_TIME);
            } else {
                timeText = getString(R.string.all_day);
            }
            timeTextView.setText(timeText);
            timeTextView.setVisibility(View.VISIBLE);

            if (!TextUtils.isEmpty(place)) {
                placeHeaderTextView.setVisibility(View.VISIBLE);
                placeTextView.setText(place);
                placeTextView.setVisibility(View.VISIBLE);
            }
            switch (priority) {
                case ScheduleContent.PRIORITY_MOST_IMPORTANT:
                    priorityImageView.setImageResource(R.drawable.ic_important);
                    priorityImageView.setVisibility(View.VISIBLE);

                    break;

                case ScheduleContent.PRIORITY_IMPORTANT:
                    priorityImageView.setImageResource(R.drawable.ic_half_important);
                    priorityImageView.setVisibility(View.VISIBLE);

                    break;

                case ScheduleContent.PRIORITY_NORMAL:
                    priorityImageView.setImageResource(R.drawable.ic_not_important);
                    priorityImageView.setVisibility(View.VISIBLE);

                    break;

                case ScheduleContent.PRIORITY_URGENT:
                    priorityImageView.setImageResource(R.drawable.ic_warning);
                    priorityImageView.setVisibility(View.VISIBLE);

                    break;

                default:
                    priorityImageView.setVisibility(View.INVISIBLE);

                    break;
            }

            bracketPattern = Pattern.compile(" ?(?:\\(|（)((?:http://|https://|rtsp://).*?)(?:\\)|）) ?");
            bracketMatcher = bracketPattern.matcher(content);
            contentTextView.setText(bracketMatcher.replaceAll(" ($1) "));
            contentTextView.setVisibility(View.VISIBLE);
            contentProgressBar.setVisibility(View.GONE);
        }
        dateTextView.setText(dateText);
        dateTextView.setVisibility(View.VISIBLE);

        textBuilder = new StringBuilder();
        textBuilder.append(getString(R.string.prefix_title));
        textBuilder.append(" ");
        textBuilder.append(title);
        textBuilder.append("\n");
        textBuilder.append(getString(R.string.prefix_date));
        textBuilder.append(" ");
        textBuilder.append(dateText);
        textBuilder.append("\n");
        if (!TextUtils.isEmpty(timeText)) {
            textBuilder.append(getString(R.string.prefix_time));
            textBuilder.append("\n");
            textBuilder.append(timeText);
            textBuilder.append("\n");
        }
        if (!TextUtils.isEmpty(place)) {
            textBuilder.append(getString(R.string.prefix_place));
            textBuilder.append("\n");
            textBuilder.append(place);
            textBuilder.append("\n");
        }
        if (!TextUtils.isEmpty(content)) {
            textBuilder.append(getString(R.string.prefix_content));
            textBuilder.append("\n");
            textBuilder.append(content);
            textBuilder.append("\n");
        }
        mScheduleText = textBuilder.toString();
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    public int getScheduleId() {
        return getArguments().getInt(KEY_DATABASE_ID);
    }

    public void setScheduleId(int scheduleId) {
        getArguments().putInt(KEY_DATABASE_ID, scheduleId);
    }

    private class PortalServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mPortalService = IPortalService.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mPortalService = null;
        }
    }
}
