/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.portal;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.provider.portal.PortalContract;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;
import com.actionbarsherlock.widget.SearchView.OnQueryTextListener;

/**
 * ポータルのスケジュールリストのフラグメントを実装したクラス。
 *
 * @author leak4mk0
 * @version 35
 * @see android.app.ListFragment
 * @since 1
 */
public class ScheduleListFragment extends SherlockListFragment
		implements LoaderCallbacks<Cursor>, OnQueryTextListener {
	private static final String KEY_QUERY_TEXT = "query_text";
	private static final String KEY_SHOW_DELETED = "show_deleted";

	private ScheduleCursorAdapter mScheduleCursorAdapter;

	public interface OnScheduleListItemClickListener {
		public void OnScheduleListItemClick(int[] ids, int index);
	}

	public static ScheduleListFragment newInstance() {
		ScheduleListFragment fragment;
		Bundle args;

		fragment = new ScheduleListFragment();
		args = new Bundle();
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mScheduleCursorAdapter = new ScheduleCursorAdapter(getActivity(), null, 0);

		setEmptyText(getText(R.string.empty_schedules));
		setListAdapter(mScheduleCursorAdapter);
		setListShown(false);
		setHasOptionsMenu(true);

		getLoaderManager().initLoader(0, getArguments(), this);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		MenuItem searchItem;
		MenuItem notTakenItem;
		MenuItem deletedItem;
		SearchView searchView;

		Bundle args;

		super.onPrepareOptionsMenu(menu);

		searchItem = menu.findItem(R.id.menu_search);
		notTakenItem = menu.findItem(R.id.menu_not_taken);
		deletedItem = menu.findItem(R.id.menu_deleted);
		searchView = (SearchView) searchItem.getActionView();

		args = getArguments();

		notTakenItem.setVisible(false);
		deletedItem.setVisible(true);
		deletedItem.setChecked(args.getBoolean(KEY_SHOW_DELETED));

        if (searchView != null) {
            searchView.setOnQueryTextListener(this);
            searchView.setIconified(TextUtils.isEmpty(args.getString(KEY_QUERY_TEXT)));
        } else {
            searchItem.setVisible(false);
        }
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean ret;
		boolean checked;
		Bundle args;

		switch (item.getItemId()) {
		case R.id.menu_deleted:
			args = getArguments();
			checked = !item.isChecked();
			item.setChecked(checked);
			args.putBoolean(KEY_SHOW_DELETED, checked);
			getLoaderManager().restartLoader(0, args, this);

			ret = true;

			break;

		default:
			ret = super.onOptionsItemSelected(item);

			break;
		}

		return ret;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Activity activity;
		OnScheduleListItemClickListener listener;

		activity = getActivity();
		if (activity instanceof OnScheduleListItemClickListener) {
            Cursor cursor;
            int[] ids;

            listener = (OnScheduleListItemClickListener) activity;
            cursor = mScheduleCursorAdapter.getCursor();
            if (cursor != null && cursor.moveToFirst()) {
                int idColumnIndex;

                ids = new int[cursor.getCount()];
                idColumnIndex = cursor.getColumnIndex(PortalContract.Schedules._ID);
                do {
                    ids[cursor.getPosition()] = cursor.getInt(idColumnIndex);
                } while (cursor.moveToNext());
            } else {
                ids = new int[1];
                ids[0] = (int) id;
            }
            listener.OnScheduleListItemClick(ids, position);
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		CursorLoader loader;
		Uri uri;
		String[] projection;
		String queryText;
		StringBuilder selectionBuilder;
		String[] selectionArgs;

		uri = PortalContract.Schedules.CONTENT_URI;
		projection = new String[] { PortalContract.Schedules._ID,
				PortalContract.Schedules.TITLE,
				PortalContract.Schedules.START_DATE,
				PortalContract.Schedules.START_TIME,
				PortalContract.Schedules.IS_DELETED };
		queryText = args.getString(KEY_QUERY_TEXT);
		selectionBuilder = new StringBuilder();
		selectionArgs = null;
		if (!TextUtils.isEmpty(queryText)) {
			queryText = "%" + queryText + "%";
			selectionBuilder.append(PortalContract.Information.TITLE);
			selectionBuilder.append(" LIKE ?");
			selectionArgs = new String[] { queryText };
		}
		if (!args.getBoolean(KEY_SHOW_DELETED)) {
			if (selectionBuilder.length() > 0) {
				selectionBuilder.append(" AND ");
			}
			selectionBuilder.append(PortalContract.CancelledLectures.IS_DELETED);
			selectionBuilder.append(" = 0");
		}
		loader = new CursorLoader(getActivity(), uri, projection, selectionBuilder.toString(), selectionArgs, null);

		return loader;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mScheduleCursorAdapter.swapCursor(data);

		if (isResumed()) {
			setListShown(true);
		} else {
			setListShownNoAnimation(true);
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mScheduleCursorAdapter.swapCursor(null);
	}

	@Override
	public boolean onQueryTextChange(String newText) {
		Bundle args;
		String oldText;

		args = getArguments();
		oldText = args.getString(KEY_QUERY_TEXT);
		newText = TextUtils.isEmpty(newText) ? null : newText;
		if ((oldText != null && !oldText.equals(newText)) || (newText != null && !newText.equals(oldText))) {
			args.putString(KEY_QUERY_TEXT, newText);
			getLoaderManager().restartLoader(0, args, this);
		}

		return true;
	}

	@Override
	public boolean onQueryTextSubmit(String query) {
		return false;
	}

	private static class ScheduleCursorAdapter extends CursorAdapter {
		public ScheduleCursorAdapter(Context context, Cursor c, int flags) {
			super(context, c, flags);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			TextView titleTextView;
			ImageView deletedImageView;
			TextView beginDateView;

			String title;
			boolean deleted;
			long beginDate;

			titleTextView = (TextView) view.findViewById(R.id.title_text_view);
			deletedImageView = (ImageView) view.findViewById(R.id.deleted_image_view);
			beginDateView = (TextView) view.findViewById(R.id.date_text_view);

			title = cursor.getString(cursor.getColumnIndex(PortalContract.Schedules.TITLE));
			deleted = cursor.getInt(cursor.getColumnIndex(PortalContract.Schedules.IS_DELETED)) > 0;
			beginDate = cursor.getLong(cursor.getColumnIndex(PortalContract.Schedules.START_DATE));

			view.setBackgroundResource(deleted ? R.drawable.bg_deleted : R.drawable.bg_read);
			titleTextView.setText(title);
			deletedImageView.setVisibility(deleted ? View.VISIBLE : View.INVISIBLE);
			beginDateView.setText(DateUtils.formatDateTime(context, beginDate,
					DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_WEEKDAY
							| DateUtils.FORMAT_ABBREV_WEEKDAY | DateUtils.FORMAT_NUMERIC_DATE));
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			View view;

			view = LayoutInflater.from(context).inflate(R.layout.portal_schedule_list_item, parent, false);

			return view;
		}
	}
}
