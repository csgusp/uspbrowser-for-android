/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.setup;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import com.actionbarsherlock.app.SherlockFragment;

/**
 * PortalFragment クラスは、ポータルの設定用のフラグメントを実装したクラスです。
 *
 * @author leak4mk0
 * @version 12
 * @since 1
 */
public class PortalFragment extends SherlockFragment implements OnClickListener {
	public static final String KEY_FETCH_AT_LAUNCH = "fetch_at_launch";
	public static final String KEY_FETCH_CONTENTS_AUTOMATICALLY = "fetch_contents_automatically";

	public interface OnPortalFragmentResultListener {
		public void onPortalFragmentResult(boolean fetchAtLaunch, boolean fetchContentsAutomatically);
	}

	public static PortalFragment newInstance(boolean fetchAtLaunch, boolean fetchContentsAutomatically) {
		PortalFragment fragment;
		Bundle args;

		fragment = new PortalFragment();
		args = new Bundle();
		args.putBoolean(KEY_FETCH_AT_LAUNCH, fetchAtLaunch);
		args.putBoolean(KEY_FETCH_CONTENTS_AUTOMATICALLY, fetchContentsAutomatically);
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view;
		CheckBox fetchAtLaunchCheckBox;
		CheckBox fetchContentsAutomaticallyCheckBox;
		Button nextButton;
		Bundle args;
		boolean fetchAtLaunch;
		boolean fetchContentsAutomatically;

		view = inflater.inflate(R.layout.setup_portal_fragment, container, false);

		fetchAtLaunchCheckBox = (CheckBox) view.findViewById(R.id.fetch_at_launch_check_box);
		fetchContentsAutomaticallyCheckBox = (CheckBox) view.findViewById(R.id.fetch_contents_automatically_check_box);
		nextButton = (Button) view.findViewById(R.id.next_button);

		args = getArguments();
		fetchAtLaunch = args.getBoolean(KEY_FETCH_AT_LAUNCH);
		fetchContentsAutomatically = args.getBoolean(KEY_FETCH_CONTENTS_AUTOMATICALLY);

		fetchAtLaunchCheckBox.setChecked(fetchAtLaunch);
		fetchContentsAutomaticallyCheckBox.setChecked(fetchContentsAutomatically);
		nextButton.setOnClickListener(this);

		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.next_button:
			setResult();

			break;
		}
	}

	private void setResult() {
		Activity activity;

		activity = getActivity();
		if (activity instanceof OnPortalFragmentResultListener) {
			CheckBox fetchAtLaunchCheckBox;
			CheckBox fetchContentsAutomaticallyCheckBox;
			boolean fetchAtLaunch;
			boolean fetchContentsAutomatically;
			OnPortalFragmentResultListener listener;

			fetchAtLaunchCheckBox = (CheckBox) activity.findViewById(R.id.fetch_at_launch_check_box);
			fetchContentsAutomaticallyCheckBox = (CheckBox) activity.findViewById(R.id.fetch_contents_automatically_check_box);

			fetchAtLaunch = fetchAtLaunchCheckBox.isChecked();
			fetchContentsAutomatically = fetchContentsAutomaticallyCheckBox.isChecked();

			listener = (OnPortalFragmentResultListener) activity;
			listener.onPortalFragmentResult(fetchAtLaunch, fetchContentsAutomatically);
		}
	}
}
