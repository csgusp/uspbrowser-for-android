/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.setup;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.preference.BrowserPreferences;
import jp.gr.java_conf.csgusp.uspbrowser.ui.BrowserActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;

import com.actionbarsherlock.app.SherlockFragmentActivity;

/**
 * SetupActivity クラスは、初期設定用のアクティビティを実装したクラスです。
 *
 * @author leak4mk0
 * @version 12
 * @since 1
 */
public class SetupActivity extends SherlockFragmentActivity
		implements TermsFragment.OnTermsFragmentResultListener, SystemFragment.OnSystemFragmentResultListener,
		PortalFragment.OnPortalFragmentResultListener, WelcomeFragment.OnWelcomeFragmentResultListener {
	private static final String KEY_MAX_PAGE = "max_page";
	private static final String KEY_CURRENT_PAGE = "current_page";

	private BrowserPreferences mBrowserPreferences;
	private SetupPagerAdapter mSetupPagerAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		ViewPager viewPager;

		super.onCreate(savedInstanceState);

		mBrowserPreferences = BrowserPreferences.getInstance(this);

		setContentView(R.layout.setup_activity);

		mSetupPagerAdapter = new SetupPagerAdapter(getSupportFragmentManager());

		viewPager = (ViewPager) findViewById(R.id.view_pager);
		viewPager.setAdapter(mSetupPagerAdapter);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		ViewPager viewPager;
		int maxPage;
		int currentPage;

		super.onRestoreInstanceState(savedInstanceState);

		viewPager = (ViewPager) findViewById(R.id.view_pager);

		maxPage = savedInstanceState.getInt(KEY_MAX_PAGE, SetupPagerAdapter.PAGE_TERMS);
		currentPage = savedInstanceState.getInt(KEY_CURRENT_PAGE, SetupPagerAdapter.PAGE_TERMS);

		mSetupPagerAdapter.setMaxPage(maxPage);
		viewPager.setCurrentItem(currentPage);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		ViewPager viewPager;
		int maxPage;
		int currentPage;

		super.onSaveInstanceState(outState);

		viewPager = (ViewPager) findViewById(R.id.view_pager);

		maxPage = mSetupPagerAdapter.getMaxPage();
		currentPage = viewPager.getCurrentItem();

		outState.putInt(KEY_MAX_PAGE, maxPage);
		outState.putInt(KEY_CURRENT_PAGE, currentPage);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		boolean consumed;
		ViewPager viewPager;
		int currentItem;

		switch (keyCode) {
		case KeyEvent.KEYCODE_BACK:
			viewPager = (ViewPager) findViewById(R.id.view_pager);

			currentItem = viewPager.getCurrentItem();
			if (currentItem > SetupPagerAdapter.PAGE_TERMS) {
				currentItem--;
				viewPager.setCurrentItem(currentItem);
			} else {
				finish();
			}

			consumed = true;

			break;

		default:
			consumed = false;
		}

		return consumed;
	}

	@Override
	public void onTermsFragmentResult(boolean accept) {
		if (accept) {
			ViewPager viewPager;
			int licenseVersion;

			viewPager = (ViewPager) findViewById(R.id.view_pager);

			mBrowserPreferences.setBrowserAcceptLicense(accept);
			licenseVersion = getResources().getInteger(R.integer.license_version);
			mBrowserPreferences.setBrowserLicenseVersion(licenseVersion);
			mSetupPagerAdapter.setMaxPage(SetupPagerAdapter.PAGE_SYSTEM);
			viewPager.setCurrentItem(SetupPagerAdapter.PAGE_SYSTEM);
		} else {
			finish();
		}
	}

	@Override
	public void onSystemFragmentResult(boolean logon, String userId, String password) {
		if (logon) {
			ViewPager viewPager;

			viewPager = (ViewPager) findViewById(R.id.view_pager);

			mBrowserPreferences.setSystemUserId(userId);
			mBrowserPreferences.setSystemPassword(password);
			mSetupPagerAdapter.setMaxPage(SetupPagerAdapter.PAGE_PORTAL);
			viewPager.setCurrentItem(SetupPagerAdapter.PAGE_PORTAL);
		} else {
			mSetupPagerAdapter.setMaxPage(SetupPagerAdapter.PAGE_SYSTEM);
		}
	}

	@Override
	public void onPortalFragmentResult(boolean fetchAtLaunch, boolean fetchContentsAutomatically) {
		ViewPager viewPager;

		viewPager = (ViewPager) findViewById(R.id.view_pager);

		mBrowserPreferences.setPortalFetchAtLaunch(fetchAtLaunch);
		mBrowserPreferences.setPortalFetchContentsAutomatically(fetchContentsAutomatically);
		mSetupPagerAdapter.setMaxPage(SetupPagerAdapter.PAGE_WELCOME);
		viewPager.setCurrentItem(SetupPagerAdapter.PAGE_WELCOME);
	}

	@Override
	public void onWelcomeFragmentResult() {
		Intent intent;

		mBrowserPreferences.setBrowserSetup(true);
		intent = new Intent(getApplicationContext(), BrowserActivity.class);
		startActivity(intent);

		finish();
	}

	private static class SetupPagerAdapter extends FragmentStatePagerAdapter {
		public static final int PAGE_TERMS = 0;
		public static final int PAGE_SYSTEM = 1;
		public static final int PAGE_PORTAL = 2;
		public static final int PAGE_WELCOME = 3;

		public static final int PAGE_MIN = PAGE_TERMS;
		public static final int PAGE_MAX = PAGE_WELCOME;

		private int mCount;

		public SetupPagerAdapter(FragmentManager manager) {
			super(manager);

			mCount = PAGE_MIN + 1;
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment;

			switch (position) {
			case PAGE_TERMS:
				fragment = TermsFragment.newInstance();

				break;

			case PAGE_SYSTEM:
				fragment = SystemFragment.newInstance(null, null);

				break;

			case PAGE_PORTAL:
				fragment = PortalFragment.newInstance(true, false);

				break;

			case PAGE_WELCOME:
				fragment = WelcomeFragment.newInstance();

				break;

			default:
				fragment = null;
			}

			return fragment;
		}

		@Override
		public int getCount() {
			return mCount;
		}

		public int getMaxPage() {
			return mCount - 1;
		}

		public void setMaxPage(int page) {
			if (page >= PAGE_MIN && page <= PAGE_MAX) {
				mCount = page + 1;
			}
			notifyDataSetChanged();
		}
	}
}
