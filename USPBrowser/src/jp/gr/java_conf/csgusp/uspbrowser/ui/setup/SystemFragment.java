/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.setup;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.IPortalService;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.IPortalServiceCallback;
import jp.gr.java_conf.csgusp.uspbrowser.service.portal.PortalService;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockFragment;

/**
 * SystemFragment クラスは、統合認証システムの設定用のフラグメントを実装したクラスです。
 *
 * @author leak4mk0
 * @version 12
 * @since 1
 */
public class SystemFragment extends SherlockFragment implements OnClickListener {
	private static final String KEY_USER_ID = "user_id";
	private static final String KEY_PASSWORD = "password";

	private PortalServiceHandler mPortalServiceHandler;
	private PortalServiceConnection mPortalServiceConnection;
	private PortalServiceCallback mPortalServiceCallback;

	public interface OnSystemFragmentResultListener {
		public void onSystemFragmentResult(boolean logon, String userId, String password);
	}

	public static SystemFragment newInstance(String userId, String password) {
		SystemFragment fragment;
		Bundle args;

		fragment = new SystemFragment();
		args = new Bundle();
		args.putString(KEY_USER_ID, userId);
		args.putString(KEY_PASSWORD, password);
		fragment.setArguments(args);

		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Activity activity;
		Intent intent;

		super.onCreate(savedInstanceState);

		activity = getActivity();
		intent = new Intent(IPortalService.class.getName());
		mPortalServiceHandler = new PortalServiceHandler(this);
		mPortalServiceCallback = new PortalServiceCallback(mPortalServiceHandler);
		mPortalServiceConnection = new PortalServiceConnection(mPortalServiceCallback);
		activity.bindService(intent, mPortalServiceConnection, Activity.BIND_AUTO_CREATE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view;
		EditText userIdEditText;
		EditText passwordEditText;
		ProgressBar progressBar;
		Button logonButton;

		Bundle args;
		String userId;
		String password;

		view = inflater.inflate(R.layout.setup_system_fragment, container, false);

		userIdEditText = (EditText) view.findViewById(R.id.user_id_edit_text);
		passwordEditText = (EditText) view.findViewById(R.id.password_edit_text);
		progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
		logonButton = (Button) view.findViewById(R.id.logon_button);

		args = getArguments();
		userId = args.getString(KEY_USER_ID);
		password = args.getString(KEY_PASSWORD);

		userIdEditText.setText(userId);
		passwordEditText.setText(password);
		progressBar.setVisibility(View.GONE);
		logonButton.setOnClickListener(this);

		return view;
	}

	@Override
	public void onDestroy() {
		Activity activity;

		super.onDestroy();

		activity = getActivity();

		mPortalServiceConnection.unregisterPortalServiceCallback();
		activity.unbindService(mPortalServiceConnection);
	}

	@Override
	public void onClick(View v) {
		Activity activity;
		EditText userIdEditText;
		EditText passwordEditText;

		String userId;
		String password;
		IPortalService portalService;

		setView(true);

		activity = getActivity();

		userIdEditText = (EditText) activity.findViewById(R.id.user_id_edit_text);
		passwordEditText = (EditText) activity.findViewById(R.id.password_edit_text);

		userId = userIdEditText.getText().toString();
		password = passwordEditText.getText().toString();

		portalService = mPortalServiceConnection.getPortalService();
		try {
			portalService.requestLogon(userId, password);
			portalService.requestFetchTop();
			portalService.requestLogout();
		} catch (RemoteException e) {
		}
	}

	private void setView(boolean logon) {
		Activity activity;
		EditText userIdEditText;
		EditText passwordEditText;
		ProgressBar progressBar;
		Button logonButton;

		activity = getActivity();
		userIdEditText = (EditText) activity.findViewById(R.id.user_id_edit_text);
		passwordEditText = (EditText) activity.findViewById(R.id.password_edit_text);
		progressBar = (ProgressBar) activity.findViewById(R.id.progress_bar);
		logonButton = (Button) activity.findViewById(R.id.logon_button);

		userIdEditText.setEnabled(!logon);
		passwordEditText.setEnabled(!logon);
		progressBar.setVisibility(logon ? View.VISIBLE : View.GONE);
		logonButton.setEnabled(!logon);
	}

	private void setResult(boolean logon) {
		Activity activity;

		activity = getActivity();
		if (activity instanceof OnSystemFragmentResultListener) {
			OnSystemFragmentResultListener listener;
			String userId;
			String password;

			if (logon) {
				EditText userIdEditText;
				EditText passwordEditText;

				userIdEditText = (EditText) activity.findViewById(R.id.user_id_edit_text);
				passwordEditText = (EditText) activity.findViewById(R.id.password_edit_text);

				userId = userIdEditText.getText().toString();
				password = passwordEditText.getText().toString();
			} else {
				userId = null;
				password = null;
			}

			listener = (OnSystemFragmentResultListener) activity;
			listener.onSystemFragmentResult(logon, userId, password);
		}
	}

	private static class PortalServiceConnection implements ServiceConnection {
		IPortalService mPortalService;
		PortalServiceCallback mPortalServiceCallback;

		public PortalServiceConnection(PortalServiceCallback portalServiceCallback) {
			mPortalServiceCallback = portalServiceCallback;
		}

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mPortalService = IPortalService.Stub.asInterface(service);
			try {
				mPortalService.registerCallback(mPortalServiceCallback);
			} catch (RemoteException e) {
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			mPortalService = null;
		}

		public void unregisterPortalServiceCallback() {
			if (mPortalService != null) {
				try {
					mPortalService.unregisterCallback(mPortalServiceCallback);
				} catch (RemoteException e) {
				}
			}
		}

		public IPortalService getPortalService() {
			return mPortalService;
		}
	}

	private static class PortalServiceCallback extends IPortalServiceCallback.Stub {
		private Handler mHandler;

		public PortalServiceCallback(Handler handler) {
			mHandler = handler;
		}

		@Override
		public void onResult(int action, int status) {
			Message message;

			message = new Message();
			message.arg1 = action;
			message.arg2 = status;
			mHandler.sendMessage(message);
		}
	}

	private static class PortalServiceHandler extends Handler {
		private SystemFragment mSystemFragment;

		public PortalServiceHandler(SystemFragment systemFragment) {
			mSystemFragment = systemFragment;
		}

		@Override
		public void handleMessage(Message message) {
			Activity activity;

			switch (message.arg1) {
			case PortalService.ACTION_LOGON:
				switch (message.arg2) {
				case PortalService.STATUS_SUCCESS:
					mSystemFragment.setResult(true);
					mSystemFragment.setView(false);

					break;

				case PortalService.STATUS_FAILURE_IN_NETWORK:
					mSystemFragment.setResult(false);
					mSystemFragment.setView(false);

					activity = mSystemFragment.getActivity();
					Toast.makeText(activity, R.string.massage_fetch_failure_network, Toast.LENGTH_LONG).show();

					break;

				case PortalService.STATUS_FAILURE_IN_AUTHORIZATION:
					mSystemFragment.setResult(false);
					mSystemFragment.setView(false);

					activity = mSystemFragment.getActivity();
					Toast.makeText(activity, R.string.message_fetch_failure_authorization, Toast.LENGTH_LONG).show();

					break;

				default:
					mSystemFragment.setResult(false);
					mSystemFragment.setView(false);

					break;
				}

				break;
			}
		}
	}
}
