/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.setup;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockFragment;

/**
 * TermsFragment クラスは、利用規約のフラグメントを実装したクラスです。
 *
 * @author leak4mk0
 * @version 12
 * @since 1
 */
public class TermsFragment extends SherlockFragment implements OnClickListener {
	public interface OnTermsFragmentResultListener {
		public void onTermsFragmentResult(boolean accept);
	}

	public static TermsFragment newInstance() {
		TermsFragment fragment;

		fragment = new TermsFragment();

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view;
		WebView webView;
		Button acceptButton;
		Button declineButton;

		view = inflater.inflate(R.layout.setup_terms_fragment, container, false);

		webView = (WebView) view.findViewById(R.id.web_view);
		acceptButton = (Button) view.findViewById(R.id.accept_button);
		declineButton = (Button) view.findViewById(R.id.decline_button);

		webView.loadUrl(getString(R.string.url_terms));
		acceptButton.setOnClickListener(this);
		declineButton.setOnClickListener(this);

		return view;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.accept_button:
			setResult(true);

			break;

		case R.id.decline_button:
			setResult(false);

			break;

		}
	}

	private void setResult(boolean accept) {
		Activity activity;

		activity = getActivity();
		if (activity instanceof OnTermsFragmentResultListener) {
			OnTermsFragmentResultListener listener;

			listener = (OnTermsFragmentResultListener) activity;
			listener.onTermsFragmentResult(accept);
		}
	}
}
