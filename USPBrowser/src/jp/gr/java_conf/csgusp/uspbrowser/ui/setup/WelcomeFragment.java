/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.setup;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.actionbarsherlock.app.SherlockFragment;

/**
 * WelcomeFragment クラスは、導入用のフラグメントを実装したクラスです。
 *
 * @author leak4mk0
 * @version 12
 * @since 1
 */
public class WelcomeFragment extends SherlockFragment implements OnClickListener {
	public interface OnWelcomeFragmentResultListener {
		public void onWelcomeFragmentResult();
	}

	public static WelcomeFragment newInstance() {
		WelcomeFragment fragment;

		fragment = new WelcomeFragment();

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view;
		Button finishButton;

		view = inflater.inflate(R.layout.setup_welcome_fragment, container, false);

		finishButton = (Button) view.findViewById(R.id.finish_button);

		finishButton.setOnClickListener(this);

		return view;
	}


	@Override
	public void onResume() {
		ImageView descriptionImageView;
		AnimationDrawable animationDrawable;

		super.onResume();

		descriptionImageView = (ImageView) getView().findViewById(R.id.description_image_view);
		animationDrawable = (AnimationDrawable) descriptionImageView.getDrawable();
		animationDrawable.start();
	}

	@Override
	public void onPause() {
		ImageView descriptionImageView;
		AnimationDrawable animationDrawable;

		super.onPause();

		descriptionImageView = (ImageView) getView().findViewById(R.id.description_image_view);
		animationDrawable = (AnimationDrawable) descriptionImageView.getDrawable();
		animationDrawable.stop();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.finish_button:
			setResult();

			break;

		}
	}

	private void setResult() {
		Activity activity;

		activity = getActivity();
		if (activity instanceof OnWelcomeFragmentResultListener) {
			OnWelcomeFragmentResultListener listener;

			listener = (OnWelcomeFragmentResultListener) activity;
			listener.onWelcomeFragmentResult();
		}
	}
}
