/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.widget;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.preference.BrowserPreferences;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import java.util.Calendar;

/**
 * AboutPreference クラスは、
 * 設定画面でのバージョン情報の表示を実装したクラスです。
 *
 * @author leak4mk0
 * @version 25
 * @since 25
 */
public class AboutPreference extends Preference {
    public AboutPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setLayoutResource(R.layout.settings_about_preference);
    }

    public AboutPreference(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AboutPreference(Context context) {
        this(context, null);
    }

    @Override
    protected void onBindView(View view) {
        TextView versionTextView;
        TextView copyrightTextView;
        TextView feedbackTextView;
        Context context;
        PackageManager packageManager;
        PackageInfo packageInfo;
        String versionName;
        int versionCode;
        int thisYear;
        String feedbackText;
        Spannable feedbackSpannable;

        super.onBindView(view);

        versionTextView = (TextView) view.findViewById(R.id.version_text_view);
        copyrightTextView = (TextView) view.findViewById(R.id.copyright_text_view);
        feedbackTextView = (TextView) view.findViewById(R.id.feedback_text_view);

        context = getContext();
        try {
            packageManager = context.getPackageManager();
            if (packageManager == null) {
                versionName = context.getString(R.string.unknown);
                versionCode = -1;
            } else {
                packageInfo = packageManager.getPackageInfo(
                        context.getPackageName(), PackageManager.GET_META_DATA);
                versionName = packageInfo.versionName;
                versionCode = packageInfo.versionCode;
            }
        } catch (PackageManager.NameNotFoundException e) {
            versionName = context.getString(R.string.unknown);
            versionCode = -1;
        }
        thisYear = Calendar.getInstance().get(Calendar.YEAR);
        feedbackText = context.getString(R.string.feedback);
        feedbackSpannable = new SpannableString(feedbackText);
        feedbackSpannable.setSpan(new FeedbackSpan(context),
                0, feedbackText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        versionTextView.setText(context.getString(R.string.template_version, versionName, versionCode));
        copyrightTextView.setText(context.getString(R.string.copyright_text, thisYear));
        feedbackTextView.setText(feedbackSpannable);
        feedbackTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private static class FeedbackSpan extends ClickableSpan {
        private Context mContext;

        public FeedbackSpan(Context context) {
            mContext = context;
        }

        @Override
        public void onClick(View widget) {
            Intent intent;
            PackageManager packageManager;
            PackageInfo packageInfo;
            String versionName;
            int versionCode;

            intent = new Intent(Intent.ACTION_SENDTO, Uri.parse(mContext.getString(R.string.url_email)));
            intent.putExtra(Intent.EXTRA_SUBJECT, mContext.getString(R.string.feedback_subject));
            try {
                packageManager = mContext.getPackageManager();
                if (packageManager == null) {
                    versionName = mContext.getString(R.string.unknown);
                    versionCode = -1;
                } else {
                    packageInfo = packageManager.getPackageInfo(
                            mContext.getPackageName(), PackageManager.GET_META_DATA);
                    versionName = packageInfo.versionName;
                    versionCode = packageInfo.versionCode;
                }
            } catch (PackageManager.NameNotFoundException e) {
                versionName = mContext.getString(R.string.unknown);
                versionCode = -1;
            }
            intent.putExtra(Intent.EXTRA_TEXT, mContext.getString(R.string.feedback_text,
                    Build.MANUFACTURER, Build.MODEL, Build.VERSION.RELEASE, versionName, versionCode));
            if (mContext.getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
                mContext.startActivity(intent);
            }
        }
    }
}
