/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.widget;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * CursorPagerAdapter クラスは、配列に基づいたページャーを表示するアダプターを実装した抽象クラスです。
 *
 * @author leak4mk0
 * @version 35
 * @since 35
 */
public abstract class ArrayPagerAdapter extends FragmentStatePagerAdapter {
    private FragmentManager mFragmentManager;
    private int[] mArray;

    public ArrayPagerAdapter(FragmentManager manager) {
        super(manager);

        mFragmentManager = manager;
        mArray = null;
    }

    @Override
    public Fragment getItem(int position) {
        if (mArray == null || position < 0 || mArray.length <= position) {
            return null;
        }

        return newFragment(mFragmentManager, mArray[position]);
    }

    @Override
    public int getCount() {
        if (mArray == null) {
            return 0;
        }

        return mArray.length;
    }

    public int[] getArray() {
        return mArray;
    }

    public int[] swapArray(int[] newArray) {
        int[] oldArray;

        if (newArray == mArray) {
            return mArray;
        }
        oldArray = mArray;
        mArray = newArray;
        if (newArray != null) {
            notifyDataSetChanged();
        }

        return oldArray;
    }

    public abstract Fragment newFragment(FragmentManager manager, int id);
}
