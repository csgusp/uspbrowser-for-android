/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.widget;

import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * CursorPagerAdapter クラスは、カーソルに基づいたページャーを表示するアダプターを実装した抽象クラスです。
 *
 * @author leak4mk0
 * @version 12
 * @since 1
 */
public abstract class CursorPagerAdapter extends FragmentStatePagerAdapter {
    private FragmentManager mFragmentManager;
    private Cursor mCursor;

    public CursorPagerAdapter(FragmentManager manager) {
        super(manager);

        mFragmentManager = manager;
        mCursor = null;
    }

    @Override
    public Fragment getItem(int position) {
        if (mCursor == null || !mCursor.moveToPosition(position)) {
            return null;
        }

        return newFragment(mFragmentManager, mCursor);
    }

    @Override
    public int getCount() {
        if (mCursor == null) {
            return 0;
        }

        return mCursor.getCount();
    }

    public Cursor getCursor() {
        return mCursor;
    }

    public Cursor swapCursor(Cursor newCursor) {
        Cursor oldCursor;

        if (newCursor == mCursor) {
            return null;
        }
        oldCursor = mCursor;
        mCursor = newCursor;
        if (newCursor != null) {
            notifyDataSetChanged();
        }

        return oldCursor;
    }

    public abstract Fragment newFragment(FragmentManager manager, Cursor cursor);
}
