/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.widget;

import jp.gr.java_conf.csgusp.uspbrowser.preference.BrowserPreferences;

import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;

/**
 * EditChipertextPreference クラスは、
 * 設定画面で用いる暗号化に対応したテキストボックスを実装したクラスです。
 *
 * @author leak4mk0
 * @version 12
 * @since 1
 */
public class EditCiphertextPreference extends EditTextPreference {
    public EditCiphertextPreference(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public EditCiphertextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditCiphertextPreference(Context context) {
        super(context);
    }

    @Override
    public String getPersistedString(String defaultReturnValue) {
        return decrypt(super.getPersistedString(defaultReturnValue));
    }

    @Override
    public boolean persistString(String text) {
        return super.persistString(encrypt(text));
    }

    private String decrypt(String ciphertext) {
        return BrowserPreferences.decrypt(ciphertext);
    }

    private String encrypt(String text) {
        return BrowserPreferences.encrypt(text);
    }
}
