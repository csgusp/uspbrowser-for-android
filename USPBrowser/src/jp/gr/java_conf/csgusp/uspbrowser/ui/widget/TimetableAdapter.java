/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.widget;

import jp.gr.java_conf.csgusp.uspbrowser.util.HolidayDetector;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.BaseColumns;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.Calendar;

/**
 * 時刻表のアダプターを実装したクラス。
 *
 * @author leak4mk0
 * @version 27
 * @since 27
 */
public class TimetableAdapter extends BaseAdapter {
    private Context mContext;
    private LoaderManager mLoaderManager;
    private Uri mUri;
    private String mDirectionName;
    private int mDirectionValue;
    private String mWeekdayName;
    private int mWorkdayValue;
    private int mHolidayValue;
    private String mTimeName;
    private int mHeaderResourceId;
    private int mItemResourceId;
    private int mNextColorId;
    private int mHourTextViewId;
    private int mWorkdayMinTextViewId;
    private int mHolidayMinTextViewId;
    private LayoutInflater mLayoutInflater;
    private Cursor mCursor;
    private int mIdIndex;
    private int mWeekdayIndex;
    private int mTimeIndex;
    private int mMaxTime;
    private int mMinTime;
    private int mNextTime;
    private boolean mHoliday;
    public TimetableCallbacks mTimetableCallbacks;
    private TimetableLoaderCallbacks mTimetableLoaderCallbacks;
    private TimetableContentObserver mTimetableContentObserver;

    public interface TimetableCallbacks {
        public void onLoadFinished();
    }

    public TimetableAdapter(
            Context context, LoaderManager loaderManager, Uri uri,
            String directionName, int directionValue,
            String weekdayName, int workdayValue, int holidayValue,
            String timeName,
            int headerResourceId, int itemResourceId, int nextColorId,
            int hourTextViewId, int workdayMinTextViewId, int holidayMinTextViewId) {
        mContext = context;
        mLoaderManager = loaderManager;
        mUri = uri;
        mDirectionName = directionName;
        mDirectionValue = directionValue;
        mWeekdayName = weekdayName;
        mWorkdayValue = workdayValue;
        mHolidayValue = holidayValue;
        mTimeName = timeName;
        mHeaderResourceId = headerResourceId;
        mItemResourceId = itemResourceId;
        mNextColorId = nextColorId;
        mHourTextViewId = hourTextViewId;
        mWorkdayMinTextViewId = workdayMinTextViewId;
        mHolidayMinTextViewId = holidayMinTextViewId;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mTimetableLoaderCallbacks = new TimetableLoaderCallbacks();
        mTimetableContentObserver = new TimetableContentObserver();

        mLoaderManager.initLoader(0, null, mTimetableLoaderCallbacks);
    }

    @Override
    public int getCount() {
        int count;

        if (mCursor != null) {
            if (mMaxTime > mMinTime) {
                count = 1 + mMaxTime / 3600 - mMinTime / 3600 + 1;
            } else {
                count = 0;
            }
        } else {
            count = 0;
        }

        return count;
    }

    @Override
    public Object getItem(int position) {
        Cursor cursor;

        if (mCursor != null) {
            cursor = mCursor;
            cursor.moveToPosition(position);
        } else {
            cursor = null;
        }

        return cursor;
    }

    @Override
    public long getItemId(int position) {
        int id;

        if (mCursor != null) {
            if (mCursor.moveToPosition(position)) {
                id = mCursor.getInt(mIdIndex);
            } else {
                id = 0;
            }
        } else {
            id = 0;
        }

        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;

        if (convertView != null) {
            int tag;

            tag = (Integer) convertView.getTag();
            if ((tag == 0 && position == 0) || (tag != 0 && position != 0)) {
                view = convertView;
            } else {
                view = null;
            }
        } else {
            view = null;
        }
        if (view == null) {
            if (position == 0) {
                view = mLayoutInflater.inflate(mHeaderResourceId, parent, false);
                assert view != null;
            } else {
                TextView hourTextView;
                TextView workdayMinTextView;
                TextView holidayMinTextView;

                view = mLayoutInflater.inflate(mItemResourceId, parent, false);
                assert view != null;

                hourTextView = (TextView) view.findViewById(mHourTextViewId);
                workdayMinTextView = (TextView) view.findViewById(mWorkdayMinTextViewId);
                holidayMinTextView = (TextView) view.findViewById(mHolidayMinTextViewId);

                hourTextView.setTypeface(Typeface.MONOSPACE);
                workdayMinTextView.setTypeface(Typeface.MONOSPACE);
                holidayMinTextView.setTypeface(Typeface.MONOSPACE);
            }
        }
        view.setTag(position);

        if (position != 0) {
            int hour;
            int[] workdayMins;
            int workdayCount;
            int[] holidayMins;
            int holidayCount;
            int maxTime;
            int minTime;
            int nextMin;

            TextView hourTextView;
            TextView workdayMinTextView;
            TextView holidayMinTextView;
            SpannableStringBuilder timeTextBuilder;

            hour = mMinTime / 3600 + position - 1;
            workdayMins = new int[61];
            workdayCount = 0;
            holidayMins = new int[61];
            holidayCount = 0;
            maxTime = (hour + 1) * 3600;
            minTime = hour * 3600;
            nextMin = (mNextTime - minTime) / 60;
            if (mCursor.moveToFirst()) {
                do {
                    int weekday;
                    int time;

                    weekday = mCursor.getInt(mWeekdayIndex);
                    time = mCursor.getInt(mTimeIndex);

                    if (time >= minTime && time < maxTime) {
                        if (weekday == mWorkdayValue) {
                            if (workdayCount < 60) {
                                workdayMins[workdayCount] = (time - minTime) / 60;
                                workdayCount++;
                            }
                        } else if (weekday == mHolidayValue) {
                            if (holidayCount < 60) {
                                holidayMins[holidayCount] = (time - minTime) / 60;
                                holidayCount++;
                            }
                        }
                    }
                } while (mCursor.moveToNext());
            }
            workdayMins[workdayCount] = -1;
            holidayMins[holidayCount] = -1;

            hourTextView = (TextView) view.findViewById(mHourTextViewId);
            workdayMinTextView = (TextView) view.findViewById(mWorkdayMinTextViewId);
            holidayMinTextView = (TextView) view.findViewById(mHolidayMinTextViewId);
            timeTextBuilder = new SpannableStringBuilder();
            timeTextBuilder.append(String.format("%2d", hour));
            hourTextView.setText(timeTextBuilder);
            timeTextBuilder.clear();
            for (int min : workdayMins) {
                if (min == -1) {
                    break;
                }
                if (timeTextBuilder.length() > 0) {
                    timeTextBuilder.append(" ");
                }
                timeTextBuilder.append(String.format("%2d", min));
                if (!mHoliday && min == nextMin) {
                    int color;
                    Object span;
                    int start;
                    int end;

                    color = mContext.getResources().getColor(mNextColorId);
                    span = new BackgroundColorSpan(color);
                    end = timeTextBuilder.length();
                    start = end - 2;
                    timeTextBuilder.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
            workdayMinTextView.setText(timeTextBuilder);
            timeTextBuilder.clear();
            for (int min : holidayMins) {
                if (min == -1) {
                    break;
                }
                if (timeTextBuilder.length() > 0) {
                    timeTextBuilder.append(" ");
                }
                timeTextBuilder.append(String.format("%2d", min));
                if (mHoliday && min == nextMin) {
                    int color;
                    Object span;
                    int start;
                    int end;

                    color = mContext.getResources().getColor(mNextColorId);
                    span = new BackgroundColorSpan(color);
                    end = timeTextBuilder.length();
                    start = end - 2;
                    timeTextBuilder.setSpan(span, start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
            holidayMinTextView.setText(timeTextBuilder);
        }

        return view;
    }

    public TimetableCallbacks getTimetableCallbacks() {
        return mTimetableCallbacks;
    }

    public void setTimetableLoaderCallbacks(TimetableCallbacks callbacks) {
        mTimetableCallbacks = callbacks;
    }

    public void refresh() {
        if (mCursor != null) {
            mLoaderManager.restartLoader(0, null, mTimetableLoaderCallbacks);
        }
    }

    private class TimetableLoaderCallbacks implements LoaderManager.LoaderCallbacks<Cursor> {
        @Override
        public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
            CursorLoader cursorLoader;
            String[] projection;
            String selection;
            String[] selectionArgs;

            projection = new String[]{
                    BaseColumns._ID,
                    mDirectionName,
                    mWeekdayName,
                    mTimeName
            };
            selection = mDirectionName + " = ?";
            selectionArgs = new String[]{
                    String.valueOf(mDirectionValue)
            };
            cursorLoader = new CursorLoader(mContext, mUri, projection, selection, selectionArgs, null);

            return cursorLoader;
        }

        @Override
        public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
            if (mCursor != null) {
                mCursor.unregisterContentObserver(mTimetableContentObserver);
            }
            mCursor = cursor;
            if (mCursor != null) {
                Calendar calendar;
                int year;
                int month;
                int day;
                int now;

                mCursor.registerContentObserver(mTimetableContentObserver);

                mIdIndex = mCursor.getColumnIndex(BaseColumns._ID);
                mWeekdayIndex = mCursor.getColumnIndex(mWeekdayName);
                mTimeIndex = mCursor.getColumnIndex(mTimeName);

                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH) + 1;
                day = calendar.get(Calendar.DAY_OF_MONTH);
                now = calendar.get(Calendar.HOUR_OF_DAY) * 3600 + calendar.get(Calendar.MINUTE) * 60;

                mHoliday = HolidayDetector.getInstance().isHoliday(year, month, day);

                mMaxTime = Integer.MIN_VALUE;
                mMinTime = Integer.MAX_VALUE;
                mNextTime = Integer.MAX_VALUE;
                if (mCursor.moveToFirst()) {
                    do {
                        int weekday;
                        int time;

                        weekday = mCursor.getInt(mWeekdayIndex);
                        time = mCursor.getInt(mTimeIndex);
                        if (mMaxTime < time) {
                            mMaxTime = time;
                        }
                        if (mMinTime > time) {
                            mMinTime = time;
                        }
                        if (mHoliday == (weekday == mHolidayValue) && mNextTime > time && now <= time) {
                            mNextTime = time;
                        }
                    } while (mCursor.moveToNext());
                }
            }

            notifyDataSetChanged();

            if (mTimetableCallbacks != null) {
                mTimetableCallbacks.onLoadFinished();
            }
        }

        @Override
        public void onLoaderReset(Loader<Cursor> cursorLoader) {
            if (mCursor != null) {
                mCursor.unregisterContentObserver(mTimetableContentObserver);
                mCursor = null;
            }

            notifyDataSetChanged();
        }
    }

    private class TimetableContentObserver extends ContentObserver {
        public TimetableContentObserver() {
            super(new Handler());
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
            if (mCursor != null) {
                mLoaderManager.restartLoader(0, null, mTimetableLoaderCallbacks);
            }
        }
    }
}
