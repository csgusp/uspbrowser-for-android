/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.wifi;

import jp.gr.java_conf.csgusp.uspbrowser.R;
import jp.gr.java_conf.csgusp.uspbrowser.preference.BrowserPreferences;
import jp.gr.java_conf.csgusp.uspbrowser.service.wifi.IWifiService;
import jp.gr.java_conf.csgusp.uspbrowser.service.wifi.IWifiServiceCallback;
import jp.gr.java_conf.csgusp.uspbrowser.service.wifi.WifiService;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.widget.SearchView;

/**
 * Wi-Fi接続のフラグメントを実装したクラス。
 *
 * @author leak4mk0
 * @version 34
 * @since 19
 */
public class ConnectionFragment extends SherlockFragment implements View.OnClickListener {
    private BrowserPreferences mBrowserPreferences;
    private IWifiService mWifiService;
    private WifiServiceConnection mWifiServiceConnection;
    private WifiServiceCallback mWifiServiceCallback;
    private ConnectionHandler mConnectionHandler;

    public static ConnectionFragment newInstance() {
        ConnectionFragment fragment;
        Bundle args;

        fragment = new ConnectionFragment();
        args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mBrowserPreferences = BrowserPreferences.getInstance(getActivity());

        setHasOptionsMenu(true);

        mWifiServiceConnection = new WifiServiceConnection();
        mWifiServiceCallback = new WifiServiceCallback();
        mConnectionHandler = new ConnectionHandler(getActivity());
        getActivity().bindService(new Intent(IWifiService.class.getName()),
                mWifiServiceConnection, SherlockFragmentActivity.BIND_AUTO_CREATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        Button onButton;
        Button offButton;

        view = inflater.inflate(R.layout.wifi_connection_fragment, container, false);
        onButton = (Button) view.findViewById(R.id.on_button);
        offButton = (Button) view.findViewById(R.id.off_button);

        onButton.setOnClickListener(this);
        offButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mWifiService != null) {
            try {
                mWifiService.unregisterCallback(mWifiServiceCallback);
            } catch (RemoteException ignored) {
            }
        }
        getActivity().unbindService(mWifiServiceConnection);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem searchItem;
        MenuItem notTakenItem;
        MenuItem deletedItem;
        SearchView searchView;

        super.onPrepareOptionsMenu(menu);

        searchItem = menu.findItem(R.id.menu_search);
        notTakenItem = menu.findItem(R.id.menu_not_taken);
        deletedItem = menu.findItem(R.id.menu_deleted);
        searchView = (SearchView) searchItem.getActionView();

        searchItem.setVisible(false);
        notTakenItem.setVisible(false);
        deletedItem.setVisible(false);

        if (searchView != null) {
            searchView.setVisibility(View.GONE);
        } else {
            searchItem.setVisible(false);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.on_button: {
                String ssid;
                String key;
                String authenticationUrl;
                String authenticationBody;
                String userId;
                String password;

                v.setEnabled(false);

                mBrowserPreferences.setWifiEnabled(true);
                ssid = mBrowserPreferences.getWifiSsid();
                key = mBrowserPreferences.getWifiKey();
                authenticationUrl = mBrowserPreferences.getWifiAuthenticationUrl();
                authenticationBody = mBrowserPreferences.getWifiAuthenticationBody();
                userId = mBrowserPreferences.getSystemUserId();
                password = mBrowserPreferences.getSystemPassword();
                try {
                    mWifiService.connect(ssid, key, authenticationUrl, authenticationBody, userId, password, true);
                } catch (RemoteException e) {
                    v.setEnabled(true);
                }

                break;
            }
            case R.id.off_button: {
                String ssid;

                v.setEnabled(false);

                mBrowserPreferences.setWifiEnabled(false);
                ssid = mBrowserPreferences.getWifiSsid();
                try {
                    mWifiService.disconnect(ssid);
                } catch (RemoteException e) {
                    v.setEnabled(true);
                }

                break;
            }
        }
    }

    private static class ConnectionHandler extends Handler {
        Activity mActivity;
        BrowserPreferences mBrowserPreferences;

        public ConnectionHandler(Activity activity) {
            mActivity = activity;
            mBrowserPreferences = BrowserPreferences.getInstance(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            Button onButton;
            Button offButton;
            TextView statusTextView;

            onButton = (Button) mActivity.findViewById(R.id.on_button);
            offButton = (Button) mActivity.findViewById(R.id.off_button);
            statusTextView = (TextView) mActivity.findViewById(R.id.status_text_view);

            switch (msg.arg1) {
                case WifiService.ACTION_CONNECT:
                    switch (msg.arg2) {
                        case WifiService.STATUS_RUNNING:
                            onButton.setEnabled(false);
                            statusTextView.setText(R.string.connecting);

                            break;

                        case WifiService.STATUS_SUCCESS:
                            offButton.setEnabled(true);
                            statusTextView.setText(R.string.connected);

                            break;

                        case WifiService.STATUS_SUCCESS_NO_SERVICE:
                            offButton.setEnabled(true);
                            statusTextView.setText(R.string.no_service);

                            break;

                        case WifiService.STATUS_SUCCESS_OTHER_SERVICE:
                            offButton.setEnabled(true);
                            statusTextView.setText(R.string.other_service);

                            break;

                        case WifiService.STATUS_SUCCESS_NO_AUTHENTICATION:
                            offButton.setEnabled(true);
                            statusTextView.setText(R.string.no_authentication);

                            break;

                        case WifiService.STATUS_FAILURE_IN_UNKNOWN:
                        case WifiService.STATUS_FAILURE_IN_NETWORK:
                        case WifiService.STATUS_FAILURE_IN_AUTHORIZATION:
                            onButton.setEnabled(true);
                            statusTextView.setText(R.string.disconnected);

                            break;
                    }

                    break;

                case WifiService.ACTION_DISCONNECT:
                    switch (msg.arg2) {
                        case WifiService.STATUS_RUNNING:
                            offButton.setEnabled(false);
                            statusTextView.setText(R.string.disconnecting);

                            break;

                        case WifiService.STATUS_SUCCESS:
                            onButton.setEnabled(true);
                            statusTextView.setText(R.string.disconnected);

                            break;

                        case WifiService.STATUS_FAILURE_IN_UNKNOWN:
                        case WifiService.STATUS_FAILURE_IN_NETWORK:
                        case WifiService.STATUS_FAILURE_IN_AUTHORIZATION:
                            offButton.setEnabled(true);
                            statusTextView.setText(R.string.connected);

                            break;
                    }

                    break;

            }
        }
    }

    private class WifiServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Button onButton;
            Button offButton;
            TextView statusTextView;
            boolean connecting;
            boolean connected;
            boolean noService;
            boolean noAuthentication;

            mWifiService = IWifiService.Stub.asInterface(service);
            try {
                mWifiService.registerCallback(mWifiServiceCallback);
            } catch (RemoteException ignored) {
            }
            onButton = (Button) getActivity().findViewById(R.id.on_button);
            offButton = (Button) getActivity().findViewById(R.id.off_button);
            statusTextView = (TextView) getActivity().findViewById(R.id.status_text_view);

            try {
                connecting = mWifiService.isConnecting();
                connected = mWifiService.isConnected();
                noService = mWifiService.isNoService();
                noAuthentication = mWifiService.isNoAuthentication();
            } catch (RemoteException e) {
                connecting = false;
                connected = false;
                noService = false;
                noAuthentication = false;
            }

            onButton.setEnabled(!connecting && !connected);
            offButton.setEnabled(!connecting && connected);
            if (connecting) {
                onButton.setEnabled(false);
                offButton.setEnabled(false);
                if (connected) {
                    statusTextView.setText(R.string.disconnecting);
                } else {
                    statusTextView.setText(R.string.connecting);
                }
            } else {
                if (connected) {
                    onButton.setEnabled(false);
                    offButton.setEnabled(true);
                    if (noService) {
                        statusTextView.setText(R.string.no_service);
                    } else if (noAuthentication) {
                        statusTextView.setText(R.string.no_authentication);
                    } else {
                        statusTextView.setText(R.string.connected);
                    }
                } else {
                    onButton.setEnabled(true);
                    offButton.setEnabled(false);
                    statusTextView.setText(R.string.disconnected);
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mWifiService = null;
        }
    }

    private class WifiServiceCallback extends IWifiServiceCallback.Stub {
        @Override
        public void onResult(int action, int status) {
            Message message;

            message = new Message();
            message.arg1 = action;
            message.arg2 = status;
            mConnectionHandler.sendMessage(message);
        }
    }
}
