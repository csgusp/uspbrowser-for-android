/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.ui.wifi;

import jp.gr.java_conf.csgusp.uspbrowser.preference.BrowserPreferences;
import jp.gr.java_conf.csgusp.uspbrowser.service.wifi.IWifiService;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.os.RemoteException;

/**
 * WifiReceiver クラスは、Wifiに接続されたときの動作を定義したクラスです。
 *
 * @author leak4mk0
 * @version 32
 * @since 19
 */
public class WifiReceiver extends BroadcastReceiver {
    private Context mContext;
    private BrowserPreferences mBrowserPreferences;
    private IWifiService mWifiService;

    public void onReceive(Context context, Intent intent) {
        String action;

        action = intent.getAction();
        if (action != null && action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
            NetworkInfo networkInfo;

            networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            if (networkInfo != null && networkInfo.isConnected()) {
                WifiManager wifiManager;
                WifiInfo wifiInfo;
                String currentSsid;
                String[] targetSsids;
                boolean connectedTarget;

                mContext = context.getApplicationContext();
                mBrowserPreferences = BrowserPreferences.getInstance(mContext);
                if (!mBrowserPreferences.getWifiEnabled()) {
                    return;
                }
                wifiManager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
                wifiInfo = wifiManager.getConnectionInfo();
                currentSsid = wifiInfo.getSSID();
                currentSsid = currentSsid != null ? currentSsid : "";
                currentSsid = currentSsid.startsWith("\"") && currentSsid.endsWith("\"") ?
                        currentSsid : "\"" + currentSsid + "\"";
                targetSsids = mBrowserPreferences.getWifiSsid().split(",");
                connectedTarget = false;
                for (String targetSsid : targetSsids) {
                    if (("\"" + targetSsid + "\"").equals(currentSsid)) {
                        connectedTarget = true;

                        break;
                    }
                }
                if (!connectedTarget) {
                    return;
                }
                mContext.bindService(new Intent(IWifiService.class.getName()), new WifiServiceConnection(), Context.BIND_AUTO_CREATE);
            }
        }
    }

    private class WifiServiceConnection implements ServiceConnection {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            try {
                mWifiService =  IWifiService.Stub.asInterface(service);
                if (!mWifiService.isConnecting()) {
                    String ssid;
                    String key;
                    String authenticationUrl;
                    String authenticationBody;
                    String userId;
                    String password;

                    mBrowserPreferences.setWifiEnabled(true);
                    ssid = mBrowserPreferences.getWifiSsid();
                    key = mBrowserPreferences.getWifiKey();
                    authenticationUrl = mBrowserPreferences.getWifiAuthenticationUrl();
                    authenticationBody = mBrowserPreferences.getWifiAuthenticationBody();
                    userId = mBrowserPreferences.getSystemUserId();
                    password = mBrowserPreferences.getSystemPassword();
                    try {
                        mWifiService.connect(ssid, key, authenticationUrl, authenticationBody, userId, password, false);
                    } catch (RemoteException ignored) {
                    }
                    mContext.unbindService(this);
                }
            } catch (RemoteException ignored) {
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mWifiService = null;
            mContext = null;
        }
    }
}
