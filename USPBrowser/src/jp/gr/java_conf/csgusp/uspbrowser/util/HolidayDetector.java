/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.util;

import ajd4jp.AJD;
import ajd4jp.AJDException;
import ajd4jp.Month;
import ajd4jp.OffProvider;
import ajd4jp.Week;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * 休日の検出を実装したクラス。
 *
 * @author leak4mk0
 * @version 28
 * @since 28
 */
public class HolidayDetector {
    private static HolidayDetector mHolidayDetector;
    private OffProvider mOffProvider;
    private int mLastYear;
    private int mLastMonth;
    private int mLastDay;
    private boolean mLastHoliday;

    private HolidayDetector() {
        mOffProvider = new OffProvider(true, Week.SATURDAY, Week.SUNDAY);
    }

    public static HolidayDetector getInstance() {
        if (mHolidayDetector == null) {
            mHolidayDetector = new HolidayDetector();
        }

        return mHolidayDetector;
    }

    public boolean isHoliday(int year, int month, int day) {
        boolean holiday;

        if (year == mLastYear && month == mLastMonth && day == mLastDay) {
            holiday = mLastHoliday;
        } else {
            try {
                holiday = mOffProvider.getOff(new AJD(year, month, day)) != null;
                mLastYear = year;
                mLastMonth = month;
                mLastDay = day;
                mLastHoliday = holiday;
            } catch (AJDException e) {
                holiday = false;
            }
        }

        return holiday;
    }
}
