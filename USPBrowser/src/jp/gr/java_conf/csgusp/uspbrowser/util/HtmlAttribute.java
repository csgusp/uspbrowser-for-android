/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.util;

import java.util.HashMap;
import java.util.Locale;

/**
 * HtmlAttribute クラスは HTML 要素の属性を定義したクラスです。
 *
 * @author leak4mk0
 * @since 13
 * @version 13
 */
public class HtmlAttribute {
	public static final String ABBR_NAME = "abbr";
	public static final String ACCEPT_CHARSET_NAME = "accept-charset";
	public static final String ACCEPT_NAME = "accept";
	public static final String ACCESSKEY_NAME = "accesskey";
	public static final String ACTION_NAME = "action";
	public static final String ALIGN_NAME = "align";
	public static final String ALINK_NAME = "alink";
	public static final String ALT_NAME = "alt";
	public static final String ARCHIVE_NAME = "archive";
	public static final String AXIS_NAME = "axis";
	public static final String BACKGROUND_NAME = "background";
	public static final String BGCOLOR_NAME = "bgcolor";
	public static final String BORDER_NAME = "border";
	public static final String CELLPADDING_NAME = "cellpadding";
	public static final String CELLSPACING_NAME = "cellspacing";
	public static final String CHAR_NAME = "char";
	public static final String CHAROFF_NAME = "charoff";
	public static final String CHARSET_NAME = "charset";
	public static final String CHECKED_NAME = "checked";
	public static final String CITE_NAME = "cite";
	public static final String CLASS_NAME = "class";
	public static final String CLASSID_NAME = "classid";
	public static final String CLEAR_NAME = "clear";
	public static final String CODE_NAME = "code";
	public static final String CODEBASE_NAME = "codebase";
	public static final String CODETYPE_NAME = "codetype";
	public static final String COLOR_NAME = "color";
	public static final String COLS_NAME = "cols";
	public static final String COLSPAN_NAME = "colspan";
	public static final String COMPACT_NAME = "compact";
	public static final String CONTENT_NAME = "content";
	public static final String COORDS_NAME = "coords";
	public static final String DATA_NAME = "data";
	public static final String DATETIME_NAME = "datetime";
	public static final String DECLARE_NAME = "declare";
	public static final String DEFER_NAME = "defer";
	public static final String DIR_NAME = "dir";
	public static final String DISABLED_NAME = "disabled";
	public static final String ENCTYPE_NAME = "enctype";
	public static final String FACE_NAME = "face";
	public static final String FOR_NAME = "for";
	public static final String FRAME_NAME = "frame";
	public static final String FRAMEBORDER_NAME = "frameborder";
	public static final String HEADERS_NAME = "headers";
	public static final String HEIGHT_NAME = "height";
	public static final String HREF_NAME = "href";
	public static final String HREFLANG_NAME = "hreflang";
	public static final String HSPACE_NAME = "hspace";
	public static final String HTTP_EQUIV_NAME = "http-equiv";
	public static final String ID_NAME = "id";
	public static final String ISMAP_NAME = "ismap";
	public static final String LABEL_NAME = "label";
	public static final String LANG_NAME = "lang";
	public static final String LANGUAGE_NAME = "language";
	public static final String LINK_NAME = "link";
	public static final String LONGDESC_NAME = "longdesc";
	public static final String MARGINHEIGHT_NAME = "marginheight";
	public static final String MARGINWIDTH_NAME = "marginwidth";
	public static final String MAXLENGTH_NAME = "maxlength";
	public static final String MEDIA_NAME = "media";
	public static final String METHOD_NAME = "method";
	public static final String MULTIPLE_NAME = "multiple";
	public static final String NAME_NAME = "name";
	public static final String NOHREF_NAME = "nohref";
	public static final String NORESIZE_NAME = "noresize";
	public static final String NOSHADE_NAME = "noshade";
	public static final String NOWRAP_NAME = "nowrap";
	public static final String OBJECT_NAME = "object";
	public static final String ONBLUR_NAME = "onblur";
	public static final String ONCHANGE_NAME = "onchange";
	public static final String ONCLICK_NAME = "onclick";
	public static final String ONDBLCLICK_NAME = "ondblclick";
	public static final String ONFOCUS_NAME = "onfocus";
	public static final String ONKEYDOWN_NAME = "onkeydown";
	public static final String ONKEYPRESS_NAME = "onkeypress";
	public static final String ONKEYUP_NAME = "onkeyup";
	public static final String ONLOAD_NAME = "onload";
	public static final String ONMOUSEDOWN_NAME = "onmousedown";
	public static final String ONMOUSEMOVE_NAME = "onmousemove";
	public static final String ONMOUSEOUT_NAME = "onmouseout";
	public static final String ONMOUSEOVER_NAME = "onmouseover";
	public static final String ONMOUSEUP_NAME = "onmouseup";
	public static final String ONRESET_NAME = "onreset";
	public static final String ONSELECT_NAME = "onselect";
	public static final String ONSUBMIT_NAME = "onsubmit";
	public static final String ONUNLOAD_NAME = "onunload";
	public static final String PROFILE_NAME = "profile";
	public static final String PROMPT_NAME = "prompt";
	public static final String READONLY_NAME = "readonly";
	public static final String REL_NAME = "rel";
	public static final String REV_NAME = "rev";
	public static final String ROWS_NAME = "rows";
	public static final String ROWSPAN_NAME = "rowspan";
	public static final String RULES_NAME = "rules";
	public static final String SCHEME_NAME = "scheme";
	public static final String SCOPE_NAME = "scope";
	public static final String SCROLLING_NAME = "scrolling";
	public static final String SELECTED_NAME = "selected";
	public static final String SHAPE_NAME = "shape";
	public static final String SIZE_NAME = "size";
	public static final String SPAN_NAME = "span";
	public static final String SRC_NAME = "src";
	public static final String STANDBY_NAME = "standby";
	public static final String START_NAME = "start";
	public static final String STYLE_NAME = "style";
	public static final String SUMMARY_NAME = "summary";
	public static final String TABINDEX_NAME = "tabindex";
	public static final String TARGET_NAME = "target";
	public static final String TEXT_NAME = "text";
	public static final String TITLE_NAME = "title";
	public static final String TYPE_NAME = "type";
	public static final String USEMAP_NAME = "usemap";
	public static final String VALIGN_NAME = "valign";
	public static final String VALUE_NAME = "value";
	public static final String VALUETYPE_NAME = "valuetype";
	public static final String VERSION_NAME = "version";
	public static final String VLINK_NAME = "vlink";
	public static final String VSPACE_NAME = "vspace";
	public static final String WIDTH_NAME = "width";
}
