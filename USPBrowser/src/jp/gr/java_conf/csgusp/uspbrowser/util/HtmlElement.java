/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.util;

/**
 * HtmlElement クラスは HTML 要素を定義したクラスです。
 *
 * @author leak4mk0
 * @version 13
 * @since 13
 */
public class HtmlElement {
    public static final String A_NAME = "a";
    public static final String ABBR_NAME = "abbr";
    public static final String ACRONYM_NAME = "acronym";
    public static final String ADDRESS_NAME = "address";
    public static final String APPLET_NAME = "applet";
    public static final String AREA_NAME = "area";
    public static final String B_NAME = "b";
    public static final String BASE_NAME = "base";
    public static final String BASEFONT_NAME = "basefont";
    public static final String BDO_NAME = "bdo";
    public static final String BIG_NAME = "big";
    public static final String BLOCKQUOTE_NAME = "blockquote";
    public static final String BODY_NAME = "body";
    public static final String BR_NAME = "br";
    public static final String BUTTON_NAME = "button";
    public static final String CAPTION_NAME = "caption";
    public static final String CENTER_NAME = "center";
    public static final String CITE_NAME = "cite";
    public static final String CODE_NAME = "code";
    public static final String COL_NAME = "col";
    public static final String COLGROUP_NAME = "colgroup";
    public static final String DD_NAME = "dd";
    public static final String DEL_NAME = "del";
    public static final String DFN_NAME = "dfn";
    public static final String DIR_NAME = "dir";
    public static final String DIV_NAME = "div";
    public static final String DL_NAME = "dl";
    public static final String DT_NAME = "dt";
    public static final String EM_NAME = "em";
    public static final String FIELDSET_NAME = "fieldset";
    public static final String FONT_NAME = "font";
    public static final String FORM_NAME = "form";
    public static final String FRAME_NAME = "frame";
    public static final String FRAMESET_NAME = "frameset";
    public static final String H1_NAME = "h1";
    public static final String H2_NAME = "h2";
    public static final String H3_NAME = "h3";
    public static final String H4_NAME = "h4";
    public static final String H5_NAME = "h5";
    public static final String H6_NAME = "h6";
    public static final String HEAD_NAME = "head";
    public static final String HR_NAME = "hr";
    public static final String HTML_NAME = "html";
    public static final String I_NAME = "i";
    public static final String IFRAME_NAME = "iframe";
    public static final String IMG_NAME = "img";
    public static final String IMAGE_NAME = "image";
    public static final String INPUT_NAME = "input";
    public static final String INS_NAME = "ins";
    public static final String ISINDEX_NAME = "isindex";
    public static final String KBD_NAME = "kbd";
    public static final String LABEL_NAME = "label";
    public static final String LEGEND_NAME = "legend";
    public static final String LI_NAME = "li";
    public static final String LINK_NAME = "link";
    public static final String MAP_NAME = "map";
    public static final String MENU_NAME = "menu";
    public static final String META_NAME = "meta";
    public static final String NOFRAMES_NAME = "noframes";
    public static final String NOSCRIPT_NAME = "noscript";
    public static final String OBJECT_NAME = "object";
    public static final String OL_NAME = "ol";
    public static final String OPTGROUP_NAME = "optgroup";
    public static final String OPTION_NAME = "option";
    public static final String P_NAME = "p";
    public static final String PARAM_NAME = "param";
    public static final String PRE_NAME = "pre";
    public static final String Q_NAME = "q";
    public static final String S_NAME = "s";
    public static final String SAMP_NAME = "samp";
    public static final String SCRIPT_NAME = "script";
    public static final String SELECT_NAME = "select";
    public static final String SMALL_NAME = "small";
    public static final String SPAN_NAME = "span";
    public static final String STRIKE_NAME = "strike";
    public static final String STRONG_NAME = "strong";
    public static final String STYLE_NAME = "style";
    public static final String SUB_NAME = "sub";
    public static final String SUP_NAME = "sup";
    public static final String TABLE_NAME = "table";
    public static final String TBODY_NAME = "tbody";
    public static final String TD_NAME = "td";
    public static final String TEXTAREA_NAME = "textarea";
    public static final String TFOOT_NAME = "tfoot";
    public static final String TH_NAME = "th";
    public static final String THEAD_NAME = "thead";
    public static final String TITLE_NAME = "title";
    public static final String TR_NAME = "tr";
    public static final String TT_NAME = "tt";
    public static final String U_NAME = "u";
    public static final String UL_NAME = "ul";
    public static final String VAR_NAME = "var";

    public static final int A_CODE = A_NAME.hashCode();
    public static final int ABBR_CODE = ABBR_NAME.hashCode();
    public static final int ACRONYM_CODE = ACRONYM_NAME.hashCode();
    public static final int ADDRESS_CODE = ADDRESS_NAME.hashCode();
    public static final int APPLET_CODE = APPLET_NAME.hashCode();
    public static final int AREA_CODE = AREA_NAME.hashCode();
    public static final int B_CODE = B_NAME.hashCode();
    public static final int BASE_CODE = BASE_NAME.hashCode();
    public static final int BASEFONT_CODE = BASEFONT_NAME.hashCode();
    public static final int BDO_CODE = BDO_NAME.hashCode();
    public static final int BIG_CODE = BIG_NAME.hashCode();
    public static final int BLOCKQUOTE_CODE = BLOCKQUOTE_NAME.hashCode();
    public static final int BODY_CODE = BODY_NAME.hashCode();
    public static final int BR_CODE = BR_NAME.hashCode();
    public static final int BUTTON_CODE = BUTTON_NAME.hashCode();
    public static final int CAPTION_CODE = CAPTION_NAME.hashCode();
    public static final int CENTER_CODE = CENTER_NAME.hashCode();
    public static final int CITE_CODE = CITE_NAME.hashCode();
    public static final int CODE_CODE = CODE_NAME.hashCode();
    public static final int COL_CODE = COL_NAME.hashCode();
    public static final int COLGROUP_CODE = COLGROUP_NAME.hashCode();
    public static final int DD_CODE = DD_NAME.hashCode();
    public static final int DEL_CODE = DEL_NAME.hashCode();
    public static final int DFN_CODE = DFN_NAME.hashCode();
    public static final int DIR_CODE = DIR_NAME.hashCode();
    public static final int DIV_CODE = DIV_NAME.hashCode();
    public static final int DL_CODE = DL_NAME.hashCode();
    public static final int DT_CODE = DT_NAME.hashCode();
    public static final int EM_CODE = EM_NAME.hashCode();
    public static final int FIELDSET_CODE = FIELDSET_NAME.hashCode();
    public static final int FONT_CODE = FONT_NAME.hashCode();
    public static final int FORM_CODE = FORM_NAME.hashCode();
    public static final int FRAME_CODE = FRAME_NAME.hashCode();
    public static final int FRAMESET_CODE = FRAMESET_NAME.hashCode();
    public static final int H1_CODE = H1_NAME.hashCode();
    public static final int H2_CODE = H2_NAME.hashCode();
    public static final int H3_CODE = H3_NAME.hashCode();
    public static final int H4_CODE = H4_NAME.hashCode();
    public static final int H5_CODE = H5_NAME.hashCode();
    public static final int H6_CODE = H6_NAME.hashCode();
    public static final int HEAD_CODE = HEAD_NAME.hashCode();
    public static final int HR_CODE = HR_NAME.hashCode();
    public static final int HTML_CODE = HTML_NAME.hashCode();
    public static final int I_CODE = I_NAME.hashCode();
    public static final int IFRAME_CODE = IFRAME_NAME.hashCode();
    public static final int IMG_CODE = IMG_NAME.hashCode();
    public static final int IMAGE_CODE = IMAGE_NAME.hashCode();
    public static final int INPUT_CODE = INPUT_NAME.hashCode();
    public static final int INS_CODE = INS_NAME.hashCode();
    public static final int ISINDEX_CODE = ISINDEX_NAME.hashCode();
    public static final int KBD_CODE = KBD_NAME.hashCode();
    public static final int LABEL_CODE = LABEL_NAME.hashCode();
    public static final int LEGEND_CODE = LEGEND_NAME.hashCode();
    public static final int LI_CODE = LI_NAME.hashCode();
    public static final int LINK_CODE = LINK_NAME.hashCode();
    public static final int MAP_CODE = MAP_NAME.hashCode();
    public static final int MENU_CODE = MENU_NAME.hashCode();
    public static final int META_CODE = META_NAME.hashCode();
    public static final int NOFRAMES_CODE = NOFRAMES_NAME.hashCode();
    public static final int NOSCRIPT_CODE = NOSCRIPT_NAME.hashCode();
    public static final int OBJECT_CODE = OBJECT_NAME.hashCode();
    public static final int OL_CODE = OL_NAME.hashCode();
    public static final int OPTGROUP_CODE = OPTGROUP_NAME.hashCode();
    public static final int OPTION_CODE = OPTION_NAME.hashCode();
    public static final int P_CODE = P_NAME.hashCode();
    public static final int PARAM_CODE = PARAM_NAME.hashCode();
    public static final int PRE_CODE = PRE_NAME.hashCode();
    public static final int Q_CODE = Q_NAME.hashCode();
    public static final int S_CODE = S_NAME.hashCode();
    public static final int SAMP_CODE = SAMP_NAME.hashCode();
    public static final int SCRIPT_CODE = SCRIPT_NAME.hashCode();
    public static final int SELECT_CODE = SELECT_NAME.hashCode();
    public static final int SMALL_CODE = SMALL_NAME.hashCode();
    public static final int SPAN_CODE = SPAN_NAME.hashCode();
    public static final int STRIKE_CODE = STRIKE_NAME.hashCode();
    public static final int STRONG_CODE = STRONG_NAME.hashCode();
    public static final int STYLE_CODE = STYLE_NAME.hashCode();
    public static final int SUB_CODE = SUB_NAME.hashCode();
    public static final int SUP_CODE = SUP_NAME.hashCode();
    public static final int TABLE_CODE = TABLE_NAME.hashCode();
    public static final int TBODY_CODE = TBODY_NAME.hashCode();
    public static final int TD_CODE = TD_NAME.hashCode();
    public static final int TEXTAREA_CODE = TEXTAREA_NAME.hashCode();
    public static final int TFOOT_CODE = TFOOT_NAME.hashCode();
    public static final int TH_CODE = TH_NAME.hashCode();
    public static final int THEAD_CODE = THEAD_NAME.hashCode();
    public static final int TITLE_CODE = TITLE_NAME.hashCode();
    public static final int TR_CODE = TR_NAME.hashCode();
    public static final int TT_CODE = TT_NAME.hashCode();
    public static final int U_CODE = U_NAME.hashCode();
    public static final int UL_CODE = UL_NAME.hashCode();
    public static final int VAR_CODE = VAR_NAME.hashCode();
}
