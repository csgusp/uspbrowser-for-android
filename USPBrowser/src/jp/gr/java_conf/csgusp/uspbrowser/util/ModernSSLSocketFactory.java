/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.util;

import android.content.Context;
import android.os.Build;

import org.apache.http.conn.ssl.SSLSocketFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**
 * ModernSSLSocketFactory クラスは、新しい SSL 証明書を許可するクラスです。
 *
 * @author leak4mk0
 * @version 34
 * @since 34
 */
public class ModernSSLSocketFactory extends SSLSocketFactory {
    private static final String CACERTS_FILE = "cacerts.bks";
    private static final String CACERTS_PASSWORD = "changeit";

    private static Context sContext;

    public ModernSSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
        super(truststore);
    }

    public static void initialize(Context context) {
        sContext = context;
    }

    public static SSLSocketFactory getSocketFactory() {
        SSLSocketFactory factory;

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
            factory = getModernSocketFactory();
        } else {
            factory = SSLSocketFactory.getSocketFactory();
        }

        return factory;
    }

    public static ModernSSLSocketFactory getModernSocketFactory() {
        KeyStore store;
        ModernSSLSocketFactory factory;

        try {
            InputStream stream;
            char[] password;

            store = KeyStore.getInstance(KeyStore.getDefaultType());
            stream = sContext.getResources().getAssets().open(CACERTS_FILE);
            password = CACERTS_PASSWORD.toCharArray();
            store.load(stream, password);
        } catch (CertificateException e) {
            return null;
        } catch (NoSuchAlgorithmException e) {
            return null;
        } catch (KeyStoreException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
        try {
            factory = new ModernSSLSocketFactory(store);
        } catch (NoSuchAlgorithmException e) {
            return null;
        } catch (KeyManagementException e) {
            return null;
        } catch (KeyStoreException e) {
            return null;
        } catch (UnrecoverableKeyException e) {
            return null;
        }

        return factory;
    }
}
