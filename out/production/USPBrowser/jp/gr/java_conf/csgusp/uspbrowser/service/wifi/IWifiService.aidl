/*
 * Copyright 2013 Computer Study Group of University of Shiga Prefecture
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.gr.java_conf.csgusp.uspbrowser.service.wifi;

import jp.gr.java_conf.csgusp.uspbrowser.service.wifi.IWifiServiceCallback;

interface IWifiService {
    oneway void registerCallback(IWifiServiceCallback callback);
    oneway void unregisterCallback(IWifiServiceCallback callback);
    void connect(String ssid, String key, String authenticationUrl, String authenticationBody, String userId, String password, boolean forced);
    void disconnect(String ssid);
    boolean isConnecting();
    boolean isConnected();
    boolean isNoService();
    boolean isOtherService();
    boolean isNoAuthentication();
}
